<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="15" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="9" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Mechanical" color="7" fill="1" visible="no" active="yes"/>
<layer number="101" name="Gehäuse" color="13" fill="1" visible="no" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="no"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="no"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="tTestdril" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="BIFRNTTEK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="BottomExtra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="AA-IC">
<packages>
<package name="ECOPACK-11">
<smd name="D2" x="0" y="-0.75" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0" y="-0.75" radius="0.17" width="0" layer="29"/>
<circle x="0" y="-0.75" radius="0.165" width="0" layer="31"/>
<smd name="D1" x="-0.5" y="-0.75" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="-0.5" y="-0.75" radius="0.17" width="0" layer="29"/>
<circle x="-0.5" y="-0.75" radius="0.165" width="0" layer="31"/>
<smd name="D3" x="0.5" y="-0.75" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0.5" y="-0.75" radius="0.17" width="0" layer="29"/>
<circle x="0.5" y="-0.75" radius="0.165" width="0" layer="31"/>
<smd name="C2" x="0" y="-0.25" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0" y="-0.25" radius="0.17" width="0" layer="29"/>
<circle x="0" y="-0.25" radius="0.165" width="0" layer="31"/>
<smd name="C1" x="-0.5" y="-0.25" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="-0.5" y="-0.25" radius="0.17" width="0" layer="29"/>
<circle x="-0.5" y="-0.25" radius="0.165" width="0" layer="31"/>
<smd name="C3" x="0.5" y="-0.25" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0.5" y="-0.25" radius="0.17" width="0" layer="29"/>
<circle x="0.5" y="-0.25" radius="0.165" width="0" layer="31"/>
<smd name="B2" x="0" y="0.25" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0" y="0.25" radius="0.17" width="0" layer="29"/>
<circle x="0" y="0.25" radius="0.165" width="0" layer="31"/>
<smd name="B1" x="-0.5" y="0.25" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="-0.5" y="0.25" radius="0.17" width="0" layer="29"/>
<circle x="-0.5" y="0.25" radius="0.165" width="0" layer="31"/>
<smd name="B3" x="0.5" y="0.25" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0.5" y="0.25" radius="0.17" width="0" layer="29"/>
<circle x="0.5" y="0.25" radius="0.165" width="0" layer="31"/>
<smd name="A2" x="0" y="0.75" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0" y="0.75" radius="0.17" width="0" layer="29"/>
<circle x="0" y="0.75" radius="0.165" width="0" layer="31"/>
<smd name="A3" x="0.5" y="0.75" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<circle x="0.5" y="0.75" radius="0.17" width="0" layer="29"/>
<circle x="0.5" y="0.75" radius="0.165" width="0" layer="31"/>
<wire x1="-0.9" y1="-1.1" x2="-0.9" y2="1.1" width="0.15" layer="21"/>
<wire x1="-0.9" y1="1.1" x2="0.9" y2="1.1" width="0.15" layer="21"/>
<wire x1="0.9" y1="1.1" x2="0.9" y2="-1.1" width="0.15" layer="21"/>
<wire x1="0.9" y1="-1.1" x2="-0.9" y2="-1.1" width="0.15" layer="21"/>
<text x="-2.5" y="1.75" size="1" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-1.25" y1="1.5" x2="1.25" y2="1.5" width="0" layer="39"/>
<wire x1="1.25" y1="1.5" x2="1.25" y2="-1.5" width="0" layer="39"/>
<wire x1="1.25" y1="-1.5" x2="-1.25" y2="-1.5" width="0" layer="39"/>
<wire x1="-1.25" y1="-1.5" x2="-1.25" y2="1.5" width="0" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="EMIF02-USB03F2">
<pin name="VBUS" x="-17.78" y="5.08" length="middle"/>
<pin name="D+OUT" x="-17.78" y="2.54" length="middle"/>
<pin name="D-OUT" x="-17.78" y="0" length="middle"/>
<pin name="DZ" x="-17.78" y="-2.54" length="middle"/>
<pin name="ID" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="D-IN" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="PD1" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="PD2" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="-10.16" length="middle" rot="R180"/>
<wire x1="-12.7" y1="7.62" x2="10.16" y2="7.62" width="0.1524" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-12.7" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="-12.7" y2="7.62" width="0.1524" layer="94"/>
<pin name="PUP" x="-17.78" y="-5.08" length="middle"/>
<pin name="D+IN" x="15.24" y="2.54" length="middle" rot="R180"/>
<text x="-10.16" y="10.16" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="-10.16" y="-17.78" size="1.4224" layer="95" ratio="10">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="EMIF02-USB03F2" prefix="U">
<gates>
<gate name="G$1" symbol="EMIF02-USB03F2" x="2.54" y="2.54"/>
</gates>
<devices>
<device name="" package="ECOPACK-11">
<connects>
<connect gate="G$1" pin="D+IN" pad="C1"/>
<connect gate="G$1" pin="D+OUT" pad="C3"/>
<connect gate="G$1" pin="D-IN" pad="D1"/>
<connect gate="G$1" pin="D-OUT" pad="D3"/>
<connect gate="G$1" pin="DZ" pad="A2"/>
<connect gate="G$1" pin="GND" pad="D2"/>
<connect gate="G$1" pin="ID" pad="A3"/>
<connect gate="G$1" pin="PD1" pad="B1"/>
<connect gate="G$1" pin="PD2" pad="C2"/>
<connect gate="G$1" pin="PUP" pad="B2"/>
<connect gate="G$1" pin="VBUS" pad="B3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="_RoweTel">
<packages>
<package name="C1206-TANT">
<wire x1="-2.35" y1="-1.1" x2="-2.35" y2="1.1" width="0" layer="39"/>
<wire x1="-2.35" y1="1.1" x2="2.35" y2="1.1" width="0" layer="39"/>
<wire x1="2.35" y1="1.1" x2="2.35" y2="-1.1" width="0" layer="39"/>
<wire x1="2.35" y1="-1.1" x2="-2.35" y2="-1.1" width="0" layer="39"/>
<wire x1="-1.6" y1="-0.75" x2="-1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0.75" x2="1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.75" x2="1.6" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.75" x2="-1.6" y2="-0.75" width="0.127" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.65" dy="1.15" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="1.65" dy="1.15" layer="1" roundness="25" rot="R90"/>
<text x="-2.075" y="1.6175" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<rectangle x1="-1.6" y1="-0.75" x2="-1.095" y2="0.75" layer="51"/>
<rectangle x1="1.095" y1="-0.75" x2="1.6" y2="0.75" layer="51"/>
<wire x1="-0.5" y1="0.8" x2="-0.5" y2="-0.8" width="0.15" layer="21"/>
<wire x1="-0.5" y1="0.8" x2="-0.4" y2="0.8" width="0.15" layer="21"/>
<wire x1="-0.4" y1="0.8" x2="-0.3" y2="0.8" width="0.15" layer="21"/>
<wire x1="-0.3" y1="0.8" x2="0.6" y2="0.8" width="0.15" layer="21"/>
<wire x1="-0.5" y1="-0.8" x2="-0.4" y2="-0.8" width="0.15" layer="21"/>
<wire x1="-0.4" y1="-0.8" x2="-0.3" y2="-0.8" width="0.15" layer="21"/>
<wire x1="-0.3" y1="-0.8" x2="0.6" y2="-0.8" width="0.15" layer="21"/>
<wire x1="-0.4" y1="0.8" x2="-0.4" y2="-0.8" width="0.15" layer="21"/>
<wire x1="-0.3" y1="0.8" x2="-0.3" y2="-0.8" width="0.15" layer="21"/>
</package>
<package name="XTAL-FY0800035">
<smd name="1" x="-2.1" y="-1.2" dx="1.6" dy="1.4" layer="1" roundness="25"/>
<smd name="2" x="2.1" y="-1.2" dx="1.6" dy="1.4" layer="1" roundness="25"/>
<smd name="3" x="2.1" y="1.2" dx="1.6" dy="1.4" layer="1" roundness="25"/>
<smd name="4" x="-2.1" y="1.2" dx="1.6" dy="1.4" layer="1" roundness="25"/>
<wire x1="-1" y1="1.6" x2="1" y2="1.6" width="0.15" layer="21"/>
<wire x1="-1" y1="-1.6" x2="1" y2="-1.6" width="0.15" layer="21"/>
<wire x1="-2.5" y1="-0.2" x2="-2.5" y2="0.2" width="0.15" layer="21"/>
<wire x1="2.5" y1="-0.2" x2="2.5" y2="0.2" width="0.15" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="3.5" y2="2.5" width="0" layer="39"/>
<wire x1="3.5" y1="2.5" x2="3.5" y2="-2.5" width="0" layer="39"/>
<wire x1="3.5" y1="-2.5" x2="-3.5" y2="-2.5" width="0" layer="39"/>
<wire x1="-3.5" y1="-2.5" x2="-3.5" y2="2.5" width="0" layer="39"/>
<text x="-2.5" y="2.5" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.5" y="-2.5" size="0.8128" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
<wire x1="-3.15" y1="-0.5" x2="-3.15" y2="-2.15" width="0.15" layer="21"/>
<wire x1="-3.15" y1="-2.15" x2="-1.5" y2="-2.15" width="0.15" layer="21"/>
<wire x1="-0.5" y1="1" x2="0.5" y2="1" width="0.15" layer="51"/>
<wire x1="0.5" y1="1" x2="0.5" y2="-1" width="0.15" layer="51"/>
<wire x1="0.5" y1="-1" x2="-0.5" y2="-1" width="0.15" layer="51"/>
<wire x1="-0.5" y1="-1" x2="-0.5" y2="1" width="0.15" layer="51"/>
<wire x1="-1" y1="1" x2="-1" y2="0" width="0.15" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-1" width="0.15" layer="51"/>
<wire x1="1" y1="1" x2="1" y2="0" width="0.15" layer="51"/>
<wire x1="1" y1="0" x2="1" y2="-1" width="0.15" layer="51"/>
<wire x1="1" y1="0" x2="2" y2="0" width="0.15" layer="51"/>
<wire x1="2" y1="0" x2="2" y2="0.25" width="0.15" layer="51"/>
<wire x1="-1" y1="0" x2="-2" y2="0" width="0.15" layer="51"/>
<wire x1="-2" y1="0" x2="-2" y2="-0.2" width="0.15" layer="51"/>
</package>
<package name="SOD323R_INFINEON">
<description>&lt;b&gt;Diode Package&lt;/b&gt; Reflow soldering&lt;p&gt;
INFINEON, www.infineon.com/cmc_upload/0/000/010/257/eh_db_5b.pdf</description>
<wire x1="-0.6" y1="0.55" x2="0.6" y2="0.55" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.55" x2="0.6" y2="-0.55" width="0.1016" layer="21"/>
<wire x1="0.6" y1="-0.55" x2="-0.6" y2="-0.55" width="0.1016" layer="21"/>
<wire x1="-0.6" y1="-0.55" x2="-0.6" y2="0.55" width="0.1016" layer="51"/>
<smd name="C" x="-1.25" y="0" dx="0.8" dy="0.6" layer="1"/>
<smd name="A" x="1.25" y="0" dx="0.8" dy="0.6" layer="1"/>
<text x="-1.65" y="0.75" size="1" layer="25">&gt;NAME</text>
<rectangle x1="-1.25" y1="-0.15" x2="-0.8" y2="0.15" layer="51"/>
<rectangle x1="0.8" y1="-0.15" x2="1.25" y2="0.15" layer="51"/>
<wire x1="-0.5" y1="0.5" x2="-0.5" y2="-0.5" width="0.15" layer="21"/>
<wire x1="-0.4" y1="0.5" x2="-0.4" y2="-0.5" width="0.15" layer="21"/>
<wire x1="-0.3" y1="0.5" x2="-0.3" y2="-0.5" width="0.15" layer="21"/>
</package>
<package name="SOT23-5L">
<smd name="2" x="0" y="1.15" dx="1.2" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="-0.95" y="1.15" dx="1.2" dy="0.6" layer="1" rot="R90"/>
<smd name="1" x="0.95" y="1.15" dx="1.2" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="-0.95" y="-1.15" dx="1.2" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="0.95" y="-1.15" dx="1.2" dy="0.6" layer="1" rot="R90"/>
<wire x1="-1.5" y1="2" x2="1.5" y2="2" width="0" layer="39"/>
<wire x1="1.5" y1="2" x2="1.5" y2="-2" width="0" layer="39"/>
<wire x1="1.5" y1="-2" x2="-1.5" y2="-2" width="0" layer="39"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="2" width="0" layer="39"/>
<text x="-1.7" y="2.5" size="1" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-1.7" y1="2.2" x2="1.7" y2="2.2" width="0.15" layer="21"/>
<wire x1="1.7" y1="2.2" x2="1.7" y2="-2.2" width="0.15" layer="21"/>
<wire x1="1.7" y1="-2.2" x2="-1.7" y2="-2.2" width="0.15" layer="21"/>
<wire x1="-1.7" y1="-2.2" x2="-1.7" y2="2.2" width="0.15" layer="21"/>
<wire x1="-1.7" y1="2.2" x2="1.7" y2="2.2" width="0.15" layer="52"/>
<wire x1="1.7" y1="2.2" x2="1.7" y2="-2.2" width="0.15" layer="52"/>
<wire x1="1.7" y1="-2.2" x2="-1.7" y2="-2.2" width="0.15" layer="52"/>
<wire x1="-1.7" y1="-2.2" x2="-1.7" y2="2.2" width="0.15" layer="52"/>
<wire x1="-1.7" y1="2.2" x2="1.7" y2="2.2" width="0.15" layer="51"/>
<wire x1="1.7" y1="2.2" x2="1.7" y2="-2.2" width="0.15" layer="51"/>
<wire x1="1.7" y1="-2.2" x2="-1.7" y2="-2.2" width="0.15" layer="51"/>
<wire x1="-1.7" y1="-2.2" x2="-1.7" y2="2.2" width="0.15" layer="51"/>
</package>
<package name="LTV-817S">
<smd name="4" x="-1.5" y="5" dx="2.5" dy="2" layer="1" rot="R90"/>
<smd name="1" x="-1.5" y="-5" dx="2.5" dy="2" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="-5" dx="2.5" dy="2" layer="1" rot="R90"/>
<smd name="3" x="1.5" y="5" dx="2.5" dy="2" layer="1" rot="R90"/>
<wire x1="-2.5" y1="-3" x2="-2.5" y2="3" width="0.15" layer="21"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.15" layer="21"/>
<wire x1="2.5" y1="3" x2="2.5" y2="-3" width="0.15" layer="21"/>
<wire x1="2.5" y1="-3" x2="-2.5" y2="-3" width="0.15" layer="21"/>
<circle x="-1.5" y="-2" radius="0.728009375" width="0" layer="21"/>
<text x="-2.5" y="7" size="1" layer="21" ratio="10">&gt;NAME</text>
<wire x1="-3" y1="7" x2="3" y2="7" width="0" layer="39"/>
<wire x1="3" y1="7" x2="3" y2="-7" width="0" layer="39"/>
<wire x1="3" y1="-7" x2="-3" y2="-7" width="0" layer="39"/>
<wire x1="-3" y1="-7" x2="-3" y2="7" width="0" layer="39"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.15" layer="51"/>
<wire x1="2.5" y1="3" x2="2.5" y2="-3" width="0.15" layer="51"/>
<wire x1="2.5" y1="-3" x2="-2.5" y2="-3" width="0.15" layer="51"/>
<wire x1="-2.5" y1="-3" x2="-2.5" y2="3" width="0.15" layer="51"/>
<circle x="-1.5" y="-2" radius="0.632453125" width="0.15" layer="51"/>
</package>
<package name="SOT23-6">
<wire x1="-1.35" y1="-0.45" x2="-1.35" y2="0.45" width="0.125" layer="21"/>
<wire x1="1.35" y1="-0.45" x2="1.35" y2="0.45" width="0.125" layer="21"/>
<wire x1="-1.35" y1="0.45" x2="1.35" y2="0.45" width="0.125" layer="21"/>
<wire x1="-1.35" y1="-0.45" x2="1.35" y2="-0.45" width="0.125" layer="21"/>
<smd name="1" x="-0.95" y="-1.2" dx="1.2" dy="0.7" layer="1" rot="R90"/>
<smd name="6" x="-0.95" y="1.2" dx="1.2" dy="0.7" layer="1" rot="R90"/>
<smd name="5" x="0" y="1.2" dx="1.2" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="0.95" y="1.2" dx="1.2" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="0" y="-1.2" dx="1.2" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="0.95" y="-1.2" dx="1.2" dy="0.7" layer="1" rot="R90"/>
<text x="-1.4" y="2.4" size="1.016" layer="25" ratio="9">&gt;NAME</text>
<wire x1="-1.7" y1="2.2" x2="1.7" y2="2.2" width="0" layer="39"/>
<wire x1="1.7" y1="2.2" x2="1.7" y2="-2.2" width="0" layer="39"/>
<wire x1="1.7" y1="-2.2" x2="-1.7" y2="-2.2" width="0" layer="39"/>
<wire x1="-1.7" y1="-2.2" x2="-1.7" y2="2.2" width="0" layer="39"/>
<rectangle x1="-1.4" y1="-0.5" x2="-0.9" y2="0" layer="21"/>
</package>
<package name="DIL08">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO08">
<description>&lt;b&gt;Small Outline Package 8&lt;/b&gt;&lt;br&gt;
NS Package M08A</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-2.667" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.937" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.1" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.1" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.1" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.1" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.1" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.1" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.1" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.1" layer="51"/>
</package>
<package name="MMSOP08">
<description>&lt;b&gt;Molded Mini Small Outline Package&lt;/b&gt; 8 - Lead (0.118" Wide)&lt;p&gt;
NS Package Number MUA08A&lt;br&gt;
Source: http://cache.national.com/ds/LM/LM386.pdf</description>
<wire x1="1.275" y1="1.425" x2="1.424" y2="1.274" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.424" y1="1.274" x2="1.424" y2="-1.276" width="0.1524" layer="21"/>
<wire x1="1.424" y1="-1.276" x2="1.275" y2="-1.425" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.275" y1="-1.425" x2="-1.275" y2="-1.425" width="0.1524" layer="21"/>
<wire x1="-1.275" y1="-1.425" x2="-1.424" y2="-1.274" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.424" y1="-1.274" x2="-1.424" y2="1.276" width="0.1524" layer="21"/>
<wire x1="-1.424" y1="1.276" x2="-1.275" y2="1.425" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.275" y1="1.425" x2="1.275" y2="1.425" width="0.1524" layer="21"/>
<circle x="-0.9456" y="-0.7906" radius="0.2339" width="0.0508" layer="21"/>
<smd name="8" x="-0.975" y="2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="7" x="-0.325" y="2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="6" x="0.325" y="2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="5" x="0.975" y="2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="4" x="0.975" y="-2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="3" x="0.325" y="-2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="2" x="-0.325" y="-2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="1" x="-0.975" y="-2.4" dx="0.41" dy="1.02" layer="1"/>
<text x="-2.032" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.302" y="-2.54" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.0975" y1="-2.45" x2="-0.8537" y2="-1.85" layer="51"/>
<rectangle x1="-1.0975" y1="-1.85" x2="-0.8537" y2="-1.4256" layer="21"/>
<rectangle x1="-0.4475" y1="-2.45" x2="-0.2037" y2="-1.85" layer="51"/>
<rectangle x1="-0.4475" y1="-1.85" x2="-0.2037" y2="-1.4256" layer="21"/>
<rectangle x1="0.2025" y1="-2.45" x2="0.4463" y2="-1.85" layer="51"/>
<rectangle x1="0.2025" y1="-1.85" x2="0.4463" y2="-1.4256" layer="21"/>
<rectangle x1="0.8525" y1="-2.45" x2="1.0963" y2="-1.85" layer="51"/>
<rectangle x1="0.8525" y1="-1.85" x2="1.0963" y2="-1.4256" layer="21"/>
<rectangle x1="0.8537" y1="1.85" x2="1.0975" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="0.8537" y1="1.4256" x2="1.0975" y2="1.85" layer="21" rot="R180"/>
<rectangle x1="0.2037" y1="1.85" x2="0.4475" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="0.2037" y1="1.4256" x2="0.4475" y2="1.85" layer="21" rot="R180"/>
<rectangle x1="-0.4463" y1="1.85" x2="-0.2025" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="-0.4463" y1="1.4256" x2="-0.2025" y2="1.85" layer="21" rot="R180"/>
<rectangle x1="-1.0963" y1="1.85" x2="-0.8525" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="-1.0963" y1="1.4256" x2="-0.8525" y2="1.85" layer="21" rot="R180"/>
</package>
<package name="SM-LP-5001">
<smd name="1" x="-6.25" y="2.54" dx="2.5" dy="1.5" layer="1"/>
<smd name="2" x="-6.25" y="0" dx="2.5" dy="1.5" layer="1"/>
<smd name="3" x="-6.25" y="-2.54" dx="2.5" dy="1.5" layer="1"/>
<smd name="4" x="6.25" y="-2.54" dx="2.5" dy="1.5" layer="1"/>
<smd name="5" x="6.25" y="0" dx="2.5" dy="1.5" layer="1"/>
<smd name="6" x="6.25" y="2.54" dx="2.5" dy="1.5" layer="1"/>
<wire x1="-6.4" y1="4.8" x2="-6.4" y2="4.2" width="0.15" layer="21"/>
<wire x1="-6.4" y1="-4" x2="-6.4" y2="-4.8" width="0.15" layer="21"/>
<wire x1="-6.4" y1="-4.8" x2="6.4" y2="-4.8" width="0.15" layer="21"/>
<wire x1="6.4" y1="-4.8" x2="6.4" y2="-4" width="0.15" layer="21"/>
<wire x1="6.4" y1="4" x2="6.4" y2="4.8" width="0.15" layer="21"/>
<wire x1="6.4" y1="4.8" x2="-6.4" y2="4.8" width="0.15" layer="21"/>
<wire x1="-8" y1="5.5" x2="8" y2="5.5" width="0" layer="39"/>
<wire x1="8" y1="5.5" x2="8" y2="-5.5" width="0" layer="39"/>
<wire x1="8" y1="-5.5" x2="-8" y2="-5.5" width="0" layer="39"/>
<wire x1="-8" y1="-5.5" x2="-8" y2="5.5" width="0" layer="39"/>
<circle x="-3.5" y="2.5" radius="0.75" width="0" layer="21"/>
<text x="-7.5" y="6" size="1" layer="21" ratio="10">&gt;NAME</text>
<wire x1="-6.4" y1="4.8" x2="6.4" y2="4.8" width="0.15" layer="51"/>
<wire x1="6.4" y1="4.8" x2="6.4" y2="-4.7" width="0.15" layer="51"/>
<wire x1="6.4" y1="-4.7" x2="-6.4" y2="-4.7" width="0.15" layer="51"/>
<wire x1="-6.4" y1="-4.7" x2="-6.4" y2="4.8" width="0.15" layer="51"/>
</package>
<package name="LTST-S2200K">
<smd name="C" x="0" y="-0.95" dx="0.9" dy="0.8" layer="1"/>
<smd name="A" x="0" y="0.95" dx="0.9" dy="0.8" layer="1"/>
<wire x1="-0.9" y1="1.5" x2="-1.4" y2="1" width="0.15" layer="21" curve="63.434949"/>
<wire x1="-1.4" y1="-1" x2="-0.9" y2="-1.5" width="0.15" layer="21" curve="90"/>
<wire x1="-1.4" y1="1" x2="-1.4" y2="-1" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1" y2="-0.15" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.15" x2="-1" y2="-0.05" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.05" x2="-1" y2="0.05" width="0.15" layer="21"/>
<wire x1="-1" y1="0.05" x2="1" y2="0.05" width="0.15" layer="21"/>
<wire x1="1" y1="0.05" x2="1" y2="-0.05" width="0.15" layer="21"/>
<wire x1="1" y1="-0.05" x2="1" y2="-0.15" width="0.15" layer="21"/>
<wire x1="1" y1="-0.15" x2="1" y2="-0.5" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.05" x2="1" y2="-0.05" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.15" x2="1" y2="-0.15" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1" y2="-0.6" width="0.15" layer="21"/>
<wire x1="1" y1="-0.5" x2="1" y2="-0.6" width="0.15" layer="21"/>
<wire x1="-0.85" y1="1.75" x2="0.85" y2="1.75" width="0" layer="39"/>
<wire x1="0.85" y1="1.75" x2="0.85" y2="-1.75" width="0" layer="39"/>
<wire x1="0.85" y1="-1.75" x2="-0.85" y2="-1.75" width="0" layer="39"/>
<wire x1="-0.85" y1="-1.75" x2="-0.85" y2="1.75" width="0" layer="39"/>
<text x="-1.5" y="2.5" size="1" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-0.9" y1="1.5" x2="-1.4" y2="1" width="0.15" layer="51" curve="63.434949"/>
<wire x1="-1.4" y1="-1" x2="-0.9" y2="-1.5" width="0.15" layer="51" curve="90"/>
<wire x1="-1.4" y1="1" x2="-1.4" y2="-1" width="0.15" layer="51"/>
<wire x1="-1" y1="-0.5" x2="-1" y2="-0.15" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.15" x2="-1" y2="-0.05" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.05" x2="-1" y2="0.05" width="0.15" layer="21"/>
<wire x1="-1" y1="0.05" x2="1" y2="0.05" width="0.15" layer="21"/>
<wire x1="1" y1="0.05" x2="1" y2="-0.05" width="0.15" layer="21"/>
<wire x1="1" y1="-0.05" x2="1" y2="-0.15" width="0.15" layer="21"/>
<wire x1="1" y1="-0.15" x2="1" y2="-0.5" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.05" x2="1" y2="-0.05" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.15" x2="1" y2="-0.15" width="0.15" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1" y2="-0.6" width="0.15" layer="21"/>
<wire x1="1" y1="-0.5" x2="1" y2="-0.6" width="0.15" layer="21"/>
<wire x1="-1" y1="1.5" x2="-1" y2="2" width="0.15" layer="51"/>
<wire x1="-1" y1="2" x2="1" y2="2" width="0.15" layer="51"/>
<wire x1="1" y1="2" x2="1" y2="-2" width="0.15" layer="51"/>
<wire x1="1" y1="-2" x2="-1" y2="-2" width="0.15" layer="51"/>
<wire x1="-1" y1="-2" x2="-1" y2="-1.5" width="0.15" layer="51"/>
<text x="1.5" y="0" size="1" layer="104" ratio="10">&gt;NAME</text>
<rectangle x1="-0.5" y1="-2" x2="0.5" y2="-0.5" layer="51"/>
<rectangle x1="-0.5" y1="0.5" x2="0.5" y2="2" layer="51"/>
</package>
<package name="NICHICON-WX-6.3">
<smd name="1" x="0" y="2.7" dx="3.5" dy="1.6" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="0" y="-2.7" dx="3.5" dy="1.6" layer="1" roundness="25" rot="R90"/>
<wire x1="-2" y1="3.3" x2="-1.2" y2="3.3" width="0.15" layer="21"/>
<wire x1="1.2" y1="3.3" x2="2" y2="3.3" width="0.15" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-1.2" y2="-3.3" width="0.15" layer="21"/>
<wire x1="1.2" y1="-3.3" x2="3.3" y2="-3.3" width="0.15" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="2" width="0.15" layer="21"/>
<wire x1="3.3" y1="-3.3" x2="3.3" y2="2" width="0.15" layer="21"/>
<wire x1="-3.3" y1="2" x2="-2" y2="3.3" width="0.15" layer="21"/>
<wire x1="2" y1="3.3" x2="3.3" y2="2" width="0.15" layer="21"/>
<text x="-2" y="5.5" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-4" y1="4" x2="4" y2="4" width="0" layer="39"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0" layer="39"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0" layer="39"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0" layer="39"/>
<wire x1="-1.2" y1="3.3" x2="-2" y2="3.3" width="0.15" layer="51"/>
<wire x1="-2" y1="3.3" x2="-3.3" y2="2" width="0.15" layer="51"/>
<wire x1="-3.3" y1="2" x2="-3.3" y2="-3.3" width="0.15" layer="51"/>
<wire x1="-3.3" y1="-3.3" x2="-1.2" y2="-3.3" width="0.15" layer="51"/>
<wire x1="1.2" y1="3.3" x2="2" y2="3.3" width="0.15" layer="51"/>
<wire x1="2" y1="3.3" x2="3.3" y2="2" width="0.15" layer="51"/>
<wire x1="3.3" y1="2" x2="3.3" y2="-3.3" width="0.15" layer="51"/>
<wire x1="3.3" y1="-3.3" x2="1.2" y2="-3.3" width="0.15" layer="51"/>
</package>
<package name="ST-3509">
<smd name="C" x="-4.5" y="3.45" dx="2.5" dy="2" layer="1" rot="R270"/>
<smd name="A" x="4.75" y="3.45" dx="2.5" dy="2" layer="1" rot="R270"/>
<smd name="B" x="2.95" y="-3.45" dx="2.5" dy="2" layer="1" rot="R270"/>
<smd name="D" x="-8.5" y="-0.75" dx="2.5" dy="2" layer="1" rot="R180"/>
<smd name="F" x="5.65" y="-3.45" dx="2.5" dy="1.5" layer="1" rot="R90"/>
<wire x1="7.25" y1="3" x2="7.25" y2="-3" width="0.15" layer="21"/>
<hole x="3.75" y="0" drill="1.8"/>
<hole x="-3.25" y="0" drill="1.8"/>
<wire x1="-7.05" y1="3" x2="-7.05" y2="-3" width="0.15" layer="21"/>
<wire x1="7.25" y1="3" x2="6.25" y2="3" width="0.15" layer="21"/>
<wire x1="7.25" y1="-3" x2="6.75" y2="-3" width="0.15" layer="21"/>
<wire x1="1.25" y1="-3" x2="-7.05" y2="-3" width="0.15" layer="21"/>
<wire x1="-6.25" y1="3" x2="-7.05" y2="3" width="0.15" layer="21"/>
<wire x1="3.25" y1="3" x2="-2.75" y2="3" width="0.15" layer="21"/>
<text x="-2.5" y="4" size="1" layer="25" ratio="10">&gt;NAME</text>
<wire x1="6.5" y1="-5" x2="6.8" y2="-5" width="0" layer="39"/>
<wire x1="6.8" y1="-5" x2="6.8" y2="-1.8" width="0" layer="39"/>
<wire x1="6.8" y1="-1.8" x2="6.2" y2="-1.8" width="0" layer="39"/>
<wire x1="6.2" y1="-1.8" x2="6.2" y2="5" width="0" layer="39"/>
<wire x1="6.2" y1="5" x2="3.3" y2="5" width="0" layer="39"/>
<wire x1="3.3" y1="5" x2="3.3" y2="3.5" width="0" layer="39"/>
<wire x1="3.3" y1="3.5" x2="-3.1" y2="3.5" width="0" layer="39"/>
<wire x1="-3.1" y1="3.5" x2="-3.1" y2="5" width="0" layer="39"/>
<wire x1="-3.1" y1="5" x2="-5.9" y2="5" width="0" layer="39"/>
<wire x1="-5.9" y1="5" x2="-5.9" y2="3.5" width="0" layer="39"/>
<wire x1="-5.9" y1="3.5" x2="-5.9" y2="0.6" width="0" layer="39"/>
<wire x1="-5.9" y1="0.6" x2="-10.1" y2="0.6" width="0" layer="39"/>
<wire x1="-10.1" y1="0.6" x2="-10.1" y2="-2.1" width="0" layer="39"/>
<wire x1="-10.1" y1="-2.1" x2="1.6" y2="-2.1" width="0" layer="39"/>
<wire x1="1.6" y1="-2.1" x2="1.6" y2="-5" width="0" layer="39"/>
<wire x1="1.6" y1="-5" x2="6.5" y2="-5" width="0" layer="39"/>
</package>
<package name="SOD523">
<description>&lt;B&gt;DIODE&lt;/B&gt;</description>
<wire x1="-0.59" y1="0.4" x2="0.59" y2="0.4" width="0.1016" layer="51"/>
<wire x1="0.59" y1="0.4" x2="0.59" y2="-0.4" width="0.1016" layer="51"/>
<wire x1="0.59" y1="-0.4" x2="-0.59" y2="-0.4" width="0.1016" layer="51"/>
<wire x1="-0.59" y1="-0.4" x2="-0.59" y2="0.4" width="0.1016" layer="51"/>
<smd name="A" x="0.7" y="0" dx="0.7" dy="0.5" layer="1"/>
<smd name="C" x="-0.6" y="0" dx="0.7" dy="0.5" layer="1"/>
<text x="-1.535" y="1.035" size="1" layer="25">&gt;NAME</text>
<text x="-1.535" y="-2.405" size="1" layer="27">&gt;VALUE</text>
<rectangle x1="-0.75" y1="-0.17" x2="-0.54" y2="0.17" layer="51"/>
<rectangle x1="0.54" y1="-0.17" x2="0.75" y2="0.17" layer="51"/>
<rectangle x1="-0.59" y1="-0.4" x2="-0.3" y2="0.4" layer="51"/>
<wire x1="-1.5" y1="1" x2="1.5" y2="1" width="0" layer="39"/>
<wire x1="1.5" y1="1" x2="1.5" y2="-1" width="0" layer="39"/>
<wire x1="1.5" y1="-1" x2="-1.5" y2="-1" width="0" layer="39"/>
<wire x1="-1.5" y1="-1" x2="-1.5" y2="1" width="0" layer="39"/>
<wire x1="-0.9" y1="0.5" x2="-0.15" y2="0.5" width="0.15" layer="21"/>
<wire x1="-0.15" y1="0.5" x2="-0.05" y2="0.5" width="0.15" layer="21"/>
<wire x1="-0.05" y1="0.5" x2="0.4" y2="0.5" width="0.15" layer="21"/>
<wire x1="-0.9" y1="-0.5" x2="-0.05" y2="-0.5" width="0.15" layer="21"/>
<wire x1="-0.05" y1="-0.5" x2="0.4" y2="-0.5" width="0.15" layer="21"/>
<wire x1="-0.05" y1="0.5" x2="-0.05" y2="-0.5" width="0.15" layer="21"/>
</package>
<package name="SML-LX0603SRW">
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<polygon width="0" layer="21">
<vertex x="-0.15" y="0.5"/>
<vertex x="-0.15" y="-0.5"/>
<vertex x="0.05" y="-0.5"/>
<vertex x="0.05" y="-0.15"/>
<vertex x="0.2" y="-0.15"/>
<vertex x="0.2" y="0.15"/>
<vertex x="0.05" y="0.15"/>
<vertex x="0.05" y="0.5"/>
</polygon>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0" layer="39"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0" layer="39"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0" layer="39"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0" layer="39"/>
<text x="-1.5" y="1.2" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-1.45" y1="0.7" x2="-0.8" y2="0.7" width="0.15" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.45" y2="0.7" width="0.15" layer="21"/>
<wire x1="1.45" y1="0.7" x2="1.45" y2="-0.7" width="0.15" layer="21"/>
<wire x1="-1.45" y1="-0.7" x2="-1.45" y2="0.7" width="0.15" layer="21"/>
<wire x1="-1.45" y1="-0.7" x2="-0.8" y2="-0.7" width="0.15" layer="21"/>
<wire x1="1.45" y1="-0.7" x2="0.8" y2="-0.7" width="0.15" layer="21"/>
</package>
<package name="TS-02B">
<smd name="1" x="-3.25" y="1.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="2" x="3.25" y="1.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="3" x="-3.25" y="-1.5" dx="1.5" dy="0.9" layer="1"/>
<smd name="4" x="3.25" y="-1.5" dx="1.5" dy="0.9" layer="1"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.15" layer="21"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.15" layer="21"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.15" layer="21"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.15" layer="21"/>
<wire x1="-4.3" y1="2.5" x2="4.3" y2="2.5" width="0" layer="39"/>
<wire x1="4.3" y1="2.5" x2="4.3" y2="-2.5" width="0" layer="39"/>
<wire x1="4.3" y1="-2.5" x2="-4.3" y2="-2.5" width="0" layer="39"/>
<wire x1="-4.3" y1="-2.5" x2="-4.3" y2="2.5" width="0" layer="39"/>
<text x="-2" y="3" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<circle x="-1.5" y="1.5" radius="0.412309375" width="0" layer="25"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.15" layer="51"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.15" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.15" layer="51"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.15" layer="51"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.15" layer="52"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.15" layer="52"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.15" layer="52"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.15" layer="52"/>
</package>
</packages>
<symbols>
<symbol name="CAP-POL">
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<wire x1="2.54" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.4224" layer="96" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="XTAL">
<pin name="4" x="-10.16" y="5.08" visible="pad" length="short"/>
<pin name="1" x="-10.16" y="0" visible="pad" length="short"/>
<pin name="3" x="10.16" y="0" visible="pad" length="short" rot="R180"/>
<pin name="2" x="10.16" y="-5.08" visible="pad" length="short" rot="R180"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="7.62" width="0.1524" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-7.62" y2="7.62" width="0.1524" layer="94"/>
<text x="-5.842" y="5.842" size="1.6764" layer="94" ratio="10" align="top-left">G</text>
<text x="4.064" y="-4.064" size="1.6764" layer="94" ratio="10" align="top-left">G</text>
<text x="-5.08" y="10.16" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.4224" layer="96" ratio="10">&gt;VALUE</text>
</symbol>
<symbol name="SHOTTKY-D">
<wire x1="-1.27" y1="-1.524" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.524" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.524" y2="1.778" width="0.1524" layer="94"/>
<wire x1="1.524" y1="1.778" x2="1.524" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.524" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.524" x2="-1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.524" x2="-0.762" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-1.778" x2="1.524" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-1.524" x2="-1.778" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="1.524" x2="-0.762" y2="1.143" width="0.1524" layer="94"/>
<text x="2.54" y="-2.54" size="1.4224" layer="95" ratio="10" rot="R180">&gt;NAME</text>
<text x="2.54" y="3.8354" size="1.4224" layer="96" ratio="10" rot="R180">&gt;VALUE</text>
<pin name="A" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<pin name="C" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
</symbol>
<symbol name="LD3985">
<pin name="VIN" x="-17.78" y="5.08" length="middle"/>
<pin name="INH" x="-17.78" y="-5.08" length="middle"/>
<pin name="GND" x="0" y="-12.7" length="middle" rot="R90"/>
<pin name="BYBASS" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="VOUT" x="17.78" y="5.08" length="middle" rot="R180"/>
<wire x1="-12.7" y1="7.62" x2="12.7" y2="7.62" width="0.1524" layer="94"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="-12.7" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.1524" layer="94"/>
<text x="-12.7" y="10.16" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="10.16" size="1.4224" layer="96" ratio="10">&gt;VALUE</text>
</symbol>
<symbol name="OK">
<wire x1="-2.413" y1="-1.143" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.254" x2="-1.905" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-0.127" x2="-1.397" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.397" y1="-0.635" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="1.397" x2="-2.032" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.016" x2="-1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.508" x2="-1.143" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.143" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-5.715" y2="1.27" width="0.254" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="4.445" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="-5.08" x2="4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-4.445" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="-6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-4.445" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.445" y1="5.08" x2="4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="2.286" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.016" x2="2.286" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-2.286" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-2.286" x2="1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.778" x2="1.778" y2="-1.016" width="0.1524" layer="94"/>
<text x="-6.985" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.985" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.381" y1="-2.54" x2="0.381" y2="2.54" layer="94"/>
<pin name="A" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="C" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="EMIT" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="COL" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="LMV341">
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-2.54" y2="3.81" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="2.54" y2="1.27" width="0.4064" layer="94"/>
<wire x1="2.54" y1="1.27" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<text x="5.08" y="7.62" size="1.4224" layer="95" ratio="9">&gt;NAME</text>
<text x="7.62" y="5.08" size="1.4224" layer="96" ratio="9">&gt;VALUE</text>
<pin name="-IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="GND" x="-2.54" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="V+" x="-2.54" y="7.62" visible="pad" length="short" rot="R270"/>
<pin name="!SHDN!" x="2.54" y="-7.62" visible="pad" length="short" rot="R90"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.81" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.032" radius="0.508" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="COHO-B-SIZE">
<wire x1="0" y1="0" x2="0" y2="279.4" width="0.254" layer="94"/>
<wire x1="0" y1="279.4" x2="431.8" y2="279.4" width="0.254" layer="94"/>
<wire x1="431.8" y1="279.4" x2="431.8" y2="0" width="0.254" layer="94"/>
<wire x1="431.8" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document</text>
<text x="20.32" y="7.62" size="3.81" layer="95">&gt;DRAWING_NAME</text>
<text x="1.27" y="7.62" size="2.54" layer="94">Number:</text>
<wire x1="0" y1="35.56" x2="0" y2="40.64" width="0.254" layer="94"/>
<wire x1="0" y1="40.64" x2="101.6" y2="40.64" width="0.254" layer="94"/>
<wire x1="101.6" y1="40.64" x2="101.6" y2="35.56" width="0.254" layer="94"/>
<text x="3.048" y="37.084" size="2.1844" layer="94" ratio="10">Licensed Under the TAPR Open Hardware License(www.tapr.org/OHL)</text>
<text x="2.8448" y="42.0116" size="2.1844" layer="94" ratio="10">Copyright 2014  Richard Barnich and David Rowe</text>
<wire x1="0" y1="40.64" x2="0" y2="45.72" width="0.254" layer="94"/>
<wire x1="0" y1="45.72" x2="101.6" y2="45.72" width="0.254" layer="94"/>
<wire x1="101.6" y1="45.72" x2="101.6" y2="40.64" width="0.254" layer="94"/>
<text x="4.3942" y="25.9842" size="6.4516" layer="94" ratio="10">ROWETEL</text>
</symbol>
<symbol name="REV-BLOCK-1">
<wire x1="67.31" y1="0" x2="67.31" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="11.43" y1="0" x2="11.43" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="73.66" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="62.23" y1="0" x2="62.23" y2="-15.494" width="0.1524" layer="94"/>
<wire x1="55.88" y1="0" x2="55.88" y2="-15.24" width="0.1524" layer="94"/>
<text x="2.032" y="-2.032" size="1.27" layer="94">DATE</text>
<text x="13.97" y="-2.032" size="1.27" layer="94">REVISION</text>
<text x="68.072" y="-2.032" size="1.27" layer="94">BY</text>
<text x="62.992" y="-2.032" size="1.27" layer="94">REV</text>
<text x="56.642" y="-2.032" size="1.27" layer="94">ECN</text>
</symbol>
<symbol name="AMP_GND_GAIN_BYP">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="4.1402" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="2.54" y2="-2.032" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="7.62" y2="0" width="0.4064" layer="94"/>
<wire x1="7.62" y1="0" x2="5.08" y2="1.016" width="0.4064" layer="94"/>
<wire x1="5.08" y1="1.016" x2="2.54" y2="2.032" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.032" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-4.1402" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="2.032" width="0.1524" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GAIN@1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="GAIN@2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="OUT" x="10.16" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="VS" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="GND" x="-2.54" y="-7.62" visible="pad" length="short" direction="pwr" rot="R90"/>
<pin name="BYPASS" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="SM-LP-5001">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-5.08" width="0.1524" layer="94" curve="-180"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="5.08" width="0.1524" layer="94" curve="-180"/>
<wire x1="5.08" y1="0" x2="5.08" y2="2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.1524" layer="94" curve="-180"/>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="2" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="3" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas"/>
<pin name="4" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="10.16" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="6" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<text x="0" y="7.62" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="-8.89" size="1.4224" layer="95" ratio="10">&gt;VALUE</text>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-4.064" y="-4.572" size="1.4224" layer="95" ratio="9" rot="R90">&gt;NAME</text>
<text x="3.81" y="-4.826" size="1.4224" layer="96" ratio="9" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="POLARISED">
<wire x1="0.381" y1="1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="1.27" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.524" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<text x="1.6764" y="0.5842" size="1.27" layer="94">+</text>
<text x="-2.54" y="2.032" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.4765" y1="-0.4445" x2="0.8255" y2="0.4445" layer="94" rot="R270"/>
<pin name="-" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="+" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="ST-3509">
<wire x1="-21.59" y1="-5.08" x2="-24.13" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-24.13" y1="-5.08" x2="-24.13" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-24.13" y1="2.54" x2="-22.86" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="2.54" x2="-21.59" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-21.59" y1="2.54" x2="-21.59" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-16.51" y1="-7.62" x2="-15.24" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-5.08" x2="-13.97" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-19.05" y1="5.08" x2="-17.78" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="2.54" x2="-16.51" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-13.97" y1="-7.62" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-16.51" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-10.16" y="5.08"/>
<vertex x="-11.43" y="2.54"/>
<vertex x="-8.89" y="2.54"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-11.43" y="-5.08"/>
<vertex x="-8.89" y="-5.08"/>
<vertex x="-10.16" y="-7.62"/>
</polygon>
<wire x1="-10.16" y1="2.54" x2="-10.16" y2="0" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="2.54" x2="-22.86" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="7.62" x2="0" y2="7.62" width="0.1524" layer="94"/>
<pin name="C" x="5.08" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="D" x="5.08" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="E" x="5.08" y="0" visible="pad" length="middle" rot="R180"/>
<pin name="B" x="5.08" y="5.08" visible="pad" length="middle" rot="R180"/>
<pin name="A" x="5.08" y="7.62" visible="pad" length="middle" rot="R180"/>
<text x="-15.24" y="10.16" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="-15.24" y="-12.7" size="1.4224" layer="95" ratio="10">&gt;VALUE</text>
</symbol>
<symbol name="STMP2141">
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="IN" x="-15.24" y="2.54" length="middle"/>
<pin name="!EN!" x="-15.24" y="-2.54" length="middle"/>
<pin name="OUT" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="!FAULT!" x="17.78" y="-2.54" length="middle" rot="R180"/>
<wire x1="-10.16" y1="5.08" x2="12.7" y2="5.08" width="0.1524" layer="94"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="-10.16" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.1524" layer="94"/>
<text x="-10.16" y="7.62" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="7.62" size="1.4224" layer="95" ratio="10">&gt;VALUE</text>
</symbol>
<symbol name="DIODE-TVS">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="0" y1="1.524" x2="0" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.524" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="1.524" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.524" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.524" x2="1.778" y2="1.524" width="0.1524" layer="94"/>
<text x="0" y="2.54" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="4.064" size="1.4224" layer="96" ratio="10">&gt;VALUE</text>
</symbol>
<symbol name="TS-02B">
<pin name="1" x="-10.16" y="2.54" visible="pad" length="short"/>
<pin name="3" x="10.16" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="4" x="10.16" y="-2.54" visible="pad" length="short" rot="R180"/>
<wire x1="-7.62" y1="3.81" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="3.81" x2="-7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.803215625" width="0.1524" layer="94"/>
<circle x="2.54" y="0" radius="0.803215625" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="-2.794" y2="2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="5.08" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.4224" layer="95" ratio="10" rot="R180">&gt;VALUE</text>
<pin name="2" x="-10.16" y="-2.54" visible="pad" length="short"/>
<wire x1="-7.62" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="0" width="0.1524" layer="94"/>
<circle x="-5.08" y="0" radius="0.762" width="0" layer="94"/>
<circle x="5.08" y="0" radius="0.762" width="0" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP-TANT-1206" prefix="C">
<gates>
<gate name="G$1" symbol="CAP-POL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206-TANT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XTAL-FY0800035" prefix="XTAL">
<gates>
<gate name="G$1" symbol="XTAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="XTAL-FY0800035">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BAT60J" prefix="D">
<gates>
<gate name="G$1" symbol="SHOTTKY-D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD323R_INFINEON">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LD3985" prefix="U">
<gates>
<gate name="G$1" symbol="LD3985" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="BYBASS" pad="4"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="INH" pad="3"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTV-817S" prefix="LD">
<gates>
<gate name="G$1" symbol="OK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LTV-817S">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="COL" pad="4"/>
<connect gate="G$1" pin="EMIT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LMV341" prefix="U">
<gates>
<gate name="G$1" symbol="LMV341" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-6">
<connects>
<connect gate="G$1" pin="!SHDN!" pad="5"/>
<connect gate="G$1" pin="+IN" pad="1"/>
<connect gate="G$1" pin="-IN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="OUT" pad="4"/>
<connect gate="G$1" pin="V+" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GENERICL-B-SIZE" prefix="B-FRAME">
<gates>
<gate name="G$1" symbol="COHO-B-SIZE" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="330.2" y="0"/>
<gate name="G$3" symbol="REV-BLOCK-1" x="358.14" y="279.4"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM386?-*" prefix="U">
<description>&lt;b&gt;Low Voltage Audio Power Amplifier&lt;/b&gt;&lt;p&gt;
Source: http://cache.national.com/ds/LM/LM386.pdf</description>
<gates>
<gate name="G$1" symbol="AMP_GND_GAIN_BYP" x="0" y="0"/>
</gates>
<devices>
<device name="N" package="DIL08">
<connects>
<connect gate="G$1" pin="+IN" pad="3"/>
<connect gate="G$1" pin="-IN" pad="2"/>
<connect gate="G$1" pin="BYPASS" pad="7"/>
<connect gate="G$1" pin="GAIN@1" pad="1"/>
<connect gate="G$1" pin="GAIN@2" pad="8"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="VS" pad="6"/>
</connects>
<technologies>
<technology name="1"/>
<technology name="3"/>
<technology name="4"/>
</technologies>
</device>
<device name="M" package="SO08">
<connects>
<connect gate="G$1" pin="+IN" pad="3"/>
<connect gate="G$1" pin="-IN" pad="2"/>
<connect gate="G$1" pin="BYPASS" pad="7"/>
<connect gate="G$1" pin="GAIN@1" pad="1"/>
<connect gate="G$1" pin="GAIN@2" pad="8"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="VS" pad="6"/>
</connects>
<technologies>
<technology name="1"/>
</technologies>
</device>
<device name="MM" package="MMSOP08">
<connects>
<connect gate="G$1" pin="+IN" pad="3"/>
<connect gate="G$1" pin="-IN" pad="2"/>
<connect gate="G$1" pin="BYPASS" pad="7"/>
<connect gate="G$1" pin="GAIN@1" pad="1"/>
<connect gate="G$1" pin="GAIN@2" pad="8"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="VS" pad="6"/>
</connects>
<technologies>
<technology name="1"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SM-LP-5001" prefix="T">
<gates>
<gate name="G$1" symbol="SM-LP-5001" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="SM-LP-5001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTST-S220K" prefix="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="LTST-S2200K">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NICHICON-WX-6.3" prefix="C">
<gates>
<gate name="G$1" symbol="POLARISED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NICHICON-WX-6.3">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ST-3509" prefix="CN">
<gates>
<gate name="G$1" symbol="ST-3509" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ST-3509">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="E" pad="F"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STMPS2141" prefix="U">
<gates>
<gate name="G$1" symbol="STMP2141" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="!EN!" pad="4"/>
<connect gate="G$1" pin="!FAULT!" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="5"/>
<connect gate="G$1" pin="OUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PESD5ZX" prefix="D">
<gates>
<gate name="G$1" symbol="DIODE-TVS" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="SOD523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SML-LX0603SRW" prefix="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SML-LX0603SRW">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TS-02B" prefix="SW">
<gates>
<gate name="G$1" symbol="TS-02B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TS-02B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="_Coherix">
<description>.1uF,50V</description>
<packages>
<package name="HDR100-3">
<wire x1="-1.27" y1="1.27" x2="6.39" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.39" y1="1.27" x2="6.39" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.39" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="0.8" diameter="1.4224" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.8" diameter="1.4224"/>
<pad name="3" x="5.08" y="0" drill="0.8" diameter="1.4224"/>
<text x="-0.97" y="1.59" size="0.8128" layer="25">&gt;NAME</text>
<wire x1="-2" y1="1.5" x2="6.5" y2="1.5" width="0" layer="39"/>
<wire x1="6.5" y1="1.5" x2="6.5" y2="-1.5" width="0" layer="39"/>
<wire x1="6.5" y1="-1.5" x2="-2" y2="-1.5" width="0" layer="39"/>
<wire x1="-2" y1="-1.5" x2="-2" y2="1.5" width="0" layer="39"/>
<rectangle x1="-1.8" y1="-1.35" x2="-1.25" y2="1.35" layer="21"/>
<text x="-1" y="-1.6" size="1" layer="27" ratio="10" align="top-left">&gt;VALUE</text>
</package>
<package name="TPAD50">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.27" shape="square"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="JMPR-2PIN">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="1.27" drill="0.8" diameter="1.4224" shape="square"/>
<pad name="2" x="0" y="-1.27" drill="0.8" diameter="1.4224"/>
<text x="-1.54" y="-3.04" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<wire x1="-1.5" y1="3.04" x2="1.5" y2="3.04" width="0" layer="39"/>
<wire x1="1.5" y1="3.04" x2="1.5" y2="-3.04" width="0" layer="39"/>
<wire x1="1.5" y1="-3.04" x2="-1.5" y2="-3.04" width="0" layer="39"/>
<wire x1="-1.5" y1="-3.04" x2="-1.5" y2="3.04" width="0" layer="39"/>
<wire x1="-1.2" y1="2.5" x2="1.2" y2="2.5" width="0.15" layer="51"/>
<wire x1="1.2" y1="2.5" x2="1.2" y2="-2.5" width="0.15" layer="51"/>
<wire x1="1.2" y1="-2.5" x2="-1.2" y2="-2.5" width="0.15" layer="51"/>
<wire x1="-1.2" y1="-2.5" x2="-1.2" y2="2.5" width="0.15" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="HDR100-3">
<wire x1="-1.9481" y1="2.561" x2="-1.9481" y2="-7.599" width="0.1524" layer="94"/>
<wire x1="-1.9481" y1="-7.599" x2="-6.0121" y2="-7.599" width="0.1524" layer="94"/>
<wire x1="-6.0121" y1="-7.599" x2="-6.0121" y2="2.561" width="0.1524" layer="94"/>
<wire x1="-6.0121" y1="2.561" x2="-1.9481" y2="2.561" width="0.1524" layer="94"/>
<text x="-4.9961" y="3.577" size="1.4224" layer="95" ratio="9">&gt;NAME</text>
<text x="-4.9961" y="-10.139" size="1.4224" layer="96" ratio="9">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pin" length="middle" function="dot" rot="R180"/>
<pin name="2" x="0" y="-2.54" visible="pin" length="middle" function="dot" rot="R180"/>
<pin name="3" x="0" y="-5.08" visible="pin" length="middle" function="dot" rot="R180"/>
</symbol>
<symbol name="TPAD50">
<circle x="0" y="5.08" radius="1.0472" width="0.254" layer="94"/>
<text x="-1.27" y="6.604" size="1.4224" layer="95" ratio="9">&gt;NAME</text>
<pin name="1" x="0" y="0" visible="off" length="middle" rot="R90"/>
</symbol>
<symbol name="JMP-2PIN">
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.8032" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.8032" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.4224" layer="95">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.4224" layer="96" ratio="9">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="short"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HDR100-3" prefix="HDR">
<gates>
<gate name="G$1" symbol="HDR100-3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR100-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPAD50" prefix="P">
<gates>
<gate name="G$1" symbol="TPAD50" x="5.08" y="-5.08"/>
</gates>
<devices>
<device name="" package="TPAD50">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JMP-2PIN" prefix="J">
<gates>
<gate name="G$1" symbol="JMP-2PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JMPR-2PIN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="A-SUPPLY2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VDD">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="3.048" size="1.4224" layer="95" ratio="9">&gt;VALUE</text>
<pin name="VDD" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-3.302" size="1.4224" layer="96" ratio="9">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+05V">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<text x="-2.413" y="0.635" size="1.4224" layer="96" ratio="9">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="AVDD">
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.1524" layer="94"/>
<text x="-1.524" y="1.016" size="1.4224" layer="96" ratio="9">&gt;VALUE</text>
<pin name="AVDD" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="RF_MIC">
<pin name="RF_MIC" x="0" y="2.54" visible="off" length="point" direction="sup" rot="R270"/>
<text x="-2.2225" y="-2.54" size="1.778" layer="96" ratio="12">&gt;VALUE</text>
<wire x1="1.1113" y1="1.5875" x2="0" y2="1.5875" width="0.254" layer="94"/>
<wire x1="0" y1="1.5875" x2="-1.1113" y2="1.5875" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.5875" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0.7938" x2="-0.635" y2="0.7938" width="0.254" layer="94"/>
<wire x1="0.1587" y1="0" x2="-0.1588" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="RF_SPKR">
<pin name="RF_SPKR" x="0" y="2.54" visible="off" length="point" direction="sup" rot="R270"/>
<text x="-2.2225" y="-2.54" size="1.778" layer="96" ratio="12">&gt;VALUE</text>
<wire x1="1.1113" y1="1.5875" x2="0" y2="1.5875" width="0.254" layer="94"/>
<wire x1="0" y1="1.5875" x2="-1.1113" y2="1.5875" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.5875" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0.7938" x2="-0.635" y2="0.7938" width="0.254" layer="94"/>
<wire x1="0.1587" y1="0" x2="-0.1588" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="RF_PTT">
<pin name="RF_PTT" x="0" y="2.54" visible="off" length="point" direction="sup" rot="R270"/>
<text x="-2.2225" y="-2.54" size="1.778" layer="96" ratio="12">&gt;VALUE</text>
<wire x1="1.1113" y1="1.5875" x2="0" y2="1.5875" width="0.254" layer="94"/>
<wire x1="0" y1="1.5875" x2="-1.1113" y2="1.5875" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.5875" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0.7938" x2="-0.635" y2="0.7938" width="0.254" layer="94"/>
<wire x1="0.1587" y1="0" x2="-0.1588" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VDD" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVDD" prefix="V">
<gates>
<gate name="G$1" symbol="AVDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RF_MIC" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="RF_MIC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RF_SPKR" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="RF_SPKR" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RF_PTT" prefix="V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="RF_PTT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SM1000">
<description>Generated from &lt;b&gt;SM1000.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="C1210">
<wire x1="-2.35" y1="-1.6" x2="-2.35" y2="1.6" width="0" layer="39"/>
<wire x1="-2.35" y1="1.6" x2="2.35" y2="1.6" width="0" layer="39"/>
<wire x1="2.35" y1="1.6" x2="2.35" y2="-1.6" width="0" layer="39"/>
<wire x1="2.35" y1="-1.6" x2="-2.35" y2="-1.6" width="0" layer="39"/>
<wire x1="-0.5" y1="1.35" x2="0.5" y2="1.35" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.35" x2="0.5" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.127" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="2.7" dy="1.15" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="2.7" dy="1.15" layer="1" rot="R90"/>
<text x="-2.075" y="1.6675" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.075" y="-2.9375" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.095" y2="1.25" layer="51"/>
<rectangle x1="1.095" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024" cap="flat"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.4224" layer="95" ratio="9">&gt;NAME</text>
<text x="1.016" y="-4.064" size="1.4224" layer="96" ratio="9">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP-1210" prefix="C">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AA-ipc-7351-resistor">
<description>&lt;b&gt;IPC-7351 compliant SMT resistors&lt;/b&gt;&lt;br&gt;
&lt;br&gt;
Symbols copied from CadSoft rcl.lbr.&lt;br&gt;
Packages generated using genpkg_chp.ulp.&lt;br&gt;
Devices are Panasonic ERJ series types.&lt;br&gt;
&lt;br&gt;
Weartronics 2006&lt;br&gt;
http://www.weartronics.com/</description>
<packages>
<package name="RESC1608X55N">
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0" layer="39"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0" layer="39"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0" layer="39"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0" layer="39"/>
<wire x1="-0.8" y1="-0.425" x2="-0.8" y2="0.425" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0.425" x2="0.8" y2="0.425" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.425" x2="0.8" y2="-0.425" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.425" x2="-0.8" y2="-0.425" width="0.127" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.95" dy="0.8" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="0.8" y="0" dx="0.95" dy="0.8" layer="1" roundness="25" rot="R90"/>
<text x="-1.55" y="1.0175" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.25" y="-2.0875" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.425" x2="-0.5" y2="0.425" layer="51"/>
<rectangle x1="0.5" y1="-0.425" x2="0.8" y2="0.425" layer="51"/>
<wire x1="-0.9" y1="0.75" x2="-1.5" y2="0.75" width="0.125" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="-0.75" width="0.125" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-0.9" y2="-0.75" width="0.125" layer="21"/>
<wire x1="0.9" y1="0.75" x2="1.5" y2="0.75" width="0.125" layer="21"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.125" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="0.9" y2="-0.75" width="0.125" layer="21"/>
</package>
<package name="RESC0603X30N">
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0" layer="39"/>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0" layer="39"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0" layer="39"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0" layer="39"/>
<wire x1="-0.3" y1="-0.15" x2="-0.3" y2="0.15" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0.15" x2="0.3" y2="0.15" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.15" x2="0.3" y2="-0.15" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="-0.3" y2="-0.15" width="0.127" layer="51"/>
<smd name="1" x="-0.35" y="0" dx="0.4" dy="0.45" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="0.35" y="0" dx="0.4" dy="0.45" layer="1" roundness="25" rot="R90"/>
<text x="-0.575" y="0.454" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.575" y="-1.724" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
</package>
<package name="RESC1005X40AN">
<wire x1="-0.95" y1="-0.5" x2="-0.95" y2="0.5" width="0" layer="39"/>
<wire x1="-0.95" y1="0.5" x2="0.95" y2="0.5" width="0" layer="39"/>
<wire x1="0.95" y1="0.5" x2="0.95" y2="-0.5" width="0" layer="39"/>
<wire x1="0.95" y1="-0.5" x2="-0.95" y2="-0.5" width="0" layer="39"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.127" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.127" layer="51"/>
<smd name="1" x="-0.5" y="0" dx="0.65" dy="0.6" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="0.5" y="0" dx="0.65" dy="0.6" layer="1" roundness="25" rot="R90"/>
<text x="-1.05" y="0.754" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.75" y="-1.824" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.25" y2="0.25" layer="51"/>
<rectangle x1="0.25" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.125" layer="21"/>
<wire x1="1" y1="0.5" x2="0.5" y2="0.5" width="0.125" layer="21"/>
<wire x1="-0.5" y1="0.5" x2="-1" y2="0.5" width="0.125" layer="21"/>
<wire x1="1" y1="0.5" x2="1" y2="-0.5" width="0.125" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-0.5" y2="-0.5" width="0.125" layer="21"/>
<wire x1="0.5" y1="-0.5" x2="1" y2="-0.5" width="0.125" layer="21"/>
</package>
<package name="RESC3115X70N">
<wire x1="-2.25" y1="-1.1" x2="-2.25" y2="1.1" width="0" layer="39"/>
<wire x1="-2.25" y1="1.1" x2="2.25" y2="1.1" width="0" layer="39"/>
<wire x1="2.25" y1="1.1" x2="2.25" y2="-1.1" width="0" layer="39"/>
<wire x1="2.25" y1="-1.1" x2="-2.25" y2="-1.1" width="0" layer="39"/>
<wire x1="-0.5" y1="0.8" x2="0.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-0.8" x2="0.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.5625" y1="-0.775" x2="-1.5625" y2="0.775" width="0.127" layer="51"/>
<wire x1="-1.5625" y1="0.775" x2="1.5625" y2="0.775" width="0.127" layer="51"/>
<wire x1="1.5625" y1="0.775" x2="1.5625" y2="-0.775" width="0.127" layer="51"/>
<wire x1="1.5625" y1="-0.775" x2="-1.5625" y2="-0.775" width="0.127" layer="51"/>
<smd name="1" x="-1.45" y="0" dx="1.7" dy="1.1" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="1.45" y="0" dx="1.7" dy="1.1" layer="1" roundness="25" rot="R90"/>
<text x="-2" y="1.1175" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2" y="-2.3875" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.5625" y1="-0.775" x2="-1.0625" y2="0.775" layer="51"/>
<rectangle x1="1.0625" y1="-0.775" x2="1.5625" y2="0.775" layer="51"/>
</package>
<package name="RESC3225X70N">
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="1.6" width="0" layer="39"/>
<wire x1="-2.3" y1="1.6" x2="2.3" y2="1.6" width="0" layer="39"/>
<wire x1="2.3" y1="1.6" x2="2.3" y2="-1.6" width="0" layer="39"/>
<wire x1="2.3" y1="-1.6" x2="-2.3" y2="-1.6" width="0" layer="39"/>
<wire x1="-0.5" y1="1.35" x2="0.5" y2="1.35" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.35" x2="0.5" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.127" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="2.7" dy="1.15" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="2.7" dy="1.15" layer="1" roundness="25" rot="R90"/>
<text x="-2.075" y="1.6675" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.075" y="-2.9375" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="RESC4532X70N">
<wire x1="-3" y1="-1.95" x2="-3" y2="1.95" width="0" layer="39"/>
<wire x1="-3" y1="1.95" x2="3" y2="1.95" width="0" layer="39"/>
<wire x1="3" y1="1.95" x2="3" y2="-1.95" width="0" layer="39"/>
<wire x1="3" y1="-1.95" x2="-3" y2="-1.95" width="0" layer="39"/>
<wire x1="-1.15" y1="1.7" x2="1.15" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.15" y1="-1.7" x2="1.15" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<smd name="1" x="-2.15" y="0" dx="3.4" dy="1.15" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="2.15" y="0" dx="3.4" dy="1.15" layer="1" roundness="25" rot="R90"/>
<text x="-2.725" y="2.0175" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.725" y="-3.2875" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.75" y2="1.6" layer="51"/>
<rectangle x1="1.75" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="RESC5025X70N">
<wire x1="-3.25" y1="-1.6" x2="-3.25" y2="1.6" width="0" layer="39"/>
<wire x1="-3.25" y1="1.6" x2="3.25" y2="1.6" width="0" layer="39"/>
<wire x1="3.25" y1="1.6" x2="3.25" y2="-1.6" width="0" layer="39"/>
<wire x1="3.25" y1="-1.6" x2="-3.25" y2="-1.6" width="0" layer="39"/>
<wire x1="-1.35" y1="1.35" x2="1.35" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.35" y1="-1.35" x2="1.35" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.25" x2="-2.5" y2="1.25" width="0.127" layer="51"/>
<wire x1="-2.5" y1="1.25" x2="2.5" y2="1.25" width="0.127" layer="51"/>
<wire x1="2.5" y1="1.25" x2="2.5" y2="-1.25" width="0.127" layer="51"/>
<wire x1="2.5" y1="-1.25" x2="-2.5" y2="-1.25" width="0.127" layer="51"/>
<smd name="1" x="-2.35" y="0" dx="2.7" dy="1.25" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="2.35" y="0" dx="2.7" dy="1.25" layer="1" roundness="25" rot="R90"/>
<text x="-2.975" y="1.6675" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.975" y="-2.9375" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.5" y1="-1.25" x2="-1.9" y2="1.25" layer="51"/>
<rectangle x1="1.9" y1="-1.25" x2="2.5" y2="1.25" layer="51"/>
</package>
<package name="RESC6432X70N">
<wire x1="-3.95" y1="-1.95" x2="-3.95" y2="1.95" width="0" layer="39"/>
<wire x1="-3.95" y1="1.95" x2="3.95" y2="1.95" width="0" layer="39"/>
<wire x1="3.95" y1="1.95" x2="3.95" y2="-1.95" width="0" layer="39"/>
<wire x1="3.95" y1="-1.95" x2="-3.95" y2="-1.95" width="0" layer="39"/>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.7" x2="2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-3.2" y1="-1.6" x2="-3.2" y2="1.6" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.6" x2="3.2" y2="1.6" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.6" x2="3.2" y2="-1.6" width="0.127" layer="51"/>
<wire x1="3.2" y1="-1.6" x2="-3.2" y2="-1.6" width="0.127" layer="51"/>
<smd name="1" x="-3.05" y="0" dx="3.4" dy="1.25" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="3.05" y="0" dx="3.4" dy="1.25" layer="1" roundness="25" rot="R90"/>
<text x="-3.675" y="2.0175" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.675" y="-3.2875" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2" y1="-1.6" x2="-2.6" y2="1.6" layer="51"/>
<rectangle x1="2.6" y1="-1.6" x2="3.2" y2="1.6" layer="51"/>
</package>
<package name="RESC2012X70N">
<wire x1="-1.75" y1="-0.95" x2="-1.75" y2="0.95" width="0" layer="39"/>
<wire x1="-1.75" y1="0.95" x2="1.75" y2="0.95" width="0" layer="39"/>
<wire x1="1.75" y1="0.95" x2="1.75" y2="-0.95" width="0" layer="39"/>
<wire x1="1.75" y1="-0.95" x2="-1.75" y2="-0.95" width="0" layer="39"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.4" dy="1.05" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="0.95" y="0" dx="1.4" dy="1.05" layer="1" roundness="25" rot="R90"/>
<text x="-1.475" y="1.0175" size="1" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.475" y="-2.2875" size="1" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.625" x2="-0.6" y2="0.625" layer="51"/>
<rectangle x1="0.6" y1="-0.625" x2="1" y2="0.625" layer="51"/>
<wire x1="-1" y1="0.9" x2="-1.7" y2="0.9" width="0.125" layer="21"/>
<wire x1="-1.7" y1="0.9" x2="-1.7" y2="-0.9" width="0.125" layer="21"/>
<wire x1="-1.7" y1="-0.9" x2="-1" y2="-0.9" width="0.125" layer="21"/>
<wire x1="1" y1="0.9" x2="1.7" y2="0.9" width="0.125" layer="21"/>
<wire x1="1.7" y1="0.9" x2="1.7" y2="-0.9" width="0.125" layer="21"/>
<wire x1="1.7" y1="-0.9" x2="1" y2="-0.9" width="0.125" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR_" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="RESC0603X30N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="RESC1005X40AN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2012X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3115X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="RESC3225X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="RESC4532X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5025X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6432X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AA-ipc-7351-capacitor">
<description>&lt;b&gt;IPC-7351 compliant SMT capacitors&lt;/b&gt;&lt;br&gt;
&lt;br&gt;
Symbols copied from CadSoft rcl.lbr&lt;br&gt;
Packages generated using genpkg_chp.ulp, genpkg_cae.ulp and genpkg_mld.ulp&lt;br&gt;
Devices are Vishay chip types, generic AEC types and Kemet T491 series molded body types.&lt;br&gt;
&lt;br&gt;
Weartronics 2006&lt;br&gt;
http://www.weartronics.com/</description>
<packages>
<package name="CAPC1608X92N">
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0" layer="39"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0" layer="39"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0" layer="39"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0" layer="39"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.95" dy="0.9" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="0.8" y="0" dx="0.95" dy="0.9" layer="1" roundness="25" rot="R90"/>
<text x="-1.55" y="1.0675" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.42" y2="0.4" layer="51"/>
<rectangle x1="0.42" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<wire x1="-1.5" y1="0.75" x2="-0.8" y2="0.75" width="0.125" layer="21"/>
<wire x1="0.8" y1="0.75" x2="1.5" y2="0.75" width="0.125" layer="21"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.125" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="0.8" y2="-0.75" width="0.125" layer="21"/>
<wire x1="-0.8" y1="-0.75" x2="-1.5" y2="-0.75" width="0.125" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.125" layer="21"/>
</package>
<package name="CAPC1005X60N">
<wire x1="-0.95" y1="-0.5" x2="-0.95" y2="0.5" width="0" layer="39"/>
<wire x1="-0.95" y1="0.5" x2="0.95" y2="0.5" width="0" layer="39"/>
<wire x1="0.95" y1="0.5" x2="0.95" y2="-0.5" width="0" layer="39"/>
<wire x1="0.95" y1="-0.5" x2="-0.95" y2="-0.5" width="0" layer="39"/>
<wire x1="-0.5125" y1="-0.2625" x2="-0.5125" y2="0.2625" width="0.127" layer="51"/>
<wire x1="-0.5125" y1="0.2625" x2="0.5125" y2="0.2625" width="0.127" layer="51"/>
<wire x1="0.5125" y1="0.2625" x2="0.5125" y2="-0.2625" width="0.127" layer="51"/>
<wire x1="0.5125" y1="-0.2625" x2="-0.5125" y2="-0.2625" width="0.127" layer="51"/>
<smd name="1" x="-0.45" y="0" dx="0.65" dy="0.65" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="0.45" y="0" dx="0.65" dy="0.65" layer="1" roundness="25" rot="R90"/>
<text x="-0.975" y="0.779" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<rectangle x1="-0.5125" y1="-0.2625" x2="-0.2575" y2="0.2625" layer="51"/>
<rectangle x1="0.2575" y1="-0.2625" x2="0.5125" y2="0.2625" layer="51"/>
<wire x1="-1" y1="0.5" x2="-0.5" y2="0.5" width="0.125" layer="21"/>
<wire x1="0.5" y1="0.5" x2="1" y2="0.5" width="0.125" layer="21"/>
<wire x1="1" y1="0.5" x2="1" y2="-0.5" width="0.125" layer="21"/>
<wire x1="1" y1="-0.5" x2="0.5" y2="-0.5" width="0.125" layer="21"/>
<wire x1="-0.5" y1="-0.5" x2="-1" y2="-0.5" width="0.125" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1" y2="0.5" width="0.125" layer="21"/>
</package>
<package name="CAPC1220X107N">
<wire x1="-1.1" y1="-1.25" x2="-1.1" y2="1.25" width="0" layer="39"/>
<wire x1="-1.1" y1="1.25" x2="1.1" y2="1.25" width="0" layer="39"/>
<wire x1="1.1" y1="1.25" x2="1.1" y2="-1.25" width="0" layer="39"/>
<wire x1="1.1" y1="-1.25" x2="-1.1" y2="-1.25" width="0" layer="39"/>
<wire x1="-0.625" y1="-1" x2="-0.625" y2="1" width="0.127" layer="51"/>
<wire x1="-0.625" y1="1" x2="0.625" y2="1" width="0.127" layer="51"/>
<wire x1="0.625" y1="1" x2="0.625" y2="-1" width="0.127" layer="51"/>
<wire x1="0.625" y1="-1" x2="-0.625" y2="-1" width="0.127" layer="51"/>
<smd name="1" x="-0.55" y="0" dx="2.2" dy="0.75" layer="1" rot="R90"/>
<smd name="2" x="0.55" y="0" dx="2.2" dy="0.75" layer="1" rot="R90"/>
<text x="-0.925" y="1.354" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.925" y="-2.624" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.625" y1="-1" x2="-0.33" y2="1" layer="51"/>
<rectangle x1="0.33" y1="-1" x2="0.625" y2="1" layer="51"/>
</package>
<package name="CAPC1632X168N">
<wire x1="-1.55" y1="-1.95" x2="-1.55" y2="1.95" width="0" layer="39"/>
<wire x1="-1.55" y1="1.95" x2="1.55" y2="1.95" width="0" layer="39"/>
<wire x1="1.55" y1="1.95" x2="1.55" y2="-1.95" width="0" layer="39"/>
<wire x1="1.55" y1="-1.95" x2="-1.55" y2="-1.95" width="0" layer="39"/>
<wire x1="0" y1="1.7" x2="0" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-1.6" x2="-0.8" y2="1.6" width="0.127" layer="51"/>
<wire x1="-0.8" y1="1.6" x2="0.8" y2="1.6" width="0.127" layer="51"/>
<wire x1="0.8" y1="1.6" x2="0.8" y2="-1.6" width="0.127" layer="51"/>
<wire x1="0.8" y1="-1.6" x2="-0.8" y2="-1.6" width="0.127" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="3.4" dy="0.95" layer="1" rot="R90"/>
<smd name="2" x="0.8" y="0" dx="3.4" dy="0.95" layer="1" rot="R90"/>
<text x="-1.275" y="2.0175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.275" y="-3.2875" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.445" y2="1.6" layer="51"/>
<rectangle x1="0.445" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="CAPC3215X168N">
<wire x1="-2.35" y1="-1.1" x2="-2.35" y2="1.1" width="0" layer="39"/>
<wire x1="-2.35" y1="1.1" x2="2.35" y2="1.1" width="0" layer="39"/>
<wire x1="2.35" y1="1.1" x2="2.35" y2="-1.1" width="0" layer="39"/>
<wire x1="2.35" y1="-1.1" x2="-2.35" y2="-1.1" width="0" layer="39"/>
<wire x1="-0.5" y1="0.8" x2="0.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-0.8" x2="0.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.75" x2="-1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0.75" x2="1.6" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.75" x2="1.6" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.75" x2="-1.6" y2="-0.75" width="0.127" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.65" dy="1.15" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="1.65" dy="1.15" layer="1" rot="R90"/>
<text x="-2.075" y="1.1175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.075" y="-2.3875" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.75" x2="-1.095" y2="0.75" layer="51"/>
<rectangle x1="1.095" y1="-0.75" x2="1.6" y2="0.75" layer="51"/>
</package>
<package name="CAPC3225X168N">
<wire x1="-2.35" y1="-1.6" x2="-2.35" y2="1.6" width="0" layer="39"/>
<wire x1="-2.35" y1="1.6" x2="2.35" y2="1.6" width="0" layer="39"/>
<wire x1="2.35" y1="1.6" x2="2.35" y2="-1.6" width="0" layer="39"/>
<wire x1="2.35" y1="-1.6" x2="-2.35" y2="-1.6" width="0" layer="39"/>
<wire x1="-0.5" y1="1.35" x2="0.5" y2="1.35" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.35" x2="0.5" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.127" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="2.7" dy="1.15" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="2.7" dy="1.15" layer="1" roundness="25" rot="R90"/>
<text x="-2.075" y="1.6675" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.075" y="-2.9375" size="0.8128" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.095" y2="1.25" layer="51"/>
<rectangle x1="1.095" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="CAPC4520X168N">
<wire x1="-3" y1="-1.4" x2="-3" y2="1.4" width="0" layer="39"/>
<wire x1="-3" y1="1.4" x2="3" y2="1.4" width="0" layer="39"/>
<wire x1="3" y1="1.4" x2="3" y2="-1.4" width="0" layer="39"/>
<wire x1="3" y1="-1.4" x2="-3" y2="-1.4" width="0" layer="39"/>
<wire x1="-1.15" y1="1.15" x2="1.15" y2="1.15" width="0.127" layer="21"/>
<wire x1="-1.15" y1="-1.15" x2="1.15" y2="-1.15" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-1.015" x2="-2.25" y2="1.015" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.015" x2="2.25" y2="1.015" width="0.127" layer="51"/>
<wire x1="2.25" y1="1.015" x2="2.25" y2="-1.015" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.015" x2="-2.25" y2="-1.015" width="0.127" layer="51"/>
<smd name="1" x="-2.15" y="0" dx="2.3" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="2.15" y="0" dx="2.3" dy="1.2" layer="1" rot="R90"/>
<text x="-2.75" y="1.4675" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.75" y="-2.7375" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.015" x2="-1.745" y2="1.015" layer="51"/>
<rectangle x1="1.745" y1="-1.015" x2="2.25" y2="1.015" layer="51"/>
</package>
<package name="CAPC4532X218N">
<wire x1="-3" y1="-1.95" x2="-3" y2="1.95" width="0" layer="39"/>
<wire x1="-3" y1="1.95" x2="3" y2="1.95" width="0" layer="39"/>
<wire x1="3" y1="1.95" x2="3" y2="-1.95" width="0" layer="39"/>
<wire x1="3" y1="-1.95" x2="-3" y2="-1.95" width="0" layer="39"/>
<wire x1="-1.15" y1="1.7" x2="1.15" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.15" y1="-1.7" x2="1.15" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<smd name="1" x="-2.15" y="0" dx="3.4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="2.15" y="0" dx="3.4" dy="1.2" layer="1" rot="R90"/>
<text x="-2.75" y="2.0175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.75" y="-3.2875" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.745" y2="1.6" layer="51"/>
<rectangle x1="1.745" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="CAPC4564X218N">
<wire x1="-3" y1="-3.6" x2="-3" y2="3.6" width="0" layer="39"/>
<wire x1="-3" y1="3.6" x2="3" y2="3.6" width="0" layer="39"/>
<wire x1="3" y1="3.6" x2="3" y2="-3.6" width="0" layer="39"/>
<wire x1="3" y1="-3.6" x2="-3" y2="-3.6" width="0" layer="39"/>
<wire x1="-1.15" y1="3.3" x2="1.15" y2="3.3" width="0.127" layer="21"/>
<wire x1="-1.15" y1="-3.3" x2="1.15" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-3.2" x2="-2.25" y2="3.2" width="0.127" layer="51"/>
<wire x1="-2.25" y1="3.2" x2="2.25" y2="3.2" width="0.127" layer="51"/>
<wire x1="2.25" y1="3.2" x2="2.25" y2="-3.2" width="0.127" layer="51"/>
<wire x1="2.25" y1="-3.2" x2="-2.25" y2="-3.2" width="0.127" layer="51"/>
<smd name="1" x="-2.15" y="0" dx="6.65" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="2.15" y="0" dx="6.65" dy="1.2" layer="1" rot="R90"/>
<text x="-2.75" y="3.6175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.75" y="-4.8875" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-3.2" x2="-1.745" y2="3.2" layer="51"/>
<rectangle x1="1.745" y1="-3.2" x2="2.25" y2="3.2" layer="51"/>
</package>
<package name="CAPC5651X218N">
<wire x1="-3.55" y1="-2.95" x2="-3.55" y2="2.95" width="0" layer="39"/>
<wire x1="-3.55" y1="2.95" x2="3.55" y2="2.95" width="0" layer="39"/>
<wire x1="3.55" y1="2.95" x2="3.55" y2="-2.95" width="0" layer="39"/>
<wire x1="3.55" y1="-2.95" x2="-3.55" y2="-2.95" width="0" layer="39"/>
<wire x1="-1.7" y1="2.65" x2="1.7" y2="2.65" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.65" x2="1.7" y2="-2.65" width="0.127" layer="21"/>
<wire x1="-2.82" y1="-2.54" x2="-2.82" y2="2.54" width="0.127" layer="51"/>
<wire x1="-2.82" y1="2.54" x2="2.82" y2="2.54" width="0.127" layer="51"/>
<wire x1="2.82" y1="2.54" x2="2.82" y2="-2.54" width="0.127" layer="51"/>
<wire x1="2.82" y1="-2.54" x2="-2.82" y2="-2.54" width="0.127" layer="51"/>
<smd name="1" x="-2.7" y="0" dx="5.35" dy="1.15" layer="1" rot="R90"/>
<smd name="2" x="2.7" y="0" dx="5.35" dy="1.15" layer="1" rot="R90"/>
<text x="-3.275" y="2.9675" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.275" y="-4.2375" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.82" y1="-2.54" x2="-2.315" y2="2.54" layer="51"/>
<rectangle x1="2.315" y1="-2.54" x2="2.82" y2="2.54" layer="51"/>
</package>
<package name="CAPC5664X218N">
<wire x1="-3.5" y1="-3.55" x2="-3.5" y2="3.55" width="0" layer="39"/>
<wire x1="-3.5" y1="3.55" x2="3.5" y2="3.55" width="0" layer="39"/>
<wire x1="3.5" y1="3.55" x2="3.5" y2="-3.55" width="0" layer="39"/>
<wire x1="3.5" y1="-3.55" x2="-3.5" y2="-3.55" width="0" layer="39"/>
<wire x1="-1.65" y1="3.3" x2="1.65" y2="3.3" width="0.127" layer="21"/>
<wire x1="-1.65" y1="-3.3" x2="1.65" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-2.77" y1="-3.175" x2="-2.77" y2="3.175" width="0.127" layer="51"/>
<wire x1="-2.77" y1="3.175" x2="2.77" y2="3.175" width="0.127" layer="51"/>
<wire x1="2.77" y1="3.175" x2="2.77" y2="-3.175" width="0.127" layer="51"/>
<wire x1="2.77" y1="-3.175" x2="-2.77" y2="-3.175" width="0.127" layer="51"/>
<smd name="1" x="-2.65" y="0" dx="6.6" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="2.65" y="0" dx="6.6" dy="1.2" layer="1" rot="R90"/>
<text x="-3.25" y="3.6175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.25" y="-4.8875" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.77" y1="-3.175" x2="-2.265" y2="3.175" layer="51"/>
<rectangle x1="2.265" y1="-3.175" x2="2.77" y2="3.175" layer="51"/>
</package>
<package name="CAPC9198X218N">
<wire x1="-5.35" y1="-5.25" x2="-5.35" y2="5.25" width="0" layer="39"/>
<wire x1="-5.35" y1="5.25" x2="5.35" y2="5.25" width="0" layer="39"/>
<wire x1="5.35" y1="5.25" x2="5.35" y2="-5.25" width="0" layer="39"/>
<wire x1="5.35" y1="-5.25" x2="-5.35" y2="-5.25" width="0" layer="39"/>
<wire x1="-3.4" y1="4.9" x2="3.4" y2="4.9" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-4.9" x2="3.4" y2="-4.9" width="0.127" layer="21"/>
<wire x1="-4.57" y1="-4.91" x2="-4.57" y2="4.91" width="0.127" layer="51"/>
<wire x1="-4.57" y1="4.91" x2="4.57" y2="4.91" width="0.127" layer="51"/>
<wire x1="4.57" y1="4.91" x2="4.57" y2="-4.91" width="0.127" layer="51"/>
<wire x1="4.57" y1="-4.91" x2="-4.57" y2="-4.91" width="0.127" layer="51"/>
<smd name="1" x="-4.45" y="0" dx="9.95" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="4.45" y="0" dx="9.95" dy="1.3" layer="1" rot="R90"/>
<text x="-5.1" y="5.229" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.1" y="-6.499" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-4.91" x2="-4.065" y2="4.91" layer="51"/>
<rectangle x1="4.065" y1="-4.91" x2="4.57" y2="4.91" layer="51"/>
</package>
<package name="CAPC2012X145AN">
<wire x1="-1.75" y1="-1" x2="-1.75" y2="1" width="0" layer="39"/>
<wire x1="-1.75" y1="1" x2="1.75" y2="1" width="0" layer="39"/>
<wire x1="1.75" y1="1" x2="1.75" y2="-1" width="0" layer="39"/>
<wire x1="1.75" y1="-1" x2="-1.75" y2="-1" width="0" layer="39"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="1.45" dy="1.15" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="0.9" y="0" dx="1.45" dy="1.15" layer="1" roundness="25" rot="R90"/>
<text x="-1.775" y="1.3175" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<rectangle x1="-1" y1="-0.625" x2="-0.52" y2="0.625" layer="51"/>
<rectangle x1="0.52" y1="-0.625" x2="1" y2="0.625" layer="51"/>
<wire x1="-1.8" y1="1" x2="-0.9" y2="1" width="0.125" layer="21"/>
<wire x1="0.9" y1="1" x2="1.8" y2="1" width="0.125" layer="21"/>
<wire x1="1.8" y1="1" x2="1.8" y2="-1" width="0.125" layer="21"/>
<wire x1="1.8" y1="-1" x2="0.9" y2="-1" width="0.125" layer="21"/>
<wire x1="-1" y1="-1" x2="-1.8" y2="-1" width="0.125" layer="21"/>
<wire x1="-1.8" y1="-1" x2="-1.8" y2="1" width="0.125" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="2.54" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-2.54" y1="-0.254" x2="1.524" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR_" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X60N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="CAPC1220X107N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X92N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0612" package="CAPC1632X168N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X145AN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3215X168N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="CAPC3225X168N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="CAPC4520X168N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X218N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="CAPC4564X218N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="CAPC5651X218N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225" package="CAPC5664X218N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3640" package="CAPC9198X218N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SEcube">
<packages>
<package name="DAC121S1_SOT" urn="urn:adsk.eagle:footprint:2809062/1">
<smd name="1" x="-0.95" y="-1.35" dx="0.6" dy="1.1" layer="1"/>
<smd name="2" x="0" y="-1.35" dx="0.6" dy="1.1" layer="1"/>
<smd name="3" x="0.95" y="-1.35" dx="0.6" dy="1.1" layer="1"/>
<smd name="4" x="0.95" y="1.35" dx="0.6" dy="1.1" layer="1"/>
<smd name="5" x="0" y="1.35" dx="0.6" dy="1.1" layer="1"/>
<smd name="6" x="-0.95" y="1.35" dx="0.6" dy="1.1" layer="1"/>
<circle x="-1.15" y="-1.8" radius="0.05" width="0" layer="21"/>
<wire x1="-1.45" y1="0.8" x2="1.45" y2="0.8" width="0.127" layer="51"/>
<wire x1="1.45" y1="0.8" x2="1.45" y2="-0.8" width="0.127" layer="51"/>
<wire x1="1.45" y1="-0.8" x2="-1.45" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.45" y1="-0.8" x2="-1.45" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.45" y1="0.8" x2="-1.45" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.45" y1="0.8" x2="1.45" y2="-0.8" width="0.127" layer="21"/>
<text x="-2.15" y="2.45" size="0.8128" layer="25">&gt;NAME</text>
<text x="-2.25" y="-3.1" size="0.8128" layer="27">&gt;VALUE</text>
</package>
<package name="TSSOP-28_FE/EB" urn="urn:adsk.eagle:footprint:2809070/1" locally_modified="yes">
<smd name="1" x="-2.87528125" y="4.226559375" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="2" x="-2.87528125" y="3.57631875" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="3" x="-2.87528125" y="2.92608125" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="4" x="-2.87528125" y="2.275840625" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="5" x="-2.87528125" y="1.6256" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="6" x="-2.87528125" y="0.975359375" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="7" x="-2.87528125" y="0.32511875" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="8" x="-2.87528125" y="-0.32511875" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="9" x="-2.87528125" y="-0.975359375" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="10" x="-2.87528125" y="-1.6256" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="11" x="-2.87528125" y="-2.275840625" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="12" x="-2.87528125" y="-2.92608125" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="13" x="-2.87528125" y="-3.57631875" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="14" x="-2.87528125" y="-4.226559375" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="15" x="2.87528125" y="-4.226559375" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="16" x="2.87528125" y="-3.57631875" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="17" x="2.87528125" y="-2.92608125" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="18" x="2.87528125" y="-2.275840625" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="19" x="2.87528125" y="-1.6256" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="20" x="2.87528125" y="-0.975359375" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="21" x="2.87528125" y="-0.32511875" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="22" x="2.87528125" y="0.32511875" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="23" x="2.87528125" y="0.975359375" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="24" x="2.87528125" y="1.6256" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="25" x="2.87528125" y="2.275840625" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="26" x="2.87528125" y="2.92608125" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="27" x="2.87528125" y="3.57631875" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="28" x="2.87528125" y="4.226559375" dx="1.361440625" dy="0.3556" layer="1"/>
<smd name="29" x="0" y="0" dx="2.7432" dy="4.7498" layer="1" cream="no"/>
<wire x1="-2.3876" y1="-5.0292" x2="2.3876" y2="-5.0292" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="-5.0292" x2="2.3876" y2="-4.7498" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="5.0292" x2="-2.3876" y2="5.0292" width="0.1524" layer="21"/>
<wire x1="-2.3876" y1="5.0292" x2="-2.3876" y2="4.7498" width="0.1524" layer="21"/>
<wire x1="-2.3876" y1="-4.7498" x2="-2.3876" y2="-5.0292" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="4.7498" x2="2.3876" y2="5.0292" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="4.064" x2="-2.2606" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-2.2606" y1="4.3688" x2="-3.2004" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="4.3688" x2="-3.2004" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="4.064" x2="-2.2352" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="3.429" x2="-2.2606" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="-2.2606" y1="3.7338" x2="-3.2004" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.7338" x2="-3.2004" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.429" x2="-2.2352" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.7686" x2="-2.2352" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="3.0734" x2="-3.2004" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.0734" x2="-3.2004" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.7686" x2="-2.2352" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.1336" x2="-2.2352" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.4384" x2="-3.2004" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.4384" x2="-3.2004" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.1336" x2="-2.2352" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.4732" x2="-2.2352" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.778" x2="-3.2004" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.778" x2="-3.2004" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.4732" x2="-2.2352" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.8128" x2="-2.2352" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.1176" x2="-3.2004" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.1176" x2="-3.2004" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.8128" x2="-2.2352" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.1778" x2="-2.2352" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.4826" x2="-3.2004" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.4826" x2="-3.2004" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.1778" x2="-2.2352" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.4826" x2="-2.2352" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.1778" x2="-3.2004" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.1778" x2="-3.2004" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.4826" x2="-2.2352" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.1176" x2="-2.2352" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.8128" x2="-3.2004" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.8128" x2="-3.2004" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.1176" x2="-2.2352" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.778" x2="-2.2352" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.4732" x2="-3.2004" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.4732" x2="-3.2004" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.778" x2="-2.2352" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.4384" x2="-2.2352" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.1336" x2="-3.2004" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.1336" x2="-3.2004" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.4384" x2="-2.2352" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.0734" x2="-2.2352" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.7686" x2="-3.2004" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.7686" x2="-3.2004" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.0734" x2="-2.2352" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.7338" x2="-2.2352" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.429" x2="-3.2004" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.429" x2="-3.2004" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.7338" x2="-2.2352" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.3688" x2="-2.2352" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.064" x2="-3.2004" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-4.064" x2="-3.2004" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-4.3688" x2="-2.2352" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-4.064" x2="2.2606" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="2.2606" y1="-4.3688" x2="3.2004" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-4.3688" x2="3.2004" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-4.064" x2="2.2352" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.429" x2="2.2352" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.7338" x2="3.2004" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.7338" x2="3.2004" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.429" x2="2.2352" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.7686" x2="2.2352" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.0734" x2="3.2004" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.0734" x2="3.2004" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.7686" x2="2.2352" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.1336" x2="2.2352" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.4384" x2="3.2004" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.4384" x2="3.2004" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.1336" x2="2.2352" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.4732" x2="2.2352" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.778" x2="3.2004" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.778" x2="3.2004" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.4732" x2="2.2352" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.8128" x2="2.2352" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.1176" x2="3.2004" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.1176" x2="3.2004" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.8128" x2="2.2352" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.1778" x2="2.2352" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.4826" x2="3.2004" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.4826" x2="3.2004" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.1778" x2="2.2352" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.4826" x2="2.2352" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.1778" x2="3.2004" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.1778" x2="3.2004" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.4826" x2="2.2352" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.1176" x2="2.2352" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.8128" x2="3.2004" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.8128" x2="3.2004" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.1176" x2="2.2352" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.778" x2="2.2352" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.4732" x2="3.2004" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.4732" x2="3.2004" y2="1.778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.778" x2="2.2352" y2="1.778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.4384" x2="2.2352" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.1336" x2="3.2004" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.1336" x2="3.2004" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.4384" x2="2.2352" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.0734" x2="2.2352" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.7686" x2="3.2004" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.7686" x2="3.2004" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.0734" x2="2.2352" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.7338" x2="2.2352" y2="3.429" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.429" x2="3.2004" y2="3.429" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.429" x2="3.2004" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.7338" x2="2.2352" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.3688" x2="2.2352" y2="4.064" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.064" x2="3.2004" y2="4.064" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="4.064" x2="3.2004" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="4.3688" x2="2.2352" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.9022" x2="2.2352" y2="-4.9022" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-4.9022" x2="2.2352" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.9022" x2="0.3048" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="4.9022" x2="-2.2352" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="4.9022" x2="-2.2352" y2="-4.9022" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0.1524" layer="51" curve="-180"/>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="2.2749"/>
<vertex x="-1.2716" y="0.891634375"/>
<vertex x="-0.1" y="0.891634375"/>
<vertex x="-0.1" y="2.2749"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="0.691634375"/>
<vertex x="-1.2716" y="-0.691634375"/>
<vertex x="-0.1" y="-0.691634375"/>
<vertex x="-0.1" y="0.691634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="-0.891634375"/>
<vertex x="-1.2716" y="-2.2749"/>
<vertex x="-0.1" y="-2.2749"/>
<vertex x="-0.1" y="-0.891634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="2.2749"/>
<vertex x="0.1" y="0.891634375"/>
<vertex x="1.2716" y="0.891634375"/>
<vertex x="1.2716" y="2.2749"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="0.691634375"/>
<vertex x="0.1" y="-0.691634375"/>
<vertex x="1.2716" y="-0.691634375"/>
<vertex x="1.2716" y="0.691634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="-0.891634375"/>
<vertex x="0.1" y="-2.2749"/>
<vertex x="1.2716" y="-2.2749"/>
<vertex x="1.2716" y="-0.891634375"/>
</polygon>
<text x="-2.4384" y="3.556" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="0.3048" y1="4.9522" x2="-0.3048" y2="4.9522" width="0.1524" layer="21" curve="-180"/>
<circle x="-2.1" y="4.8" radius="0.1581125" width="0" layer="21"/>
</package>
<package name="RC0603N" urn="urn:adsk.eagle:footprint:2809073/1" locally_modified="yes">
<smd name="1" x="-0.7778" y="0" dx="0.7556" dy="0.799996875" layer="1"/>
<smd name="2" x="0.7778" y="0" dx="0.7556" dy="0.799996875" layer="1"/>
<text x="-0.7874" y="0.7866" size="0.3048" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-0.4064" y1="-0.4064" x2="-0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.4064" y1="0.4064" x2="-0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="-0.4064" x2="-0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="0.4064" x2="0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="-0.4064" x2="0.7874" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.4064" x2="0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.4064" y1="-0.4064" x2="0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="-0.4064" x2="0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="0.4064" x2="-0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="0.4064" x2="-0.7874" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-1.45" y1="0.65" x2="-1.45" y2="-0.65" width="0.1524" layer="21"/>
<wire x1="-1.45" y1="-0.65" x2="1.45" y2="-0.65" width="0.1524" layer="21"/>
<wire x1="1.45" y1="-0.65" x2="1.45" y2="0.65" width="0.1524" layer="21"/>
<wire x1="1.45" y1="0.65" x2="-1.45" y2="0.65" width="0.1524" layer="21"/>
<wire x1="0" y1="0.2" x2="0" y2="-0.2" width="0.1524" layer="21"/>
</package>
<package name="RC0805N" urn="urn:adsk.eagle:footprint:2809085/1" locally_modified="yes">
<smd name="1" x="-0.852796875" y="0" dx="1.0056" dy="1.199996875" layer="1"/>
<smd name="2" x="0.852796875" y="0" dx="1.0056" dy="1.199996875" layer="1"/>
<text x="-1.8406" y="-0.9104" size="0.4064" layer="25" ratio="6" rot="SR90">&gt;Name</text>
<wire x1="-0.3556" y1="-0.6096" x2="-0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="0.6096" x2="-0.9906" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.9906" y1="-0.6096" x2="-0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="0.6096" x2="0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="-0.6096" x2="0.9906" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9906" y1="0.6096" x2="0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="-0.6096" x2="0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9906" y1="-0.6096" x2="0.9906" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="0.6096" x2="-0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.9906" y1="0.6096" x2="-0.9906" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.5" y1="0.75" x2="-1.5" y2="0.75" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="-0.75" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="1.5" y2="-0.75" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.1524" layer="21"/>
<wire x1="0" y1="0.4" x2="0" y2="-0.4" width="0.1524" layer="21"/>
</package>
<package name="CAP_1608" urn="urn:adsk.eagle:footprint:2809088/1" locally_modified="yes">
<smd name="1" x="-0.8509" y="0" dx="0.4064" dy="0.889" layer="1"/>
<smd name="2" x="0.8509" y="0" dx="0.4064" dy="0.889" layer="1"/>
<wire x1="-0.6604" y1="-0.4572" x2="-0.6604" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.6604" y1="0" x2="-0.6604" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.6604" y1="0.4572" x2="-0.8636" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="-0.4572" x2="-0.6604" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0.4572" x2="0.6604" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="0.6604" y1="-0.4318" x2="0.8636" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="-0.4318" x2="0.8382" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="0.8382" y1="0.4572" x2="0.635" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.6604" y1="-0.4572" x2="0.8636" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="-0.4572" x2="0.8636" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="0.4572" x2="-0.6604" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="0.4572" x2="-0.8636" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="-0.6604" y1="0" x2="-0.8128" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-0.8128" y1="0" x2="-0.6604" y2="0" width="0" layer="51" curve="-180"/>
<text x="-0.9398" y="0.762" size="0.4064" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="1.32" y1="0.66" x2="-1.34" y2="0.66" width="0.1524" layer="21"/>
<wire x1="-1.34" y1="0.66" x2="-1.34" y2="-0.68" width="0.1524" layer="21"/>
<wire x1="-1.34" y1="-0.68" x2="1.32" y2="-0.68" width="0.1524" layer="21"/>
<wire x1="1.32" y1="-0.68" x2="1.32" y2="0.66" width="0.1524" layer="21"/>
<wire x1="0" y1="0.27" x2="0" y2="-0.28" width="0.1524" layer="21"/>
</package>
<package name="SRN4026" urn="urn:adsk.eagle:footprint:2809094/1" locally_modified="yes">
<smd name="1" x="-1.525" y="0" dx="1.5" dy="3.6" layer="1"/>
<smd name="2" x="1.525" y="0" dx="1.5" dy="3.6" layer="1"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.127" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.127" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.127" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.127" layer="51"/>
<wire x1="-2.55" y1="2" x2="2.55" y2="2" width="0.127" layer="21"/>
<text x="-2.05" y="2.45" size="0.8128" layer="25">&gt;NAME</text>
<wire x1="-2.55" y1="-2" x2="2.55" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.55" y1="2" x2="-2.55" y2="-2" width="0.127" layer="21"/>
<wire x1="2.55" y1="-2" x2="2.55" y2="2" width="0.127" layer="21"/>
</package>
<package name="G-188" urn="urn:adsk.eagle:footprint:2809095/1" locally_modified="yes">
<smd name="1" x="-0.6477" y="0" dx="0.7112" dy="0.8128" layer="1"/>
<smd name="2" x="0.6477" y="0" dx="0.7112" dy="0.8128" layer="1"/>
<wire x1="-0.3048" y1="-0.4064" x2="-0.3048" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="0.4064" x2="-0.8128" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.8128" y1="-0.4064" x2="-0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.2794" y1="0.4064" x2="0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="-0.4064" x2="0.8128" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="-0.4064" x2="0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.4064" x2="0.2794" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="-0.4064" x2="0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="-0.4064" x2="0.8128" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="0.4064" x2="-0.3048" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.8128" y1="0.4064" x2="-0.8128" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-0.4572" y1="0" x2="-0.6096" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-0.6096" y1="0" x2="-0.4572" y2="0" width="0" layer="51" curve="-180"/>
<text x="-0.762" y="0.7596" size="0.3048" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-1.2" y1="0.6" x2="1.2" y2="0.6" width="0.15" layer="21"/>
<wire x1="1.2" y1="0.6" x2="1.2" y2="-0.6" width="0.15" layer="21"/>
<wire x1="1.2" y1="-0.6" x2="-1.2" y2="-0.6" width="0.15" layer="21"/>
<wire x1="-1.2" y1="-0.6" x2="-1.2" y2="0.6" width="0.15" layer="21"/>
<wire x1="0" y1="0.25" x2="0" y2="-0.25" width="0.15" layer="21"/>
</package>
<package name="IND_BOURNS_SRR6038" urn="urn:adsk.eagle:footprint:2809102/1" locally_modified="yes">
<smd name="1" x="-2.3495" y="0" dx="2.5908" dy="7.2898" layer="1"/>
<smd name="2" x="2.3495" y="0" dx="2.5908" dy="7.2898" layer="1"/>
<wire x1="-3.5052" y1="-3.5052" x2="3.5052" y2="-3.5052" width="0.1524" layer="51"/>
<wire x1="3.5052" y1="-3.5052" x2="3.5052" y2="3.5052" width="0.1524" layer="51"/>
<wire x1="3.5052" y1="3.5052" x2="-3.5052" y2="3.5052" width="0.1524" layer="51"/>
<wire x1="-3.5052" y1="3.5052" x2="-3.5052" y2="0" width="0.1524" layer="51"/>
<wire x1="-3.5052" y1="0" x2="-3.5052" y2="-3.5052" width="0.1524" layer="51"/>
<wire x1="-3.3528" y1="0" x2="-3.5052" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-3.5052" y1="0" x2="-3.3528" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="3.85" y1="3.9" x2="-3.9" y2="3.9" width="0.15" layer="21"/>
<wire x1="-3.9" y1="3.9" x2="-3.9" y2="-3.85" width="0.15" layer="21"/>
<wire x1="-3.9" y1="-3.85" x2="3.85" y2="-3.85" width="0.15" layer="21"/>
<wire x1="3.85" y1="-3.85" x2="3.85" y2="3.9" width="0.15" layer="21"/>
<wire x1="0" y1="3.25" x2="0" y2="-3.25" width="0.15" layer="21"/>
</package>
<package name="RES_ERA6A_0805" urn="urn:adsk.eagle:footprint:2809103/1" locally_modified="yes">
<smd name="1" x="-0.8763" y="0" dx="0.8636" dy="1.3462" layer="1"/>
<smd name="2" x="0.8763" y="0" dx="0.8636" dy="1.3462" layer="1"/>
<wire x1="-0.127" y1="-0.8128" x2="0.127" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="0.127" y1="0.8128" x2="-0.127" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="-0.6604" x2="-0.4572" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.4572" y1="0.6604" x2="-1.1176" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1176" y1="-0.6604" x2="-0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="0.4318" y1="0.6858" x2="0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="0.4572" y1="-0.6604" x2="1.1176" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="-0.6604" x2="1.0922" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="1.0922" y1="0.6858" x2="0.4318" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-0.4572" y1="-0.6604" x2="0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="-0.6604" x2="1.1176" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="0.6604" x2="-0.4572" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1176" y1="0.6604" x2="-1.1176" y2="-0.6604" width="0.1524" layer="51"/>
<text x="-0.7112" y="1.0414" size="0.3048" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="UCLAMP3671P" urn="urn:adsk.eagle:footprint:2809110/1" locally_modified="yes">
<description>D</description>
<smd name="1" x="-0.55" y="0" dx="0.4" dy="1" layer="1"/>
<smd name="2" x="0.55" y="0" dx="0.4" dy="1" layer="1"/>
<wire x1="-0.8" y1="0.5" x2="0.8" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.5" x2="0.8" y2="-0.5" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.5" x2="-0.8" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.5" x2="-0.8" y2="0.5" width="0.127" layer="51"/>
<wire x1="-0.25" y1="0.5" x2="0.25" y2="0.5" width="0.1016" layer="21"/>
<wire x1="-0.25" y1="-0.5" x2="0.25" y2="-0.5" width="0.1016" layer="21"/>
<circle x="-0.65" y="-0.4" radius="0.05" width="0" layer="21"/>
<text x="-0.7618" y="0.7484" size="0.3048" layer="51">&gt;NAME</text>
</package>
<package name="TSSOP-28_FE/EB-M" urn="urn:adsk.eagle:footprint:2809071/1">
<smd name="1" x="-2.92608125" y="4.226559375" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="2" x="-2.92608125" y="3.57631875" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="3" x="-2.92608125" y="2.92608125" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="4" x="-2.92608125" y="2.275840625" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="5" x="-2.92608125" y="1.6256" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="6" x="-2.92608125" y="0.975359375" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="7" x="-2.92608125" y="0.32511875" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="8" x="-2.92608125" y="-0.32511875" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="9" x="-2.92608125" y="-0.975359375" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="10" x="-2.92608125" y="-1.6256" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="11" x="-2.92608125" y="-2.275840625" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="12" x="-2.92608125" y="-2.92608125" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="13" x="-2.92608125" y="-3.57631875" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="14" x="-2.92608125" y="-4.226559375" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="15" x="2.92608125" y="-4.226559375" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="16" x="2.92608125" y="-3.57631875" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="17" x="2.92608125" y="-2.92608125" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="18" x="2.92608125" y="-2.275840625" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="19" x="2.92608125" y="-1.6256" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="20" x="2.92608125" y="-0.975359375" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="21" x="2.92608125" y="-0.32511875" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="22" x="2.92608125" y="0.32511875" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="23" x="2.92608125" y="0.975359375" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="24" x="2.92608125" y="1.6256" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="25" x="2.92608125" y="2.275840625" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="26" x="2.92608125" y="2.92608125" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="27" x="2.92608125" y="3.57631875" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="28" x="2.92608125" y="4.226559375" dx="1.666240625" dy="0.4064" layer="1"/>
<smd name="EPAD" x="0" y="0" dx="2.7432" dy="4.7498" layer="1" cream="no"/>
<wire x1="-2.3876" y1="-5.0292" x2="2.3876" y2="-5.0292" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="-5.0292" x2="2.3876" y2="-4.7752" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="5.0292" x2="-2.3876" y2="5.0292" width="0.1524" layer="21"/>
<wire x1="-2.3876" y1="5.0292" x2="-2.3876" y2="4.7752" width="0.1524" layer="21"/>
<wire x1="-2.3876" y1="-4.7752" x2="-2.3876" y2="-5.0292" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="4.7752" x2="2.3876" y2="5.0292" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="4.064" x2="-2.2606" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-2.2606" y1="4.3688" x2="-3.2004" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="4.3688" x2="-3.2004" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="4.064" x2="-2.2352" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="3.429" x2="-2.2606" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="-2.2606" y1="3.7338" x2="-3.2004" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.7338" x2="-3.2004" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.429" x2="-2.2352" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.7686" x2="-2.2352" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="3.0734" x2="-3.2004" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.0734" x2="-3.2004" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.7686" x2="-2.2352" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.1336" x2="-2.2352" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.4384" x2="-3.2004" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.4384" x2="-3.2004" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.1336" x2="-2.2352" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.4732" x2="-2.2352" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.778" x2="-3.2004" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.778" x2="-3.2004" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.4732" x2="-2.2352" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.8128" x2="-2.2352" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.1176" x2="-3.2004" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.1176" x2="-3.2004" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.8128" x2="-2.2352" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.1778" x2="-2.2352" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.4826" x2="-3.2004" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.4826" x2="-3.2004" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.1778" x2="-2.2352" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.4826" x2="-2.2352" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.1778" x2="-3.2004" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.1778" x2="-3.2004" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.4826" x2="-2.2352" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.1176" x2="-2.2352" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.8128" x2="-3.2004" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.8128" x2="-3.2004" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.1176" x2="-2.2352" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.778" x2="-2.2352" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.4732" x2="-3.2004" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.4732" x2="-3.2004" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.778" x2="-2.2352" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.4384" x2="-2.2352" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.1336" x2="-3.2004" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.1336" x2="-3.2004" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.4384" x2="-2.2352" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.0734" x2="-2.2352" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.7686" x2="-3.2004" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.7686" x2="-3.2004" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.0734" x2="-2.2352" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.7338" x2="-2.2352" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.429" x2="-3.2004" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.429" x2="-3.2004" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.7338" x2="-2.2352" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.3688" x2="-2.2352" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.064" x2="-3.2004" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-4.064" x2="-3.2004" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-4.3688" x2="-2.2352" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-4.064" x2="2.2606" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="2.2606" y1="-4.3688" x2="3.2004" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-4.3688" x2="3.2004" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-4.064" x2="2.2352" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.429" x2="2.2352" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.7338" x2="3.2004" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.7338" x2="3.2004" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.429" x2="2.2352" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.7686" x2="2.2352" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.0734" x2="3.2004" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.0734" x2="3.2004" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.7686" x2="2.2352" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.1336" x2="2.2352" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.4384" x2="3.2004" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.4384" x2="3.2004" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.1336" x2="2.2352" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.4732" x2="2.2352" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.778" x2="3.2004" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.778" x2="3.2004" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.4732" x2="2.2352" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.8128" x2="2.2352" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.1176" x2="3.2004" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.1176" x2="3.2004" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.8128" x2="2.2352" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.1778" x2="2.2352" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.4826" x2="3.2004" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.4826" x2="3.2004" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.1778" x2="2.2352" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.4826" x2="2.2352" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.1778" x2="3.2004" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.1778" x2="3.2004" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.4826" x2="2.2352" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.1176" x2="2.2352" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.8128" x2="3.2004" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.8128" x2="3.2004" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.1176" x2="2.2352" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.778" x2="2.2352" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.4732" x2="3.2004" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.4732" x2="3.2004" y2="1.778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.778" x2="2.2352" y2="1.778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.4384" x2="2.2352" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.1336" x2="3.2004" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.1336" x2="3.2004" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.4384" x2="2.2352" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.0734" x2="2.2352" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.7686" x2="3.2004" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.7686" x2="3.2004" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.0734" x2="2.2352" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.7338" x2="2.2352" y2="3.429" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.429" x2="3.2004" y2="3.429" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.429" x2="3.2004" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.7338" x2="2.2352" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.3688" x2="2.2352" y2="4.064" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.064" x2="3.2004" y2="4.064" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="4.064" x2="3.2004" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="4.3688" x2="2.2352" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.9022" x2="2.2352" y2="-4.9022" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-4.9022" x2="2.2352" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.9022" x2="0.3048" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="4.9022" x2="-2.2352" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="4.9022" x2="-2.2352" y2="-4.9022" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0.1524" layer="51" curve="-180"/>
<polygon width="0.0254" layer="21">
<vertex x="-4.2672" y="-1.4351"/>
<vertex x="-4.2672" y="-1.8161"/>
<vertex x="-4.0132" y="-1.8161"/>
<vertex x="-4.0132" y="-1.4351"/>
</polygon>
<polygon width="0.0254" layer="21">
<vertex x="4.2672" y="-0.784859375"/>
<vertex x="4.2672" y="-1.165859375"/>
<vertex x="4.0132" y="-1.165859375"/>
<vertex x="4.0132" y="-0.784859375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="2.2749"/>
<vertex x="-1.2716" y="0.891634375"/>
<vertex x="-0.1" y="0.891634375"/>
<vertex x="-0.1" y="2.2749"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="0.691634375"/>
<vertex x="-1.2716" y="-0.691634375"/>
<vertex x="-0.1" y="-0.691634375"/>
<vertex x="-0.1" y="0.691634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="-0.891634375"/>
<vertex x="-1.2716" y="-2.2749"/>
<vertex x="-0.1" y="-2.2749"/>
<vertex x="-0.1" y="-0.891634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="2.2749"/>
<vertex x="0.1" y="0.891634375"/>
<vertex x="1.2716" y="0.891634375"/>
<vertex x="1.2716" y="2.2749"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="0.691634375"/>
<vertex x="0.1" y="-0.691634375"/>
<vertex x="1.2716" y="-0.691634375"/>
<vertex x="1.2716" y="0.691634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="-0.891634375"/>
<vertex x="0.1" y="-2.2749"/>
<vertex x="1.2716" y="-2.2749"/>
<vertex x="1.2716" y="-0.891634375"/>
</polygon>
<text x="-3.7592" y="4.4958" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-2.4384" y="3.556" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="TSSOP-28_FE/EB-L" urn="urn:adsk.eagle:footprint:2809072/1">
<smd name="1" x="-2.82448125" y="4.226559375" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="2" x="-2.82448125" y="3.57631875" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="3" x="-2.82448125" y="2.92608125" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="4" x="-2.82448125" y="2.275840625" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="5" x="-2.82448125" y="1.6256" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="6" x="-2.82448125" y="0.975359375" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="7" x="-2.82448125" y="0.32511875" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="8" x="-2.82448125" y="-0.32511875" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="9" x="-2.82448125" y="-0.975359375" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="10" x="-2.82448125" y="-1.6256" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="11" x="-2.82448125" y="-2.275840625" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="12" x="-2.82448125" y="-2.92608125" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="13" x="-2.82448125" y="-3.57631875" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="14" x="-2.82448125" y="-4.226559375" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="15" x="2.82448125" y="-4.226559375" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="16" x="2.82448125" y="-3.57631875" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="17" x="2.82448125" y="-2.92608125" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="18" x="2.82448125" y="-2.275840625" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="19" x="2.82448125" y="-1.6256" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="20" x="2.82448125" y="-0.975359375" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="21" x="2.82448125" y="-0.32511875" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="22" x="2.82448125" y="0.32511875" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="23" x="2.82448125" y="0.975359375" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="24" x="2.82448125" y="1.6256" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="25" x="2.82448125" y="2.275840625" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="26" x="2.82448125" y="2.92608125" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="27" x="2.82448125" y="3.57631875" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="28" x="2.82448125" y="4.226559375" dx="1.056640625" dy="0.3048" layer="1"/>
<smd name="EPAD" x="0" y="0" dx="2.7432" dy="4.7498" layer="1" cream="no"/>
<wire x1="-2.3876" y1="-5.0292" x2="2.3876" y2="-5.0292" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="-5.0292" x2="2.3876" y2="-4.7244" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="5.0292" x2="-2.3876" y2="5.0292" width="0.1524" layer="21"/>
<wire x1="-2.3876" y1="5.0292" x2="-2.3876" y2="4.7244" width="0.1524" layer="21"/>
<wire x1="-2.3876" y1="-4.7244" x2="-2.3876" y2="-5.0292" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="4.7244" x2="2.3876" y2="5.0292" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="4.064" x2="-2.2606" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-2.2606" y1="4.3688" x2="-3.2004" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="4.3688" x2="-3.2004" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="4.064" x2="-2.2352" y2="4.064" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="3.429" x2="-2.2606" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="-2.2606" y1="3.7338" x2="-3.2004" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.7338" x2="-3.2004" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.429" x2="-2.2352" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.7686" x2="-2.2352" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="3.0734" x2="-3.2004" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="3.0734" x2="-3.2004" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.7686" x2="-2.2352" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.1336" x2="-2.2352" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.4384" x2="-3.2004" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.4384" x2="-3.2004" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.1336" x2="-2.2352" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.4732" x2="-2.2352" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.778" x2="-3.2004" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.778" x2="-3.2004" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.4732" x2="-2.2352" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.8128" x2="-2.2352" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.1176" x2="-3.2004" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.1176" x2="-3.2004" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.8128" x2="-2.2352" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.1778" x2="-2.2352" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.4826" x2="-3.2004" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.4826" x2="-3.2004" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.1778" x2="-2.2352" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.4826" x2="-2.2352" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.1778" x2="-3.2004" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.1778" x2="-3.2004" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.4826" x2="-2.2352" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.1176" x2="-2.2352" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.8128" x2="-3.2004" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.8128" x2="-3.2004" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.1176" x2="-2.2352" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.778" x2="-2.2352" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.4732" x2="-3.2004" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.4732" x2="-3.2004" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.778" x2="-2.2352" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.4384" x2="-2.2352" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.1336" x2="-3.2004" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.1336" x2="-3.2004" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.4384" x2="-2.2352" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.0734" x2="-2.2352" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.7686" x2="-3.2004" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.7686" x2="-3.2004" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.0734" x2="-2.2352" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.7338" x2="-2.2352" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-3.429" x2="-3.2004" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.429" x2="-3.2004" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-3.7338" x2="-2.2352" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.3688" x2="-2.2352" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.064" x2="-3.2004" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-4.064" x2="-3.2004" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-4.3688" x2="-2.2352" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-4.064" x2="2.2606" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="2.2606" y1="-4.3688" x2="3.2004" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-4.3688" x2="3.2004" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-4.064" x2="2.2352" y2="-4.064" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.429" x2="2.2352" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.7338" x2="3.2004" y2="-3.7338" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.7338" x2="3.2004" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.429" x2="2.2352" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.7686" x2="2.2352" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-3.0734" x2="3.2004" y2="-3.0734" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-3.0734" x2="3.2004" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.7686" x2="2.2352" y2="-2.7686" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.1336" x2="2.2352" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.4384" x2="3.2004" y2="-2.4384" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.4384" x2="3.2004" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.1336" x2="2.2352" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.4732" x2="2.2352" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.778" x2="3.2004" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.778" x2="3.2004" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.4732" x2="2.2352" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.8128" x2="2.2352" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.1176" x2="3.2004" y2="-1.1176" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.1176" x2="3.2004" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.8128" x2="2.2352" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.1778" x2="2.2352" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.4826" x2="3.2004" y2="-0.4826" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.4826" x2="3.2004" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.1778" x2="2.2352" y2="-0.1778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.4826" x2="2.2352" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.1778" x2="3.2004" y2="0.1778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.1778" x2="3.2004" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.4826" x2="2.2352" y2="0.4826" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.1176" x2="2.2352" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.8128" x2="3.2004" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.8128" x2="3.2004" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.1176" x2="2.2352" y2="1.1176" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.778" x2="2.2352" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.4732" x2="3.2004" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.4732" x2="3.2004" y2="1.778" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.778" x2="2.2352" y2="1.778" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.4384" x2="2.2352" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.1336" x2="3.2004" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.1336" x2="3.2004" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.4384" x2="2.2352" y2="2.4384" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.0734" x2="2.2352" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.7686" x2="3.2004" y2="2.7686" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.7686" x2="3.2004" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.0734" x2="2.2352" y2="3.0734" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.7338" x2="2.2352" y2="3.429" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="3.429" x2="3.2004" y2="3.429" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.429" x2="3.2004" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="3.7338" x2="2.2352" y2="3.7338" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.3688" x2="2.2352" y2="4.064" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.064" x2="3.2004" y2="4.064" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="4.064" x2="3.2004" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="4.3688" x2="2.2352" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-4.9022" x2="2.2352" y2="-4.9022" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-4.9022" x2="2.2352" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="4.9022" x2="0.3048" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="4.9022" x2="-2.2352" y2="4.9022" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="4.9022" x2="-2.2352" y2="-4.9022" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="4.9022" x2="-0.3048" y2="4.9022" width="0.1524" layer="51" curve="-180"/>
<polygon width="0.0254" layer="21">
<vertex x="-3.8608" y="-1.4351"/>
<vertex x="-3.8608" y="-1.8161"/>
<vertex x="-3.6068" y="-1.8161"/>
<vertex x="-3.6068" y="-1.4351"/>
</polygon>
<polygon width="0.0254" layer="21">
<vertex x="3.8608" y="-0.784859375"/>
<vertex x="3.8608" y="-1.165859375"/>
<vertex x="3.6068" y="-1.165859375"/>
<vertex x="3.6068" y="-0.784859375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="2.2749"/>
<vertex x="-1.2716" y="0.891634375"/>
<vertex x="-0.1" y="0.891634375"/>
<vertex x="-0.1" y="2.2749"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="0.691634375"/>
<vertex x="-1.2716" y="-0.691634375"/>
<vertex x="-0.1" y="-0.691634375"/>
<vertex x="-0.1" y="0.691634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.2716" y="-0.891634375"/>
<vertex x="-1.2716" y="-2.2749"/>
<vertex x="-0.1" y="-2.2749"/>
<vertex x="-0.1" y="-0.891634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="2.2749"/>
<vertex x="0.1" y="0.891634375"/>
<vertex x="1.2716" y="0.891634375"/>
<vertex x="1.2716" y="2.2749"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="0.691634375"/>
<vertex x="0.1" y="-0.691634375"/>
<vertex x="1.2716" y="-0.691634375"/>
<vertex x="1.2716" y="0.691634375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="-0.891634375"/>
<vertex x="0.1" y="-2.2749"/>
<vertex x="1.2716" y="-2.2749"/>
<vertex x="1.2716" y="-0.891634375"/>
</polygon>
<text x="-3.6576" y="4.3942" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-2.4384" y="3.556" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="RES_ERA6A_0805-M" urn="urn:adsk.eagle:footprint:2809104/1">
<smd name="1" x="-0.9271" y="0" dx="0.9652" dy="1.4478" layer="1"/>
<smd name="2" x="0.9271" y="0" dx="0.9652" dy="1.4478" layer="1"/>
<wire x1="-0.127" y1="-0.8128" x2="0.127" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="0.127" y1="0.8128" x2="-0.127" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="-0.6604" x2="-0.4572" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.4572" y1="0.6604" x2="-1.1176" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1176" y1="-0.6604" x2="-0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="0.4318" y1="0.6858" x2="0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="0.4572" y1="-0.6604" x2="1.1176" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="-0.6604" x2="1.0922" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="1.0922" y1="0.6858" x2="0.4318" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-0.4572" y1="-0.6604" x2="0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="-0.6604" x2="1.1176" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="0.6604" x2="-0.4572" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1176" y1="0.6604" x2="-1.1176" y2="-0.6604" width="0.1524" layer="51"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="RES_ERA6A_0805-L" urn="urn:adsk.eagle:footprint:2809105/1">
<smd name="1" x="-0.8255" y="0" dx="0.762" dy="1.3462" layer="1"/>
<smd name="2" x="0.8255" y="0" dx="0.762" dy="1.3462" layer="1"/>
<wire x1="-0.127" y1="-0.8128" x2="0.127" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="0.127" y1="0.8128" x2="-0.127" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="-0.6604" x2="-0.4572" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.4572" y1="0.6604" x2="-1.1176" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1176" y1="-0.6604" x2="-0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="0.4318" y1="0.6858" x2="0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="0.4572" y1="-0.6604" x2="1.1176" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="-0.6604" x2="1.0922" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="1.0922" y1="0.6858" x2="0.4318" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-0.4572" y1="-0.6604" x2="0.4572" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="-0.6604" x2="1.1176" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="1.1176" y1="0.6604" x2="-0.4572" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1176" y1="0.6604" x2="-1.1176" y2="-0.6604" width="0.1524" layer="51"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="RC0603N-M" urn="urn:adsk.eagle:footprint:2809074/1">
<smd name="1" x="-0.8794" y="0" dx="0.9588" dy="0.901596875" layer="1"/>
<smd name="2" x="0.8794" y="0" dx="0.9588" dy="0.901596875" layer="1"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-0.4064" y1="-0.4064" x2="-0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.4064" y1="0.4064" x2="-0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="-0.4064" x2="-0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="0.4064" x2="0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="-0.4064" x2="0.7874" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.4064" x2="0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.4064" y1="-0.4064" x2="0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="-0.4064" x2="0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="0.4064" x2="-0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="0.4064" x2="-0.7874" y2="-0.4064" width="0.1524" layer="51"/>
</package>
<package name="RC0603N-L" urn="urn:adsk.eagle:footprint:2809075/1">
<smd name="1" x="-0.6762" y="0" dx="0.5524" dy="0.799996875" layer="1"/>
<smd name="2" x="0.6762" y="0" dx="0.5524" dy="0.799996875" layer="1"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-0.1016" y1="-0.5334" x2="0.1016" y2="-0.5334" width="0.1524" layer="21"/>
<wire x1="0.1016" y1="0.5334" x2="-0.1016" y2="0.5334" width="0.1524" layer="21"/>
<wire x1="-0.4064" y1="-0.4064" x2="-0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.4064" y1="0.4064" x2="-0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="-0.4064" x2="-0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="0.4064" x2="0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="-0.4064" x2="0.7874" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.4064" x2="0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.4064" y1="-0.4064" x2="0.4064" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="-0.4064" x2="0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.4064" y1="0.4064" x2="-0.4064" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="0.4064" x2="-0.7874" y2="-0.4064" width="0.1524" layer="51"/>
</package>
<package name="RC0805N-M" urn="urn:adsk.eagle:footprint:2809086/1">
<smd name="1" x="-0.954396875" y="0" dx="1.2088" dy="1.301596875" layer="1"/>
<smd name="2" x="0.954396875" y="0" dx="1.2088" dy="1.301596875" layer="1"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-0.3556" y1="-0.6096" x2="-0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="0.6096" x2="-0.9906" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.9906" y1="-0.6096" x2="-0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="0.6096" x2="0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="-0.6096" x2="0.9906" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9906" y1="0.6096" x2="0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="-0.6096" x2="0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9906" y1="-0.6096" x2="0.9906" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="0.6096" x2="-0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.9906" y1="0.6096" x2="-0.9906" y2="-0.6096" width="0.1524" layer="51"/>
</package>
<package name="RC0805N-L" urn="urn:adsk.eagle:footprint:2809087/1">
<smd name="1" x="-0.7512" y="0" dx="0.8024" dy="1.199996875" layer="1"/>
<smd name="2" x="0.7512" y="0" dx="0.8024" dy="1.199996875" layer="1"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-0.3556" y1="-0.6096" x2="-0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="0.6096" x2="-0.9906" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.9906" y1="-0.6096" x2="-0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="0.6096" x2="0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="-0.6096" x2="0.9906" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9906" y1="0.6096" x2="0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="-0.6096" x2="0.3556" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9906" y1="-0.6096" x2="0.9906" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="0.6096" x2="-0.3556" y2="0.6096" width="0.1524" layer="51"/>
<wire x1="-0.9906" y1="0.6096" x2="-0.9906" y2="-0.6096" width="0.1524" layer="51"/>
</package>
<package name="G-188-L" urn="urn:adsk.eagle:footprint:2809097/1">
<smd name="1" x="-0.5969" y="0" dx="0.6096" dy="0.8128" layer="1"/>
<smd name="2" x="0.5969" y="0" dx="0.6096" dy="0.8128" layer="1"/>
<wire x1="-1.6764" y1="0" x2="-1.8288" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.8288" y1="0" x2="-1.6764" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.3048" y1="-0.4064" x2="-0.3048" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="0.4064" x2="-0.8128" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.8128" y1="-0.4064" x2="-0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.2794" y1="0.4064" x2="0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="-0.4064" x2="0.8128" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="-0.4064" x2="0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.4064" x2="0.2794" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="-0.4064" x2="0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="-0.4064" x2="0.8128" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="0.4064" x2="-0.3048" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.8128" y1="0.4064" x2="-0.8128" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-0.4064" y1="0" x2="-0.5588" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-0.5588" y1="0" x2="-0.4064" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="CAP_1608-M" urn="urn:adsk.eagle:footprint:2809089/1">
<smd name="1" x="-0.9017" y="0" dx="0.508" dy="0.9906" layer="1"/>
<smd name="2" x="0.9017" y="0" dx="0.508" dy="0.9906" layer="1"/>
<wire x1="-0.3302" y1="-0.5588" x2="0.3302" y2="-0.5588" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.5588" x2="-0.3302" y2="0.5588" width="0.1524" layer="21"/>
<wire x1="-1.9304" y1="0" x2="-2.0828" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.0828" y1="0" x2="-1.9304" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.6604" y1="-0.4572" x2="-0.6604" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.6604" y1="0.4572" x2="-0.8636" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="-0.4572" x2="-0.6604" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0.4572" x2="0.6604" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="0.6604" y1="-0.4318" x2="0.8636" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="-0.4318" x2="0.8382" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="0.8382" y1="0.4572" x2="0.635" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.6604" y1="-0.4572" x2="0.8636" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="-0.4572" x2="0.8636" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="0.4572" x2="-0.6604" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="0.4572" x2="-0.8636" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="0" x2="-0.8636" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="-0.7112" y1="0" x2="-0.8636" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-0.8636" y1="0" x2="-0.7112" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="CAP_1608-L" urn="urn:adsk.eagle:footprint:2809090/1">
<smd name="1" x="-0.8001" y="0" dx="0.3048" dy="0.889" layer="1"/>
<smd name="2" x="0.8001" y="0" dx="0.3048" dy="0.889" layer="1"/>
<wire x1="-0.3302" y1="-0.5588" x2="0.3302" y2="-0.5588" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.5588" x2="-0.3302" y2="0.5588" width="0.1524" layer="21"/>
<wire x1="-1.7272" y1="0" x2="-1.8796" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.8796" y1="0" x2="-1.7272" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.6604" y1="-0.4572" x2="-0.6604" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.6604" y1="0.4572" x2="-0.8636" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="-0.4572" x2="-0.6604" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0.4572" x2="0.6604" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="0.6604" y1="-0.4318" x2="0.8636" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="-0.4318" x2="0.8382" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="0.8382" y1="0.4572" x2="0.635" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.6604" y1="-0.4572" x2="0.8636" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="-0.4572" x2="0.8636" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="0.4572" x2="-0.6604" y2="0.4572" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="0.4572" x2="-0.8636" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="-0.6096" y1="0" x2="-0.762" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-0.762" y1="0" x2="-0.6096" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="RU_14" urn="urn:adsk.eagle:footprint:2809063/1" locally_modified="yes">
<smd name="1" x="-2.8194" y="1.95" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="2" x="-2.8194" y="1.3" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="3" x="-2.8194" y="0.65" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="4" x="-2.8194" y="0" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="5" x="-2.8194" y="-0.65" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="6" x="-2.8194" y="-1.3" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="7" x="-2.8194" y="-1.95" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="8" x="2.8194" y="-1.95" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="9" x="2.8194" y="-1.3" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="10" x="2.8194" y="-0.65" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="11" x="2.8194" y="0" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="12" x="2.8194" y="0.65" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="13" x="2.8194" y="1.3" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="14" x="2.8194" y="1.95" dx="1.4732" dy="0.3556" layer="1"/>
<wire x1="-2.2352" y1="1.8034" x2="-2.2606" y2="2.1082" width="0.1524" layer="51"/>
<wire x1="-2.2606" y1="2.1082" x2="-3.2004" y2="2.1082" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="2.1082" x2="-3.2004" y2="1.8034" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.8034" x2="-2.2352" y2="1.8034" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="1.143" x2="-2.2606" y2="1.4478" width="0.1524" layer="51"/>
<wire x1="-2.2606" y1="1.4478" x2="-3.2004" y2="1.4478" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.4478" x2="-3.2004" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="1.143" x2="-2.2352" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.508" x2="-2.2352" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.8128" x2="-3.2004" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.8128" x2="-3.2004" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.508" x2="-2.2352" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.1524" x2="-2.2352" y2="0.1524" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0.1524" x2="-3.2004" y2="0.1524" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="0.1524" x2="-3.2004" y2="-0.1524" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.1524" x2="-2.2352" y2="-0.1524" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.8128" x2="-2.2352" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-0.508" x2="-3.2004" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.508" x2="-3.2004" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-0.8128" x2="-2.2352" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.4478" x2="-2.2352" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.143" x2="-3.2004" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.143" x2="-3.2004" y2="-1.4478" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.4478" x2="-2.2352" y2="-1.4478" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.1082" x2="-2.2352" y2="-1.8034" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-1.8034" x2="-3.2004" y2="-1.8034" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-1.8034" x2="-3.2004" y2="-2.1082" width="0.1524" layer="51"/>
<wire x1="-3.2004" y1="-2.1082" x2="-2.2352" y2="-2.1082" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.8034" x2="2.2606" y2="-2.1082" width="0.1524" layer="51"/>
<wire x1="2.2606" y1="-2.1082" x2="3.2004" y2="-2.1082" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-2.1082" x2="3.2004" y2="-1.8034" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.8034" x2="2.2352" y2="-1.8034" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.143" x2="2.2352" y2="-1.4478" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-1.4478" x2="3.2004" y2="-1.4478" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.4478" x2="3.2004" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-1.143" x2="2.2352" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.508" x2="2.2352" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.8128" x2="3.2004" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.8128" x2="3.2004" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.508" x2="2.2352" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.1524" x2="2.2352" y2="-0.1524" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.1524" x2="3.2004" y2="-0.1524" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="-0.1524" x2="3.2004" y2="0.1524" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.1524" x2="2.2352" y2="0.1524" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.8128" x2="2.2352" y2="0.508" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="0.508" x2="3.2004" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.508" x2="3.2004" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="0.8128" x2="2.2352" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.4478" x2="2.2352" y2="1.143" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.143" x2="3.2004" y2="1.143" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.143" x2="3.2004" y2="1.4478" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.4478" x2="2.2352" y2="1.4478" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.1082" x2="2.2352" y2="1.8034" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="1.8034" x2="3.2004" y2="1.8034" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="1.8034" x2="3.2004" y2="2.1082" width="0.1524" layer="51"/>
<wire x1="3.2004" y1="2.1082" x2="2.2352" y2="2.1082" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="-2.54" x2="2.2352" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-2.54" x2="2.2352" y2="2.54" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="2.54" x2="-0.3048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.54" x2="-2.2352" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="2.54" x2="-2.2352" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5654" x2="-0.3048" y2="2.54" width="0.1524" layer="51" curve="-180"/>
<wire x1="-2.3876" y1="-2.6924" x2="2.3876" y2="-2.6924" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="-2.6924" x2="2.3876" y2="-2.4638" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="2.6924" x2="-2.3876" y2="2.6924" width="0.1524" layer="21"/>
<wire x1="-2.3876" y1="2.6924" x2="-2.3876" y2="2.4638" width="0.1524" layer="21"/>
<wire x1="-2.3876" y1="-2.4638" x2="-2.3876" y2="-2.6924" width="0.1524" layer="21"/>
<wire x1="2.3876" y1="2.4638" x2="2.3876" y2="2.6924" width="0.1524" layer="21"/>
<text x="-2.4384" y="1.2192" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<polygon width="0.0254" layer="21">
<vertex x="4.064" y="-0.4595"/>
<vertex x="4.064" y="-0.8405"/>
<vertex x="3.81" y="-0.8405"/>
<vertex x="3.81" y="-0.4595"/>
</polygon>
<wire x1="0.3048" y1="2.6154" x2="-0.3048" y2="2.59" width="0.1524" layer="21" curve="-180"/>
</package>
<package name="C1">
<smd name="1" x="-1.4224" y="0" dx="1.27" dy="1.778" layer="1"/>
<smd name="2" x="1.4224" y="0" dx="1.27" dy="1.778" layer="1"/>
<wire x1="-0.4826" y1="-1.016" x2="0.4826" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.4826" y1="1.016" x2="-0.4826" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.8194" y1="0" x2="-2.9718" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.9718" y1="0" x2="-2.8194" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.7874" y1="-0.889" x2="-0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="0.889" x2="-1.7018" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="-0.889" x2="-0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.889" x2="0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="-0.889" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="0.889" x2="0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="-0.889" x2="0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.889" x2="-0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="0.889" x2="-1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.3716" y1="0" x2="-1.524" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.524" y1="0" x2="-1.3716" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="C1-M">
<smd name="1" x="-1.524" y="0" dx="1.4732" dy="1.8796" layer="1"/>
<smd name="2" x="1.524" y="0" dx="1.4732" dy="1.8796" layer="1"/>
<wire x1="-0.4572" y1="-1.016" x2="0.4572" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.4572" y1="1.016" x2="-0.4572" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.0226" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.175" y1="0" x2="-3.0226" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.7874" y1="-0.889" x2="-0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="0.889" x2="-1.7018" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="-0.889" x2="-0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.889" x2="0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="-0.889" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="0.889" x2="0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="-0.889" x2="0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.889" x2="-0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="0.889" x2="-1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.3716" y1="0" x2="-1.524" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.524" y1="0" x2="-1.3716" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="C1-L">
<smd name="1" x="-1.3208" y="0" dx="1.0668" dy="1.778" layer="1"/>
<smd name="2" x="1.3208" y="0" dx="1.0668" dy="1.778" layer="1"/>
<wire x1="-0.4826" y1="-1.016" x2="0.4826" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.4826" y1="1.016" x2="-0.4826" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.6162" y1="0" x2="-2.7686" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.7686" y1="0" x2="-2.6162" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.7874" y1="-0.889" x2="-0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="0.889" x2="-1.7018" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="-0.889" x2="-0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.889" x2="0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="-0.889" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="0.889" x2="0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="-0.889" x2="0.7874" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.889" x2="-0.7874" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="0.889" x2="-1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.3716" y1="0" x2="-1.524" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.524" y1="0" x2="-1.3716" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="SECUBE" urn="urn:adsk.eagle:footprint:2809060/1" locally_modified="yes">
<smd name="A1" x="0" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A2" x="0.5" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A3" x="1" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A4" x="1.5" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A5" x="2" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A6" x="2.5" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A7" x="3" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A8" x="3.5" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A9" x="4" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A10" x="4.5" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="A11" x="5" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="A12" x="5.5" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="A13" x="6" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="A14" x="6.5" y="0" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B1" x="0" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B2" x="0.5" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B3" x="1" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B4" x="1.5" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B5" x="2" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B6" x="2.5" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B7" x="3" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B8" x="3.5" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B9" x="4" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B10" x="4.5" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B11" x="5" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B12" x="5.5" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B13" x="6" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="B14" x="6.5" y="-0.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C1" x="0" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C2" x="0.5" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C3" x="1" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C4" x="1.5" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C5" x="2" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C6" x="2.5" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C7" x="3" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C8" x="3.5" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C9" x="4" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C10" x="4.5" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C11" x="5" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C12" x="5.5" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C13" x="6" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="C14" x="6.5" y="-1" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="D1" x="0" y="-1.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="D2" x="0.5" y="-1.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="D3" x="1" y="-1.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="D12" x="5.5" y="-1.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="D13" x="6" y="-1.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="D14" x="6.5" y="-1.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="E1" x="0" y="-2" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="E2" x="0.5" y="-2" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="E3" x="1" y="-2" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="E12" x="5.5" y="-2" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="E13" x="6" y="-2" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="E14" x="6.5" y="-2" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F1" x="0" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F2" x="0.5" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F3" x="1" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F6" x="2.5" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F7" x="3" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F8" x="3.5" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F9" x="4" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F12" x="5.5" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F13" x="6" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="F14" x="6.5" y="-2.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G1" x="0" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G2" x="0.5" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G3" x="1" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G6" x="2.5" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G7" x="3" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G8" x="3.5" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G9" x="4" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G12" x="5.5" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G13" x="6" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="G14" x="6.5" y="-3" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H1" x="0" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H2" x="0.5" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H3" x="1" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H6" x="2.5" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H7" x="3" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H8" x="3.5" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H9" x="4" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H12" x="5.5" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H13" x="6" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="H14" x="6.5" y="-3.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J1" x="0" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J2" x="0.5" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J3" x="1" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J6" x="2.5" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J7" x="3" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J8" x="3.5" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J9" x="4" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J12" x="5.5" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J13" x="6" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="J14" x="6.5" y="-4" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="K1" x="0" y="-4.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="K2" x="0.5" y="-4.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="K3" x="1" y="-4.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="K12" x="5.5" y="-4.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="K13" x="6" y="-4.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="K14" x="6.5" y="-4.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="L1" x="0" y="-5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="L2" x="0.5" y="-5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="L3" x="1" y="-5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="L12" x="5.5" y="-5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="L13" x="6" y="-5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="L14" x="6.5" y="-5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M1" x="0" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M2" x="0.5" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M3" x="1" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M4" x="1.5" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M5" x="2" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M6" x="2.5" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M7" x="3" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M8" x="3.5" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M9" x="4" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M10" x="4.5" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M11" x="5" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M12" x="5.5" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M13" x="6" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="M14" x="6.5" y="-5.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N1" x="0" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N2" x="0.5" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N3" x="1" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N4" x="1.5" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N5" x="2" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N6" x="2.5" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N7" x="3" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N8" x="3.5" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N9" x="4" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N10" x="4.5" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N11" x="5" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N12" x="5.5" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N13" x="6" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="N14" x="6.5" y="-6" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P1" x="0" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P2" x="0.5" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P3" x="1" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P4" x="1.5" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P5" x="2" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P6" x="2.5" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P7" x="3" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P8" x="3.5" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P9" x="4" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P10" x="4.5" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P11" x="5" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P12" x="5.5" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P13" x="6" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="P14" x="6.5" y="-6.5" dx="0.25" dy="0.25" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<wire x1="-1.25" y1="1.25" x2="7.75" y2="1.25" width="0.1524" layer="51"/>
<wire x1="7.75" y1="1.25" x2="7.75" y2="-7.75" width="0.1524" layer="51"/>
<wire x1="7.75" y1="-7.75" x2="-1.25" y2="-7.75" width="0.1524" layer="51"/>
<wire x1="-1.25" y1="-7.75" x2="-1.25" y2="1.25" width="0.1524" layer="51"/>
<wire x1="-1.25" y1="1.25" x2="7.75" y2="1.25" width="0.1524" layer="21"/>
<wire x1="7.75" y1="1.25" x2="7.75" y2="-7.75" width="0.1524" layer="21"/>
<wire x1="7.75" y1="-7.75" x2="-1.25" y2="-7.75" width="0.1524" layer="21"/>
<wire x1="-1.25" y1="-7.75" x2="-1.25" y2="1.25" width="0.1524" layer="21"/>
<text x="1.381" y="1.654" size="0.778" layer="25">&gt;NAME</text>
<text x="1.3" y="-8.95" size="0.778" layer="27">&gt;VALUE</text>
<circle x="-0.762" y="0.762" radius="0.1778" width="0" layer="21"/>
<circle x="0" y="0" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="0" radius="0.15" width="0" layer="29"/>
<circle x="1" y="0" radius="0.15" width="0" layer="29"/>
<circle x="1.5" y="0" radius="0.15" width="0" layer="29"/>
<circle x="2" y="0" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="0" radius="0.15" width="0" layer="29"/>
<circle x="3" y="0" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="0" radius="0.15" width="0" layer="29"/>
<circle x="4" y="0" radius="0.15" width="0" layer="29"/>
<circle x="4.5" y="0" radius="0.15" width="0" layer="29"/>
<circle x="5" y="0" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="0" radius="0.15" width="0" layer="29"/>
<circle x="6" y="0" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="0" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="1.5" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="2" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="4.5" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="5" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-0.5" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="1.5" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="2" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="4.5" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="5" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-1" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-1.5" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-1.5" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-1.5" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-1.5" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-1.5" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-1.5" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-2" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-2" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-2" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-2" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-2" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-2" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-2.5" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-3" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-3.5" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-4" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-4.5" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-4.5" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-4.5" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-4.5" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-4.5" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-4.5" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-5" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-5" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-5" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-5" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-5" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-5" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="1.5" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="2" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="4.5" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="5" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-5.5" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="1.5" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="2" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="4.5" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="5" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-6" radius="0.15" width="0" layer="29"/>
<circle x="0" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="0.5" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="1" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="1.5" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="2" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="2.5" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="3" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="3.5" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="4" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="4.5" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="5" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="5.5" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="6" y="-6.5" radius="0.15" width="0" layer="29"/>
<circle x="6.5" y="-6.5" radius="0.15" width="0" layer="29"/>
<text x="-1.95" y="0.5" size="0.4064" layer="21" rot="SR270">A B C D E F G H J K L M N P</text>
</package>
<package name="CB042D0684_PKG">
<wire x1="-2.9" y1="2.5" x2="-2.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2.9" y1="-2.5" x2="2.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.9" y1="-2.5" x2="2.9" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.9" y1="2.5" x2="-2.9" y2="2.5" width="0.127" layer="51"/>
<smd name="2" x="2.8" y="0" dx="1.2" dy="5" layer="1" rot="R180"/>
<smd name="1" x="-2.8" y="0" dx="1.2" dy="5" layer="1" rot="R180"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<circle x="-1.9" y="2.2" radius="0.1" width="0" layer="21"/>
<text x="-1.6" y="0.9" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.7" y="-0.9" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="U1-R">
<pad name="1" x="-5.0038" y="0" drill="0.6" diameter="1.3716" shape="square"/>
<pad name="2" x="0" y="0" drill="0.6" diameter="1.3716" rot="R180"/>
<wire x1="-9.0424" y1="0" x2="-7.7724" y2="0" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="0.635" x2="-8.382" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.6416" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-7.62" y1="0" x2="2.6416" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-9.0424" y1="0" x2="-7.7724" y2="0" width="0.1524" layer="51"/>
<wire x1="-8.382" y1="0.635" x2="-8.382" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="2.4892" y1="0" x2="-7.5184" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-7.5184" y1="0" x2="2.4892" y2="0" width="0" layer="51" curve="-180"/>
<text x="-5.7658" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="CAP_3216" urn="urn:adsk.eagle:footprint:2809106/1" locally_modified="yes">
<smd name="1" x="-1.7018" y="0" dx="0.4064" dy="1.8034" layer="1"/>
<smd name="2" x="1.7018" y="0" dx="0.4064" dy="1.8034" layer="1"/>
<wire x1="-1.4986" y1="0" x2="-1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="0" x2="-1.651" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.651" y1="0" x2="-1.4986" y2="0" width="0" layer="51" curve="-180"/>
<text x="-2.395" y="-0.6466" size="0.6096" layer="25" ratio="6" rot="SR90">&gt;Name</text>
<wire x1="-1.4986" y1="-0.9144" x2="-1.4986" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.9144" x2="1.4986" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="-0.889" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="0.9144" x2="1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="-0.9144" x2="-1.4986" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.9144" x2="-1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="0" x2="-1.651" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.651" y1="0" x2="-1.4986" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-2.13" y1="1.12" x2="-2.13" y2="-1.11" width="0.1524" layer="21"/>
<wire x1="-2.13" y1="-1.11" x2="2.11" y2="-1.11" width="0.1524" layer="21"/>
<wire x1="2.11" y1="-1.11" x2="2.11" y2="1.12" width="0.1524" layer="21"/>
<wire x1="2.11" y1="1.12" x2="-2.13" y2="1.12" width="0.1524" layer="21"/>
<wire x1="0" y1="0.75" x2="0" y2="-0.75" width="0.1524" layer="21"/>
</package>
<package name="CAP_3216-M" urn="urn:adsk.eagle:footprint:2809107/1" locally_modified="yes">
<smd name="1" x="-1.7526" y="0" dx="0.508" dy="1.905" layer="1"/>
<smd name="2" x="1.7526" y="0" dx="0.508" dy="1.905" layer="1"/>
<wire x1="-2.7686" y1="0" x2="-2.921" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.921" y1="0" x2="-2.7686" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.4986" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="0" x2="-1.7018" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="-1.5494" y1="0" x2="-1.7018" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.7018" y1="0" x2="-1.5494" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-1.1684" y1="-1.016" x2="1.1684" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.1684" y1="1.016" x2="-1.1684" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.7686" y1="0" x2="-2.921" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.921" y1="0" x2="-2.7686" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.4986" y1="-0.9144" x2="-1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.9144" x2="1.4986" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="-0.889" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="0.9144" x2="1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="-0.9144" x2="-1.4986" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.9144" x2="-1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.5494" y1="0" x2="-1.7018" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.7018" y1="0" x2="-1.5494" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="CAP_3216-L" urn="urn:adsk.eagle:footprint:2809108/1" locally_modified="yes">
<smd name="1" x="-1.651" y="0" dx="0.3048" dy="1.8034" layer="1"/>
<smd name="2" x="1.651" y="0" dx="0.3048" dy="1.8034" layer="1"/>
<wire x1="-2.5654" y1="0" x2="-2.7178" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.7178" y1="0" x2="-2.5654" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.4986" y1="-0.9144" x2="1.7018" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="-1.4478" y1="0" x2="-1.6002" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.6002" y1="0" x2="-1.4478" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-1.1938" y1="-1.016" x2="1.1938" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.1938" y1="1.016" x2="-1.1938" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.5654" y1="0" x2="-2.7178" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.7178" y1="0" x2="-2.5654" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.4986" y1="-0.9144" x2="-1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="0.9144" x2="-1.7018" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.9144" x2="1.4986" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="-0.889" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.889" x2="1.7018" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="0.9144" x2="1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="-0.9144" x2="-1.4986" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="1.7018" y1="-0.9144" x2="1.7018" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.9144" x2="-1.4986" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="-1.4478" y1="0" x2="-1.6002" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.6002" y1="0" x2="-1.4478" y2="0" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="G-188-M" urn="urn:adsk.eagle:footprint:2809096/1" locally_modified="yes">
<smd name="1" x="-0.6985" y="0" dx="0.8128" dy="0.9144" layer="1"/>
<smd name="2" x="0.6985" y="0" dx="0.8128" dy="0.9144" layer="1"/>
<wire x1="-0.3048" y1="-0.4064" x2="-0.3048" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="0.4064" x2="-0.8128" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.8128" y1="-0.4064" x2="-0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.2794" y1="0.4064" x2="0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="-0.4064" x2="0.8128" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="-0.4064" x2="0.7874" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.7874" y1="0.4064" x2="0.2794" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="-0.4064" x2="0.3048" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="-0.4064" x2="0.8128" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="0.8128" y1="0.4064" x2="-0.3048" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-0.8128" y1="0.4064" x2="-0.8128" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0" x2="-0.6604" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-0.6604" y1="0" x2="-0.508" y2="0" width="0" layer="51" curve="-180"/>
<text x="-0.9766" y="0.735" size="0.4064" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<wire x1="-1.25" y1="0.6" x2="1.25" y2="0.6" width="0.1524" layer="21"/>
<wire x1="1.25" y1="0.6" x2="1.25" y2="-0.6" width="0.1524" layer="21"/>
<wire x1="1.25" y1="-0.6" x2="-1.25" y2="-0.6" width="0.1524" layer="21"/>
<wire x1="-1.25" y1="-0.6" x2="-1.25" y2="0.6" width="0.1524" layer="21"/>
</package>
<package name="CON20_2X10_TU_TSW">
<pad name="1" x="11.43" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="2" x="11.43" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="3" x="8.89" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="4" x="8.89" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="5" x="6.35" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="6" x="6.35" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="9" x="1.27" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="10" x="1.27" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="11" x="-1.27" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="12" x="-1.27" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="13" x="-3.81" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="14" x="-3.81" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="15" x="-6.35" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="16" x="-6.35" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="17" x="-8.89" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="18" x="-8.89" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="19" x="-11.43" y="-1.27" drill="1.016" diameter="1.524" rot="R270"/>
<pad name="20" x="-11.43" y="1.27" drill="1.016" diameter="1.524" rot="R270"/>
<wire x1="-13.081" y1="-2.5908" x2="13.081" y2="-2.5908" width="0.1524" layer="21"/>
<wire x1="13.081" y1="-2.5908" x2="13.081" y2="2.6162" width="0.1524" layer="21"/>
<wire x1="13.081" y1="2.6162" x2="-13.081" y2="2.6162" width="0.1524" layer="21"/>
<wire x1="-13.081" y1="2.6162" x2="-13.081" y2="-2.5908" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-2.4638" x2="12.954" y2="-2.4638" width="0.1524" layer="51"/>
<wire x1="12.954" y1="-2.4638" x2="12.954" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="12.954" y1="2.4892" x2="-12.954" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-12.954" y1="2.4892" x2="-12.954" y2="-2.4638" width="0.1524" layer="51"/>
<wire x1="11.684" y1="-1.27" x2="11.176" y2="-1.27" width="0" layer="51" curve="-180"/>
<wire x1="11.176" y1="-1.27" x2="11.684" y2="-1.27" width="0" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="16_PINS">
<pad name="2" x="-17.78" y="-2.54" drill="1.016" diameter="1.524"/>
<pad name="4" x="-15.24" y="-2.54" drill="1.016" diameter="1.524"/>
<pad name="6" x="-12.7" y="-2.54" drill="1.016" diameter="1.524"/>
<pad name="8" x="-10.16" y="-2.54" drill="1.016" diameter="1.524"/>
<pad name="10" x="-7.62" y="-2.54" drill="1.016" diameter="1.524"/>
<pad name="12" x="-5.08" y="-2.54" drill="1.016" diameter="1.524"/>
<pad name="14" x="-2.54" y="-2.54" drill="1.016" diameter="1.524"/>
<pad name="16" x="0" y="-2.54" drill="1.016" diameter="1.524"/>
<pad name="1" x="-17.78" y="0" drill="1.016" diameter="1.524"/>
<pad name="3" x="-15.24" y="0" drill="1.016" diameter="1.524"/>
<pad name="5" x="-12.7" y="0" drill="1.016" diameter="1.524"/>
<pad name="7" x="-10.16" y="0" drill="1.016" diameter="1.524"/>
<pad name="9" x="-7.62" y="0" drill="1.016" diameter="1.524"/>
<pad name="11" x="-5.08" y="0" drill="1.016" diameter="1.524"/>
<pad name="13" x="-2.54" y="0" drill="1.016" diameter="1.524"/>
<pad name="15" x="0" y="0" drill="1.016" diameter="1.524"/>
<wire x1="-19.177" y1="-3.81" x2="1.397" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-3.81" x2="1.397" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.27" x2="-19.177" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-19.177" y1="1.27" x2="-19.177" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-19.05" y1="-3.683" x2="1.27" y2="-3.683" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-3.683" x2="1.27" y2="1.143" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.143" x2="-19.05" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-19.05" y1="1.143" x2="-19.05" y2="-3.683" width="0.1524" layer="51"/>
<wire x1="-17.399" y1="-4.445" x2="-18.161" y2="-4.445" width="0.508" layer="51" curve="-180"/>
<wire x1="-18.161" y1="-4.445" x2="-17.399" y2="-4.445" width="0.508" layer="51" curve="-180"/>
<text x="-12.1666" y="-1.905" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
</packages>
<packages3d>
<package3d name="DAC121S1_SOT" urn="urn:adsk.eagle:package:2809113/1" type="box">
<packageinstances>
<packageinstance name="DAC121S1_SOT"/>
</packageinstances>
</package3d>
<package3d name="TSSOP-28_FE/EB" urn="urn:adsk.eagle:package:2809121/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="TSSOP-28_FE/EB"/>
</packageinstances>
</package3d>
<package3d name="TSSOP-28_FE/EB-M" urn="urn:adsk.eagle:package:2809122/1" type="box">
<packageinstances>
<packageinstance name="TSSOP-28_FE/EB-M"/>
</packageinstances>
</package3d>
<package3d name="TSSOP-28_FE/EB-L" urn="urn:adsk.eagle:package:2809123/1" type="box">
<packageinstances>
<packageinstance name="TSSOP-28_FE/EB-L"/>
</packageinstances>
</package3d>
<package3d name="RES_ERA6A_0805" urn="urn:adsk.eagle:package:2809155/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="RES_ERA6A_0805"/>
</packageinstances>
</package3d>
<package3d name="RES_ERA6A_0805-M" urn="urn:adsk.eagle:package:2809156/1" type="box">
<packageinstances>
<packageinstance name="RES_ERA6A_0805-M"/>
</packageinstances>
</package3d>
<package3d name="RES_ERA6A_0805-L" urn="urn:adsk.eagle:package:2809157/1" type="box">
<packageinstances>
<packageinstance name="RES_ERA6A_0805-L"/>
</packageinstances>
</package3d>
<package3d name="RC0603N" urn="urn:adsk.eagle:package:2809124/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="RC0603N"/>
</packageinstances>
</package3d>
<package3d name="RC0603N-M" urn="urn:adsk.eagle:package:2809125/1" type="box">
<packageinstances>
<packageinstance name="RC0603N-M"/>
</packageinstances>
</package3d>
<package3d name="RC0603N-L" urn="urn:adsk.eagle:package:2809126/1" type="box">
<packageinstances>
<packageinstance name="RC0603N-L"/>
</packageinstances>
</package3d>
<package3d name="RC0805N" urn="urn:adsk.eagle:package:2809136/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="RC0805N"/>
</packageinstances>
</package3d>
<package3d name="RC0805N-M" urn="urn:adsk.eagle:package:2809137/1" type="box">
<packageinstances>
<packageinstance name="RC0805N-M"/>
</packageinstances>
</package3d>
<package3d name="RC0805N-L" urn="urn:adsk.eagle:package:2809138/1" type="box">
<packageinstances>
<packageinstance name="RC0805N-L"/>
</packageinstances>
</package3d>
<package3d name="G-188" urn="urn:adsk.eagle:package:2809146/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="G-188"/>
</packageinstances>
</package3d>
<package3d name="G-188-L" urn="urn:adsk.eagle:package:2809148/1" type="box">
<packageinstances>
<packageinstance name="G-188-L"/>
</packageinstances>
</package3d>
<package3d name="CAP_1608" urn="urn:adsk.eagle:package:2809139/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="CAP_1608"/>
</packageinstances>
</package3d>
<package3d name="CAP_1608-M" urn="urn:adsk.eagle:package:2809140/1" type="box">
<packageinstances>
<packageinstance name="CAP_1608-M"/>
</packageinstances>
</package3d>
<package3d name="CAP_1608-L" urn="urn:adsk.eagle:package:2809141/1" type="box">
<packageinstances>
<packageinstance name="CAP_1608-L"/>
</packageinstances>
</package3d>
<package3d name="SRN4026" urn="urn:adsk.eagle:package:2809145/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="SRN4026"/>
</packageinstances>
</package3d>
<package3d name="IND_BOURNS_SRR6038" urn="urn:adsk.eagle:package:2809154/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="IND_BOURNS_SRR6038"/>
</packageinstances>
</package3d>
<package3d name="UCLAMP3671P" urn="urn:adsk.eagle:package:2809162/1" locally_modified="yes" type="box">
<description>D</description>
<packageinstances>
<packageinstance name="UCLAMP3671P"/>
</packageinstances>
</package3d>
<package3d name="RU_14" urn="urn:adsk.eagle:package:2809114/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="RU_14"/>
</packageinstances>
</package3d>
<package3d name="SECUBE" urn="urn:adsk.eagle:package:2809111/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="SECUBE"/>
</packageinstances>
</package3d>
<package3d name="CAP_3216" urn="urn:adsk.eagle:package:2809158/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="CAP_3216"/>
</packageinstances>
</package3d>
<package3d name="CAP_3216-M" urn="urn:adsk.eagle:package:2809159/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="CAP_3216-M"/>
</packageinstances>
</package3d>
<package3d name="CAP_3216-L" urn="urn:adsk.eagle:package:2809160/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="CAP_3216-L"/>
</packageinstances>
</package3d>
<package3d name="G-188-M" urn="urn:adsk.eagle:package:2809147/1" locally_modified="yes" type="box">
<packageinstances>
<packageinstance name="G-188-M"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DAC121S1_SYM">
<pin name="VOUT" x="-15.24" y="5.08" length="middle"/>
<pin name="GND" x="-15.24" y="0" length="middle"/>
<pin name="VA" x="-15.24" y="-5.08" length="middle"/>
<pin name="DIN" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="SCLK" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="!SYNC!" x="15.24" y="5.08" length="middle" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.1524" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.1524" layer="94"/>
<text x="-10.16" y="-15.24" size="1.778" layer="94">&gt;NAME</text>
</symbol>
<symbol name="LT8616EFEPBF_A">
<pin name="NC_2" x="-17.78" y="2.54" length="middle" direction="nc"/>
<pin name="NC_3" x="-17.78" y="0" length="middle" direction="nc"/>
<pin name="NC_4" x="-17.78" y="-2.54" length="middle" direction="nc"/>
<pin name="NC_5" x="17.78" y="-2.54" length="middle" direction="nc" rot="R180"/>
<pin name="NC" x="17.78" y="0" length="middle" direction="nc" rot="R180"/>
<pin name="GND" x="17.78" y="2.54" length="middle" direction="pwr" rot="R180"/>
<wire x1="-12.7" y1="7.62" x2="-12.7" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="12.7" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="7.62" width="0.1524" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-12.7" y2="7.62" width="0.1524" layer="94"/>
<text x="-9.8044" y="11.6586" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
</symbol>
<symbol name="RES">
<pin name="22" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="11" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-4.2164" y="1.8288" size="1.6764" layer="95" ratio="10" rot="SR0">&gt;Name</text>
<text x="-3.81" y="-3.302" size="1.4224" layer="96">&gt;VALUE</text>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="94"/>
</symbol>
<symbol name="CAPH">
<pin name="22" x="3.2512" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="-3.2258" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-0.6858" y1="-1.905" x2="-0.6858" y2="1.905" width="0.2032" layer="94"/>
<wire x1="0.6858" y1="-1.905" x2="0.6858" y2="1.905" width="0.2032" layer="94"/>
<text x="-4.2364" y="2.7528" size="1.27" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-4.65" y="-3.73" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="INDUCTOR">
<description>&lt;h3&gt;Inductors&lt;/h3&gt;
&lt;p&gt;Resist changes in electrical current. Basically a coil of wire.&lt;/p&gt;</description>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.1524" layer="94" curve="-180"/>
<text x="-5.08" y="2.54" size="1.4224" layer="95">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.4224" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="IND">
<pin name="1" x="15.24" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="0" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="5.08" y1="0.02" x2="7.62" y2="0.02" width="0.1524" layer="94" curve="-180"/>
<wire x1="2.54" y1="0.02" x2="5.08" y2="0.02" width="0.1524" layer="94" curve="-180"/>
<wire x1="7.62" y1="0.02" x2="10.16" y2="0.02" width="0.1524" layer="94" curve="-180"/>
<wire x1="10.16" y1="0.02" x2="12.7" y2="0.02" width="0.1524" layer="94" curve="-180"/>
<text x="1.5688" y="-2.4672" size="1.4224" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="1.5356" y="2.3528" size="1.4224" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
<symbol name="COHO-B-SIZE">
<wire x1="0" y1="0" x2="0" y2="279.4" width="0.254" layer="94"/>
<wire x1="0" y1="279.4" x2="431.8" y2="279.4" width="0.254" layer="94"/>
<wire x1="431.8" y1="279.4" x2="431.8" y2="0" width="0.254" layer="94"/>
<wire x1="431.8" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document</text>
<text x="20.32" y="7.62" size="3.81" layer="95"></text>
<text x="1.27" y="7.62" size="2.54" layer="94">Number:</text>
<text x="4.3942" y="25.9842" size="6.4516" layer="94" ratio="10">Blu5 Labs Ltd.</text>
</symbol>
<symbol name="DIODE-TVS">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="0" y1="1.524" x2="0" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.524" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="1.524" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.524" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.524" x2="1.778" y2="1.524" width="0.1524" layer="94"/>
<text x="0" y="4.064" size="1.4224" layer="96" ratio="10">&gt;VALUE</text>
<text x="0" y="2.54" size="1.4224" layer="95">&gt;NAME</text>
</symbol>
<symbol name="AD5252BRUZ1-RL7_EDITED">
<pin name="VDD" x="-17.78" y="12.7" length="middle"/>
<pin name="VSS" x="-17.78" y="10.16" length="middle"/>
<pin name="DGND" x="-17.78" y="7.62" length="middle"/>
<pin name="!WP!" x="-17.78" y="5.08" length="middle"/>
<pin name="SDA" x="-17.78" y="-5.08" length="middle"/>
<pin name="SCL" x="-17.78" y="-2.54" length="middle"/>
<pin name="AD0" x="-17.78" y="-10.16" length="middle"/>
<pin name="AD1" x="-17.78" y="-12.7" length="middle"/>
<pin name="A1" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="W1" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="B1" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="A3" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="W3" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="B3" x="17.78" y="-10.16" length="middle" rot="R180"/>
<wire x1="-12.7" y1="-15.24" x2="12.7" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="15.24" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="12.7" y2="15.24" width="0.1524" layer="94"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="-15.24" width="0.1524" layer="94"/>
<text x="-12.7" y="-20.32" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="FUSE">
<pin name="1" x="0" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="10.16" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="7.62" y1="0" x2="5.08" y2="0" width="0.1524" layer="94" curve="-180"/>
<text x="-0.3302" y="-3.1496" size="1.4224" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="-0.2794" y="2.1844" size="1.4224" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
<symbol name="SECUBE_PWR">
<pin name="CPU_VDD@0" x="30.48" y="50.8" length="middle" rot="R180"/>
<pin name="CPU_VDD@2" x="30.48" y="45.72" length="middle" rot="R180"/>
<pin name="CPU_VDD@3" x="30.48" y="43.18" length="middle" rot="R180"/>
<pin name="CPU_VDD@4" x="30.48" y="40.64" length="middle" rot="R180"/>
<pin name="CPU_VDD@5" x="30.48" y="38.1" length="middle" rot="R180"/>
<pin name="CPU_VDD@6" x="30.48" y="35.56" length="middle" rot="R180"/>
<pin name="CPU_VDD@7" x="30.48" y="33.02" length="middle" rot="R180"/>
<pin name="CPU_VDD@8" x="30.48" y="30.48" length="middle" rot="R180"/>
<pin name="CPU_VDD@9" x="30.48" y="27.94" length="middle" rot="R180"/>
<pin name="CPU_VDD@1" x="30.48" y="48.26" length="middle" rot="R180"/>
<pin name="CPU_VCAP1" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="CPU_VCAP2" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="CPU_RSTN" x="30.48" y="5.08" length="middle" rot="R180"/>
<pin name="CPU_WKUP" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="VSS@1" x="-30.48" y="-35.56" length="middle"/>
<pin name="VSS@21" x="30.48" y="-33.02" length="middle" rot="R180"/>
<pin name="VSS@20" x="30.48" y="-35.56" length="middle" rot="R180"/>
<pin name="VSS@19" x="30.48" y="-38.1" length="middle" rot="R180"/>
<pin name="VSS@18" x="30.48" y="-40.64" length="middle" rot="R180"/>
<pin name="VSS@17" x="30.48" y="-43.18" length="middle" rot="R180"/>
<pin name="VSS@16" x="30.48" y="-45.72" length="middle" rot="R180"/>
<pin name="VSS@15" x="30.48" y="-48.26" length="middle" rot="R180"/>
<pin name="VSS@14" x="30.48" y="-50.8" length="middle" rot="R180"/>
<pin name="VSS@13" x="30.48" y="-53.34" length="middle" rot="R180"/>
<pin name="VSS@12" x="30.48" y="-55.88" length="middle" rot="R180"/>
<pin name="FPGA_VCORE@0" x="-30.48" y="50.8" length="middle"/>
<pin name="FPGA_VCORE@1" x="-30.48" y="48.26" length="middle"/>
<pin name="FPGA_VCORE@2" x="-30.48" y="45.72" length="middle"/>
<pin name="FPGA_VCORE@3" x="-30.48" y="43.18" length="middle"/>
<pin name="FPGA_VCORE@4" x="-30.48" y="40.64" length="middle"/>
<pin name="FPGA_VCORE@5" x="-30.48" y="38.1" length="middle"/>
<pin name="FPGA_VCORE@6" x="-30.48" y="35.56" length="middle"/>
<pin name="FPGA_VCORE@7" x="-30.48" y="33.02" length="middle"/>
<pin name="FPGA_VCORE@8" x="-30.48" y="30.48" length="middle"/>
<pin name="FPGA_VCORE@9" x="-30.48" y="27.94" length="middle"/>
<pin name="FPGA_VCCIO0@0" x="-30.48" y="22.86" length="middle"/>
<pin name="FPGA_VCCIO0@1" x="-30.48" y="20.32" length="middle"/>
<pin name="FPGA_VCCIO1@0" x="-30.48" y="12.7" length="middle"/>
<pin name="FPGA_VCCIO1@1" x="-30.48" y="10.16" length="middle"/>
<pin name="FPGA_VCCIO3" x="-30.48" y="-7.62" length="middle"/>
<pin name="FPGA_VCCIO4" x="-30.48" y="-12.7" length="middle"/>
<pin name="VSS@0" x="-30.48" y="-33.02" length="middle"/>
<pin name="VSS@2" x="-30.48" y="-38.1" length="middle"/>
<pin name="VSS@3" x="-30.48" y="-40.64" length="middle"/>
<pin name="VSS@4" x="-30.48" y="-43.18" length="middle"/>
<pin name="VSS@5" x="-30.48" y="-45.72" length="middle"/>
<pin name="VSS@6" x="-30.48" y="-48.26" length="middle"/>
<pin name="VSS@7" x="-30.48" y="-50.8" length="middle"/>
<pin name="VSS@8" x="-30.48" y="-53.34" length="middle"/>
<pin name="VSS@9" x="-30.48" y="-55.88" length="middle"/>
<pin name="VSS@10" x="-30.48" y="-58.42" length="middle"/>
<pin name="VSS@11" x="30.48" y="-58.42" length="middle" rot="R180"/>
<pin name="FPGA_VCCIO0@2" x="-30.48" y="17.78" length="middle"/>
<pin name="FPGA_VCCIO1@2" x="-30.48" y="7.62" length="middle"/>
<pin name="FPGA_VCCIO2@0" x="-30.48" y="2.54" length="middle"/>
<pin name="FPGA_VCCIO2@1" x="-30.48" y="0" length="middle"/>
<pin name="FPGA_VCCIO2@2" x="-30.48" y="-2.54" length="middle"/>
<pin name="FPGA_VCCIO5" x="-30.48" y="-17.78" length="middle"/>
<pin name="CPU_SC_PWR" x="-30.48" y="-25.4" length="middle"/>
<pin name="SC_VCC" x="30.48" y="-25.4" length="middle" rot="R180"/>
<wire x1="-25.4" y1="55.88" x2="25.4" y2="55.88" width="0.254" layer="94"/>
<wire x1="25.4" y1="55.88" x2="25.4" y2="-63.5" width="0.254" layer="94"/>
<wire x1="25.4" y1="-63.5" x2="-25.4" y2="-63.5" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-63.5" x2="-25.4" y2="55.88" width="0.254" layer="94"/>
<text x="-25.4" y="-67.31" size="1.778" layer="95">&gt;NAME</text>
<text x="-25.4" y="-69.85" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SECUBE_SIGNAL_1">
<pin name="PA3" x="-20.32" y="30.48" length="middle"/>
<pin name="PA5" x="-20.32" y="22.86" length="middle"/>
<pin name="PA10" x="-20.32" y="17.78" length="middle"/>
<pin name="PA11" x="-20.32" y="15.24" length="middle"/>
<pin name="PA12" x="-20.32" y="12.7" length="middle"/>
<pin name="PA13" x="-20.32" y="10.16" length="middle"/>
<pin name="PA14" x="-20.32" y="7.62" length="middle"/>
<pin name="PA15" x="-20.32" y="5.08" length="middle"/>
<pin name="PB0" x="-20.32" y="-10.16" length="middle"/>
<pin name="PB1" x="-20.32" y="-12.7" length="middle"/>
<pin name="PB3" x="-20.32" y="-15.24" length="middle"/>
<pin name="PB4" x="-20.32" y="-17.78" length="middle"/>
<pin name="PB5" x="-20.32" y="-20.32" length="middle"/>
<pin name="PB6" x="-20.32" y="-22.86" length="middle"/>
<pin name="PB10" x="-20.32" y="-27.94" length="middle"/>
<pin name="PB11" x="-20.32" y="-30.48" length="middle"/>
<pin name="PB12" x="-20.32" y="-33.02" length="middle"/>
<pin name="PB13" x="-20.32" y="-35.56" length="middle"/>
<pin name="PB14" x="-20.32" y="-38.1" length="middle"/>
<pin name="PB15" x="-20.32" y="-40.64" length="middle"/>
<pin name="PC0" x="20.32" y="30.48" length="middle" rot="R180"/>
<pin name="PC3" x="20.32" y="25.4" length="middle" rot="R180"/>
<pin name="PC10" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="PC11" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="PC12" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="PC9" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="PC8" x="20.32" y="22.86" length="middle" rot="R180"/>
<pin name="PC2" x="20.32" y="27.94" length="middle" rot="R180"/>
<pin name="PD2" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="PD12" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="PF6" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="PF7" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="PF8" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="PF9" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="PF10" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="PG3" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="PH0" x="20.32" y="-25.4" length="middle" rot="R180"/>
<pin name="PH1" x="20.32" y="-27.94" length="middle" rot="R180"/>
<pin name="PH4" x="20.32" y="-33.02" length="middle" rot="R180"/>
<pin name="PH5" x="20.32" y="-35.56" length="middle" rot="R180"/>
<pin name="PI11" x="20.32" y="-40.64" length="middle" rot="R180"/>
<text x="-15.24" y="-48.26" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="-50.8" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-15.24" y1="35.56" x2="-15.24" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-45.72" x2="15.24" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-45.72" x2="15.24" y2="35.56" width="0.1524" layer="94"/>
<wire x1="15.24" y1="35.56" x2="-15.24" y2="35.56" width="0.1524" layer="94"/>
</symbol>
<symbol name="SECUBE_FPGA">
<pin name="FPGA_IO_D0" x="30.48" y="58.42" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D1" x="30.48" y="55.88" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D2" x="30.48" y="53.34" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D3" x="30.48" y="50.8" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D4" x="30.48" y="48.26" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D5" x="30.48" y="45.72" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D6" x="30.48" y="43.18" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D7" x="30.48" y="40.64" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D8" x="30.48" y="38.1" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D9" x="30.48" y="35.56" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D10" x="30.48" y="33.02" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D11" x="30.48" y="30.48" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D12" x="30.48" y="27.94" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D13" x="30.48" y="25.4" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D14" x="30.48" y="22.86" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_D15" x="30.48" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_INT_N" x="30.48" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_PCLK" x="30.48" y="12.7" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL0" x="30.48" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL1" x="30.48" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL2" x="30.48" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL3" x="30.48" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL4" x="30.48" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL5" x="30.48" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL6" x="30.48" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL7" x="30.48" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL8" x="30.48" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL9" x="30.48" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL10" x="30.48" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL11" x="30.48" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_CTRL12" x="30.48" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP0" x="30.48" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP1" x="30.48" y="-30.48" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP2" x="30.48" y="-33.02" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP3" x="30.48" y="-35.56" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP4" x="30.48" y="-38.1" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP5" x="30.48" y="-40.64" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP6" x="30.48" y="-43.18" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP7" x="30.48" y="-45.72" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP8" x="30.48" y="-48.26" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP9" x="30.48" y="-50.8" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP10" x="30.48" y="-53.34" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP11" x="30.48" y="-55.88" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP12" x="30.48" y="-58.42" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP13" x="30.48" y="-60.96" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP14" x="30.48" y="-63.5" length="middle" direction="pas" rot="R180"/>
<pin name="FPGA_IO_GP15" x="30.48" y="-66.04" length="middle" direction="pas" rot="R180"/>
<wire x1="25.4" y1="63.5" x2="25.4" y2="-71.12" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-71.12" x2="0" y2="-71.12" width="0.1524" layer="94"/>
<wire x1="0" y1="-71.12" x2="0" y2="63.5" width="0.1524" layer="94"/>
<wire x1="0" y1="63.5" x2="25.4" y2="63.5" width="0.1524" layer="94"/>
<text x="0" y="-73.66" size="2.0828" layer="95" ratio="6">&gt;NAME</text>
<text x="0" y="-76.2" size="2.0828" layer="96" ratio="6">&gt;VALUE</text>
</symbol>
<symbol name="LT8616EFEPBF">
<pin name="BOOST1" x="25.4" y="15.24" length="middle" direction="in" rot="R180"/>
<pin name="BOOST2" x="25.4" y="-38.1" length="middle" direction="in" rot="R180"/>
<pin name="VIN1" x="-25.4" y="20.32" length="middle" direction="in"/>
<pin name="VIN2" x="-25.4" y="7.62" length="middle" direction="in"/>
<pin name="EN/UV1" x="-25.4" y="15.24" length="middle" direction="pas"/>
<pin name="EN/UV2" x="-25.4" y="2.54" length="middle" direction="pas"/>
<pin name="FB1@1" x="25.4" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="FB1@0" x="25.4" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="FB2@0" x="25.4" y="-58.42" length="middle" direction="pas" rot="R180"/>
<pin name="FB2@1" x="25.4" y="-60.96" length="middle" direction="pas" rot="R180"/>
<pin name="INTVCC" x="-25.4" y="-58.42" length="middle" direction="pas"/>
<pin name="RT" x="-25.4" y="-66.04" length="middle" direction="pas"/>
<pin name="BIAS" x="25.4" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="PG1" x="-25.4" y="-2.54" length="middle" direction="out"/>
<pin name="PG2" x="-25.4" y="-5.08" length="middle" direction="out"/>
<pin name="SW1@1" x="25.4" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="SWI@0" x="25.4" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="SW2@1" x="25.4" y="-45.72" length="middle" direction="out" rot="R180"/>
<pin name="SW2@2" x="25.4" y="-48.26" length="middle" direction="out" rot="R180"/>
<pin name="SW2@0" x="25.4" y="-43.18" length="middle" direction="out" rot="R180"/>
<pin name="SYNC/MODE" x="-25.4" y="-45.72" length="middle" direction="out"/>
<pin name="TR/SS1" x="-25.4" y="-50.8" length="middle" direction="out"/>
<pin name="TR/SS2" x="-25.4" y="-53.34" length="middle" direction="out"/>
<wire x1="-20.32" y1="27.94" x2="-20.32" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="-73.66" x2="20.32" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-73.66" x2="20.32" y2="27.94" width="0.1524" layer="94"/>
<wire x1="20.32" y1="27.94" x2="-20.32" y2="27.94" width="0.1524" layer="94"/>
<text x="-5.3594" y="24.3586" size="2.0828" layer="97" ratio="6" rot="SR0">1 of 2</text>
<text x="-4.7244" y="31.9786" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-5.3594" y="29.4386" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="1.2_VDD">
<text x="-2.794" y="2.794" size="1.4224" layer="95">+1.2V</text>
<pin name="1.2V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="-2.54" y1="2.54" x2="2.286" y2="2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="CON20_2X10_TU_TSW">
<pin name="1" x="22.86" y="0" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="0" y="0" visible="pin" length="middle" direction="pas"/>
<pin name="3" x="22.86" y="-2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="4" x="0" y="-2.54" visible="pin" length="middle" direction="pas"/>
<pin name="5" x="22.86" y="-5.08" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="6" x="0" y="-5.08" visible="pin" length="middle" direction="pas"/>
<pin name="7" x="22.86" y="-7.62" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="8" x="0" y="-7.62" visible="pin" length="middle" direction="pas"/>
<pin name="9" x="22.86" y="-10.16" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="10" x="0" y="-10.16" visible="pin" length="middle" direction="pas"/>
<pin name="11" x="22.86" y="-12.7" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="12" x="0" y="-12.7" visible="pin" length="middle" direction="pas"/>
<pin name="13" x="22.86" y="-15.24" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="14" x="0" y="-15.24" visible="pin" length="middle" direction="pas"/>
<pin name="15" x="22.86" y="-17.78" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="16" x="0" y="-17.78" visible="pin" length="middle" direction="pas"/>
<pin name="17" x="22.86" y="-20.32" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="18" x="0" y="-20.32" visible="pin" length="middle" direction="pas"/>
<pin name="19" x="22.86" y="-22.86" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="20" x="0" y="-22.86" visible="pin" length="middle" direction="pas"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-25.4" x2="17.78" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="17.78" y1="-25.4" x2="17.78" y2="2.54" width="0.1524" layer="94"/>
<wire x1="17.78" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<text x="4.1656" y="5.3086" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
</symbol>
<symbol name="16_PINS">
<pin name="1" x="2.54" y="0" visible="pin" length="middle" direction="pas"/>
<pin name="3" x="2.54" y="-2.54" visible="pin" length="middle" direction="pas"/>
<pin name="5" x="2.54" y="-5.08" visible="pin" length="middle" direction="pas"/>
<pin name="7" x="2.54" y="-7.62" visible="pin" length="middle" direction="pas"/>
<pin name="9" x="2.54" y="-10.16" visible="pin" length="middle" direction="pas"/>
<pin name="11" x="2.54" y="-12.7" visible="pin" length="middle" direction="pas"/>
<pin name="13" x="2.54" y="-15.24" visible="pin" length="middle" direction="pas"/>
<pin name="15" x="2.54" y="-17.78" visible="pin" length="middle" direction="pas"/>
<pin name="16" x="30.48" y="-17.78" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="14" x="30.48" y="-15.24" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="12" x="30.48" y="-12.7" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="10" x="30.48" y="-10.16" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="8" x="30.48" y="-7.62" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="6" x="30.48" y="-5.08" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="4" x="30.48" y="-2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="30.48" y="0" visible="pin" length="middle" direction="pas" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-22.86" x2="25.4" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-22.86" x2="25.4" y2="5.08" width="0.1524" layer="94"/>
<wire x1="25.4" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="10.5156" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="9.8806" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DAC121S1_DEV" prefix="U">
<gates>
<gate name="A" symbol="DAC121S1_SYM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DAC121S1_SOT">
<connects>
<connect gate="A" pin="!SYNC!" pad="6"/>
<connect gate="A" pin="DIN" pad="4"/>
<connect gate="A" pin="GND" pad="2"/>
<connect gate="A" pin="SCLK" pad="5"/>
<connect gate="A" pin="VA" pad="3"/>
<connect gate="A" pin="VOUT" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809113/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LT8616EFEPBF" prefix="U">
<gates>
<gate name="A" symbol="LT8616EFEPBF" x="0" y="0"/>
<gate name="B" symbol="LT8616EFEPBF_A" x="69.596" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP-28_FE/EB">
<connects>
<connect gate="A" pin="BIAS" pad="22"/>
<connect gate="A" pin="BOOST1" pad="8"/>
<connect gate="A" pin="BOOST2" pad="6"/>
<connect gate="A" pin="EN/UV1" pad="16"/>
<connect gate="A" pin="EN/UV2" pad="1"/>
<connect gate="A" pin="FB1@0" pad="14"/>
<connect gate="A" pin="FB1@1" pad="13"/>
<connect gate="A" pin="FB2@0" pad="26"/>
<connect gate="A" pin="FB2@1" pad="27"/>
<connect gate="A" pin="INTVCC" pad="21"/>
<connect gate="A" pin="PG1" pad="11"/>
<connect gate="A" pin="PG2" pad="2"/>
<connect gate="A" pin="RT" pad="15"/>
<connect gate="A" pin="SW1@1" pad="9"/>
<connect gate="A" pin="SW2@0" pad="5"/>
<connect gate="A" pin="SW2@1" pad="3"/>
<connect gate="A" pin="SW2@2" pad="4"/>
<connect gate="A" pin="SWI@0" pad="10"/>
<connect gate="A" pin="SYNC/MODE" pad="17"/>
<connect gate="A" pin="TR/SS1" pad="12"/>
<connect gate="A" pin="TR/SS2" pad="28"/>
<connect gate="A" pin="VIN1" pad="19"/>
<connect gate="A" pin="VIN2" pad="24"/>
<connect gate="B" pin="GND" pad="29"/>
<connect gate="B" pin="NC" pad="25"/>
<connect gate="B" pin="NC_2" pad="7"/>
<connect gate="B" pin="NC_3" pad="18"/>
<connect gate="B" pin="NC_4" pad="20"/>
<connect gate="B" pin="NC_5" pad="23"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809121/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="LT8616EFE#PBF" constant="no"/>
<attribute name="VENDOR" value="Linear Technology" constant="no"/>
</technology>
</technologies>
</device>
<device name="TSSOP-28_FE/EB-M" package="TSSOP-28_FE/EB-M">
<connects>
<connect gate="A" pin="BIAS" pad="22"/>
<connect gate="A" pin="BOOST1" pad="8"/>
<connect gate="A" pin="BOOST2" pad="6"/>
<connect gate="A" pin="EN/UV1" pad="16"/>
<connect gate="A" pin="EN/UV2" pad="1"/>
<connect gate="A" pin="FB1@0" pad="14"/>
<connect gate="A" pin="FB1@1" pad="13"/>
<connect gate="A" pin="FB2@0" pad="26"/>
<connect gate="A" pin="FB2@1" pad="27"/>
<connect gate="A" pin="INTVCC" pad="21"/>
<connect gate="A" pin="PG1" pad="11"/>
<connect gate="A" pin="PG2" pad="2"/>
<connect gate="A" pin="RT" pad="15"/>
<connect gate="A" pin="SW1@1" pad="9"/>
<connect gate="A" pin="SW2@0" pad="5"/>
<connect gate="A" pin="SW2@1" pad="3"/>
<connect gate="A" pin="SW2@2" pad="4"/>
<connect gate="A" pin="SWI@0" pad="10"/>
<connect gate="A" pin="SYNC/MODE" pad="17"/>
<connect gate="A" pin="TR/SS1" pad="12"/>
<connect gate="A" pin="TR/SS2" pad="28"/>
<connect gate="A" pin="VIN1" pad="19"/>
<connect gate="A" pin="VIN2" pad="24"/>
<connect gate="B" pin="NC" pad="25"/>
<connect gate="B" pin="NC_2" pad="7"/>
<connect gate="B" pin="NC_3" pad="18"/>
<connect gate="B" pin="NC_4" pad="20"/>
<connect gate="B" pin="NC_5" pad="23"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809122/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="LT8616EFE#PBF" constant="no"/>
<attribute name="VENDOR" value="Linear Technology" constant="no"/>
</technology>
</technologies>
</device>
<device name="TSSOP-28_FE/EB-L" package="TSSOP-28_FE/EB-L">
<connects>
<connect gate="A" pin="BIAS" pad="22"/>
<connect gate="A" pin="BOOST1" pad="8"/>
<connect gate="A" pin="BOOST2" pad="6"/>
<connect gate="A" pin="EN/UV1" pad="16"/>
<connect gate="A" pin="EN/UV2" pad="1"/>
<connect gate="A" pin="FB1@0" pad="14"/>
<connect gate="A" pin="FB1@1" pad="13"/>
<connect gate="A" pin="FB2@0" pad="26"/>
<connect gate="A" pin="FB2@1" pad="27"/>
<connect gate="A" pin="INTVCC" pad="21"/>
<connect gate="A" pin="PG1" pad="11"/>
<connect gate="A" pin="PG2" pad="2"/>
<connect gate="A" pin="RT" pad="15"/>
<connect gate="A" pin="SW1@1" pad="9"/>
<connect gate="A" pin="SW2@0" pad="5"/>
<connect gate="A" pin="SW2@1" pad="3"/>
<connect gate="A" pin="SW2@2" pad="4"/>
<connect gate="A" pin="SWI@0" pad="10"/>
<connect gate="A" pin="SYNC/MODE" pad="17"/>
<connect gate="A" pin="TR/SS1" pad="12"/>
<connect gate="A" pin="TR/SS2" pad="28"/>
<connect gate="A" pin="VIN1" pad="19"/>
<connect gate="A" pin="VIN2" pad="24"/>
<connect gate="B" pin="NC" pad="25"/>
<connect gate="B" pin="NC_2" pad="7"/>
<connect gate="B" pin="NC_3" pad="18"/>
<connect gate="B" pin="NC_4" pad="20"/>
<connect gate="B" pin="NC_5" pad="23"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809123/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="LT8616EFE#PBF" constant="no"/>
<attribute name="VENDOR" value="Linear Technology" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ERA-3AEB473V" prefix="R">
<gates>
<gate name="A" symbol="RES" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="RC0603N">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809124/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="VALUE" value="47k" constant="no"/>
</technology>
</technologies>
</device>
<device name="RC0603N-M" package="RC0603N-M">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809125/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA3AEB473V" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
<device name="RC0603N-L" package="RC0603N-L">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809126/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA3AEB473V" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ERA-6AEB105V" prefix="R">
<gates>
<gate name="A" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RC0805N">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809136/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA6AEB105V" constant="no"/>
<attribute name="VALUE" value="1M" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
<device name="RC0805N-M" package="RC0805N-M">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809137/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA6AEB105V" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
<device name="RC0805N-L" package="RC0805N-L">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809138/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA6AEB105V" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ERJ-3EKF2003V" prefix="R">
<gates>
<gate name="A" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RC0603N">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809124/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERJ3EKF2003V" constant="no"/>
<attribute name="VALUE" value="200k" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
<device name="RC0603N-M" package="RC0603N-M">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809125/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERJ3EKF2003V" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
<device name="RC0603N-L" package="RC0603N-L">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809126/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERJ3EKF2003V" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CGA3E3X5R1H105K080AB" prefix="C">
<gates>
<gate name="A" symbol="CAPH" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="CAP_1608">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809139/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="CGA3E3X5R1H105K080AB" constant="no"/>
<attribute name="VALUE" value="1uF" constant="no"/>
<attribute name="VENDOR" value="TDK" constant="no"/>
</technology>
</technologies>
</device>
<device name="CAP_1608-M" package="CAP_1608-M">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809140/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="CGA3E3X5R1H105K080AB" constant="no"/>
<attribute name="VENDOR" value="TDK" constant="no"/>
</technology>
</technologies>
</device>
<device name="CAP_1608-L" package="CAP_1608-L">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809141/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="CGA3E3X5R1H105K080AB" constant="no"/>
<attribute name="VALUE" value="1uF" constant="no"/>
<attribute name="VENDOR" value="TDK" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SRN4026" prefix="L">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SRN4026">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809145/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="VALUE" value="3.3uH" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRCW06031M91FKEA" prefix="R">
<gates>
<gate name="A" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RC0603N">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809124/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="CRCW06031M91FKEA" constant="no"/>
<attribute name="VALUE" value="1.9M" constant="no"/>
<attribute name="VENDOR" value="Vishay" constant="no"/>
</technology>
</technologies>
</device>
<device name="RC0603N-M" package="RC0603N-M">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809125/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="CRCW06031M91FKEA" constant="no"/>
<attribute name="VENDOR" value="Vishay" constant="no"/>
</technology>
</technologies>
</device>
<device name="RC0603N-L" package="RC0603N-L">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809126/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="CRCW06031M91FKEA" constant="no"/>
<attribute name="VENDOR" value="Vishay" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GRM1885C1H100JA01D" prefix="C">
<gates>
<gate name="A" symbol="CAPH" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="G-188">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809146/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="GRM1885C1H100JA01D" constant="no"/>
<attribute name="VALUE" value="10pF" constant="no"/>
<attribute name="VENDOR" value="Murata" constant="no"/>
</technology>
</technologies>
</device>
<device name="G-188-M" package="G-188-M">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809147/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="GRM1885C1H100JA01D" constant="no"/>
<attribute name="VENDOR" value="Murata" constant="no"/>
</technology>
</technologies>
</device>
<device name="G-188-L" package="G-188-L">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809148/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="GRM1885C1H100JA01D" constant="no"/>
<attribute name="VALUE" value="10pF" constant="no"/>
<attribute name="VENDOR" value="Murata" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SRR6038-100Y" prefix="L">
<gates>
<gate name="A" symbol="IND" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="IND_BOURNS_SRR6038">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809154/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="SRR6038100Y" constant="no"/>
<attribute name="VALUE" value="10uH" constant="no"/>
<attribute name="VENDOR" value="Bourns Electronics" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ERA-6AEB1873V" prefix="R">
<gates>
<gate name="A" symbol="RES" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="RES_ERA6A_0805">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809155/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA6AEB1873V" constant="no"/>
<attribute name="VALUE" value="187K" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
<device name="RES_ERA6A_0805-M" package="RES_ERA6A_0805-M">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809156/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA6AEB1873V" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
<device name="RES_ERA6A_0805-L" package="RES_ERA6A_0805-L">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809157/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA6AEB1873V" constant="no"/>
<attribute name="VENDOR" value="Panasonic" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BORDER_TITLE" prefix="FD">
<gates>
<gate name="G$1" symbol="COHO-B-SIZE" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="330.2" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UCLAMP3671P" prefix="D">
<gates>
<gate name="G$1" symbol="DIODE-TVS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UCLAMP3671P">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809162/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AD5252BRUZ10-RL7" prefix="U">
<gates>
<gate name="A" symbol="AD5252BRUZ1-RL7_EDITED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RU_14">
<connects>
<connect gate="A" pin="!WP!" pad="3"/>
<connect gate="A" pin="A1" pad="6"/>
<connect gate="A" pin="A3" pad="12"/>
<connect gate="A" pin="AD0" pad="2"/>
<connect gate="A" pin="AD1" pad="11"/>
<connect gate="A" pin="B1" pad="5"/>
<connect gate="A" pin="B3" pad="13"/>
<connect gate="A" pin="DGND" pad="10"/>
<connect gate="A" pin="SCL" pad="9"/>
<connect gate="A" pin="SDA" pad="7"/>
<connect gate="A" pin="VDD" pad="1"/>
<connect gate="A" pin="VSS" pad="8"/>
<connect gate="A" pin="W1" pad="4"/>
<connect gate="A" pin="W3" pad="14"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809114/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="VALUE" value="AD5252BRUZ10-RL7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C1Q_1" prefix="F">
<gates>
<gate name="A" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="C1Q 1" constant="no"/>
<attribute name="VENDOR" value="BelFuse" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1-M" package="C1-M">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="C1Q 1" constant="no"/>
<attribute name="VENDOR" value="BelFuse" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1-L" package="C1-L">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="C1Q 1" constant="no"/>
<attribute name="VENDOR" value="BelFuse" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SECUBE_DEV" prefix="U" uservalue="yes">
<gates>
<gate name="SECUBE_PWR" symbol="SECUBE_PWR" x="0" y="0"/>
<gate name="SECUBE_SIGNAL" symbol="SECUBE_SIGNAL_1" x="101.6" y="0"/>
<gate name="SECUBE_FPGA" symbol="SECUBE_FPGA" x="-109.22" y="0"/>
</gates>
<devices>
<device name="" package="SECUBE">
<connects>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL0" pad="B3"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL1" pad="L1"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL10" pad="P11"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL11" pad="P12"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL12" pad="P13"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL2" pad="A3"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL3" pad="M1"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL4" pad="A2"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL5" pad="N2"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL6" pad="P4"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL7" pad="N4"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL8" pad="P9"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_CTRL9" pad="P10"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D0" pad="E14"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D1" pad="H14"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D10" pad="B2"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D11" pad="B6"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D12" pad="A1"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D13" pad="B5"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D14" pad="A5"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D15" pad="F1"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D2" pad="J13"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D3" pad="J14"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D4" pad="B12"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D5" pad="A12"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D6" pad="A8"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D7" pad="A6"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D8" pad="B4"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_D9" pad="A4"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP0" pad="A7"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP1" pad="B7"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP10" pad="D14"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP11" pad="F14"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP12" pad="G14"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP13" pad="H13"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP14" pad="B13"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP15" pad="A13"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP2" pad="B8"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP3" pad="C6"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP4" pad="B9"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP5" pad="A10"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP6" pad="A9"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP7" pad="B11"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP8" pad="B10"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_GP9" pad="A11"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_INT_N" pad="D1"/>
<connect gate="SECUBE_FPGA" pin="FPGA_IO_PCLK" pad="E1"/>
<connect gate="SECUBE_PWR" pin="CPU_RSTN" pad="M8"/>
<connect gate="SECUBE_PWR" pin="CPU_SC_PWR" pad="L2"/>
<connect gate="SECUBE_PWR" pin="CPU_VCAP1" pad="F12"/>
<connect gate="SECUBE_PWR" pin="CPU_VCAP2" pad="C2"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@0" pad="C3"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@1" pad="C4"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@2" pad="C11"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@3" pad="C12"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@4" pad="H12"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@5" pad="J2"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@6" pad="J3"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@7" pad="J12"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@8" pad="M4"/>
<connect gate="SECUBE_PWR" pin="CPU_VDD@9" pad="M11"/>
<connect gate="SECUBE_PWR" pin="CPU_WKUP" pad="M10"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO0@0" pad="C1"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO0@1" pad="G1"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO0@2" pad="P1"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO1@0" pad="A14"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO1@1" pad="B1"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO1@2" pad="C10"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO2@0" pad="C14"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO2@1" pad="K14"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO2@2" pad="M14"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO3" pad="P14"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO4" pad="P7"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCCIO5" pad="P3"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@0" pad="B14"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@1" pad="C13"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@2" pad="D2"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@3" pad="D3"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@4" pad="K1"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@5" pad="K2"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@6" pad="L13"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@7" pad="L14"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@8" pad="N6"/>
<connect gate="SECUBE_PWR" pin="FPGA_VCORE@9" pad="P6"/>
<connect gate="SECUBE_PWR" pin="SC_VCC" pad="N1"/>
<connect gate="SECUBE_PWR" pin="VSS@0" pad="F6"/>
<connect gate="SECUBE_PWR" pin="VSS@1" pad="F7"/>
<connect gate="SECUBE_PWR" pin="VSS@10" pad="H8"/>
<connect gate="SECUBE_PWR" pin="VSS@11" pad="H9"/>
<connect gate="SECUBE_PWR" pin="VSS@12" pad="J6"/>
<connect gate="SECUBE_PWR" pin="VSS@13" pad="J7"/>
<connect gate="SECUBE_PWR" pin="VSS@14" pad="J8"/>
<connect gate="SECUBE_PWR" pin="VSS@15" pad="J9"/>
<connect gate="SECUBE_PWR" pin="VSS@16" pad="M3"/>
<connect gate="SECUBE_PWR" pin="VSS@17" pad="M12"/>
<connect gate="SECUBE_PWR" pin="VSS@18" pad="M13"/>
<connect gate="SECUBE_PWR" pin="VSS@19" pad="N3"/>
<connect gate="SECUBE_PWR" pin="VSS@2" pad="F8"/>
<connect gate="SECUBE_PWR" pin="VSS@20" pad="N14"/>
<connect gate="SECUBE_PWR" pin="VSS@21" pad="P2"/>
<connect gate="SECUBE_PWR" pin="VSS@3" pad="F9"/>
<connect gate="SECUBE_PWR" pin="VSS@4" pad="G6"/>
<connect gate="SECUBE_PWR" pin="VSS@5" pad="G7"/>
<connect gate="SECUBE_PWR" pin="VSS@6" pad="G8"/>
<connect gate="SECUBE_PWR" pin="VSS@7" pad="G9"/>
<connect gate="SECUBE_PWR" pin="VSS@8" pad="H6"/>
<connect gate="SECUBE_PWR" pin="VSS@9" pad="H7"/>
<connect gate="SECUBE_SIGNAL" pin="PA10" pad="C5"/>
<connect gate="SECUBE_SIGNAL" pin="PA11" pad="F3"/>
<connect gate="SECUBE_SIGNAL" pin="PA12" pad="E2"/>
<connect gate="SECUBE_SIGNAL" pin="PA13" pad="E3"/>
<connect gate="SECUBE_SIGNAL" pin="PA14" pad="G2"/>
<connect gate="SECUBE_SIGNAL" pin="PA15" pad="F2"/>
<connect gate="SECUBE_SIGNAL" pin="PA3" pad="N13"/>
<connect gate="SECUBE_SIGNAL" pin="PA5" pad="L12"/>
<connect gate="SECUBE_SIGNAL" pin="PB0" pad="K13"/>
<connect gate="SECUBE_SIGNAL" pin="PB1" pad="K12"/>
<connect gate="SECUBE_SIGNAL" pin="PB10" pad="G13"/>
<connect gate="SECUBE_SIGNAL" pin="PB11" pad="F13"/>
<connect gate="SECUBE_SIGNAL" pin="PB12" pad="E13"/>
<connect gate="SECUBE_SIGNAL" pin="PB13" pad="E12"/>
<connect gate="SECUBE_SIGNAL" pin="PB14" pad="D12"/>
<connect gate="SECUBE_SIGNAL" pin="PB15" pad="G12"/>
<connect gate="SECUBE_SIGNAL" pin="PB3" pad="K3"/>
<connect gate="SECUBE_SIGNAL" pin="PB4" pad="M2"/>
<connect gate="SECUBE_SIGNAL" pin="PB5" pad="J1"/>
<connect gate="SECUBE_SIGNAL" pin="PB6" pad="L3"/>
<connect gate="SECUBE_SIGNAL" pin="PC0" pad="N10"/>
<connect gate="SECUBE_SIGNAL" pin="PC10" pad="H2"/>
<connect gate="SECUBE_SIGNAL" pin="PC11" pad="G3"/>
<connect gate="SECUBE_SIGNAL" pin="PC12" pad="H3"/>
<connect gate="SECUBE_SIGNAL" pin="PC2" pad="M9"/>
<connect gate="SECUBE_SIGNAL" pin="PC3" pad="N8"/>
<connect gate="SECUBE_SIGNAL" pin="PC8" pad="C8"/>
<connect gate="SECUBE_SIGNAL" pin="PC9" pad="C7"/>
<connect gate="SECUBE_SIGNAL" pin="PD12" pad="D13"/>
<connect gate="SECUBE_SIGNAL" pin="PD2" pad="H1"/>
<connect gate="SECUBE_SIGNAL" pin="PF10" pad="N9"/>
<connect gate="SECUBE_SIGNAL" pin="PF6" pad="N7"/>
<connect gate="SECUBE_SIGNAL" pin="PF7" pad="M5"/>
<connect gate="SECUBE_SIGNAL" pin="PF8" pad="P8"/>
<connect gate="SECUBE_SIGNAL" pin="PF9" pad="M7"/>
<connect gate="SECUBE_SIGNAL" pin="PG3" pad="C9"/>
<connect gate="SECUBE_SIGNAL" pin="PH0" pad="M6"/>
<connect gate="SECUBE_SIGNAL" pin="PH1" pad="N5"/>
<connect gate="SECUBE_SIGNAL" pin="PH4" pad="N11"/>
<connect gate="SECUBE_SIGNAL" pin="PH5" pad="N12"/>
<connect gate="SECUBE_SIGNAL" pin="PI11" pad="P5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809111/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1.2_VDD" prefix="FD">
<gates>
<gate name="G$1" symbol="1.2_VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CB042D0684JBC" prefix="C" uservalue="yes">
<description>Datasheet: 

http://datasheets.avx.com/cb-petht.pdf</description>
<gates>
<gate name="G$1" symbol="CAPH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CB042D0684_PKG">
<connects>
<connect gate="G$1" pin="11" pad="1"/>
<connect gate="G$1" pin="22" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="VALUE" value="POLY,0.68UF,5%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UES1C101MPM1TD" prefix="C" uservalue="yes">
<gates>
<gate name="A" symbol="CAPH" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="U1-R">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HEIGHT" value="14.00mm" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="UES1C101MPM1TD" constant="no"/>
<attribute name="VALUE" value="100uF, BIPOLAR ELEC, 16V" constant="no"/>
<attribute name="VENDOR" value="Nichicon" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C3216JB1V226M160AC" prefix="C">
<gates>
<gate name="A" symbol="CAPH" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="CAP_3216">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809158/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="C3216JB1V226M160AC" constant="no"/>
<attribute name="VALUE" value="22uF,35V" constant="no"/>
<attribute name="VENDOR" value="TDK" constant="no"/>
</technology>
</technologies>
</device>
<device name="CAP_3216-M" package="CAP_3216-M">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809159/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="C3216JB1V226M160AC" constant="no"/>
<attribute name="VENDOR" value="TDK" constant="no"/>
</technology>
</technologies>
</device>
<device name="CAP_3216-L" package="CAP_3216-L">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2809160/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="C3216JB1V226M160AC" constant="no"/>
<attribute name="VENDOR" value="TDK" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TSW-110-14-F-D" prefix="J">
<gates>
<gate name="A" symbol="CON20_2X10_TU_TSW" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CON20_2X10_TU_TSW">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="TSW11014FD" constant="no"/>
<attribute name="VENDOR" value="Samtec Inc" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="67997-116HLF" prefix="J">
<gates>
<gate name="A" symbol="16_PINS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="16_PINS">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="67997116HLF" constant="no"/>
<attribute name="VENDOR" value="Amphenol FCI" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="EEE-FK1H221P">
<description>&lt;Aluminum Electrolytic Capacitors - SMD 220UF 50V FK SMD&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="FKV(G)">
<description>&lt;b&gt;FKV(G)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-4.65" y="0" dx="4.3" dy="2.1" layer="1"/>
<smd name="2" x="4.65" y="0" dx="4.3" dy="2.1" layer="1"/>
<text x="-0.3054" y="2.6148" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.3054" y="-2.4652" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-5.15" y1="5.15" x2="5.15" y2="5.15" width="0.254" layer="51"/>
<wire x1="5.15" y1="5.15" x2="5.15" y2="-5.15" width="0.254" layer="51"/>
<wire x1="5.15" y1="-5.15" x2="-5.15" y2="-5.15" width="0.254" layer="51"/>
<wire x1="-5.15" y1="-5.15" x2="-5.15" y2="5.15" width="0.254" layer="51"/>
<wire x1="-5.15" y1="5.15" x2="5.15" y2="5.15" width="0.254" layer="21"/>
<wire x1="5.15" y1="-5.15" x2="-5.15" y2="-5.15" width="0.254" layer="21"/>
<wire x1="-5.15" y1="5.15" x2="-5.15" y2="1.4" width="0.254" layer="21"/>
<wire x1="-5.15" y1="-5.15" x2="-5.15" y2="-1.4" width="0.254" layer="21"/>
<wire x1="5.15" y1="-5.15" x2="5.15" y2="-1.4" width="0.254" layer="21"/>
<wire x1="5.15" y1="5.15" x2="5.15" y2="1.4" width="0.254" layer="21"/>
<circle x="-7.894" y="0.126" radius="0.09168125" width="0.254" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="EEE-FK1H221P">
<wire x1="5.588" y1="2.54" x2="5.588" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.588" y2="0" width="0.254" layer="94"/>
<wire x1="7.112" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="12.7" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EEE-FK1H221P" prefix="C">
<description>&lt;b&gt;Aluminum Electrolytic Capacitors - SMD 220UF 50V FK SMD&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.mouser.com/ds/2/315/ABA0000C1181-947564.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="EEE-FK1H221P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FKV(G)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70256552" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="https://www.alliedelec.com/panasonic-eee-fk1h221p/70256552/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="EEE-FK1H221P" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="Aluminum Electrolytic Capacitors - SMD 220UF 50V FK SMD" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="EEE-FK1H221P" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
<attribute name="VALUE" value="220uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BLM21PG221SN1D">
<description>&lt;Ferrite bead 0805 SMD 220R 2A Ferrite bead 0805 SMD 220R 2A&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="BEADC2012X105N">
<description>&lt;b&gt;805&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.9" y="0" dx="1.45" dy="1.15" layer="1" rot="R90"/>
<smd name="2" x="0.9" y="0" dx="1.45" dy="1.15" layer="1" rot="R90"/>
<text x="-0.04" y="1.45" size="0.4064" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.725" y1="1" x2="1.725" y2="1" width="0.05" layer="51"/>
<wire x1="1.725" y1="1" x2="1.725" y2="-1" width="0.05" layer="51"/>
<wire x1="1.725" y1="-1" x2="-1.725" y2="-1" width="0.05" layer="51"/>
<wire x1="-1.725" y1="-1" x2="-1.725" y2="1" width="0.05" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.1" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.1" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.1" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.1" layer="51"/>
<wire x1="0" y1="0.525" x2="0" y2="-0.525" width="0.2" layer="21"/>
<wire x1="-1.7" y1="1" x2="-1.66" y2="1" width="0.1524" layer="21"/>
<wire x1="-1.66" y1="1" x2="1.74" y2="1" width="0.1524" layer="21"/>
<wire x1="1.74" y1="1" x2="1.74" y2="-1" width="0.1524" layer="21"/>
<wire x1="1.74" y1="-1" x2="-1.72" y2="-1" width="0.1524" layer="21"/>
<wire x1="-1.72" y1="-1" x2="-1.72" y2="1" width="0.1524" layer="21"/>
<wire x1="-1.72" y1="1" x2="-1.7" y2="1" width="0.1524" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="BLM21PG221SN1D">
<wire x1="-1.27" y1="0.508" x2="1.016" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.508" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<text x="-3.81" y="4.318" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-4.064" y="2.032" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short"/>
<pin name="2" x="3.556" y="0" visible="off" length="short" rot="R180"/>
<text x="-0.3302" y="-0.127" size="0.3048" layer="97">FB</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BLM21PG221SN1D" prefix="FB">
<description>&lt;b&gt;Ferrite bead 0805 SMD 220R 2A Ferrite bead 0805 SMD 220R 2A&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://docs-asia.electrocomponents.com/webdocs/1388/0900766b813885e8.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="BLM21PG221SN1D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BEADC2012X105N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="BLM21PG221SN1D" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/blm21pg221sn1d/murata-manufacturing" constant="no"/>
<attribute name="DESCRIPTION" value="Ferrite bead 0805 SMD 220R 2A Ferrite bead 0805 SMD 220R 2A" constant="no"/>
<attribute name="HEIGHT" value="1.05mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="BLM21PG221SN1D" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7241532P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/7241532P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="10104111-0001LF">
<description>&lt;FCI Right Angle SMT Female Type AB Version 2.0 Micro USB Connector, 100 V ac, 1.8A&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="MICRO_USB_A/B_TYPE_RECEPTACLE">
<description>&lt;b&gt;MICRO_USB_A/B_TYPE_RECEPTACLE&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="2.5" y="5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="3.15" y="5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="3" x="3.8" y="5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="4" x="4.45" y="5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="5" x="5.1" y="5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="6" x="7.55" y="2.15" dx="2.3" dy="1.9" layer="1"/>
<smd name="7" x="0.05" y="2.15" dx="2.3" dy="1.9" layer="1"/>
<smd name="8" x="4.925" y="2.15" dx="1.9" dy="1.85" layer="1" rot="R90"/>
<smd name="9" x="2.675" y="2.15" dx="1.9" dy="1.85" layer="1" rot="R90"/>
<text x="3.8" y="5" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="3.8" y="5" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="0" y1="5" x2="7.6" y2="5" width="0.1" layer="51"/>
<wire x1="7.6" y1="5" x2="7.6" y2="0" width="0.1" layer="51"/>
<wire x1="7.6" y1="0" x2="0" y2="0" width="0.1" layer="51"/>
<wire x1="0" y1="0" x2="0" y2="5" width="0.1" layer="51"/>
<wire x1="0" y1="0" x2="7.6" y2="0" width="0.1" layer="21"/>
<wire x1="0" y1="5" x2="0" y2="3.48566875" width="0.1" layer="21"/>
<wire x1="0" y1="5" x2="1.98833125" y2="5" width="0.1" layer="21"/>
<wire x1="7.6" y1="5" x2="7.6" y2="3.48566875" width="0.1" layer="21"/>
<wire x1="7.6" y1="5" x2="5.61166875" y2="5" width="0.1" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.84166875" width="0.1" layer="21"/>
<wire x1="7.6" y1="0" x2="7.6" y2="0.84166875" width="0.1" layer="21"/>
<circle x="1.682665625" y="5.544" radius="0.17829375" width="0.2" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="10104111-0001LF">
<wire x1="-7.62" y1="15.24" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<text x="-6.35" y="20.32" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-6.35" y="17.78" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="VBUS" x="-12.7" y="12.7" length="middle"/>
<pin name="DP" x="-12.7" y="7.62" length="middle"/>
<pin name="GND" x="-12.7" y="2.54" length="middle"/>
<pin name="SHIELD2" x="-12.7" y="-7.62" length="middle"/>
<pin name="SHIELD4" x="-12.7" y="-12.7" length="middle"/>
<pin name="DM" x="-12.7" y="10.16" length="middle"/>
<pin name="ID" x="-12.7" y="5.08" length="middle"/>
<pin name="SHIELD1" x="-12.7" y="-5.08" length="middle"/>
<pin name="SHIELD3" x="-12.7" y="-10.16" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10104111-0001LF" prefix="J">
<description>&lt;b&gt;FCI Right Angle SMT Female Type AB Version 2.0 Micro USB Connector, 100 V ac, 1.8A&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://docs-europe.electrocomponents.com/webdocs/0ed1/0900766b80ed1ae2.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="10104111-0001LF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MICRO_USB_A/B_TYPE_RECEPTACLE">
<connects>
<connect gate="G$1" pin="DM" pad="2"/>
<connect gate="G$1" pin="DP" pad="3"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="SHIELD1" pad="6"/>
<connect gate="G$1" pin="SHIELD2" pad="7"/>
<connect gate="G$1" pin="SHIELD3" pad="8"/>
<connect gate="G$1" pin="SHIELD4" pad="9"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70236825" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="http://www.alliedelec.com/fci-10104111-0001lf/70236825/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="10104111-0001LF" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/10104111-0001lf/amphenol-fci" constant="no"/>
<attribute name="DESCRIPTION" value="FCI Right Angle SMT Female Type AB Version 2.0 Micro USB Connector, 100 V ac, 1.8A" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="FCI" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="10104111-0001LF" constant="no"/>
<attribute name="RS_PART_NUMBER" value="1622014" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/1622014" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C2012X5R1A336M125AC">
<description>&lt;TDK 0805 C 33F Ceramic Multilayer Capacitor, 10 V dc, +85C, X5R Dielectric, 20%&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPC2012X145N">
<description>&lt;b&gt;C2012 [EIA CC0805]&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1" y="0" dx="1.47" dy="0.62" layer="1" rot="R90"/>
<smd name="2" x="1" y="0" dx="1.47" dy="0.62" layer="1" rot="R90"/>
<text x="0" y="1.2192" size="0.3048" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.46" y1="0.89" x2="1.46" y2="0.89" width="0.05" layer="51"/>
<wire x1="1.46" y1="0.89" x2="1.46" y2="-0.89" width="0.05" layer="51"/>
<wire x1="1.46" y1="-0.89" x2="-1.46" y2="-0.89" width="0.05" layer="51"/>
<wire x1="-1.46" y1="-0.89" x2="-1.46" y2="0.89" width="0.05" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.1" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.1" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.1" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.1" layer="51"/>
<wire x1="0" y1="0.525" x2="0" y2="-0.525" width="0.2" layer="21"/>
<wire x1="-1.45" y1="0.9" x2="1.45" y2="0.9" width="0.15" layer="21"/>
<wire x1="1.45" y1="0.9" x2="1.45" y2="-0.9" width="0.15" layer="21"/>
<wire x1="1.45" y1="-0.9" x2="-1.45" y2="-0.9" width="0.15" layer="21"/>
<wire x1="-1.45" y1="-0.9" x2="-1.45" y2="0.9" width="0.15" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="C2012X5R1A336M125AC">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<text x="-4.572" y="4.318" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-5.08" y="-4.826" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-3.302" y="0" visible="off" length="short"/>
<pin name="2" x="3.302" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C2012X5R1A336M125AC" prefix="C">
<description>&lt;b&gt;TDK 0805 C 33F Ceramic Multilayer Capacitor, 10 V dc, +85C, X5R Dielectric, 20%&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_midvoltage_en.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="C2012X5R1A336M125AC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC2012X145N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="C2012X5R1A336M125AC" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/c2012x5r1a336m125ac/tdk" constant="no"/>
<attribute name="DESCRIPTION" value="TDK 0805 C 33F Ceramic Multilayer Capacitor, 10 V dc, +85C, X5R Dielectric, 20%" constant="no"/>
<attribute name="HEIGHT" value="1.45mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TDK" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="C2012X5R1A336M125AC" constant="no"/>
<attribute name="RS_PART_NUMBER" value="9159274P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/9159274P" constant="no"/>
<attribute name="VALUE" value="33uF, CER, 20%, 10V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SI2305CDS-T1-GE3">
<description>&lt;SI2305CDS-T1-GE3, P-channel MOSFET Transistor 4.4 A 8 V, 3-Pin SOT-23&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT95P237X112-3N">
<description>&lt;b&gt;SOT-23 (TO-236)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.05" y="0.95" dx="1.3" dy="0.6" layer="1"/>
<smd name="2" x="-1.05" y="-0.95" dx="1.3" dy="0.6" layer="1"/>
<smd name="3" x="1.05" y="0" dx="1.3" dy="0.6" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.95" y1="1.77" x2="1.95" y2="1.77" width="0.05" layer="51"/>
<wire x1="1.95" y1="1.77" x2="1.95" y2="-1.77" width="0.05" layer="51"/>
<wire x1="1.95" y1="-1.77" x2="-1.95" y2="-1.77" width="0.05" layer="51"/>
<wire x1="-1.95" y1="-1.77" x2="-1.95" y2="1.77" width="0.05" layer="51"/>
<wire x1="-0.65" y1="1.46" x2="0.65" y2="1.46" width="0.1" layer="51"/>
<wire x1="0.65" y1="1.46" x2="0.65" y2="-1.46" width="0.1" layer="51"/>
<wire x1="0.65" y1="-1.46" x2="-0.65" y2="-1.46" width="0.1" layer="51"/>
<wire x1="-0.65" y1="-1.46" x2="-0.65" y2="1.46" width="0.1" layer="51"/>
<wire x1="-0.65" y1="0.51" x2="0.3" y2="1.46" width="0.1" layer="51"/>
<wire x1="-0.05" y1="1.46" x2="0.05" y2="1.46" width="0.2" layer="21"/>
<wire x1="0.05" y1="1.46" x2="0.05" y2="-1.46" width="0.2" layer="21"/>
<wire x1="0.05" y1="-1.46" x2="-0.05" y2="-1.46" width="0.2" layer="21"/>
<wire x1="-0.05" y1="-1.46" x2="-0.05" y2="1.46" width="0.2" layer="21"/>
<wire x1="-1.7" y1="1.5" x2="-0.4" y2="1.5" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SI2305CDS-T1-GE3">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<text x="-13.97" y="10.16" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-13.97" y="7.62" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="G" x="0" y="-7.62" length="middle" rot="R90"/>
<pin name="S" x="-2.54" y="10.16" length="middle" rot="R270"/>
<pin name="D" x="2.54" y="10.16" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SI2305CDS-T1-GE3" prefix="IC">
<description>&lt;b&gt;SI2305CDS-T1-GE3, P-channel MOSFET Transistor 4.4 A 8 V, 3-Pin SOT-23&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.vishay.com/docs/64847/si2305cd.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SI2305CDS-T1-GE3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P237X112-3N">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="R1082505" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="http://www.alliedelec.com/vishay-dale-si2305cds-t1-ge3/R1082505/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="SI2305CDS-T1-GE3" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/si2305cds-t1-ge3/vishay" constant="no"/>
<attribute name="DESCRIPTION" value="SI2305CDS-T1-GE3, P-channel MOSFET Transistor 4.4 A 8 V, 3-Pin SOT-23" constant="no"/>
<attribute name="HEIGHT" value="1.12mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Vishay" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SI2305CDS-T1-GE3" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7103248P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/7103248P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AD5241BRZ100">
<description>&lt;AD5241BRZ100, Digital Potentiometer 100k 256-Position Serial-2 Wire, Serial-I2C 14-Pin SOIC N&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOIC127P600X175-14N">
<description>&lt;b&gt;R-14 (SOIC)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.712" y="3.81" dx="1.525" dy="0.65" layer="1"/>
<smd name="2" x="-2.712" y="2.54" dx="1.525" dy="0.65" layer="1"/>
<smd name="3" x="-2.712" y="1.27" dx="1.525" dy="0.65" layer="1"/>
<smd name="4" x="-2.712" y="0" dx="1.525" dy="0.65" layer="1"/>
<smd name="5" x="-2.712" y="-1.27" dx="1.525" dy="0.65" layer="1"/>
<smd name="6" x="-2.712" y="-2.54" dx="1.525" dy="0.65" layer="1"/>
<smd name="7" x="-2.712" y="-3.81" dx="1.525" dy="0.65" layer="1"/>
<smd name="8" x="2.712" y="-3.81" dx="1.525" dy="0.65" layer="1"/>
<smd name="9" x="2.712" y="-2.54" dx="1.525" dy="0.65" layer="1"/>
<smd name="10" x="2.712" y="-1.27" dx="1.525" dy="0.65" layer="1"/>
<smd name="11" x="2.712" y="0" dx="1.525" dy="0.65" layer="1"/>
<smd name="12" x="2.712" y="1.27" dx="1.525" dy="0.65" layer="1"/>
<smd name="13" x="2.712" y="2.54" dx="1.525" dy="0.65" layer="1"/>
<smd name="14" x="2.712" y="3.81" dx="1.525" dy="0.65" layer="1"/>
<text x="0" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<wire x1="-3.725" y1="4.625" x2="3.725" y2="4.625" width="0.05" layer="51"/>
<wire x1="3.725" y1="4.625" x2="3.725" y2="-4.625" width="0.05" layer="51"/>
<wire x1="3.725" y1="-4.625" x2="-3.725" y2="-4.625" width="0.05" layer="51"/>
<wire x1="-3.725" y1="-4.625" x2="-3.725" y2="4.625" width="0.05" layer="51"/>
<wire x1="-1.95" y1="4.325" x2="1.95" y2="4.325" width="0.1" layer="51"/>
<wire x1="1.95" y1="4.325" x2="1.95" y2="-4.325" width="0.1" layer="51"/>
<wire x1="1.95" y1="-4.325" x2="-1.95" y2="-4.325" width="0.1" layer="51"/>
<wire x1="-1.95" y1="-4.325" x2="-1.95" y2="4.325" width="0.1" layer="51"/>
<wire x1="-1.95" y1="3.055" x2="-0.68" y2="4.325" width="0.1" layer="51"/>
<wire x1="-1.6" y1="4.325" x2="1.6" y2="4.325" width="0.2" layer="21"/>
<wire x1="1.6" y1="4.325" x2="1.6" y2="-4.325" width="0.2" layer="21"/>
<wire x1="1.6" y1="-4.325" x2="-1.6" y2="-4.325" width="0.2" layer="21"/>
<wire x1="-1.6" y1="-4.325" x2="-1.6" y2="4.325" width="0.2" layer="21"/>
<wire x1="-3.475" y1="4.485" x2="-1.95" y2="4.485" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AD5241BRZ100">
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-17.78" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-17.78" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<text x="1.27" y="10.16" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="1.27" y="7.62" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="A1" x="0" y="0" length="middle"/>
<pin name="W1" x="0" y="-2.54" length="middle"/>
<pin name="B1" x="0" y="-5.08" length="middle"/>
<pin name="VDD" x="0" y="-7.62" length="middle" direction="pwr"/>
<pin name="!SHDN" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="SCL" x="0" y="-12.7" length="middle"/>
<pin name="SDA" x="0" y="-15.24" length="middle"/>
<pin name="O1" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="NC" x="30.48" y="-2.54" length="middle" direction="nc" rot="R180"/>
<pin name="O2" x="0" y="-10.16" length="middle"/>
<pin name="VSS" x="30.48" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="DGND" x="30.48" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="AD1" x="30.48" y="-12.7" length="middle" rot="R180"/>
<pin name="AD0" x="30.48" y="-15.24" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AD5241BRZ100" prefix="IC">
<description>&lt;b&gt;AD5241BRZ100, Digital Potentiometer 100k 256-Position Serial-2 Wire, Serial-I2C 14-Pin SOIC N&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.analog.com/media/en/technical-documentation/data-sheets/AD5241_5242.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="AD5241BRZ100" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-14N">
<connects>
<connect gate="G$1" pin="!SHDN" pad="5"/>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="AD0" pad="8"/>
<connect gate="G$1" pin="AD1" pad="9"/>
<connect gate="G$1" pin="B1" pad="3"/>
<connect gate="G$1" pin="DGND" pad="10"/>
<connect gate="G$1" pin="NC" pad="13"/>
<connect gate="G$1" pin="O1" pad="14"/>
<connect gate="G$1" pin="O2" pad="12"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="7"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="11"/>
<connect gate="G$1" pin="W1" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="AD5241BRZ100" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/ad5241brz100/analog-devices" constant="no"/>
<attribute name="DESCRIPTION" value="AD5241BRZ100, Digital Potentiometer 100k 256-Position Serial-2 Wire, Serial-I2C 14-Pin SOIC N" constant="no"/>
<attribute name="HEIGHT" value="1.75mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Analog Devices" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="AD5241BRZ100" constant="no"/>
<attribute name="RS_PART_NUMBER" value="4972662P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/4972662P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AD5241BRZ1M">
<description>&lt;AD5241BRZ1M, Digital Potentiometer 1000k 256-Position Serial-2 Wire, Serial-I2C 14-Pin SOIC N&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOIC127P600X175-14N">
<description>&lt;b&gt;R-14 (SOIC)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.712" y="3.81" dx="1.525" dy="0.65" layer="1"/>
<smd name="2" x="-2.712" y="2.54" dx="1.525" dy="0.65" layer="1"/>
<smd name="3" x="-2.712" y="1.27" dx="1.525" dy="0.65" layer="1"/>
<smd name="4" x="-2.712" y="0" dx="1.525" dy="0.65" layer="1"/>
<smd name="5" x="-2.712" y="-1.27" dx="1.525" dy="0.65" layer="1"/>
<smd name="6" x="-2.712" y="-2.54" dx="1.525" dy="0.65" layer="1"/>
<smd name="7" x="-2.712" y="-3.81" dx="1.525" dy="0.65" layer="1"/>
<smd name="8" x="2.712" y="-3.81" dx="1.525" dy="0.65" layer="1"/>
<smd name="9" x="2.712" y="-2.54" dx="1.525" dy="0.65" layer="1"/>
<smd name="10" x="2.712" y="-1.27" dx="1.525" dy="0.65" layer="1"/>
<smd name="11" x="2.712" y="0" dx="1.525" dy="0.65" layer="1"/>
<smd name="12" x="2.712" y="1.27" dx="1.525" dy="0.65" layer="1"/>
<smd name="13" x="2.712" y="2.54" dx="1.525" dy="0.65" layer="1"/>
<smd name="14" x="2.712" y="3.81" dx="1.525" dy="0.65" layer="1"/>
<text x="0" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<wire x1="-3.725" y1="4.625" x2="3.725" y2="4.625" width="0.05" layer="51"/>
<wire x1="3.725" y1="4.625" x2="3.725" y2="-4.625" width="0.05" layer="51"/>
<wire x1="3.725" y1="-4.625" x2="-3.725" y2="-4.625" width="0.05" layer="51"/>
<wire x1="-3.725" y1="-4.625" x2="-3.725" y2="4.625" width="0.05" layer="51"/>
<wire x1="-1.95" y1="4.325" x2="1.95" y2="4.325" width="0.1" layer="51"/>
<wire x1="1.95" y1="4.325" x2="1.95" y2="-4.325" width="0.1" layer="51"/>
<wire x1="1.95" y1="-4.325" x2="-1.95" y2="-4.325" width="0.1" layer="51"/>
<wire x1="-1.95" y1="-4.325" x2="-1.95" y2="4.325" width="0.1" layer="51"/>
<wire x1="-1.95" y1="3.055" x2="-0.68" y2="4.325" width="0.1" layer="51"/>
<wire x1="-1.6" y1="4.325" x2="1.6" y2="4.325" width="0.2" layer="21"/>
<wire x1="1.6" y1="4.325" x2="1.6" y2="-4.325" width="0.2" layer="21"/>
<wire x1="1.6" y1="-4.325" x2="-1.6" y2="-4.325" width="0.2" layer="21"/>
<wire x1="-1.6" y1="-4.325" x2="-1.6" y2="4.325" width="0.2" layer="21"/>
<wire x1="-3.475" y1="4.485" x2="-1.95" y2="4.485" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AD5241BRZ1M">
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-17.78" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-17.78" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<text x="26.67" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="26.67" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="A1" x="0" y="0" length="middle"/>
<pin name="W1" x="0" y="-2.54" length="middle"/>
<pin name="B1" x="0" y="-5.08" length="middle"/>
<pin name="VDD" x="0" y="-7.62" length="middle" direction="pwr"/>
<pin name="!SHDN" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="SCL" x="0" y="-12.7" length="middle"/>
<pin name="SDA" x="0" y="-15.24" length="middle"/>
<pin name="O1" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="NC" x="30.48" y="-2.54" length="middle" direction="nc" rot="R180"/>
<pin name="O2" x="0" y="-10.16" length="middle"/>
<pin name="VSS" x="30.48" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="DGND" x="30.48" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="AD1" x="30.48" y="-12.7" length="middle" rot="R180"/>
<pin name="AD0" x="30.48" y="-15.24" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AD5241BRZ1M" prefix="U">
<description>&lt;b&gt;AD5241BRZ1M, Digital Potentiometer 1000k 256-Position Serial-2 Wire, Serial-I2C 14-Pin SOIC N&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.analog.com/static/imported-files/data_sheets/AD5241_5242.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="AD5241BRZ1M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-14N">
<connects>
<connect gate="G$1" pin="!SHDN" pad="5"/>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="AD0" pad="8"/>
<connect gate="G$1" pin="AD1" pad="9"/>
<connect gate="G$1" pin="B1" pad="3"/>
<connect gate="G$1" pin="DGND" pad="10"/>
<connect gate="G$1" pin="NC" pad="13"/>
<connect gate="G$1" pin="O1" pad="14"/>
<connect gate="G$1" pin="O2" pad="12"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="7"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="11"/>
<connect gate="G$1" pin="W1" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="AD5241BRZ1M" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/ad5241brz1m/analog-devices" constant="no"/>
<attribute name="DESCRIPTION" value="AD5241BRZ1M, Digital Potentiometer 1000k 256-Position Serial-2 Wire, Serial-I2C 14-Pin SOIC N" constant="no"/>
<attribute name="HEIGHT" value="1.75mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Analog Devices" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="AD5241BRZ1M" constant="no"/>
<attribute name="RS_PART_NUMBER" value="1455727" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/1455727" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LTST-C193TBKT-5A">
<description>&lt;Standard LEDs - SMD Blue 470nm 28mcd 20mA&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="LEDC1608X45N">
<description>&lt;b&gt;LTST-C193&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.8" y="0" dx="0.95" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="0.8" y="0" dx="0.95" dy="0.8" layer="1" rot="R90"/>
<text x="0" y="1.524" size="0.4064" layer="25" align="center">&gt;NAME</text>
<text x="0" y="-1.524" size="0.3048" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.65" y1="0.925" x2="1.65" y2="0.925" width="0.05" layer="51"/>
<wire x1="1.65" y1="0.925" x2="1.65" y2="-0.925" width="0.05" layer="51"/>
<wire x1="1.65" y1="-0.925" x2="-1.65" y2="-0.925" width="0.05" layer="51"/>
<wire x1="-1.65" y1="-0.925" x2="-1.65" y2="0.925" width="0.05" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.1" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.1" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0.133" x2="-0.533" y2="0.4" width="0.1" layer="51"/>
<wire x1="0.8" y1="0.825" x2="-1.55" y2="0.825" width="0.2" layer="21"/>
<wire x1="-1.55" y1="0.825" x2="-1.55" y2="-0.825" width="0.2" layer="21"/>
<wire x1="-1.55" y1="-0.825" x2="0.8" y2="-0.825" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LTST-C193TBKT-5A">
<text x="-5.08" y="8.89" size="1.4224" layer="95">&gt;NAME</text>
<text x="-5.08" y="6.35" size="1.4224" layer="96">&gt;VALUE</text>
<pin name="K" x="-3.556" y="0" visible="off" length="short"/>
<pin name="A" x="3.048" y="0" visible="off" length="short" rot="R180"/>
<polygon width="0.127" layer="94">
<vertex x="-2.032" y="3.048"/>
<vertex x="-2.794" y="2.286"/>
<vertex x="-2.794" y="3.048"/>
</polygon>
<polygon width="0.127" layer="94">
<vertex x="0" y="3.048"/>
<vertex x="-0.762" y="2.286"/>
<vertex x="-0.762" y="3.048"/>
</polygon>
<wire x1="-1.016" y1="-1.27" x2="-1.016" y2="0" width="0.127" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="1.27" width="0.127" layer="94"/>
<wire x1="-1.016" y1="0" x2="0.508" y2="1.27" width="0.127" layer="94"/>
<wire x1="0.508" y1="1.27" x2="0.508" y2="-1.27" width="0.127" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="-1.016" y2="0" width="0.127" layer="94"/>
<wire x1="-0.762" y1="3.048" x2="0.254" y2="2.032" width="0.127" layer="94"/>
<wire x1="-2.794" y1="3.048" x2="-1.778" y2="2.032" width="0.127" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LTST-C193TBKT-5A" prefix="LED">
<description>&lt;b&gt;Standard LEDs - SMD Blue 470nm 28mcd 20mA&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/LTST-C193TBKT-5A.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="LTST-C193TBKT-5A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LEDC1608X45N">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="LTST-C193TBKT-5A" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/ltst-c193tbkt-5a/lite-on-technology" constant="no"/>
<attribute name="DESCRIPTION" value="Standard LEDs - SMD Blue 470nm 28mcd 20mA" constant="no"/>
<attribute name="HEIGHT" value="0.45mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Lite-On" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LTST-C193TBKT-5A" constant="no"/>
<attribute name="RS_PART_NUMBER" value="6921051" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/6921051" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="GRM033R61E104KE14J">
<description>&lt;Multilayer Ceramic Capacitors MLCC - SMD/SMT&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPC0603X33N">
<description>&lt;b&gt;GRM033&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.33" y="0" dx="0.46" dy="0.42" layer="1"/>
<smd name="2" x="0.33" y="0" dx="0.46" dy="0.42" layer="1"/>
<text x="0.0746" y="1.005" size="0.8128" layer="25" align="center">&gt;NAME</text>
<wire x1="-0.71" y1="0.36" x2="0.71" y2="0.36" width="0.05" layer="51"/>
<wire x1="0.71" y1="0.36" x2="0.71" y2="-0.36" width="0.05" layer="51"/>
<wire x1="0.71" y1="-0.36" x2="-0.71" y2="-0.36" width="0.05" layer="51"/>
<wire x1="-0.71" y1="-0.36" x2="-0.71" y2="0.36" width="0.05" layer="51"/>
<wire x1="-0.3" y1="0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="-0.71" y1="0.36" x2="0.71" y2="0.36" width="0.05" layer="21"/>
<wire x1="0.71" y1="0.36" x2="0.71" y2="-0.36" width="0.05" layer="21"/>
<wire x1="0.71" y1="-0.36" x2="-0.71" y2="-0.36" width="0.05" layer="21"/>
<wire x1="-0.71" y1="-0.36" x2="-0.71" y2="0.36" width="0.05" layer="21"/>
<wire x1="-0.73" y1="0.38" x2="0.73" y2="0.38" width="0.15" layer="21"/>
<wire x1="0.73" y1="0.38" x2="0.73" y2="-0.38" width="0.15" layer="21"/>
<wire x1="0.73" y1="-0.38" x2="-0.73" y2="-0.38" width="0.15" layer="21"/>
<wire x1="-0.73" y1="-0.38" x2="-0.73" y2="0.38" width="0.15" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="GRM033R61E104KE14J">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<text x="1.524" y="14.224" size="1.4224" layer="95" rot="R270" align="center-left">&gt;NAME</text>
<text x="-0.254" y="14.224" size="1.4224" layer="96" rot="R270" align="center-left">&gt;VALUE</text>
<pin name="1" x="-3.302" y="0" visible="off" length="short"/>
<pin name="2" x="3.302" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GRM033R61E104KE14J" prefix="C">
<description>&lt;b&gt;Multilayer Ceramic Capacitors MLCC - SMD/SMT&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="GRM033R61E104KE14J" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC0603X33N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="GRM033R61E104KE14J" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/grm033r61e104ke14j/murata-manufacturing" constant="no"/>
<attribute name="DESCRIPTION" value="Multilayer Ceramic Capacitors MLCC - SMD/SMT" constant="no"/>
<attribute name="HEIGHT" value="0.33mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="GRM033R61E104KE14J" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
<attribute name="VALUE" value="100nF, 25V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CRCW06030000Z0EA">
<description>&lt;VISHAY - CRCW06030000Z0EA - RESISTOR, 0R, 0.1W, JUMPER, 0603, SMD&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="RESC1608X50N">
<description>&lt;b&gt;CRCW0603&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.75" y="0" dx="1" dy="0.85" layer="1" rot="R90"/>
<smd name="2" x="0.75" y="0" dx="1" dy="0.85" layer="1" rot="R90"/>
<text x="-0.0508" y="1.143" size="0.4064" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.425" y1="0.75" x2="1.425" y2="0.75" width="0.05" layer="51"/>
<wire x1="1.425" y1="0.75" x2="1.425" y2="-0.75" width="0.05" layer="51"/>
<wire x1="1.425" y1="-0.75" x2="-1.425" y2="-0.75" width="0.05" layer="51"/>
<wire x1="-1.425" y1="-0.75" x2="-1.425" y2="0.75" width="0.05" layer="51"/>
<wire x1="-0.788" y1="0.425" x2="0.788" y2="0.425" width="0.1" layer="51"/>
<wire x1="0.788" y1="0.425" x2="0.788" y2="-0.425" width="0.1" layer="51"/>
<wire x1="0.788" y1="-0.425" x2="-0.788" y2="-0.425" width="0.1" layer="51"/>
<wire x1="-0.788" y1="-0.425" x2="-0.788" y2="0.425" width="0.1" layer="51"/>
<wire x1="-1.42" y1="0.75" x2="1.425" y2="0.75" width="0.05" layer="21"/>
<wire x1="1.425" y1="0.75" x2="1.425" y2="-0.745" width="0.05" layer="21"/>
<wire x1="1.425" y1="-0.745" x2="-1.42" y2="-0.745" width="0.05" layer="21"/>
<wire x1="-1.42" y1="-0.745" x2="-1.42" y2="0.75" width="0.05" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CRCW06030000Z0EA">
<wire x1="-2.54" y1="0.762" x2="2.286" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.286" y1="-0.762" x2="2.286" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.286" y1="-0.762" x2="-2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.762" x2="-2.54" y2="-0.762" width="0.254" layer="94"/>
<text x="-3.81" y="5.334" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-3.81" y="3.556" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="4.826" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRCW06030000Z0EA" prefix="R">
<description>&lt;b&gt;VISHAY - CRCW06030000Z0EA - RESISTOR, 0R, 0.1W, JUMPER, 0603, SMD&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.vishay.com/docs/20035/dcrcwe3.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CRCW06030000Z0EA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70202889" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="https://www.alliedelec.com/vishay-dale-crcw06030000z0ea/70202889/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="CRCW06030000Z0EA" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/crcw06030000z0ea/vishay" constant="no"/>
<attribute name="DESCRIPTION" value="VISHAY - CRCW06030000Z0EA - RESISTOR, 0R, 0.1W, JUMPER, 0603, SMD" constant="no"/>
<attribute name="HEIGHT" value="0.5mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Vishay" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CRCW06030000Z0EA" constant="no"/>
<attribute name="RS_PART_NUMBER" value="2508364822" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/2508364822" constant="no"/>
<attribute name="VALUE" value="0 Ohm" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CRCW040210K0FKEDHP">
<description>&lt;CRCW Power Resistor 0402 0.063W 10K Vishay CRCW Series Thick Film Power Resistor 0402 Case 10k 1% 0.125W 100ppm/K&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="RESC1005X40N">
<description>&lt;b&gt;CRCW0402HP&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.55" y="0" dx="0.7" dy="0.6" layer="1"/>
<smd name="2" x="0.55" y="0" dx="0.7" dy="0.6" layer="1"/>
<text x="-0.03588125" y="1.13675625" size="0.4064" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.15" y1="0.55" x2="1.15" y2="0.55" width="0.05" layer="51"/>
<wire x1="1.15" y1="0.55" x2="1.15" y2="-0.55" width="0.05" layer="51"/>
<wire x1="1.15" y1="-0.55" x2="-1.15" y2="-0.55" width="0.05" layer="51"/>
<wire x1="-1.15" y1="-0.55" x2="-1.15" y2="0.55" width="0.05" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="-1.15" y1="0.55" x2="1.15" y2="0.55" width="0.05" layer="21"/>
<wire x1="1.15" y1="0.55" x2="1.15" y2="-0.55" width="0.05" layer="21"/>
<wire x1="1.15" y1="-0.55" x2="-1.15" y2="-0.55" width="0.05" layer="21"/>
<wire x1="-1.15" y1="-0.55" x2="-1.15" y2="0.55" width="0.05" layer="21"/>
<wire x1="-1.2" y1="0.6" x2="1.2" y2="0.6" width="0.15" layer="21"/>
<wire x1="1.2" y1="0.6" x2="1.2" y2="-0.59" width="0.15" layer="21"/>
<wire x1="1.2" y1="-0.59" x2="-1.2" y2="-0.6" width="0.15" layer="21"/>
<wire x1="-1.2" y1="-0.6" x2="-1.2" y2="0.6" width="0.15" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CRCW040210K0FKEDHP">
<wire x1="-1.778" y1="0.762" x2="1.778" y2="0.762" width="0.254" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.778" y2="0.762" width="0.254" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="-1.778" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0.762" x2="-1.778" y2="-0.762" width="0.254" layer="94"/>
<text x="-3.556" y="4.064" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-3.556" y="2.286" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-4.318" y="0" visible="off" length="short"/>
<pin name="2" x="4.318" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRCW040210K0FKEDHP" prefix="R">
<description>&lt;b&gt;CRCW Power Resistor 0402 0.063W 10K Vishay CRCW Series Thick Film Power Resistor 0402 Case 10k 1% 0.125W 100ppm/K&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.vishay.com/docs/20035/dcrcwe3.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CRCW040210K0FKEDHP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70615863" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="https://www.alliedelec.com/vishay-dale-crcw040210k0fkedhp/70615863/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="CRCW040210K0FKEDHP" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/crcw040210k0fkedhp/vishay" constant="no"/>
<attribute name="DESCRIPTION" value="CRCW Power Resistor 0402 0.063W 10K Vishay CRCW Series Thick Film Power Resistor 0402 Case 10k 1% 0.125W 100ppm/K" constant="no"/>
<attribute name="HEIGHT" value="0.4mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Vishay" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CRCW040210K0FKEDHP" constant="no"/>
<attribute name="RS_PART_NUMBER" value="8121562P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/8121562P" constant="no"/>
<attribute name="VALUE" value="10k, 1%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C0603C103J5RACTU">
<description>&lt;Kemet 0603 C 10nF Ceramic Multilayer Capacitor, 50 V, +125C, X7R Dielectric, 5%&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPC1608X87N">
<description>&lt;b&gt;C0603 (0.8 (0.87) thickness)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.7" y="0" dx="0.98" dy="0.78" layer="1" rot="R90"/>
<smd name="2" x="0.7" y="0" dx="0.98" dy="0.78" layer="1" rot="R90"/>
<text x="-0.0254" y="0.889" size="0.254" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.24" y1="0.64" x2="1.24" y2="0.64" width="0.05" layer="51"/>
<wire x1="1.24" y1="0.64" x2="1.24" y2="-0.64" width="0.05" layer="51"/>
<wire x1="1.24" y1="-0.64" x2="-1.24" y2="-0.64" width="0.05" layer="51"/>
<wire x1="-1.24" y1="-0.64" x2="-1.24" y2="0.64" width="0.05" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.1" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.1" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.1" layer="51"/>
<wire x1="-1.24" y1="0.64" x2="1.24" y2="0.64" width="0.05" layer="21"/>
<wire x1="1.24" y1="0.64" x2="1.24" y2="-0.64" width="0.05" layer="21"/>
<wire x1="1.24" y1="-0.64" x2="-1.24" y2="-0.64" width="0.05" layer="21"/>
<wire x1="-1.24" y1="-0.64" x2="-1.24" y2="0.64" width="0.05" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="C0603C103J5RACTU">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<text x="-3.81" y="6.35" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-3.81" y="4.318" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-3.302" y="0" visible="off" length="short"/>
<pin name="2" x="3.302" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C0603C103J5RACTU" prefix="C">
<description>&lt;b&gt;Kemet 0603 C 10nF Ceramic Multilayer Capacitor, 50 V, +125C, X7R Dielectric, 5%&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/C0603C103J5RACTU.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="C0603C103J5RACTU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC1608X87N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70096875" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="https://www.alliedelec.com/kemet-c0603c103j5ractu/70096875/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="C0603C103J5RACTU" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/c0603c103j5ractu/kemet-corporation" constant="no"/>
<attribute name="DESCRIPTION" value="Kemet 0603 C 10nF Ceramic Multilayer Capacitor, 50 V, +125C, X7R Dielectric, 5%" constant="no"/>
<attribute name="HEIGHT" value="0.87mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="C0603C103J5RACTU" constant="no"/>
<attribute name="RS_PART_NUMBER" value="8015316" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/8015316" constant="no"/>
<attribute name="VALUE" value="10nF, 50V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CPF0603B470KE1">
<description>&lt;TE CONNECTIVITY - CPF0603B470KE1 - RES, THIN FILM, 470K, 0.1%, 0.063W, 0603&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="RESC1608X55N">
<description>&lt;b&gt;CPF0603&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.75" y="0" dx="0.95" dy="0.9" layer="1" rot="R90"/>
<smd name="2" x="0.75" y="0" dx="0.95" dy="0.9" layer="1" rot="R90"/>
<text x="-0.0254" y="1.0922" size="0.3048" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.45" y1="0.75" x2="1.45" y2="0.75" width="0.05" layer="51"/>
<wire x1="1.45" y1="0.75" x2="1.45" y2="-0.75" width="0.05" layer="51"/>
<wire x1="1.45" y1="-0.75" x2="-1.45" y2="-0.75" width="0.05" layer="51"/>
<wire x1="-1.45" y1="-0.75" x2="-1.45" y2="0.75" width="0.05" layer="51"/>
<wire x1="-0.775" y1="0.4" x2="0.775" y2="0.4" width="0.1" layer="51"/>
<wire x1="0.775" y1="0.4" x2="0.775" y2="-0.4" width="0.1" layer="51"/>
<wire x1="0.775" y1="-0.4" x2="-0.775" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-0.775" y1="-0.4" x2="-0.775" y2="0.4" width="0.1" layer="51"/>
<wire x1="-1.45" y1="0.75" x2="1.45" y2="0.75" width="0.05" layer="21"/>
<wire x1="1.45" y1="0.75" x2="1.45" y2="-0.75" width="0.05" layer="21"/>
<wire x1="1.45" y1="-0.75" x2="-1.45" y2="-0.75" width="0.05" layer="21"/>
<wire x1="-1.45" y1="-0.75" x2="-1.45" y2="0.75" width="0.05" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CPF0603B470KE1">
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="1.778" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="-1.778" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0.508" x2="-1.778" y2="-0.508" width="0.254" layer="94"/>
<text x="-3.556" y="4.572" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-3.556" y="2.032" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-4.318" y="0" visible="off" length="short"/>
<pin name="2" x="4.318" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CPF0603B470KE1" prefix="R">
<description>&lt;b&gt;TE CONNECTIVITY - CPF0603B470KE1 - RES, THIN FILM, 470K, 0.1%, 0.063W, 0603&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.farnell.com/datasheets/1786775.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CPF0603B470KE1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="CPF0603B470KE1" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/cpf0603b470ke1/te-connectivity" constant="no"/>
<attribute name="DESCRIPTION" value="TE CONNECTIVITY - CPF0603B470KE1 - RES, THIN FILM, 470K, 0.1%, 0.063W, 0603" constant="no"/>
<attribute name="HEIGHT" value="0.55mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TE Connectivity" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CPF0603B470KE1" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
<attribute name="VALUE" value="470K,0.1%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ESD9B3.3ST5G">
<description>&lt;TVS ESD Suppressor 18KV SOD-923 ON Semiconductor ESD9B3.3ST5G Bi-Directional TVS Diode, 300mW peak, 2-Pin SOD-923&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SODFL1006X40N">
<description>&lt;b&gt;SOD-923 CASE 514AB&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.5" y="0" dx="0.51" dy="0.32" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.51" dy="0.32" layer="1"/>
<text x="-0.0508" y="0.762" size="0.3048" layer="25" align="center">&gt;NAME</text>
<wire x1="-0.905" y1="0.475" x2="0.905" y2="0.475" width="0.05" layer="51"/>
<wire x1="0.905" y1="0.475" x2="0.905" y2="-0.475" width="0.05" layer="51"/>
<wire x1="0.905" y1="-0.475" x2="-0.905" y2="-0.475" width="0.05" layer="51"/>
<wire x1="-0.905" y1="-0.475" x2="-0.905" y2="0.475" width="0.05" layer="51"/>
<wire x1="-0.4" y1="0.3" x2="0.4" y2="0.3" width="0.1" layer="51"/>
<wire x1="0.4" y1="0.3" x2="0.4" y2="-0.3" width="0.1" layer="51"/>
<wire x1="0.4" y1="-0.3" x2="-0.4" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.4" y1="-0.3" x2="-0.4" y2="0.3" width="0.1" layer="51"/>
<wire x1="-0.4" y1="0.045" x2="-0.145" y2="0.3" width="0.1" layer="51"/>
<wire x1="-0.755" y1="0.3" x2="0.4" y2="0.3" width="0.2" layer="21"/>
<wire x1="-0.4" y1="-0.3" x2="0.4" y2="-0.3" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="ESD9B3.3ST5G">
<text x="-6.604" y="3.556" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-6.858" y="-3.556" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="K" x="-9.398" y="0" visible="off" length="short"/>
<pin name="A" x="3.556" y="0" visible="off" length="short" rot="R180"/>
<wire x1="1.016" y1="-1.524" x2="1.016" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.016" y1="1.524" x2="-1.524" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="1.016" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.524" x2="-1.524" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="1.524" x2="-2.286" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.524" x2="-0.762" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="-6.858" y1="1.524" x2="-6.858" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-6.858" y1="-1.524" x2="-4.318" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.318" y1="0" x2="-6.858" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-4.318" y1="1.524" x2="-4.318" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.318" y1="0" x2="-4.318" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-4.318" y1="-1.524" x2="-3.556" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="-4.318" y1="1.524" x2="-5.08" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-4.318" y1="0" x2="-1.524" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESD9B3.3ST5G" prefix="D">
<description>&lt;b&gt;TVS ESD Suppressor 18KV SOD-923 ON Semiconductor ESD9B3.3ST5G Bi-Directional TVS Diode, 300mW peak, 2-Pin SOD-923&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.onsemi.com/pub/Collateral/ESD9B-D.PDF"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ESD9B3.3ST5G" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SODFL1006X40N">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70339629" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="http://www.alliedelec.com/on-semiconductor-esd9b3-3st5g/70339629/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="ESD9B3.3ST5G" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/esd9b3.3st5g/on-semiconductor" constant="no"/>
<attribute name="DESCRIPTION" value="TVS ESD Suppressor 18KV SOD-923 ON Semiconductor ESD9B3.3ST5G Bi-Directional TVS Diode, 300mW peak, 2-Pin SOD-923" constant="no"/>
<attribute name="HEIGHT" value="0.4mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="ON Semiconductor" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ESD9B3.3ST5G" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7925583P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/7925583P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="282836-3">
<description>&lt;3 way PCB screw terminal,5mm pitch&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="282836-3">
<description>&lt;b&gt;282836-3&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="1.2" diameter="1.7"/>
<pad name="2" x="5" y="0" drill="1.2" diameter="1.7"/>
<pad name="3" x="10" y="0" drill="1.2" diameter="1.7"/>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<wire x1="-3.1" y1="4.1" x2="12.5" y2="4.1" width="0.2" layer="21"/>
<wire x1="12.5" y1="4.1" x2="12.5" y2="-4.1" width="0.2" layer="21"/>
<wire x1="12.5" y1="-4.1" x2="-3.1" y2="-4.1" width="0.2" layer="21"/>
<wire x1="-3.1" y1="-4.1" x2="-3.1" y2="4.1" width="0.2" layer="21"/>
<wire x1="-3.1" y1="4.1" x2="12.5" y2="4.1" width="0.2" layer="51"/>
<wire x1="12.5" y1="4.1" x2="12.5" y2="-4.1" width="0.2" layer="51"/>
<wire x1="12.5" y1="-4.1" x2="-3.1" y2="-4.1" width="0.2" layer="51"/>
<wire x1="-3.1" y1="-4.1" x2="-3.1" y2="4.1" width="0.2" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="282836-3">
<wire x1="-4.064" y1="10.16" x2="3.556" y2="10.16" width="0.254" layer="94"/>
<wire x1="3.556" y1="-10.16" x2="3.556" y2="10.16" width="0.254" layer="94"/>
<wire x1="3.556" y1="-10.16" x2="-4.064" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-4.064" y1="10.16" x2="-4.064" y2="-10.16" width="0.254" layer="94"/>
<text x="-5.334" y="-12.7" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-5.334" y="-15.24" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="8.636" y="5.08" length="middle" rot="R180"/>
<pin name="3" x="8.636" y="-5.08" length="middle" rot="R180"/>
<pin name="2" x="8.636" y="0" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="282836-3" prefix="J">
<description>&lt;b&gt;3 way PCB screw terminal,5mm pitch&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&amp;DocId=Catalog Section1308389_EUROSTYLE_TERMINAL_BLOCKS0607pdfEnglishENG_CS_1308389_EUROSTYLE_TERMINAL_BLOCKS_0607.pdf282836-3.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="282836-3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="282836-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70086276" constant="no"/>
<attribute name="ALLIED_PRICE/STOCK" value="http://www.alliedelec.com/te-connectivity-282836-3/70086276/" constant="no"/>
<attribute name="ARROW_PART_NUMBER" value="282836-3" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/282836-3/te-connectivity" constant="no"/>
<attribute name="DESCRIPTION" value="3 way PCB screw terminal,5mm pitch" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TE Connectivity" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="282836-3" constant="no"/>
<attribute name="RS_PART_NUMBER" value="1648600" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/1648600" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RC0201FR-07100RL">
<description>&lt;Thick Film Resistors - SMD 100 OHM 1%&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="RESC0603X26N">
<description>&lt;b&gt;RC0201&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.34" y="0" dx="0.44" dy="0.42" layer="1"/>
<smd name="2" x="0.34" y="0" dx="0.44" dy="0.42" layer="1"/>
<text x="-0.0254" y="0.7874" size="0.3048" layer="25" align="center">&gt;NAME</text>
<text x="-0.0254" y="-0.7366" size="0.3048" layer="27" align="center">&gt;VALUE</text>
<wire x1="-0.315" y1="-0.165" x2="0.315" y2="-0.165" width="0.001" layer="51"/>
<wire x1="0.315" y1="-0.165" x2="0.315" y2="0.165" width="0.001" layer="51"/>
<wire x1="0.315" y1="0.165" x2="-0.315" y2="0.165" width="0.001" layer="51"/>
<wire x1="-0.315" y1="0.165" x2="-0.315" y2="-0.165" width="0.001" layer="51"/>
<wire x1="-0.71" y1="-0.36" x2="0.71" y2="-0.36" width="0.05" layer="51"/>
<wire x1="0.71" y1="-0.36" x2="0.71" y2="0.36" width="0.05" layer="51"/>
<wire x1="0.71" y1="0.36" x2="-0.71" y2="0.36" width="0.05" layer="51"/>
<wire x1="-0.71" y1="0.36" x2="-0.71" y2="-0.36" width="0.05" layer="51"/>
<circle x="0" y="0" radius="0.35" width="0.05" layer="21"/>
<wire x1="0" y1="-0.4746" x2="0" y2="0.5254" width="0.05" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.05" layer="51"/>
<wire x1="-0.7" y1="-0.35" x2="0.7" y2="-0.35" width="0.1" layer="51"/>
<wire x1="0.7" y1="-0.35" x2="0.7" y2="0.35" width="0.1" layer="51"/>
<wire x1="0.7" y1="0.35" x2="-0.7" y2="0.35" width="0.1" layer="51"/>
<wire x1="-0.7" y1="0.35" x2="-0.7" y2="-0.35" width="0.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="RC0201FR-07100RL">
<wire x1="-2.032" y1="0.762" x2="2.032" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.762" x2="2.032" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.762" x2="-2.032" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.032" y1="0.762" x2="-2.032" y2="-0.762" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-4.064" y="-2.794" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-4.572" y="0" visible="off" length="short"/>
<pin name="2" x="4.572" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RC0201FR-07100RL" prefix="R">
<description>&lt;b&gt;Thick Film Resistors - SMD 100 OHM 1%&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.yageo.com/documents/recent/PYu-RC0201_51_RoHS_L_8.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="RC0201FR-07100RL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESC0603X26N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="RC0201FR-07100RL" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="Thick Film Resistors - SMD 100 OHM 1%" constant="no"/>
<attribute name="HEIGHT" value="0.26mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="YAGEO (PHYCOMP)" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="RC0201FR-07100RL" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
<attribute name="VALUE" value="100, 1%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CGA5L1X7R1V106K160AE">
<description>&lt;Capacitor Auto CGA 1206 35V 10uF X7R&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPC3216X190N">
<description>&lt;b&gt;CGA5 (L THICKNESS)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.5" y="0" dx="1.82" dy="0.82" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="1.82" dy="0.82" layer="1" rot="R90"/>
<text x="-2.424" y="-0.004" size="0.6096" layer="25" rot="R90" align="center">&gt;NAME</text>
<wire x1="-2.06" y1="1.06" x2="2.06" y2="1.06" width="0.05" layer="51"/>
<wire x1="2.06" y1="1.06" x2="2.06" y2="-1.06" width="0.05" layer="51"/>
<wire x1="2.06" y1="-1.06" x2="-2.06" y2="-1.06" width="0.05" layer="51"/>
<wire x1="-2.06" y1="-1.06" x2="-2.06" y2="1.06" width="0.05" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.1" layer="51"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.1" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.1" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.1" layer="51"/>
<wire x1="0" y1="0.7" x2="0" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-2.05" y1="1.05" x2="2.05" y2="1.05" width="0.1" layer="21"/>
<wire x1="2.05" y1="1.05" x2="2.05" y2="-1.05" width="0.1" layer="21"/>
<wire x1="2.05" y1="-1.05" x2="-2.05" y2="-1.05" width="0.1" layer="21"/>
<wire x1="-2.05" y1="-1.05" x2="-2.05" y2="1.05" width="0.1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CGA5L1X7R1V106K160AE">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<text x="-4.826" y="6.604" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-4.826" y="4.318" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-3.302" y="0" visible="off" length="short"/>
<pin name="2" x="3.302" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CGA5L1X7R1V106K160AE" prefix="C">
<description>&lt;b&gt;Capacitor Auto CGA 1206 35V 10uF X7R&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://docs-europe.electrocomponents.com/webdocs/14e9/0900766b814e9125.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CGA5L1X7R1V106K160AE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC3216X190N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="CGA5L1X7R1V106K160AE" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/cga5l1x7r1v106k160ae/tdk" constant="no"/>
<attribute name="DESCRIPTION" value="Capacitor Auto CGA 1206 35V 10uF X7R" constant="no"/>
<attribute name="HEIGHT" value="1.9mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TDK" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CGA5L1X7R1V106K160AE" constant="no"/>
<attribute name="RS_PART_NUMBER" value="9155354P" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/9155354P" constant="no"/>
<attribute name="VALUE" value="10uF,35V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="GRM022R60J104ME15L">
<description>&lt;Multilayer Ceramic Capacitors MLCC - SMD/SMT 01005 0.10uF 6.3volt X5R + - 20%&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPC0402X22N">
<description>&lt;b&gt;GRM0222C1H150GA03&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.282" y="0" dx="0.365" dy="0.33" layer="1"/>
<smd name="2" x="0.282" y="0" dx="0.365" dy="0.33" layer="1"/>
<wire x1="-0.615" y1="0.32" x2="0.615" y2="0.32" width="0.05" layer="51"/>
<wire x1="0.615" y1="0.32" x2="0.615" y2="-0.32" width="0.05" layer="51"/>
<wire x1="0.615" y1="-0.32" x2="-0.615" y2="-0.32" width="0.05" layer="51"/>
<wire x1="-0.615" y1="-0.32" x2="-0.615" y2="0.32" width="0.05" layer="51"/>
<wire x1="-0.2" y1="0.1" x2="0.2" y2="0.1" width="0.1" layer="51"/>
<wire x1="0.2" y1="0.1" x2="0.2" y2="-0.1" width="0.1" layer="51"/>
<wire x1="0.2" y1="-0.1" x2="-0.2" y2="-0.1" width="0.1" layer="51"/>
<wire x1="-0.2" y1="-0.1" x2="-0.2" y2="0.1" width="0.1" layer="51"/>
<wire x1="-0.615" y1="0.32" x2="0.615" y2="0.32" width="0.05" layer="21"/>
<wire x1="0.615" y1="0.32" x2="0.615" y2="-0.32" width="0.05" layer="21"/>
<wire x1="0.615" y1="-0.32" x2="-0.615" y2="-0.32" width="0.05" layer="21"/>
<wire x1="-0.615" y1="-0.32" x2="-0.615" y2="0.32" width="0.05" layer="21"/>
<text x="-0.085" y="1.153" size="0.8128" layer="25" rot="R180" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="GRM022R60J104ME15L">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<text x="-4.318" y="4.318" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<pin name="1" x="-3.302" y="0" visible="off" length="short"/>
<pin name="2" x="3.302" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GRM022R60J104ME15L" prefix="C">
<description>&lt;b&gt;Multilayer Ceramic Capacitors MLCC - SMD/SMT 01005 0.10uF 6.3volt X5R + - 20%&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="GRM022R60J104ME15L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC0402X22N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="GRM022R60J104ME15L" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/grm022r60j104me15l/murata-manufacturing" constant="no"/>
<attribute name="DESCRIPTION" value="Multilayer Ceramic Capacitors MLCC - SMD/SMT 01005 0.10uF 6.3volt X5R + - 20%" constant="no"/>
<attribute name="HEIGHT" value="0.22mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="GRM022R60J104ME15L" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="UWX1A221MCL1GB">
<packages>
<package name="PCAP_8X5.4-ELECT">
<smd name="1" x="-3.2512" y="0" dx="4.2164" dy="1.8034" layer="1"/>
<smd name="2" x="3.2512" y="0" dx="4.2164" dy="1.8034" layer="1"/>
<wire x1="-6.096" y1="0" x2="-6.858" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="-4.3688" x2="4.3688" y2="-4.3688" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="-4.3688" x2="4.3688" y2="-1.2446" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="4.3688" x2="-4.3688" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="4.3688" x2="-4.3688" y2="1.2446" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="-1.2446" x2="-4.3688" y2="-4.3688" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="1.2446" x2="4.3688" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="0" x2="-6.858" y2="0" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="2.1336" x2="-2.1336" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="-2.1336" x2="-2.1336" y2="-4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="-4.2672" x2="4.2672" y2="-4.2672" width="0.1524" layer="51"/>
<wire x1="4.2672" y1="-4.2672" x2="4.2672" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="4.2672" y1="4.2672" x2="-4.2672" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="4.2672" x2="-4.2672" y2="-4.2672" width="0.1524" layer="51"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="PCAP_8X5.4-ELECT-M">
<smd name="1" x="-3.3909" y="0" dx="4.4958" dy="2.0066" layer="1"/>
<smd name="2" x="3.3909" y="0" dx="4.4958" dy="2.0066" layer="1"/>
<wire x1="-6.3754" y1="0" x2="-7.1374" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.7564" y1="0.381" x2="-6.7564" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="-4.3688" x2="4.3688" y2="-4.3688" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="-4.3688" x2="4.3688" y2="-1.3462" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="4.3688" x2="-4.3688" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="4.3688" x2="-4.3688" y2="1.3462" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="-1.3462" x2="-4.3688" y2="-4.3688" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="1.3462" x2="4.3688" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="-6.3754" y1="0" x2="-7.1374" y2="0" width="0.1524" layer="51"/>
<wire x1="-6.7564" y1="0.381" x2="-6.7564" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="2.1336" x2="-2.1336" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="-2.1336" x2="-2.1336" y2="-4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="-4.2672" x2="4.2672" y2="-4.2672" width="0.1524" layer="51"/>
<wire x1="4.2672" y1="-4.2672" x2="4.2672" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="4.2672" y1="4.2672" x2="-4.2672" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="4.2672" x2="-4.2672" y2="-4.2672" width="0.1524" layer="51"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="PCAP_8X5.4-ELECT-L">
<smd name="1" x="-3.0988" y="0" dx="3.9116" dy="1.6002" layer="1"/>
<smd name="2" x="3.0988" y="0" dx="3.9116" dy="1.6002" layer="1"/>
<wire x1="-5.7912" y1="0" x2="-6.5532" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.1722" y1="0.381" x2="-6.1722" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="-4.3688" x2="4.3688" y2="-4.3688" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="-4.3688" x2="4.3688" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="4.3688" x2="-4.3688" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="4.3688" x2="-4.3688" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="-1.143" x2="-4.3688" y2="-4.3688" width="0.1524" layer="21"/>
<wire x1="4.3688" y1="1.143" x2="4.3688" y2="4.3688" width="0.1524" layer="21"/>
<wire x1="-5.7912" y1="0" x2="-6.5532" y2="0" width="0.1524" layer="51"/>
<wire x1="-6.1722" y1="0.381" x2="-6.1722" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="2.1336" x2="-2.1336" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="-2.1336" x2="-2.1336" y2="-4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="-4.2672" x2="4.2672" y2="-4.2672" width="0.1524" layer="51"/>
<wire x1="4.2672" y1="-4.2672" x2="4.2672" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="4.2672" y1="4.2672" x2="-4.2672" y2="4.2672" width="0.1524" layer="51"/>
<wire x1="-4.2672" y1="4.2672" x2="-4.2672" y2="-4.2672" width="0.1524" layer="51"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="PCAPH">
<pin name="22" x="5.842" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="11" x="0" y="0" visible="off" length="short" direction="pas"/>
<wire x1="1.3208" y1="0.635" x2="1.3208" y2="1.905" width="0.2032" layer="94"/>
<wire x1="2.5908" y1="-1.905" x2="2.5908" y2="1.905" width="0.2032" layer="94"/>
<wire x1="0.6858" y1="1.27" x2="1.9558" y2="1.27" width="0.2032" layer="94"/>
<wire x1="3.8608" y1="-1.905" x2="3.8608" y2="1.905" width="0.1524" layer="94" curve="-74"/>
<text x="-4.064" y="-3.7338" size="1.4224" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="-4.0894" y="2.8448" size="1.4224" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="UWX1A221MCL1GB" prefix="C">
<gates>
<gate name="A" symbol="PCAPH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PCAP_8X5.4-ELECT">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="UWX1A221MCL1GB" constant="no"/>
<attribute name="VENDOR" value="Nichicon" constant="no"/>
</technology>
</technologies>
</device>
<device name="PCAP_8X5.4-ELECT-M" package="PCAP_8X5.4-ELECT-M">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="UWX1A221MCL1GB" constant="no"/>
<attribute name="VENDOR" value="Nichicon" constant="no"/>
</technology>
</technologies>
</device>
<device name="PCAP_8X5.4-ELECT-L" package="PCAP_8X5.4-ELECT-L">
<connects>
<connect gate="A" pin="11" pad="1"/>
<connect gate="A" pin="22" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="UWX1A221MCL1GB" constant="no"/>
<attribute name="VENDOR" value="Nichicon" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CL03A104KQ3NNNC">
<description>&lt;Samsung Electro-Mechanics 0201 CL 220nF Ceramic Multilayer Capacitor, 6.3 V, +85C, X5R Dielectric, 10% SMD&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPC0603X33N">
<description>&lt;b&gt;CL03&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.33" y="0" dx="0.46" dy="0.42" layer="1"/>
<smd name="2" x="0.33" y="0" dx="0.46" dy="0.42" layer="1"/>
<text x="0" y="1.07" size="0.8128" layer="25" align="center">&gt;NAME</text>
<wire x1="-0.71" y1="0.36" x2="0.71" y2="0.36" width="0.05" layer="51"/>
<wire x1="0.71" y1="0.36" x2="0.71" y2="-0.36" width="0.05" layer="51"/>
<wire x1="0.71" y1="-0.36" x2="-0.71" y2="-0.36" width="0.05" layer="51"/>
<wire x1="-0.71" y1="-0.36" x2="-0.71" y2="0.36" width="0.05" layer="51"/>
<wire x1="-0.3" y1="0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="-0.7" y1="0.4" x2="-0.65" y2="0.4" width="0.1" layer="21"/>
<wire x1="-0.65" y1="0.4" x2="0.75" y2="0.4" width="0.1" layer="21"/>
<wire x1="0.75" y1="0.4" x2="0.75" y2="-0.4" width="0.1" layer="21"/>
<wire x1="0.75" y1="-0.4" x2="-0.75" y2="-0.4" width="0.1" layer="21"/>
<wire x1="-0.75" y1="-0.4" x2="-0.75" y2="0.4" width="0.1" layer="21"/>
<wire x1="-0.75" y1="0.4" x2="-0.7" y2="0.4" width="0.1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CL03A104KQ3NNNC">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<text x="-4.572" y="4.318" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<pin name="1" x="-3.302" y="0" visible="off" length="short"/>
<pin name="2" x="3.302" y="0" visible="off" length="short" rot="R180"/>
<text x="-5.08" y="-5.08" size="1.4224" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CL03A104KQ3NNNC" prefix="C">
<description>&lt;b&gt;Samsung Electro-Mechanics 0201 CL 220nF Ceramic Multilayer Capacitor, 6.3 V, +85C, X5R Dielectric, 10% SMD&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://media.digikey.com/pdf/Data Sheets/Samsung PDFs/CL_Series_MLCC_ds.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CL03A104KQ3NNNC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC0603X33N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="CL03A104KQ3NNNC" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/cl03a104kq3nnnc/samsung-electro-mechanics" constant="no"/>
<attribute name="DESCRIPTION" value="Samsung Electro-Mechanics 0201 CL 220nF Ceramic Multilayer Capacitor, 6.3 V, +85C, X5R Dielectric, 10% SMD" constant="no"/>
<attribute name="HEIGHT" value="0.33mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Samsung Electro-Mechanics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CL03A104KQ3NNNC" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7661255" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/7661255" constant="no"/>
<attribute name="VALUE" value="+6.3V, 100nF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C0603C104J5RACAUTO">
<description>&lt;KEMET - C0603C104J5RACAUTO - CAP, MLCC, X7R, 0.1UF, 50V, 0603&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPC1608X87N">
<description>&lt;b&gt;C0603C104J5RACAUTO&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.7" y="0" dx="0.98" dy="0.78" layer="1" rot="R90"/>
<smd name="2" x="0.7" y="0" dx="0.98" dy="0.78" layer="1" rot="R90"/>
<text x="0" y="1.44" size="0.8128" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.24" y1="0.64" x2="1.24" y2="0.64" width="0.05" layer="51"/>
<wire x1="1.24" y1="0.64" x2="1.24" y2="-0.64" width="0.05" layer="51"/>
<wire x1="1.24" y1="-0.64" x2="-1.24" y2="-0.64" width="0.05" layer="51"/>
<wire x1="-1.24" y1="-0.64" x2="-1.24" y2="0.64" width="0.05" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.1" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.1" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="C0603C104J5RACAUTO">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<text x="-3.81" y="3.81" size="1.4224" layer="95" align="center-left">&gt;NAME</text>
<text x="-4.064" y="-3.81" size="1.4224" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-3.302" y="0" visible="off" length="short"/>
<pin name="2" x="3.302" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C0603C104J5RACAUTO" prefix="C">
<description>&lt;b&gt;KEMET - C0603C104J5RACAUTO - CAP, MLCC, X7R, 0.1UF, 50V, 0603&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/C0603C104J5RACAUTO.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="C0603C104J5RACAUTO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC1608X87N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="C0603C104J5RACAUTO" constant="no"/>
<attribute name="ARROW_PRICE/STOCK" value="https://www.arrow.com/en/products/c0603c104j5racauto/kemet-corporation" constant="no"/>
<attribute name="DESCRIPTION" value="KEMET - C0603C104J5RACAUTO - CAP, MLCC, X7R, 0.1UF, 50V, 0603" constant="no"/>
<attribute name="HEIGHT" value="0.87mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="C0603C104J5RACAUTO" constant="no"/>
<attribute name="RS_PART_NUMBER" value="1734780" constant="no"/>
<attribute name="RS_PRICE/STOCK" value="http://uk.rs-online.com/web/p/products/1734780" constant="no"/>
<attribute name="VALUE" value="100nF, 50V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.046" drill="0.25">
<clearance class="0" value="0.1016"/>
</class>
<class number="1" name="normal" width="0.2" drill="0.25">
<clearance class="1" value="0.2"/>
</class>
</classes>
<parts>
<part name="U2" library="AA-IC" deviceset="EMIF02-USB03F2" device=""/>
<part name="R6" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="4.7K,1%"/>
<part name="R7" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="4.7K,1%"/>
<part name="C11" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="20pF,50V"/>
<part name="C8" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="20pF,50V"/>
<part name="R15" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="220,1%"/>
<part name="V26" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V33" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="U4" library="_RoweTel" deviceset="LD3985" device="" value="LD3985M33R"/>
<part name="V21" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="C14" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="1uF,16V"/>
<part name="V30" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V34" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V37" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V28" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C6" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="1uF,16V"/>
<part name="V25" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V22" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V45" library="A-SUPPLY2" deviceset="VDD" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="LED1" library="_RoweTel" deviceset="LTST-S220K" device="" value="LTST-S220KGKT"/>
<part name="LED2" library="_RoweTel" deviceset="LTST-S220K" device="" value="LTST-S220KRKT"/>
<part name="LED3" library="_RoweTel" deviceset="LTST-S220K" device="" value="LTST-S220KGKT"/>
<part name="LED4" library="_RoweTel" deviceset="LTST-S220K" device="" value="LTST-S220KRKT"/>
<part name="V4" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R1" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="680,1%"/>
<part name="R2" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="680,1%"/>
<part name="R3" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="680,1%"/>
<part name="R4" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="680,1%"/>
<part name="V7" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R10" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="22,1%"/>
<part name="R11" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="22,1%"/>
<part name="U1" library="_RoweTel" deviceset="STMPS2141" device=""/>
<part name="V6" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="C3" library="_RoweTel" deviceset="CAP-TANT-1206" device="" value="4.7uF,16V,TANT"/>
<part name="V14" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R12" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="47K,1%"/>
<part name="V13" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V130" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C74" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="1nF,25V,0402"/>
<part name="R44" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="2.2K,1%"/>
<part name="R47" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="2.2K,1%"/>
<part name="V146" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V136" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R48" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="2.2K,1%"/>
<part name="V134" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C76" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="47nF,25V"/>
<part name="R45" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0603" value="10,1%"/>
<part name="V138" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V148" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V133" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="LD1" library="_RoweTel" deviceset="LTV-817S" device=""/>
<part name="R60" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="60,1%"/>
<part name="V169" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V172" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V171" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C77" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="220nF,10V"/>
<part name="U10" library="_RoweTel" deviceset="LMV341" device=""/>
<part name="U9" library="_RoweTel" deviceset="LMV341" device=""/>
<part name="V162" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V161" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V167" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C90" library="SM1000" deviceset="CAP-1210" device="" value="100uF,10V"/>
<part name="B-FRAME1" library="_RoweTel" deviceset="GENERICL-B-SIZE" device=""/>
<part name="V126" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V150" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V154" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C93" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="100pF,50V"/>
<part name="C95" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="100pF,50V"/>
<part name="C94" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="100pF,50V"/>
<part name="U7" library="_RoweTel" deviceset="LM386?-*" device="M" technology="1"/>
<part name="T2" library="_RoweTel" deviceset="SM-LP-5001" device=""/>
<part name="T1" library="_RoweTel" deviceset="SM-LP-5001" device=""/>
<part name="U8" library="_RoweTel" deviceset="LMV341" device=""/>
<part name="R46" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="2.2K,1%"/>
<part name="V142" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V139" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C72" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="2.2uF,16V"/>
<part name="V129" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C87" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="2.2uF,16V"/>
<part name="V158" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V5" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V12" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V1" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V2" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V27" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V29" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="XTAL1" library="_RoweTel" deviceset="XTAL-FY0800035" device=""/>
<part name="P7" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P8" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P6" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="R41" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0603" value="100,1%"/>
<part name="V135" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V125" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V128" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="R50" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="1.5K,1%"/>
<part name="V145" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V147" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V144" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V141" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C81" library="_RoweTel" deviceset="NICHICON-WX-6.3" device="" value="100uF,6.3V,EL"/>
<part name="V140" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R43" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0603" value="100,1%"/>
<part name="C80" library="_RoweTel" deviceset="NICHICON-WX-6.3" device="" value="100uF,6.3V,EL"/>
<part name="R40" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="1206" value="4.7,0.5W,1206"/>
<part name="C73" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="3.9nF,16V,0402"/>
<part name="V132" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R37" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0603" value="22.1K,1%"/>
<part name="V127" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V153" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V157" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V160" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V159" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V163" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V168" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V170" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V181" library="A-SUPPLY2" deviceset="RF_MIC" device=""/>
<part name="V177" library="A-SUPPLY2" deviceset="RF_MIC" device=""/>
<part name="V179" library="A-SUPPLY2" deviceset="RF_SPKR" device=""/>
<part name="V180" library="A-SUPPLY2" deviceset="RF_SPKR" device=""/>
<part name="V178" library="A-SUPPLY2" deviceset="RF_PTT" device=""/>
<part name="V176" library="A-SUPPLY2" deviceset="RF_PTT" device=""/>
<part name="R53" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="47K,1%"/>
<part name="V166" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R52" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="1.5K,1%"/>
<part name="CN6" library="_RoweTel" deviceset="ST-3509" device=""/>
<part name="CN5" library="_RoweTel" deviceset="ST-3509" device=""/>
<part name="CN2" library="_RoweTel" deviceset="ST-3509" device=""/>
<part name="CN3" library="_RoweTel" deviceset="ST-3509" device=""/>
<part name="CN7" library="_RoweTel" deviceset="ST-3509" device=""/>
<part name="V143" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="D6" library="_RoweTel" deviceset="PESD5ZX" device="" value="DNL"/>
<part name="U3" library="SEcube" deviceset="DAC121S1_DEV" device="" package3d_urn="urn:adsk.eagle:package:2809113/1"/>
<part name="V19" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V20" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="HDR1" library="_Coherix" deviceset="HDR100-3" device=""/>
<part name="V3" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V42" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V44" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V131" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V113" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V102" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V109" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V110" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V112" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V114" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V116" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V118" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V119" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V120" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V121" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V105" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V104" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V101" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V100" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V99" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V98" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V97" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V92" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V87" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V122" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C25" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="22uF,25V"/>
<part name="V76" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V124" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="C63" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="2.2uF,25V"/>
<part name="V117" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C61" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="2.2uF,25V"/>
<part name="V115" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V83" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V88" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V93" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V84" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V89" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V94" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V85" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V90" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V95" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V86" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V91" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V96" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C84" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="2.2uF,16V"/>
<part name="V152" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V151" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="FD1" library="SEcube" deviceset="BORDER_TITLE" device=""/>
<part name="FD2" library="SEcube" deviceset="BORDER_TITLE" device=""/>
<part name="FD4" library="SEcube" deviceset="BORDER_TITLE" device=""/>
<part name="FD5" library="SEcube" deviceset="BORDER_TITLE" device=""/>
<part name="FD6" library="SEcube" deviceset="BORDER_TITLE" device=""/>
<part name="R28" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="2.2K,1%"/>
<part name="V60" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V54" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V61" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="CN1" library="_RoweTel" deviceset="ST-3509" device=""/>
<part name="D1" library="_RoweTel" deviceset="PESD5ZX" device="" value="DNL"/>
<part name="V56" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V149" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V64" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C22" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="2.2uF,16V"/>
<part name="V68" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V72" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V73" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V74" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V75" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="R61" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="1K,1%"/>
<part name="R51" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="1K,1%"/>
<part name="R54" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="1K,1%"/>
<part name="U5" library="SEcube" deviceset="AD5252BRUZ10-RL7" device="" package3d_urn="urn:adsk.eagle:package:2809114/1" value="AD5252BRUZ10-RL7"/>
<part name="J1" library="10104111-0001LF" deviceset="10104111-0001LF" device=""/>
<part name="P1" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P4" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="C23" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="1nF,25V,0402"/>
<part name="V69" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C24" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="1nF,25V,0402"/>
<part name="V70" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C5" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="100pF,50V"/>
<part name="V17" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V9" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C2" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="2.2uF,16V"/>
<part name="V11" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V165" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V164" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R49" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="2.2K,1%"/>
<part name="R62" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="1206" value="330,1%"/>
<part name="J5" library="_Coherix" deviceset="JMP-2PIN" device=""/>
<part name="FB3" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="FB9" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="FB7" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="FB8" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="FB5" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="FB6" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="P2" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P3" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="U11" library="SEcube" deviceset="LT8616EFEPBF" device="" package3d_urn="urn:adsk.eagle:package:2809121/1"/>
<part name="R65" library="SEcube" deviceset="ERA-3AEB473V" device="" package3d_urn="urn:adsk.eagle:package:2809124/1" value="47k"/>
<part name="V197" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V205" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="R66" library="SEcube" deviceset="ERA-6AEB105V" device="" package3d_urn="urn:adsk.eagle:package:2809136/1" value="1M"/>
<part name="R67" library="SEcube" deviceset="ERJ-3EKF2003V" device="" package3d_urn="urn:adsk.eagle:package:2809124/1" value="200k"/>
<part name="V202" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C103" library="SEcube" deviceset="CGA3E3X5R1H105K080AB" device="" package3d_urn="urn:adsk.eagle:package:2809139/1" value="1uF"/>
<part name="C104" library="SEcube" deviceset="CGA3E3X5R1H105K080AB" device="" package3d_urn="urn:adsk.eagle:package:2809139/1" value="1uF"/>
<part name="V201" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V200" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="L2" library="SEcube" deviceset="SRN4026" device="" package3d_urn="urn:adsk.eagle:package:2809145/1" value="3.3uH"/>
<part name="R69" library="SEcube" deviceset="ERA-6AEB105V" device="" package3d_urn="urn:adsk.eagle:package:2809136/1" value="1M"/>
<part name="R70" library="SEcube" deviceset="CRCW06031M91FKEA" device="" package3d_urn="urn:adsk.eagle:package:2809124/1" value="1.9M"/>
<part name="V204" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C109" library="SEcube" deviceset="GRM1885C1H100JA01D" device="" package3d_urn="urn:adsk.eagle:package:2809146/1" value="10pF"/>
<part name="L1" library="SEcube" deviceset="SRR6038-100Y" device="" package3d_urn="urn:adsk.eagle:package:2809154/1" value="10uH"/>
<part name="R71" library="SEcube" deviceset="ERA-6AEB105V" device="" package3d_urn="urn:adsk.eagle:package:2809136/1" value="1M"/>
<part name="C110" library="SEcube" deviceset="GRM1885C1H100JA01D" device="" package3d_urn="urn:adsk.eagle:package:2809146/1" value="10pF"/>
<part name="R72" library="SEcube" deviceset="ERA-6AEB1873V" device="" package3d_urn="urn:adsk.eagle:package:2809155/1" value="187K"/>
<part name="V214" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="C97" library="EEE-FK1H221P" deviceset="EEE-FK1H221P" device="" value="220uF"/>
<part name="V192" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="D8" library="_RoweTel" deviceset="BAT60J" device=""/>
<part name="V191" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="P17" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="D7" library="SEcube" deviceset="UCLAMP3671P" device="" package3d_urn="urn:adsk.eagle:package:2809162/1"/>
<part name="V190" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C113" library="C2012X5R1A336M125AC" deviceset="C2012X5R1A336M125AC" device="" value="33uF, CER, 20%, 10V"/>
<part name="C111" library="C2012X5R1A336M125AC" deviceset="C2012X5R1A336M125AC" device="" value="33uF, CER, 20%, 10V"/>
<part name="C112" library="C2012X5R1A336M125AC" deviceset="C2012X5R1A336M125AC" device="" value="33uF, CER, 20%, 10V"/>
<part name="C114" library="C2012X5R1A336M125AC" deviceset="C2012X5R1A336M125AC" device="" value="33uF, CER, 20%, 10V"/>
<part name="V206" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V207" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V209" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C115" library="C2012X5R1A336M125AC" deviceset="C2012X5R1A336M125AC" device="" value="33uF, CER, 20%, 10V"/>
<part name="V210" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V208" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="F1" library="SEcube" deviceset="C1Q_1" device=""/>
<part name="FB10" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="U6" library="SEcube" deviceset="SECUBE_DEV" device="" package3d_urn="urn:adsk.eagle:package:2809111/1"/>
<part name="V71" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="FD7" library="SEcube" deviceset="BORDER_TITLE" device=""/>
<part name="FD9" library="SEcube" deviceset="BORDER_TITLE" device=""/>
<part name="U14" library="SI2305CDS-T1-GE3" deviceset="SI2305CDS-T1-GE3" device=""/>
<part name="R34" library="SEcube" deviceset="ERA-3AEB473V" device="" package3d_urn="urn:adsk.eagle:package:2809124/1" value="47k"/>
<part name="R35" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="1206" value="330,1%"/>
<part name="V103" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V106" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V107" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V108" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V57" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V50" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="U13" library="AD5241BRZ100" deviceset="AD5241BRZ100" device=""/>
<part name="V62" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="U12" library="AD5241BRZ1M" deviceset="AD5241BRZ1M" device=""/>
<part name="V18" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="LED5" library="LTST-C193TBKT-5A" deviceset="LTST-C193TBKT-5A" device=""/>
<part name="R23" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="1k, 1%"/>
<part name="V38" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="LED6" library="_RoweTel" deviceset="SML-LX0603SRW" device="" value="SML-LX0603SRW"/>
<part name="FB1" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="V16" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V32" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C13" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="2.2uF,16V"/>
<part name="V36" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V31" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V35" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="V59" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V66" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C116" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="22uF,25V"/>
<part name="V211" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C118" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0402" value="22uF,25V"/>
<part name="V213" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="FD10" library="SEcube" deviceset="1.2_VDD" device=""/>
<part name="FD3" library="SEcube" deviceset="1.2_VDD" device=""/>
<part name="R38" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="1K,1%"/>
<part name="R39" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="1K,1%"/>
<part name="C21" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="2.2uF,16V"/>
<part name="R8" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R13" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R25" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R29" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R30" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R31" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R32" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R33" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R36" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R42" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R64" library="CRCW06030000Z0EA" deviceset="CRCW06030000Z0EA" device="" value="0 Ohm"/>
<part name="R14" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R27" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R26" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R9" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R55" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R56" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R58" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R63" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="C96" library="C0603C103J5RACTU" deviceset="C0603C103J5RACTU" device="" value="10nF, 50V"/>
<part name="C9" library="C0603C103J5RACTU" deviceset="C0603C103J5RACTU" device="" value="10nF, 50V"/>
<part name="C105" library="C0603C103J5RACTU" deviceset="C0603C103J5RACTU" device="" value="10nF, 50V"/>
<part name="C53" library="SEcube" deviceset="CGA3E3X5R1H105K080AB" device="" package3d_urn="urn:adsk.eagle:package:2809139/1" value="1uF"/>
<part name="C106" library="SEcube" deviceset="CGA3E3X5R1H105K080AB" device="" package3d_urn="urn:adsk.eagle:package:2809139/1" value="1uF"/>
<part name="R57" library="CPF0603B470KE1" deviceset="CPF0603B470KE1" device="" value="470K,0.1%"/>
<part name="R59" library="CPF0603B470KE1" deviceset="CPF0603B470KE1" device="" value="470K,0.1%"/>
<part name="C88" library="SEcube" deviceset="CB042D0684JBC" device="" value="POLY,0.68UF,5%"/>
<part name="C92" library="SEcube" deviceset="UES1C101MPM1TD" device="" value="100uF, BIPOLAR ELEC, 16V"/>
<part name="C91" library="SEcube" deviceset="UES1C101MPM1TD" device="" value="100uF, BIPOLAR ELEC, 16V"/>
<part name="V203" library="A-SUPPLY2" deviceset="VDD" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="R68" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="4.7K,1%"/>
<part name="V193" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V194" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="P18" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P14" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P15" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P10" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="V186" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V188" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V183" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="P11" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="FD8" library="SEcube" deviceset="1.2_VDD" device=""/>
<part name="V184" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="P12" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P13" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="V185" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C15" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="1210" value="22uF,25V,1210"/>
<part name="V46" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V47" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V49" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V39" library="A-SUPPLY2" deviceset="VDD" device="">
<attribute name="PREFIX" value="PS"/>
</part>
<part name="V51" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="FB2" library="BLM21PG221SN1D" deviceset="BLM21PG221SN1D" device=""/>
<part name="C16" library="SEcube" deviceset="CGA3E3X5R1H105K080AB" device="" package3d_urn="urn:adsk.eagle:package:2809139/1" value="1uF"/>
<part name="D5" library="ESD9B3.3ST5G" deviceset="ESD9B3.3ST5G" device=""/>
<part name="D4" library="ESD9B3.3ST5G" deviceset="ESD9B3.3ST5G" device=""/>
<part name="D2" library="ESD9B3.3ST5G" deviceset="ESD9B3.3ST5G" device=""/>
<part name="D3" library="ESD9B3.3ST5G" deviceset="ESD9B3.3ST5G" device=""/>
<part name="V24" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V23" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="R19" library="AA-ipc-7351-resistor" deviceset="RESISTOR_" device="0402" value="22,1%"/>
<part name="V40" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="J3" library="_Coherix" deviceset="JMP-2PIN" device=""/>
<part name="R16" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R17" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R22" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R24" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R18" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R20" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R21" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="V52" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V53" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="J6" library="282836-3" deviceset="282836-3" device=""/>
<part name="V187" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V156" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V155" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="C86" library="GRM033R61E104KE14J" deviceset="GRM033R61E104KE14J" device="" value="100nF, 25V"/>
<part name="R5" library="RC0201FR-07100RL" deviceset="RC0201FR-07100RL" device="" value="100, 1%"/>
<part name="SW1" library="_RoweTel" deviceset="TS-02B" device=""/>
<part name="C99" library="SEcube" deviceset="C3216JB1V226M160AC" device="" package3d_urn="urn:adsk.eagle:package:2809158/1" value="22uF,35V"/>
<part name="C100" library="SEcube" deviceset="C3216JB1V226M160AC" device="" package3d_urn="urn:adsk.eagle:package:2809158/1" value="22uF,35V"/>
<part name="V196" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V195" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C101" library="CGA5L1X7R1V106K160AE" deviceset="CGA5L1X7R1V106K160AE" device="" value="10uF,35V"/>
<part name="C102" library="CGA5L1X7R1V106K160AE" deviceset="CGA5L1X7R1V106K160AE" device="" value="10uF,35V"/>
<part name="V199" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V198" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C117" library="C2012X5R1A336M125AC" deviceset="C2012X5R1A336M125AC" device="" value="33uF, CER, 20%, 10V"/>
<part name="V212" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C54" library="CGA5L1X7R1V106K160AE" deviceset="CGA5L1X7R1V106K160AE" device="" value="10uF,35V"/>
<part name="C69" library="CGA5L1X7R1V106K160AE" deviceset="CGA5L1X7R1V106K160AE" device="" value="10uF,35V"/>
<part name="V123" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C36" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C41" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C46" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C47" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C48" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C49" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C50" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C51" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C52" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C32" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C37" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C42" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C33" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C38" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C43" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C34" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C39" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C44" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C35" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C40" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C45" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C56" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C57" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C59" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C60" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C62" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C64" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C65" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C66" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C67" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C68" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="C31" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="V82" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C58" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="V111" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C119" library="AA-ipc-7351-capacitor" deviceset="CAPACITOR_" device="0603" value="2.2uF,16V"/>
<part name="V215" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V216" library="A-SUPPLY2" deviceset="AVDD" device=""/>
<part name="C120" library="_RoweTel" deviceset="NICHICON-WX-6.3" device="" value="100uF,6.3V,EL"/>
<part name="V173" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C121" library="UWX1A221MCL1GB" deviceset="UWX1A221MCL1GB" device=""/>
<part name="V174" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C75" library="UWX1A221MCL1GB" deviceset="UWX1A221MCL1GB" device=""/>
<part name="V78" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="P20" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="P24" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="V137" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V8" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V10" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V15" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V41" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V43" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V55" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V58" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V63" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V65" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V48" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V67" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V77" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="V81" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="R73" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="R74" library="CRCW040210K0FKEDHP" deviceset="CRCW040210K0FKEDHP" device="" value="10k, 1%"/>
<part name="V80" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="P22" library="_Coherix" deviceset="TPAD50" device=""/>
<part name="J9" library="SEcube" deviceset="TSW-110-14-F-D" device=""/>
<part name="J7" library="SEcube" deviceset="TSW-110-14-F-D" device=""/>
<part name="J2" library="SEcube" deviceset="67997-116HLF" device=""/>
<part name="C26" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="V79" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V175" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="C28" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="V217" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V218" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="C29" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="V219" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V220" library="A-SUPPLY2" deviceset="VDD" device=""/>
<part name="C27" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="V182" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="C30" library="GRM022R60J104ME15L" deviceset="GRM022R60J104ME15L" device="" value="GRM022R60J104ME15L"/>
<part name="V221" library="A-SUPPLY2" deviceset="GND" device=""/>
<part name="V189" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="V222" library="A-SUPPLY2" deviceset="+5V" device=""/>
<part name="C1" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C4" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C7" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C10" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C12" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C17" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C18" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C19" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C20" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C55" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C70" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C71" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C78" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C79" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C82" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C83" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C85" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C89" library="CL03A104KQ3NNNC" deviceset="CL03A104KQ3NNNC" device="" value="+6.3V, 100nF"/>
<part name="C98" library="C0603C104J5RACAUTO" deviceset="C0603C104J5RACAUTO" device="" value="100nF, 50V"/>
<part name="C107" library="C0603C104J5RACAUTO" deviceset="C0603C104J5RACAUTO" device="" value="100nF, 50V"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="58.42" y1="276.86" x2="58.42" y2="139.7" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="223.52" x2="279.4" y2="223.52" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="223.52" x2="180.34" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="172.72" x2="180.34" y2="139.7" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="139.7" x2="180.34" y2="116.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="350.52" y1="35.56" x2="350.52" y2="116.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="350.52" y1="116.84" x2="314.96" y2="116.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="314.96" y1="116.84" x2="180.34" y2="116.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="279.4" y1="172.72" x2="180.34" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<text x="5.08" y="271.78" size="2.54" layer="94" ratio="10">PULLUPS</text>
<text x="7.62" y="231.14" size="2.54" layer="94" ratio="10">LEDs</text>
<text x="190.5" y="210.82" size="2.54" layer="94" ratio="10">OSCILLATOR CRYSTAL</text>
<text x="193.04" y="165.1" size="2.54" layer="94" ratio="10">5.0V -&gt; 3.3V PS</text>
<text x="38.1" y="114.3" size="2.54" layer="94" ratio="10">OTG USB INTERFACE</text>
<text x="347.472" y="17.272" size="3.81" layer="94" ratio="10">New Circuit Board</text>
<text x="420.624" y="6.35" size="3.81" layer="94" ratio="10">A</text>
<text x="30.48" y="226.06" size="1.6764" layer="94" ratio="10">Power </text>
<text x="30.48" y="215.9" size="1.6764" layer="94" ratio="10">PTT</text>
<text x="30.48" y="205.74" size="1.6764" layer="94" ratio="10">Sync</text>
<text x="27.94" y="195.58" size="1.6764" layer="94" ratio="10">Clip/Error</text>
<wire x1="279.4" y1="223.52" x2="279.4" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<text x="243.84" y="210.82" size="1.4224" layer="94" ratio="10">8.000MHz</text>
<text x="43.18" y="215.9" size="1.4224" layer="94" ratio="10">LED,GREEN,RA</text>
<text x="43.18" y="195.58" size="1.4224" layer="94" ratio="10">LED,GREEN,RA</text>
<text x="43.18" y="205.74" size="1.4224" layer="94" ratio="10">LED,RED,RA</text>
<text x="43.18" y="185.42" size="1.4224" layer="94" ratio="10">LED,RED,RA</text>
<wire x1="279.4" y1="172.72" x2="314.96" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="314.96" y1="172.72" x2="431.8" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="350.52" y1="116.84" x2="431.8" y2="116.84" width="0.1524" layer="94" style="longdash"/>
<text x="27.94" y="177.8" size="1.6764" layer="94" ratio="10">Debug</text>
<text x="12.7" y="165.1" size="2.54" layer="94" ratio="10">UART</text>
<text x="12.7" y="160.02" size="1.4224" layer="94" ratio="10">3.3V TTL Levels</text>
<wire x1="314.96" y1="172.72" x2="314.96" y2="116.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="223.52" x2="180.34" y2="279.4" width="0.1524" layer="94" style="longdash"/>
<text x="195.58" y="266.7" size="2.54" layer="94">EXTERNAL_DAC</text>
<text x="330.2" y="218.44" size="2.54" layer="94">RIG_DIGIPOT</text>
<wire x1="279.4" y1="223.52" x2="281.94" y2="223.52" width="0.1524" layer="94" style="longdash"/>
<text x="360.68" y="96.52" size="2.54" layer="94" ratio="10">EXT. PTT INPUT</text>
<text x="403.86" y="81.28" size="1.4224" layer="94" ratio="10" rot="R180">PESD5Z3.3</text>
<wire x1="281.94" y1="223.52" x2="355.6" y2="223.52" width="0.1524" layer="94" style="longdash"/>
<wire x1="355.6" y1="223.52" x2="431.8" y2="223.52" width="0.1524" layer="94" style="longdash"/>
<wire x1="355.6" y1="264.16" x2="355.6" y2="223.52" width="0.1524" layer="94"/>
<text x="320.04" y="165.1" size="2.54" layer="94" ratio="10">EXT_SPKR_DIGIPOT</text>
<text x="73.66" y="172.72" size="2.54" layer="94" ratio="10">EXT_MIC_DIGIPOT</text>
<text x="365.76" y="167.64" size="1.778" layer="97">Address:  "0101 111"</text>
<text x="137.16" y="172.72" size="1.778" layer="97">Address: "0101 100"</text>
<text x="363.22" y="175.26" size="1.778" layer="97">Address:  "0101 101"</text>
<text x="289.56" y="132.08" size="1.4224" layer="94" ratio="10">SML-LX0603SRW-TR</text>
<text x="289.56" y="266.7" size="2.54" layer="94" ratio="10">VDD -&gt; AVDD POWER SOURCE</text>
<wire x1="281.94" y1="279.4" x2="281.94" y2="223.52" width="0.1524" layer="94" style="longdash"/>
<text x="220.98" y="101.6" size="2.54" layer="94" ratio="10">JTAG CONNECTOR</text>
<wire x1="58.42" y1="139.7" x2="180.34" y2="139.7" width="0.1524" layer="94" style="longdash"/>
<wire x1="58.42" y1="139.7" x2="58.42" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="58.42" y1="134.62" x2="0" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="116.84" x2="180.34" y2="0" width="0.1524" layer="94" style="longdash"/>
<text x="137.16" y="167.64" size="1.778" layer="97">I_MAX: 50u/100uA</text>
<text x="365.76" y="165.1" size="1.778" layer="97">I_MAX: 50u/100uA</text>
<text x="388.62" y="175.26" size="1.778" layer="97">I_MAX: 35mA</text>
</plain>
<instances>
<instance part="U2" gate="G$1" x="91.44" y="38.1" smashed="yes">
<attribute name="NAME" x="81.28" y="48.26" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="81.28" y="20.32" size="1.4224" layer="95" ratio="10"/>
</instance>
<instance part="R6" gate="G$1" x="22.86" y="264.16" smashed="yes">
<attribute name="NAME" x="17.78" y="266.7" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="22.86" y="266.7" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R7" gate="G$1" x="22.86" y="256.54" smashed="yes">
<attribute name="NAME" x="17.78" y="259.08" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="22.86" y="259.08" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C11" gate="G$1" x="259.08" y="193.04" smashed="yes" rot="R270">
<attribute name="NAME" x="261.62" y="187.96" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="261.62" y="185.42" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C8" gate="G$1" x="218.44" y="193.04" smashed="yes" rot="R270">
<attribute name="NAME" x="210.82" y="193.04" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="210.82" y="190.5" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R15" gate="G$1" x="210.82" y="200.66" smashed="yes">
<attribute name="NAME" x="208.28" y="205.74" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="213.36" y="203.2" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V26" gate="GND" x="218.44" y="182.88" smashed="yes">
<attribute name="VALUE" x="215.9" y="179.578" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V33" gate="GND" x="259.08" y="182.88" smashed="yes">
<attribute name="VALUE" x="256.54" y="179.578" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U4" gate="G$1" x="236.22" y="149.86" smashed="yes">
<attribute name="NAME" x="226.06" y="160.02" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="233.68" y="160.02" size="1.4224" layer="96" ratio="10"/>
</instance>
<instance part="V21" gate="G$1" x="195.58" y="154.94" smashed="yes" rot="R90">
<attribute name="VALUE" x="194.945" y="152.527" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="C14" gate="G$1" x="276.86" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="276.86" y="160.02" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="274.32" y="157.48" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V30" gate="GND" x="256.54" y="129.54" smashed="yes">
<attribute name="VALUE" x="254" y="126.238" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V34" gate="GND" x="266.7" y="139.7" smashed="yes">
<attribute name="VALUE" x="264.16" y="136.398" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V37" gate="GND" x="276.86" y="139.7" smashed="yes">
<attribute name="VALUE" x="274.32" y="136.398" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V28" gate="GND" x="236.22" y="129.54" smashed="yes">
<attribute name="VALUE" x="233.68" y="126.238" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C6" gate="G$1" x="203.2" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="193.04" y="142.24" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="193.04" y="139.7" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V25" gate="GND" x="215.9" y="127" smashed="yes">
<attribute name="VALUE" x="213.36" y="123.698" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V22" gate="GND" x="203.2" y="129.54" smashed="yes">
<attribute name="VALUE" x="200.66" y="126.238" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V45" gate="G$1" x="307.34" y="154.94" smashed="yes" rot="R270">
<attribute name="VALUE" x="310.388" y="157.48" size="1.4224" layer="95" ratio="9" rot="R270"/>
</instance>
<instance part="LED1" gate="G$1" x="30.48" y="223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="40.64" y="220.98" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="40.64" y="218.44" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="LED2" gate="G$1" x="30.48" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="40.64" y="210.82" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="40.64" y="208.28" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="LED3" gate="G$1" x="30.48" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="40.64" y="200.66" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="40.64" y="198.12" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="LED4" gate="G$1" x="30.48" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="40.64" y="190.5" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="40.64" y="187.96" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V4" gate="GND" x="38.1" y="175.26" smashed="yes">
<attribute name="VALUE" x="35.56" y="171.958" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R1" gate="G$1" x="20.32" y="223.52" smashed="yes">
<attribute name="NAME" x="15.24" y="226.06" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="20.32" y="226.06" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R2" gate="G$1" x="20.32" y="213.36" smashed="yes">
<attribute name="NAME" x="15.24" y="215.9" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="20.32" y="215.9" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R3" gate="G$1" x="20.32" y="203.2" smashed="yes">
<attribute name="NAME" x="15.24" y="205.74" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="20.32" y="205.74" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R4" gate="G$1" x="20.32" y="193.04" smashed="yes">
<attribute name="NAME" x="15.24" y="195.58" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="20.32" y="195.58" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V7" gate="GND" x="78.74" y="76.2" smashed="yes">
<attribute name="VALUE" x="76.2" y="72.898" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R10" gate="G$1" x="78.74" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="73.66" y="63.5" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="78.74" y="63.5" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R11" gate="G$1" x="91.44" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="83.82" y="58.42" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="88.9" y="58.42" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U1" gate="G$1" x="78.74" y="91.44" smashed="yes">
<attribute name="NAME" x="68.58" y="99.06" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="81.28" y="99.06" size="1.4224" layer="95" ratio="10"/>
</instance>
<instance part="V6" gate="G$1" x="60.96" y="101.6" smashed="yes">
<attribute name="VALUE" x="58.547" y="102.235" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C3" gate="G$1" x="114.3" y="88.9" smashed="yes">
<attribute name="NAME" x="109.22" y="96.52" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="114.3" y="96.52" size="1.4224" layer="96" ratio="10"/>
</instance>
<instance part="V14" gate="GND" x="114.3" y="78.74" smashed="yes">
<attribute name="VALUE" x="111.76" y="75.438" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R12" gate="G$1" x="99.06" y="101.6" smashed="yes" rot="R270">
<attribute name="NAME" x="96.52" y="96.52" size="1.4224" layer="95" ratio="9" rot="R90"/>
<attribute name="VALUE" x="96.52" y="101.6" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="V13" gate="GND" x="109.22" y="20.32" smashed="yes">
<attribute name="VALUE" x="106.68" y="17.018" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="B-FRAME1" gate="G$3" x="358.14" y="279.4" smashed="yes"/>
<instance part="V5" gate="G$1" x="53.34" y="104.14" smashed="yes">
<attribute name="VALUE" x="50.8" y="107.188" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="V12" gate="G$1" x="99.06" y="109.22" smashed="yes">
<attribute name="VALUE" x="96.52" y="112.268" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="V1" gate="G$1" x="10.16" y="264.16" smashed="yes" rot="R90">
<attribute name="VALUE" x="7.112" y="261.62" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="V2" gate="G$1" x="10.16" y="256.54" smashed="yes" rot="R90">
<attribute name="VALUE" x="7.112" y="254" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="V27" gate="GND" x="226.06" y="182.88" smashed="yes">
<attribute name="VALUE" x="223.52" y="179.578" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V29" gate="GND" x="251.46" y="182.88" smashed="yes">
<attribute name="VALUE" x="248.92" y="179.578" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="XTAL1" gate="G$1" x="238.76" y="200.66" smashed="yes">
<attribute name="NAME" x="236.22" y="210.82" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="231.14" y="190.5" size="1.4224" layer="96" ratio="10"/>
</instance>
<instance part="U3" gate="A" x="220.98" y="241.3" smashed="yes">
<attribute name="NAME" x="210.82" y="226.06" size="1.778" layer="94"/>
</instance>
<instance part="V19" gate="GND" x="190.5" y="236.22" smashed="yes">
<attribute name="VALUE" x="187.96" y="232.918" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V20" gate="G$1" x="195.58" y="236.22" smashed="yes" rot="R90">
<attribute name="VALUE" x="194.564" y="234.696" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="HDR1" gate="G$1" x="33.02" y="154.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="32.8499" y="164.063" size="1.4224" layer="95" ratio="9" rot="MR180"/>
<attribute name="VALUE" x="33.1039" y="161.523" size="1.4224" layer="96" ratio="9" rot="MR180"/>
</instance>
<instance part="V3" gate="GND" x="30.48" y="144.78" smashed="yes">
<attribute name="VALUE" x="27.94" y="141.478" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V42" gate="GND" x="302.26" y="200.66" smashed="yes">
<attribute name="VALUE" x="299.72" y="197.358" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V44" gate="GND" x="302.26" y="182.88" smashed="yes">
<attribute name="VALUE" x="299.72" y="179.578" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="FD1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FD1" gate="G$2" x="330.2" y="0" smashed="yes">
<attribute name="SHEET" x="416.56" y="1.27" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="R28" gate="G$1" x="419.1" y="78.74" smashed="yes">
<attribute name="NAME" x="416.56" y="83.82" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="414.02" y="81.28" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V60" gate="G$1" x="406.4" y="96.52" smashed="yes">
<attribute name="VALUE" x="403.86" y="99.568" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="V54" gate="GND" x="391.16" y="58.42" smashed="yes">
<attribute name="VALUE" x="388.62" y="55.118" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V61" gate="GND" x="406.4" y="58.42" smashed="yes">
<attribute name="VALUE" x="403.86" y="55.118" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="CN1" gate="G$1" x="383.54" y="71.12" smashed="yes" rot="MR180">
<attribute name="NAME" x="368.3" y="60.96" size="1.4224" layer="95" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="368.3" y="83.82" size="1.4224" layer="95" ratio="10" rot="MR180"/>
</instance>
<instance part="D1" gate="G$1" x="398.78" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="393.7" y="71.12" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="393.954" y="68.834" size="1.4224" layer="96" ratio="10"/>
</instance>
<instance part="V56" gate="GND" x="398.78" y="63.5" smashed="yes">
<attribute name="VALUE" x="396.24" y="60.198" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V64" gate="GND" x="414.02" y="147.32" smashed="yes">
<attribute name="VALUE" x="411.48" y="144.018" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C22" gate="G$1" x="421.64" y="157.48" smashed="yes" rot="R270">
<attribute name="NAME" x="421.64" y="152.4" size="1.4224" layer="95"/>
<attribute name="VALUE" x="421.64" y="149.86" size="1.4224" layer="96"/>
</instance>
<instance part="V68" gate="GND" x="421.64" y="147.32" smashed="yes">
<attribute name="VALUE" x="419.1" y="144.018" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U5" gate="A" x="342.9" y="198.12" smashed="yes">
<attribute name="NAME" x="330.2" y="177.8" size="1.778" layer="95"/>
<attribute name="VALUE" x="330.2" y="175.26" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="152.4" y="81.28" smashed="yes">
<attribute name="NAME" x="146.05" y="101.6" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="146.05" y="99.06" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C5" gate="G$1" x="147.32" y="223.52" smashed="yes">
<attribute name="NAME" x="149.86" y="226.06" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="144.78" y="218.44" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V17" gate="GND" x="162.56" y="190.5" smashed="yes">
<attribute name="VALUE" x="160.02" y="187.198" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V9" gate="GND" x="76.2" y="200.66" smashed="yes">
<attribute name="VALUE" x="73.66" y="197.358" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C2" gate="G$1" x="86.36" y="210.82" smashed="yes" rot="R270">
<attribute name="NAME" x="91.44" y="215.9" size="1.4224" layer="95"/>
<attribute name="VALUE" x="91.44" y="213.36" size="1.4224" layer="96"/>
</instance>
<instance part="V11" gate="GND" x="86.36" y="200.66" smashed="yes">
<attribute name="VALUE" x="83.82" y="197.358" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V57" gate="GND" x="403.86" y="129.54" smashed="yes">
<attribute name="VALUE" x="401.32" y="126.238" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V50" gate="GND" x="337.82" y="124.46" smashed="yes">
<attribute name="VALUE" x="335.28" y="121.158" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U13" gate="G$1" x="360.68" y="144.78" smashed="yes">
<attribute name="NAME" x="361.95" y="154.94" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="361.95" y="152.4" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="V62" gate="GND" x="408.94" y="127" smashed="yes">
<attribute name="VALUE" x="406.4" y="123.698" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U12" gate="G$1" x="119.38" y="203.2" smashed="yes">
<attribute name="NAME" x="146.05" y="210.82" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="146.05" y="208.28" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="V18" gate="GND" x="170.18" y="198.12" smashed="yes">
<attribute name="VALUE" x="167.64" y="194.818" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="LED5" gate="G$1" x="30.48" y="182.88" smashed="yes" rot="R180">
<attribute name="NAME" x="35.56" y="173.99" size="1.4224" layer="95" rot="R180"/>
<attribute name="VALUE" x="35.56" y="176.53" size="1.4224" layer="96" rot="R180"/>
</instance>
<instance part="R23" gate="G$1" x="287.02" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="289.56" y="149.86" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="289.56" y="147.32" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V38" gate="GND" x="287.02" y="127" smashed="yes">
<attribute name="VALUE" x="284.48" y="123.698" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="LED6" gate="G$1" x="287.02" y="137.16" smashed="yes">
<attribute name="NAME" x="289.56" y="134.62" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="289.56" y="132.08" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="FB1" gate="G$1" x="144.78" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="149.098" y="54.61" size="1.4224" layer="95" rot="R270" align="center-left"/>
<attribute name="VALUE" x="146.812" y="54.864" size="1.4224" layer="96" rot="R270" align="center-left"/>
</instance>
<instance part="V16" gate="GND" x="139.7" y="33.02" smashed="yes">
<attribute name="VALUE" x="137.16" y="29.718" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V32" gate="GND" x="259.08" y="251.46" smashed="yes">
<attribute name="VALUE" x="256.54" y="248.158" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C13" gate="G$1" x="271.78" y="261.62" smashed="yes" rot="R270">
<attribute name="NAME" x="264.16" y="259.08" size="1.4224" layer="95"/>
<attribute name="VALUE" x="264.16" y="256.54" size="1.4224" layer="96"/>
</instance>
<instance part="V36" gate="GND" x="271.78" y="251.46" smashed="yes">
<attribute name="VALUE" x="269.24" y="248.158" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V31" gate="G$1" x="259.08" y="271.78" smashed="yes">
<attribute name="VALUE" x="257.556" y="272.796" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V35" gate="G$1" x="271.78" y="271.78" smashed="yes">
<attribute name="VALUE" x="270.256" y="272.796" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V59" gate="GND" x="396.24" y="187.96" smashed="yes">
<attribute name="VALUE" x="393.7" y="184.658" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V66" gate="GND" x="406.4" y="187.96" smashed="yes">
<attribute name="VALUE" x="403.86" y="184.658" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C21" gate="G$1" x="406.4" y="200.66" smashed="yes" rot="R270">
<attribute name="NAME" x="406.4" y="195.58" size="1.4224" layer="95"/>
<attribute name="VALUE" x="406.4" y="193.04" size="1.4224" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="45.72" y="88.9" smashed="yes">
<attribute name="NAME" x="39.37" y="92.964" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="39.37" y="91.186" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R13" gate="G$1" x="104.14" y="50.8" smashed="yes">
<attribute name="NAME" x="97.79" y="54.864" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="97.79" y="53.086" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R25" gate="G$1" x="299.72" y="154.94" smashed="yes">
<attribute name="NAME" x="293.37" y="159.004" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="293.37" y="157.226" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R14" gate="G$1" x="162.56" y="203.2" smashed="yes">
<attribute name="NAME" x="156.464" y="202.184" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="156.464" y="200.406" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R27" gate="G$1" x="408.94" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="409.956" y="133.604" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="411.734" y="133.604" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R26" gate="G$1" x="406.4" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="407.416" y="82.804" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="409.194" y="82.804" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R9" gate="G$1" x="53.34" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="54.356" y="90.424" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="56.134" y="90.424" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C9" gate="G$1" x="256.54" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="260.35" y="133.35" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="260.35" y="131.318" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="C15" gate="G$1" x="314.96" y="248.92" smashed="yes" rot="R270">
<attribute name="NAME" x="317.5" y="251.46" size="1.4224" layer="95" ratio="9" rot="R270"/>
<attribute name="VALUE" x="309.88" y="251.46" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V46" gate="GND" x="314.96" y="238.76" smashed="yes">
<attribute name="VALUE" x="312.42" y="235.458" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V47" gate="GND" x="325.12" y="238.76" smashed="yes">
<attribute name="VALUE" x="322.58" y="235.458" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V49" gate="GND" x="335.28" y="238.76" smashed="yes">
<attribute name="VALUE" x="332.74" y="235.458" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V39" gate="G$1" x="294.64" y="256.54" smashed="yes" rot="R90">
<attribute name="PREFIX" x="294.64" y="256.54" size="1.4224" layer="96" rot="R90" display="off"/>
<attribute name="VALUE" x="291.592" y="254" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="V51" gate="G$1" x="345.44" y="256.54" smashed="yes" rot="R270">
<attribute name="VALUE" x="346.456" y="258.064" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="FB2" gate="G$1" x="304.8" y="256.54" smashed="yes" rot="R180">
<attribute name="NAME" x="308.61" y="252.222" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="308.864" y="254.508" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="C16" gate="A" x="325.12" y="248.92" smashed="yes" rot="R90">
<attribute name="NAME" x="322.3672" y="244.6836" size="1.27" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="328.85" y="244.27" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="V24" gate="GND" x="210.82" y="38.1" smashed="yes">
<attribute name="VALUE" x="210.82" y="34.798" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V23" gate="G$1" x="210.82" y="86.36" smashed="yes">
<attribute name="VALUE" x="210.82" y="89.408" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="R19" gate="G$1" x="276.86" y="48.26" smashed="yes" rot="R180">
<attribute name="NAME" x="269.24" y="48.26" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="281.94" y="48.26" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V40" gate="GND" x="299.72" y="38.1" smashed="yes">
<attribute name="VALUE" x="299.72" y="34.798" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="J3" gate="G$1" x="312.42" y="48.26" smashed="yes" rot="R180">
<attribute name="NAME" x="314.96" y="45.72" size="1.4224" layer="95"/>
<attribute name="VALUE" x="314.96" y="43.18" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R16" gate="G$1" x="264.16" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="266.192" y="67.818" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="267.97" y="67.818" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R17" gate="G$1" x="271.78" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="273.812" y="67.818" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="275.59" y="67.818" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R22" gate="G$1" x="279.4" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="281.432" y="67.818" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="283.21" y="67.818" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R24" gate="G$1" x="287.02" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="289.052" y="67.818" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="290.83" y="67.818" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R18" gate="G$1" x="276.86" y="53.34" smashed="yes">
<attribute name="NAME" x="268.478" y="54.61" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="280.162" y="54.61" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R20" gate="G$1" x="276.86" y="45.72" smashed="yes">
<attribute name="NAME" x="268.478" y="46.99" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="280.162" y="46.99" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R21" gate="G$1" x="276.86" y="43.18" smashed="yes">
<attribute name="NAME" x="268.478" y="44.45" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="280.162" y="44.45" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="V52" gate="GND" x="386.08" y="198.12" smashed="yes">
<attribute name="VALUE" x="383.54" y="194.818" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V53" gate="GND" x="386.08" y="182.88" smashed="yes">
<attribute name="VALUE" x="383.54" y="179.578" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R5" gate="G$1" x="20.32" y="182.88" smashed="yes">
<attribute name="NAME" x="16.51" y="185.42" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="16.256" y="180.086" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="V8" gate="G$1" x="76.2" y="223.52" smashed="yes">
<attribute name="VALUE" x="73.787" y="224.155" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V10" gate="G$1" x="86.36" y="223.52" smashed="yes">
<attribute name="VALUE" x="83.947" y="224.155" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V15" gate="G$1" x="109.22" y="226.06" smashed="yes">
<attribute name="VALUE" x="106.807" y="226.695" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V41" gate="G$1" x="302.26" y="215.9" smashed="yes">
<attribute name="VALUE" x="299.847" y="216.535" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V43" gate="G$1" x="302.26" y="193.04" smashed="yes">
<attribute name="VALUE" x="299.847" y="193.675" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V55" gate="G$1" x="396.24" y="213.36" smashed="yes">
<attribute name="VALUE" x="393.827" y="213.995" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V58" gate="G$1" x="406.4" y="213.36" smashed="yes">
<attribute name="VALUE" x="403.987" y="213.995" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V63" gate="G$1" x="414.02" y="167.64" smashed="yes">
<attribute name="VALUE" x="411.607" y="168.275" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V65" gate="G$1" x="421.64" y="167.64" smashed="yes">
<attribute name="VALUE" x="419.227" y="168.275" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V48" gate="G$1" x="332.74" y="152.4" smashed="yes">
<attribute name="VALUE" x="330.327" y="153.035" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V67" gate="G$1" x="398.78" y="160.02" smashed="yes">
<attribute name="VALUE" x="396.367" y="160.655" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V77" gate="G$1" x="10.16" y="248.92" smashed="yes" rot="R90">
<attribute name="VALUE" x="7.112" y="246.38" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="V81" gate="G$1" x="10.16" y="241.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="7.112" y="238.76" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="R73" gate="G$1" x="22.86" y="248.92" smashed="yes" rot="R180">
<attribute name="NAME" x="28.956" y="249.936" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="28.956" y="251.714" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="R74" gate="G$1" x="22.86" y="241.3" smashed="yes" rot="R180">
<attribute name="NAME" x="28.956" y="242.316" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="28.956" y="244.094" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="J7" gate="A" x="226.06" y="66.04" smashed="yes">
<attribute name="NAME" x="230.2256" y="71.3486" size="2.0828" layer="95" ratio="6" rot="SR0"/>
</instance>
<instance part="C27" gate="G$1" x="416.56" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="412.242" y="196.342" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="V182" gate="GND" x="416.56" y="190.5" smashed="yes">
<attribute name="VALUE" x="414.02" y="187.198" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C30" gate="G$1" x="426.72" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="422.402" y="196.342" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="V221" gate="GND" x="426.72" y="190.5" smashed="yes">
<attribute name="VALUE" x="424.18" y="187.198" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V189" gate="G$1" x="416.56" y="213.36" smashed="yes">
<attribute name="VALUE" x="414.147" y="213.995" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V222" gate="G$1" x="426.72" y="213.36" smashed="yes">
<attribute name="VALUE" x="424.307" y="213.995" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C1" gate="G$1" x="76.2" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="71.882" y="206.248" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="81.28" y="205.74" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="137.16" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="132.842" y="46.228" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="142.24" y="45.72" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="215.9" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="211.582" y="132.588" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="220.98" y="132.08" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="259.08" y="261.62" smashed="yes" rot="R90">
<attribute name="NAME" x="254.762" y="257.048" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="264.16" y="256.54" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C12" gate="G$1" x="266.7" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="262.382" y="145.288" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="271.78" y="144.78" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C17" gate="G$1" x="335.28" y="248.92" smashed="yes" rot="R90">
<attribute name="NAME" x="330.962" y="244.348" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="340.36" y="243.84" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C18" gate="G$1" x="396.24" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="391.922" y="196.088" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="401.32" y="195.58" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C19" gate="G$1" x="406.4" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="402.082" y="66.548" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="411.48" y="66.04" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C20" gate="G$1" x="414.02" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="409.702" y="152.908" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="419.1" y="152.4" size="1.4224" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VDD" class="0">
<segment>
<wire x1="304.546" y1="154.94" x2="307.34" y2="154.94" width="0.1524" layer="91"/>
<pinref part="V45" gate="G$1" pin="VDD"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="53.34" y1="100.838" x2="53.34" y2="104.14" width="0.1524" layer="91"/>
<pinref part="V5" gate="G$1" pin="VDD"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="99.06" y1="106.68" x2="99.06" y2="109.22" width="0.1524" layer="91"/>
<pinref part="V12" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="17.78" y1="264.16" x2="10.16" y2="264.16" width="0.1524" layer="91"/>
<pinref part="V1" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="17.78" y1="256.54" x2="10.16" y2="256.54" width="0.1524" layer="91"/>
<pinref part="V2" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="V60" gate="G$1" pin="VDD"/>
<wire x1="406.4" y1="93.218" x2="406.4" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="301.244" y1="256.54" x2="294.64" y2="256.54" width="0.1524" layer="91"/>
<pinref part="V39" gate="G$1" pin="VDD"/>
<pinref part="FB2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="226.06" y1="66.04" x2="210.82" y2="66.04" width="0.1524" layer="91"/>
<pinref part="V23" gate="G$1" pin="VDD"/>
<wire x1="210.82" y1="66.04" x2="210.82" y2="81.28" width="0.1524" layer="91"/>
<wire x1="210.82" y1="81.28" x2="210.82" y2="86.36" width="0.1524" layer="91"/>
<wire x1="248.92" y1="66.04" x2="259.08" y2="66.04" width="0.1524" layer="91"/>
<wire x1="259.08" y1="66.04" x2="259.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="259.08" y1="81.28" x2="264.16" y2="81.28" width="0.1524" layer="91"/>
<wire x1="264.16" y1="81.28" x2="264.16" y2="77.978" width="0.1524" layer="91"/>
<wire x1="264.16" y1="81.28" x2="271.78" y2="81.28" width="0.1524" layer="91"/>
<junction x="264.16" y="81.28"/>
<wire x1="271.78" y1="81.28" x2="271.78" y2="77.978" width="0.1524" layer="91"/>
<wire x1="271.78" y1="81.28" x2="279.4" y2="81.28" width="0.1524" layer="91"/>
<junction x="271.78" y="81.28"/>
<wire x1="279.4" y1="81.28" x2="279.4" y2="77.978" width="0.1524" layer="91"/>
<wire x1="279.4" y1="81.28" x2="287.02" y2="81.28" width="0.1524" layer="91"/>
<junction x="279.4" y="81.28"/>
<wire x1="287.02" y1="81.28" x2="287.02" y2="77.978" width="0.1524" layer="91"/>
<wire x1="259.08" y1="81.28" x2="210.82" y2="81.28" width="0.1524" layer="91"/>
<junction x="259.08" y="81.28"/>
<junction x="210.82" y="81.28"/>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="J7" gate="A" pin="2"/>
<pinref part="J7" gate="A" pin="1"/>
</segment>
<segment>
<pinref part="R73" gate="G$1" pin="2"/>
<pinref part="V77" gate="G$1" pin="VDD"/>
<wire x1="18.542" y1="248.92" x2="10.16" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R74" gate="G$1" pin="2"/>
<pinref part="V81" gate="G$1" pin="VDD"/>
<wire x1="18.542" y1="241.3" x2="10.16" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="256.54" y1="135.382" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
<pinref part="V30" gate="GND" pin="GND"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="256.54" y1="134.62" x2="256.54" y2="132.08" width="0.1524" layer="91"/>
<wire x1="256.54" y1="136.398" x2="256.54" y2="135.382" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="V37" gate="GND" pin="GND"/>
<wire x1="276.86" y1="144.78" x2="276.86" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V28" gate="GND" pin="GND"/>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="236.22" y1="132.08" x2="236.22" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V25" gate="GND" pin="GND"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="215.9" y1="132.08" x2="215.9" y2="129.54" width="0.1524" layer="91"/>
<wire x1="215.9" y1="133.858" x2="215.9" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="203.2" y1="142.24" x2="203.2" y2="132.08" width="0.1524" layer="91"/>
<pinref part="V22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="35.56" y1="223.52" x2="38.1" y2="223.52" width="0.1524" layer="91"/>
<wire x1="38.1" y1="223.52" x2="38.1" y2="213.36" width="0.1524" layer="91"/>
<pinref part="LED4" gate="G$1" pin="C"/>
<wire x1="38.1" y1="213.36" x2="38.1" y2="203.2" width="0.1524" layer="91"/>
<wire x1="38.1" y1="203.2" x2="38.1" y2="193.04" width="0.1524" layer="91"/>
<wire x1="38.1" y1="193.04" x2="38.1" y2="182.88" width="0.1524" layer="91"/>
<wire x1="38.1" y1="182.88" x2="38.1" y2="177.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="193.04" x2="38.1" y2="193.04" width="0.1524" layer="91"/>
<pinref part="LED3" gate="G$1" pin="C"/>
<wire x1="35.56" y1="203.2" x2="38.1" y2="203.2" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="35.56" y1="213.36" x2="38.1" y2="213.36" width="0.1524" layer="91"/>
<junction x="38.1" y="213.36"/>
<junction x="38.1" y="203.2"/>
<junction x="38.1" y="193.04"/>
<pinref part="V4" gate="GND" pin="GND"/>
<junction x="38.1" y="182.88"/>
<junction x="38.1" y="182.88"/>
<wire x1="38.1" y1="182.88" x2="34.036" y2="182.88" width="0.1524" layer="91"/>
<pinref part="LED5" gate="G$1" pin="K"/>
</segment>
<segment>
<wire x1="78.74" y1="81.28" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<pinref part="V7" gate="GND" pin="GND"/>
<pinref part="U1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="114.3" y1="83.82" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<pinref part="V14" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="27.94" x2="109.22" y2="27.94" width="0.1524" layer="91"/>
<wire x1="109.22" y1="27.94" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
<pinref part="V13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="V26" gate="GND" pin="GND"/>
<wire x1="218.44" y1="185.42" x2="218.44" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="259.08" y1="187.96" x2="259.08" y2="185.42" width="0.1524" layer="91"/>
<pinref part="V33" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="228.6" y1="205.74" x2="226.06" y2="205.74" width="0.1524" layer="91"/>
<wire x1="226.06" y1="205.74" x2="226.06" y2="185.42" width="0.1524" layer="91"/>
<pinref part="V27" gate="GND" pin="GND"/>
<pinref part="XTAL1" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="248.92" y1="195.58" x2="251.46" y2="195.58" width="0.1524" layer="91"/>
<wire x1="251.46" y1="195.58" x2="251.46" y2="185.42" width="0.1524" layer="91"/>
<pinref part="V29" gate="GND" pin="GND"/>
<pinref part="XTAL1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="GND"/>
<wire x1="205.74" y1="241.3" x2="190.5" y2="241.3" width="0.1524" layer="91"/>
<wire x1="190.5" y1="241.3" x2="190.5" y2="238.76" width="0.1524" layer="91"/>
<pinref part="V19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="HDR1" gate="G$1" pin="2"/>
<wire x1="33.02" y1="152.4" x2="30.48" y2="152.4" width="0.1524" layer="91"/>
<wire x1="30.48" y1="152.4" x2="30.48" y2="147.32" width="0.1524" layer="91"/>
<pinref part="V3" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="325.12" y1="208.28" x2="302.26" y2="208.28" width="0.1524" layer="91"/>
<wire x1="302.26" y1="208.28" x2="302.26" y2="205.74" width="0.1524" layer="91"/>
<wire x1="325.12" y1="205.74" x2="302.26" y2="205.74" width="0.1524" layer="91"/>
<pinref part="V42" gate="GND" pin="GND"/>
<wire x1="302.26" y1="205.74" x2="302.26" y2="203.2" width="0.1524" layer="91"/>
<junction x="302.26" y="205.74"/>
<pinref part="U5" gate="A" pin="VSS"/>
<pinref part="U5" gate="A" pin="DGND"/>
</segment>
<segment>
<wire x1="325.12" y1="185.42" x2="302.26" y2="185.42" width="0.1524" layer="91"/>
<pinref part="V44" gate="GND" pin="GND"/>
<pinref part="U5" gate="A" pin="AD1"/>
</segment>
<segment>
<pinref part="V61" gate="GND" pin="GND"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="406.4" y1="67.818" x2="406.4" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="A"/>
<pinref part="V54" gate="GND" pin="GND"/>
<wire x1="388.62" y1="63.5" x2="391.16" y2="63.5" width="0.1524" layer="91"/>
<wire x1="391.16" y1="63.5" x2="391.16" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="398.78" y1="68.58" x2="398.78" y2="66.04" width="0.1524" layer="91"/>
<pinref part="V56" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="V64" gate="GND" pin="GND"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="414.02" y1="154.178" x2="414.02" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="421.64" y1="152.4" x2="421.64" y2="149.86" width="0.1524" layer="91"/>
<pinref part="V68" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="V17" gate="GND" pin="GND"/>
<wire x1="149.86" y1="193.04" x2="152.4" y2="193.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="193.04" x2="154.94" y2="193.04" width="0.1524" layer="91"/>
<wire x1="154.94" y1="193.04" x2="157.48" y2="193.04" width="0.1524" layer="91"/>
<wire x1="157.48" y1="193.04" x2="162.56" y2="193.04" width="0.1524" layer="91"/>
<wire x1="149.86" y1="195.58" x2="154.94" y2="195.58" width="0.1524" layer="91"/>
<wire x1="154.94" y1="195.58" x2="154.94" y2="193.04" width="0.1524" layer="91"/>
<junction x="154.94" y="193.04"/>
<wire x1="149.86" y1="187.96" x2="152.4" y2="187.96" width="0.1524" layer="91"/>
<wire x1="152.4" y1="187.96" x2="152.4" y2="193.04" width="0.1524" layer="91"/>
<junction x="152.4" y="193.04"/>
<wire x1="149.86" y1="190.5" x2="157.48" y2="190.5" width="0.1524" layer="91"/>
<wire x1="157.48" y1="190.5" x2="157.48" y2="193.04" width="0.1524" layer="91"/>
<junction x="157.48" y="193.04"/>
<pinref part="U12" gate="G$1" pin="VSS"/>
<pinref part="U12" gate="G$1" pin="DGND"/>
<pinref part="U12" gate="G$1" pin="AD1"/>
<pinref part="U12" gate="G$1" pin="AD0"/>
</segment>
<segment>
<pinref part="V9" gate="GND" pin="GND"/>
<wire x1="76.2" y1="203.2" x2="76.2" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="76.2" y1="207.518" x2="76.2" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="86.36" y1="205.74" x2="86.36" y2="203.2" width="0.1524" layer="91"/>
<pinref part="V11" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="391.16" y1="134.62" x2="403.86" y2="134.62" width="0.1524" layer="91"/>
<pinref part="V57" gate="GND" pin="GND"/>
<wire x1="403.86" y1="134.62" x2="403.86" y2="132.08" width="0.1524" layer="91"/>
<wire x1="391.16" y1="137.16" x2="403.86" y2="137.16" width="0.1524" layer="91"/>
<wire x1="403.86" y1="137.16" x2="403.86" y2="134.62" width="0.1524" layer="91"/>
<junction x="403.86" y="134.62"/>
<pinref part="U13" gate="G$1" pin="VSS"/>
<pinref part="U13" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="166.878" y1="203.2" x2="170.18" y2="203.2" width="0.1524" layer="91"/>
<pinref part="V18" gate="GND" pin="GND"/>
<wire x1="170.18" y1="203.2" x2="170.18" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="287.02" y1="132.08" x2="287.02" y2="129.54" width="0.1524" layer="91"/>
<pinref part="V38" gate="GND" pin="GND"/>
<pinref part="LED6" gate="G$1" pin="C"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="139.7" y1="83.82" x2="132.08" y2="83.82" width="0.1524" layer="91"/>
<wire x1="132.08" y1="83.82" x2="132.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="137.16" y1="47.498" x2="137.16" y2="40.64" width="0.1524" layer="91"/>
<wire x1="137.16" y1="40.64" x2="139.7" y2="40.64" width="0.1524" layer="91"/>
<wire x1="139.7" y1="40.64" x2="144.78" y2="40.64" width="0.1524" layer="91"/>
<wire x1="132.08" y1="40.64" x2="137.16" y2="40.64" width="0.1524" layer="91"/>
<junction x="137.16" y="40.64"/>
<wire x1="139.7" y1="40.64" x2="139.7" y2="35.56" width="0.1524" layer="91"/>
<junction x="139.7" y="40.64"/>
<pinref part="V16" gate="GND" pin="GND"/>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="FB1" gate="G$1" pin="2"/>
<wire x1="144.78" y1="47.244" x2="144.78" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V32" gate="GND" pin="GND"/>
<wire x1="259.08" y1="254" x2="259.08" y2="256.54" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="259.08" y1="258.318" x2="259.08" y2="256.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="271.78" y1="256.54" x2="271.78" y2="254" width="0.1524" layer="91"/>
<pinref part="V36" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="V59" gate="GND" pin="GND"/>
<wire x1="396.24" y1="190.5" x2="396.24" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="396.24" y1="197.358" x2="396.24" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V66" gate="GND" pin="GND"/>
<wire x1="406.4" y1="190.5" x2="406.4" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="V34" gate="GND" pin="GND"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="266.7" y1="144.78" x2="266.7" y2="142.24" width="0.1524" layer="91"/>
<wire x1="266.7" y1="146.558" x2="266.7" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="V46" gate="GND" pin="GND"/>
<wire x1="314.96" y1="241.3" x2="314.96" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V47" gate="GND" pin="GND"/>
<wire x1="325.12" y1="241.3" x2="325.12" y2="245.6942" width="0.1524" layer="91"/>
<pinref part="C16" gate="A" pin="11"/>
</segment>
<segment>
<pinref part="V49" gate="GND" pin="GND"/>
<wire x1="335.28" y1="241.3" x2="335.28" y2="243.84" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="335.28" y1="245.618" x2="335.28" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V24" gate="GND" pin="GND"/>
<wire x1="210.82" y1="40.64" x2="210.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="210.82" y1="43.18" x2="226.06" y2="43.18" width="0.1524" layer="91"/>
<wire x1="226.06" y1="45.72" x2="210.82" y2="45.72" width="0.1524" layer="91"/>
<wire x1="210.82" y1="45.72" x2="210.82" y2="43.18" width="0.1524" layer="91"/>
<junction x="210.82" y="43.18"/>
<wire x1="226.06" y1="48.26" x2="210.82" y2="48.26" width="0.1524" layer="91"/>
<wire x1="210.82" y1="48.26" x2="210.82" y2="45.72" width="0.1524" layer="91"/>
<junction x="210.82" y="45.72"/>
<wire x1="226.06" y1="50.8" x2="210.82" y2="50.8" width="0.1524" layer="91"/>
<wire x1="210.82" y1="50.8" x2="210.82" y2="48.26" width="0.1524" layer="91"/>
<junction x="210.82" y="48.26"/>
<wire x1="226.06" y1="53.34" x2="210.82" y2="53.34" width="0.1524" layer="91"/>
<wire x1="210.82" y1="53.34" x2="210.82" y2="50.8" width="0.1524" layer="91"/>
<junction x="210.82" y="50.8"/>
<wire x1="226.06" y1="55.88" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="210.82" y1="55.88" x2="210.82" y2="53.34" width="0.1524" layer="91"/>
<junction x="210.82" y="53.34"/>
<wire x1="226.06" y1="58.42" x2="210.82" y2="58.42" width="0.1524" layer="91"/>
<wire x1="210.82" y1="58.42" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<junction x="210.82" y="55.88"/>
<wire x1="226.06" y1="60.96" x2="210.82" y2="60.96" width="0.1524" layer="91"/>
<wire x1="210.82" y1="60.96" x2="210.82" y2="58.42" width="0.1524" layer="91"/>
<junction x="210.82" y="58.42"/>
<wire x1="226.06" y1="63.5" x2="210.82" y2="63.5" width="0.1524" layer="91"/>
<wire x1="210.82" y1="63.5" x2="210.82" y2="60.96" width="0.1524" layer="91"/>
<junction x="210.82" y="60.96"/>
<pinref part="J7" gate="A" pin="4"/>
<pinref part="J7" gate="A" pin="6"/>
<pinref part="J7" gate="A" pin="8"/>
<pinref part="J7" gate="A" pin="10"/>
<pinref part="J7" gate="A" pin="12"/>
<pinref part="J7" gate="A" pin="14"/>
<pinref part="J7" gate="A" pin="16"/>
<pinref part="J7" gate="A" pin="18"/>
<pinref part="J7" gate="A" pin="20"/>
</segment>
<segment>
<wire x1="281.178" y1="43.18" x2="299.72" y2="43.18" width="0.1524" layer="91"/>
<pinref part="V40" gate="GND" pin="GND"/>
<wire x1="299.72" y1="43.18" x2="299.72" y2="40.64" width="0.1524" layer="91"/>
<wire x1="281.178" y1="45.72" x2="299.72" y2="45.72" width="0.1524" layer="91"/>
<wire x1="299.72" y1="45.72" x2="299.72" y2="43.18" width="0.1524" layer="91"/>
<junction x="299.72" y="43.18"/>
<junction x="299.72" y="45.72"/>
<wire x1="281.178" y1="53.34" x2="299.72" y2="53.34" width="0.1524" layer="91"/>
<wire x1="299.72" y1="53.34" x2="299.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="R18" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="360.68" y1="187.96" x2="381" y2="187.96" width="0.1524" layer="91"/>
<pinref part="U5" gate="A" pin="B3"/>
<wire x1="381" y1="187.96" x2="386.08" y2="187.96" width="0.1524" layer="91" style="longdash"/>
<pinref part="V53" gate="GND" pin="GND"/>
<wire x1="386.08" y1="187.96" x2="386.08" y2="185.42" width="0.1524" layer="91" style="longdash"/>
</segment>
<segment>
<wire x1="360.68" y1="203.2" x2="381" y2="203.2" width="0.1524" layer="91"/>
<pinref part="U5" gate="A" pin="B1"/>
<wire x1="381" y1="203.2" x2="386.08" y2="203.2" width="0.1524" layer="91" style="longdash"/>
<pinref part="V52" gate="GND" pin="GND"/>
<wire x1="386.08" y1="203.2" x2="386.08" y2="200.66" width="0.1524" layer="91" style="longdash"/>
</segment>
<segment>
<wire x1="360.68" y1="139.7" x2="337.82" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="B1"/>
<pinref part="V50" gate="GND" pin="GND"/>
<wire x1="337.82" y1="127" x2="337.82" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V62" gate="GND" pin="GND"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="408.94" y1="129.54" x2="408.94" y2="135.382" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="V182" gate="GND" pin="GND"/>
<wire x1="416.56" y1="197.358" x2="416.56" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="V221" gate="GND" pin="GND"/>
<wire x1="426.72" y1="197.358" x2="426.72" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="218.44" y1="198.12" x2="218.44" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="215.9" y1="200.66" x2="218.44" y2="200.66" width="0.1524" layer="91"/>
<wire x1="228.6" y1="200.66" x2="218.44" y2="200.66" width="0.1524" layer="91"/>
<junction x="218.44" y="200.66"/>
<pinref part="XTAL1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="OSC_IN" class="0">
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="259.08" y1="198.12" x2="259.08" y2="200.66" width="0.1524" layer="91"/>
<wire x1="259.08" y1="200.66" x2="274.32" y2="200.66" width="0.1524" layer="91"/>
<label x="266.446" y="201.168" size="1.4224" layer="95" ratio="10"/>
<wire x1="248.92" y1="200.66" x2="259.08" y2="200.66" width="0.1524" layer="91"/>
<junction x="259.08" y="200.66"/>
<pinref part="XTAL1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="OSC_OUT" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="205.74" y1="200.66" x2="195.58" y2="200.66" width="0.1524" layer="91"/>
<label x="198.12" y="201.168" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="VIN"/>
<wire x1="218.44" y1="154.94" x2="215.9" y2="154.94" width="0.1524" layer="91"/>
<pinref part="V21" gate="G$1" pin="+5V"/>
<wire x1="215.9" y1="154.94" x2="203.2" y2="154.94" width="0.1524" layer="91"/>
<wire x1="203.2" y1="154.94" x2="198.12" y2="154.94" width="0.1524" layer="91"/>
<junction x="203.2" y="154.94"/>
<junction x="215.9" y="154.94"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="215.9" y1="142.24" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<wire x1="215.9" y1="144.78" x2="215.9" y2="154.94" width="0.1524" layer="91"/>
<wire x1="203.2" y1="152.4" x2="203.2" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="INH"/>
<wire x1="218.44" y1="144.78" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<junction x="215.9" y="144.78"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="215.9" y1="140.462" x2="215.9" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IN"/>
<wire x1="63.5" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<wire x1="60.96" y1="93.98" x2="60.96" y2="99.06" width="0.1524" layer="91"/>
<pinref part="V6" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="V8" gate="G$1" pin="+5V"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="76.2" y1="215.9" x2="76.2" y2="220.98" width="0.1524" layer="91"/>
<wire x1="76.2" y1="214.122" x2="76.2" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="86.36" y1="220.98" x2="86.36" y2="215.9" width="0.1524" layer="91"/>
<pinref part="V10" gate="G$1" pin="+5V"/>
</segment>
<segment>
<wire x1="109.22" y1="195.58" x2="119.38" y2="195.58" width="0.1524" layer="91"/>
<wire x1="109.22" y1="195.58" x2="109.22" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U12" gate="G$1" pin="VDD"/>
<pinref part="V15" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="V55" gate="G$1" pin="+5V"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="396.24" y1="205.74" x2="396.24" y2="210.82" width="0.1524" layer="91"/>
<wire x1="396.24" y1="203.962" x2="396.24" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="V58" gate="G$1" pin="+5V"/>
<wire x1="406.4" y1="205.74" x2="406.4" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V41" gate="G$1" pin="+5V"/>
<wire x1="302.26" y1="213.36" x2="302.26" y2="210.82" width="0.1524" layer="91"/>
<pinref part="U5" gate="A" pin="VDD"/>
<wire x1="302.26" y1="210.82" x2="325.12" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="A" pin="AD0"/>
<wire x1="325.12" y1="187.96" x2="302.26" y2="187.96" width="0.1524" layer="91"/>
<pinref part="V43" gate="G$1" pin="+5V"/>
<wire x1="302.26" y1="187.96" x2="302.26" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V63" gate="G$1" pin="+5V"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="414.02" y1="160.782" x2="414.02" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="V65" gate="G$1" pin="+5V"/>
<wire x1="421.64" y1="165.1" x2="421.64" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="391.16" y1="132.08" x2="398.78" y2="132.08" width="0.1524" layer="91"/>
<wire x1="398.78" y1="132.08" x2="398.78" y2="129.54" width="0.1524" layer="91"/>
<wire x1="398.78" y1="129.54" x2="391.16" y2="129.54" width="0.1524" layer="91"/>
<wire x1="398.78" y1="132.08" x2="398.78" y2="157.48" width="0.1524" layer="91"/>
<junction x="398.78" y="132.08"/>
<pinref part="U13" gate="G$1" pin="AD1"/>
<pinref part="U13" gate="G$1" pin="AD0"/>
<pinref part="V67" gate="G$1" pin="+5V"/>
</segment>
<segment>
<wire x1="332.74" y1="149.86" x2="332.74" y2="137.16" width="0.1524" layer="91"/>
<wire x1="332.74" y1="137.16" x2="360.68" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="VDD"/>
<pinref part="V48" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="416.56" y1="203.962" x2="416.56" y2="210.82" width="0.1524" layer="91" style="longdash"/>
<pinref part="V189" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="426.72" y1="203.962" x2="426.72" y2="210.82" width="0.1524" layer="91" style="longdash"/>
<pinref part="V222" gate="G$1" pin="+5V"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="BYBASS"/>
<wire x1="256.54" y1="144.78" x2="254" y2="144.78" width="0.1524" layer="91"/>
<wire x1="256.54" y1="143.891" x2="256.54" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="256.54" y1="143.002" x2="256.54" y2="143.891" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="LED1" gate="G$1" pin="A"/>
<wire x1="25.4" y1="223.52" x2="27.94" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="LED2" gate="G$1" pin="A"/>
<wire x1="25.4" y1="213.36" x2="27.94" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="LED3" gate="G$1" pin="A"/>
<wire x1="25.4" y1="203.2" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="LED4" gate="G$1" pin="A"/>
<wire x1="25.4" y1="193.04" x2="27.94" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED-PWR" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="15.24" y1="223.52" x2="2.54" y2="223.52" width="0.1524" layer="91"/>
<label x="5.334" y="224.028" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="LED-PTT" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="15.24" y1="213.36" x2="2.54" y2="213.36" width="0.1524" layer="91"/>
<label x="5.08" y="213.868" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="LED-RT" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="15.24" y1="203.2" x2="2.54" y2="203.2" width="0.1524" layer="91"/>
<label x="5.08" y="203.708" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="LED-ERR" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="15.24" y1="193.04" x2="2.54" y2="193.04" width="0.1524" layer="91"/>
<label x="5.08" y="193.548" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="139.7" y1="88.9" x2="127" y2="88.9" width="0.1524" layer="91"/>
<wire x1="127" y1="88.9" x2="127" y2="55.88" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="127" y1="55.88" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="D+IN"/>
<wire x1="116.84" y1="55.88" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<wire x1="106.68" y1="40.64" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<wire x1="116.84" y1="40.64" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
<junction x="116.84" y="55.88"/>
<pinref part="J1" gate="G$1" pin="DP"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="124.46" y1="91.44" x2="124.46" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="124.46" y1="60.96" x2="119.38" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="D-IN"/>
<wire x1="119.38" y1="60.96" x2="83.82" y2="60.96" width="0.1524" layer="91"/>
<wire x1="106.68" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="38.1" x2="119.38" y2="60.96" width="0.1524" layer="91"/>
<junction x="119.38" y="60.96"/>
<wire x1="124.46" y1="91.44" x2="139.7" y2="91.44" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="DM"/>
</segment>
</net>
<net name="VBUS" class="0">
<segment>
<wire x1="121.92" y1="93.98" x2="121.92" y2="66.04" width="0.1524" layer="91"/>
<wire x1="121.92" y1="66.04" x2="71.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="71.12" y1="66.04" x2="48.26" y2="66.04" width="0.1524" layer="91"/>
<label x="50.8" y="66.548" size="1.4224" layer="95" ratio="10"/>
<wire x1="139.7" y1="93.98" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
<wire x1="114.3" y1="93.98" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="114.3" y1="91.44" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<junction x="114.3" y="93.98"/>
<wire x1="96.52" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT"/>
<junction x="121.92" y="93.98"/>
<pinref part="U2" gate="G$1" pin="VBUS"/>
<wire x1="73.66" y1="43.18" x2="71.12" y2="43.18" width="0.1524" layer="91"/>
<wire x1="71.12" y1="43.18" x2="71.12" y2="66.04" width="0.1524" layer="91"/>
<junction x="71.12" y="66.04"/>
<pinref part="J1" gate="G$1" pin="VBUS"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ID"/>
<wire x1="106.68" y1="43.18" x2="114.3" y2="43.18" width="0.1524" layer="91"/>
<wire x1="139.7" y1="86.36" x2="129.54" y2="86.36" width="0.1524" layer="91"/>
<wire x1="129.54" y1="86.36" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="129.54" y1="50.8" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<wire x1="114.3" y1="43.18" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<wire x1="114.3" y1="50.8" x2="108.966" y2="50.8" width="0.1524" layer="91"/>
<junction x="114.3" y="50.8"/>
<pinref part="J1" gate="G$1" pin="ID"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="OTG_DP" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="D+OUT"/>
<wire x1="73.66" y1="40.64" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="40.64" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="86.36" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<junction x="68.58" y="55.88"/>
<wire x1="68.58" y1="55.88" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<label x="50.8" y="56.388" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="OTG_DM" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="D-OUT"/>
<wire x1="73.66" y1="38.1" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="38.1" x2="66.04" y2="60.96" width="0.1524" layer="91"/>
<wire x1="66.04" y1="60.96" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<label x="50.8" y="61.468" size="1.4224" layer="95" ratio="10"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="66.04" y1="60.96" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<junction x="66.04" y="60.96"/>
</segment>
</net>
<net name="OTG_ID" class="0">
<segment>
<wire x1="99.06" y1="50.8" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<label x="50.8" y="51.308" size="1.4224" layer="95" ratio="10"/>
<pinref part="R13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="!EN!"/>
<wire x1="63.5" y1="88.9" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="88.9" x2="53.34" y2="92.202" width="0.1524" layer="91"/>
<wire x1="53.34" y1="88.9" x2="50.546" y2="88.9" width="0.1524" layer="91"/>
<junction x="53.34" y="88.9"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="R9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="OTG_OVR_CUR" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="!FAULT!"/>
<wire x1="96.52" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
<junction x="99.06" y="88.9"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="71.12" width="0.1524" layer="91"/>
<wire x1="99.06" y1="71.12" x2="53.34" y2="71.12" width="0.1524" layer="91"/>
<label x="55.88" y="71.628" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="OTG_PWR_ON" class="0">
<segment>
<wire x1="40.64" y1="88.9" x2="38.1" y2="88.9" width="0.1524" layer="91"/>
<label x="38.1" y="84.328" size="1.4224" layer="95" ratio="10"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="USART3_RX" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="27.94" y1="264.16" x2="38.1" y2="264.16" width="0.1524" layer="91"/>
<label x="33.02" y="264.668" size="1.4224" layer="95" ratio="10"/>
</segment>
<segment>
<pinref part="HDR1" gate="G$1" pin="3"/>
<wire x1="33.02" y1="149.86" x2="12.7" y2="149.86" width="0.1524" layer="91"/>
<label x="15.24" y="150.114" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="USART3_TX" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="27.94" y1="256.54" x2="38.1" y2="256.54" width="0.1524" layer="91"/>
<label x="33.02" y="257.048" size="1.4224" layer="95" ratio="10"/>
</segment>
<segment>
<pinref part="HDR1" gate="G$1" pin="1"/>
<wire x1="33.02" y1="154.94" x2="12.7" y2="154.94" width="0.1524" layer="91"/>
<label x="15.24" y="155.448" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="SDA3" class="0">
<segment>
<wire x1="325.12" y1="193.04" x2="309.88" y2="193.04" width="0.1524" layer="91"/>
<label x="309.88" y="193.04" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="SDA"/>
</segment>
<segment>
<wire x1="119.38" y1="187.96" x2="109.22" y2="187.96" width="0.1524" layer="91"/>
<label x="109.22" y="187.96" size="1.778" layer="95"/>
<pinref part="U12" gate="G$1" pin="SDA"/>
</segment>
<segment>
<wire x1="360.68" y1="129.54" x2="345.44" y2="129.54" width="0.1524" layer="91"/>
<label x="345.44" y="129.54" size="1.4224" layer="95"/>
<pinref part="U13" gate="G$1" pin="SDA"/>
</segment>
</net>
<net name="SCL3" class="0">
<segment>
<wire x1="325.12" y1="195.58" x2="309.88" y2="195.58" width="0.1524" layer="91"/>
<label x="309.88" y="195.58" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="SCL"/>
</segment>
<segment>
<wire x1="119.38" y1="190.5" x2="109.22" y2="190.5" width="0.1524" layer="91"/>
<label x="109.22" y="190.5" size="1.778" layer="95"/>
<pinref part="U12" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="360.68" y1="132.08" x2="345.44" y2="132.08" width="0.1524" layer="91"/>
<label x="345.44" y="132.08" size="1.4224" layer="95"/>
<pinref part="U13" gate="G$1" pin="SCL"/>
</segment>
</net>
<net name="!EXT-PTT!" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="424.18" y1="78.74" x2="429.26" y2="78.74" width="0.1524" layer="91"/>
<label x="421.386" y="74.168" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="AD5252_WP" class="0">
<segment>
<wire x1="325.12" y1="203.2" x2="309.88" y2="203.2" width="0.1524" layer="91"/>
<label x="312.42" y="203.2" size="1.4224" layer="95"/>
<pinref part="U5" gate="A" pin="!WP!"/>
</segment>
</net>
<net name="NSS" class="0">
<segment>
<pinref part="U3" gate="A" pin="!SYNC!"/>
<wire x1="236.22" y1="246.38" x2="251.46" y2="246.38" width="0.1524" layer="91"/>
<label x="246.38" y="246.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U3" gate="A" pin="SCLK"/>
<wire x1="236.22" y1="241.3" x2="251.46" y2="241.3" width="0.1524" layer="91"/>
<label x="246.38" y="241.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U3" gate="A" pin="DIN"/>
<wire x1="236.22" y1="236.22" x2="251.46" y2="236.22" width="0.1524" layer="91"/>
<label x="246.38" y="236.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="AD5252_A1" class="0">
<segment>
<wire x1="360.68" y1="208.28" x2="381" y2="208.28" width="0.1524" layer="91"/>
<label x="368.3" y="208.28" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="A1"/>
</segment>
</net>
<net name="AD5252_W1" class="0">
<segment>
<wire x1="360.68" y1="205.74" x2="381" y2="205.74" width="0.1524" layer="91"/>
<label x="368.3" y="205.74" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="W1"/>
</segment>
</net>
<net name="AD5252_A3" class="0">
<segment>
<wire x1="360.68" y1="193.04" x2="381" y2="193.04" width="0.1524" layer="91"/>
<label x="368.3" y="193.04" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="A3"/>
</segment>
</net>
<net name="AD5252_W3" class="0">
<segment>
<wire x1="360.68" y1="190.5" x2="381" y2="190.5" width="0.1524" layer="91"/>
<label x="368.3" y="190.5" size="1.778" layer="95"/>
<pinref part="U5" gate="A" pin="W3"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="388.62" y1="78.74" x2="398.78" y2="78.74" width="0.1524" layer="91"/>
<wire x1="398.78" y1="78.74" x2="406.4" y2="78.74" width="0.1524" layer="91"/>
<wire x1="406.4" y1="78.74" x2="414.02" y2="78.74" width="0.1524" layer="91"/>
<junction x="406.4" y="78.74"/>
<wire x1="406.4" y1="74.422" x2="406.4" y2="78.74" width="0.1524" layer="91"/>
<wire x1="406.4" y1="78.74" x2="406.4" y2="84.582" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="C"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="398.78" y1="76.2" x2="398.78" y2="78.74" width="0.1524" layer="91"/>
<junction x="398.78" y="78.74"/>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="C19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="AD5241_1M_B1" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="119.38" y1="198.12" x2="116.84" y2="198.12" width="0.1524" layer="91"/>
<wire x1="116.84" y1="198.12" x2="116.84" y2="215.9" width="0.1524" layer="91"/>
<wire x1="116.84" y1="215.9" x2="172.72" y2="215.9" width="0.1524" layer="91"/>
<wire x1="172.72" y1="215.9" x2="172.72" y2="223.52" width="0.1524" layer="91"/>
<wire x1="172.72" y1="223.52" x2="152.4" y2="223.52" width="0.1524" layer="91"/>
<wire x1="172.72" y1="215.9" x2="172.72" y2="182.88" width="0.1524" layer="91"/>
<junction x="172.72" y="215.9"/>
<wire x1="172.72" y1="182.88" x2="172.72" y2="180.34" width="0.1524" layer="91" style="longdash"/>
<label x="172.72" y="180.34" size="1.4224" layer="95" rot="R90"/>
<pinref part="U12" gate="G$1" pin="B1"/>
</segment>
</net>
<net name="AVDD" class="0">
<segment>
<pinref part="V20" gate="G$1" pin="AVDD"/>
<pinref part="U3" gate="A" pin="VA"/>
<wire x1="198.12" y1="236.22" x2="205.74" y2="236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="308.61" y1="256.54" x2="314.96" y2="256.54" width="0.1524" layer="91"/>
<wire x1="314.96" y1="256.54" x2="314.96" y2="254" width="0.1524" layer="91"/>
<wire x1="314.96" y1="256.54" x2="325.12" y2="256.54" width="0.1524" layer="91"/>
<wire x1="325.12" y1="256.54" x2="325.12" y2="252.1712" width="0.1524" layer="91"/>
<wire x1="325.12" y1="256.54" x2="335.28" y2="256.54" width="0.1524" layer="91"/>
<wire x1="335.28" y1="256.54" x2="335.28" y2="252.222" width="0.1524" layer="91"/>
<junction x="314.96" y="256.54"/>
<junction x="325.12" y="256.54"/>
<junction x="335.28" y="256.54"/>
<wire x1="335.28" y1="256.54" x2="342.9" y2="256.54" width="0.1524" layer="91"/>
<pinref part="V51" gate="G$1" pin="AVDD"/>
<pinref part="FB2" gate="G$1" pin="1"/>
<pinref part="C16" gate="A" pin="22"/>
<pinref part="C17" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="V31" gate="G$1" pin="AVDD"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="259.08" y1="266.7" x2="259.08" y2="269.24" width="0.1524" layer="91"/>
<wire x1="259.08" y1="264.922" x2="259.08" y2="266.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="271.78" y1="266.7" x2="271.78" y2="269.24" width="0.1524" layer="91"/>
<pinref part="V35" gate="G$1" pin="AVDD"/>
</segment>
</net>
<net name="AD5241_100_W1" class="0">
<segment>
<wire x1="360.68" y1="142.24" x2="340.36" y2="142.24" width="0.1524" layer="91"/>
<wire x1="340.36" y1="142.24" x2="340.36" y2="160.02" width="0.1524" layer="91"/>
<label x="340.36" y="144.78" size="1.4224" layer="95" rot="R90"/>
<pinref part="U13" gate="G$1" pin="W1"/>
</segment>
</net>
<net name="AD5241_100_A1" class="0">
<segment>
<wire x1="345.44" y1="144.78" x2="345.44" y2="160.02" width="0.1524" layer="91"/>
<label x="345.44" y="144.78" size="1.4224" layer="95" rot="R90"/>
<pinref part="U13" gate="G$1" pin="A1"/>
<wire x1="345.44" y1="144.78" x2="360.68" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="O1"/>
<wire x1="391.16" y1="144.78" x2="393.7" y2="144.78" width="0.1524" layer="91"/>
<wire x1="393.7" y1="144.78" x2="393.7" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="!SHDN"/>
<wire x1="393.7" y1="139.7" x2="391.16" y2="139.7" width="0.1524" layer="91"/>
<junction x="393.7" y="144.78"/>
<wire x1="393.7" y1="144.78" x2="403.86" y2="144.78" width="0.1524" layer="91"/>
<wire x1="403.86" y1="144.78" x2="403.86" y2="149.86" width="0.1524" layer="91"/>
<wire x1="403.86" y1="149.86" x2="408.94" y2="149.86" width="0.1524" layer="91"/>
<wire x1="408.94" y1="149.86" x2="408.94" y2="144.018" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="!SHDN"/>
<wire x1="149.86" y1="198.12" x2="152.4" y2="198.12" width="0.1524" layer="91"/>
<wire x1="152.4" y1="198.12" x2="152.4" y2="203.2" width="0.1524" layer="91"/>
<pinref part="U12" gate="G$1" pin="O1"/>
<wire x1="152.4" y1="203.2" x2="149.86" y2="203.2" width="0.1524" layer="91"/>
<junction x="152.4" y="203.2"/>
<wire x1="152.4" y1="203.2" x2="158.242" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="AD5241_1M_W1" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="142.24" y1="223.52" x2="111.76" y2="223.52" width="0.1524" layer="91"/>
<wire x1="111.76" y1="223.52" x2="111.76" y2="203.2" width="0.1524" layer="91"/>
<wire x1="111.76" y1="203.2" x2="111.76" y2="200.66" width="0.1524" layer="91"/>
<wire x1="111.76" y1="200.66" x2="119.38" y2="200.66" width="0.1524" layer="91"/>
<wire x1="111.76" y1="203.2" x2="119.38" y2="203.2" width="0.1524" layer="91"/>
<junction x="111.76" y="203.2"/>
<wire x1="111.76" y1="203.2" x2="104.14" y2="203.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="203.2" x2="104.14" y2="180.34" width="0.1524" layer="91"/>
<label x="104.14" y="180.34" size="1.4224" layer="95" rot="R90"/>
<pinref part="U12" gate="G$1" pin="A1"/>
<pinref part="U12" gate="G$1" pin="W1"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="287.02" y1="152.4" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VOUT"/>
<wire x1="254" y1="154.94" x2="266.7" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="276.86" y="154.94"/>
<wire x1="266.7" y1="154.94" x2="276.86" y2="154.94" width="0.1524" layer="91"/>
<wire x1="276.86" y1="154.94" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<junction x="266.7" y="154.94"/>
<junction x="287.02" y="154.94"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="294.64" y1="154.94" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="266.7" y1="153.162" x2="266.7" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="287.02" y1="139.7" x2="287.02" y2="142.24" width="0.1524" layer="91"/>
<pinref part="LED6" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="137.16" y1="54.102" x2="137.16" y2="58.42" width="0.1524" layer="91"/>
<wire x1="137.16" y1="58.42" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="SHIELD4"/>
<wire x1="139.7" y1="68.58" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="SHIELD3"/>
<wire x1="139.7" y1="71.12" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="137.16" y1="71.12" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<junction x="137.16" y="68.58"/>
<pinref part="J1" gate="G$1" pin="SHIELD2"/>
<wire x1="139.7" y1="73.66" x2="137.16" y2="73.66" width="0.1524" layer="91"/>
<wire x1="137.16" y1="73.66" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<junction x="137.16" y="71.12"/>
<pinref part="J1" gate="G$1" pin="SHIELD1"/>
<wire x1="139.7" y1="76.2" x2="137.16" y2="76.2" width="0.1524" layer="91"/>
<wire x1="137.16" y1="76.2" x2="137.16" y2="73.66" width="0.1524" layer="91"/>
<junction x="137.16" y="73.66"/>
<wire x1="137.16" y1="58.42" x2="144.78" y2="58.42" width="0.1524" layer="91"/>
<junction x="137.16" y="58.42"/>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="FB1" gate="G$1" pin="1"/>
<wire x1="144.78" y1="54.61" x2="144.78" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CPU_TRST" class="0">
<segment>
<wire x1="248.92" y1="63.5" x2="264.16" y2="63.5" width="0.1524" layer="91"/>
<wire x1="264.16" y1="63.5" x2="327.66" y2="63.5" width="0.1524" layer="91"/>
<wire x1="264.16" y1="69.342" x2="264.16" y2="63.5" width="0.1524" layer="91"/>
<junction x="264.16" y="63.5"/>
<label x="314.96" y="63.5" size="1.778" layer="95"/>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="J7" gate="A" pin="3"/>
</segment>
</net>
<net name="CPU_TDI" class="0">
<segment>
<wire x1="248.92" y1="60.96" x2="271.78" y2="60.96" width="0.1524" layer="91"/>
<wire x1="271.78" y1="60.96" x2="327.66" y2="60.96" width="0.1524" layer="91"/>
<wire x1="271.78" y1="69.342" x2="271.78" y2="60.96" width="0.1524" layer="91"/>
<junction x="271.78" y="60.96"/>
<label x="314.96" y="60.96" size="1.778" layer="95"/>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="J7" gate="A" pin="5"/>
</segment>
</net>
<net name="CPU_TMS" class="0">
<segment>
<wire x1="248.92" y1="58.42" x2="279.4" y2="58.42" width="0.1524" layer="91"/>
<wire x1="279.4" y1="58.42" x2="327.66" y2="58.42" width="0.1524" layer="91"/>
<wire x1="279.4" y1="69.342" x2="279.4" y2="58.42" width="0.1524" layer="91"/>
<junction x="279.4" y="58.42"/>
<label x="314.96" y="58.42" size="1.778" layer="95"/>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="J7" gate="A" pin="7"/>
</segment>
</net>
<net name="CPU_TCK" class="0">
<segment>
<wire x1="251.46" y1="55.88" x2="254" y2="55.88" width="0.1524" layer="91"/>
<wire x1="254" y1="55.88" x2="287.02" y2="55.88" width="0.1524" layer="91"/>
<wire x1="287.02" y1="69.342" x2="287.02" y2="55.88" width="0.1524" layer="91"/>
<junction x="287.02" y="55.88"/>
<wire x1="287.02" y1="55.88" x2="327.66" y2="55.88" width="0.1524" layer="91"/>
<label x="314.96" y="55.88" size="1.778" layer="95"/>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="J7" gate="A" pin="9"/>
<wire x1="248.92" y1="55.88" x2="251.46" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="248.92" y1="53.34" x2="272.542" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="J7" gate="A" pin="11"/>
</segment>
</net>
<net name="CPU_TDO" class="0">
<segment>
<wire x1="251.46" y1="50.8" x2="254" y2="50.8" width="0.1524" layer="91"/>
<label x="314.96" y="50.8" size="1.778" layer="95"/>
<pinref part="J7" gate="A" pin="13"/>
<wire x1="254" y1="50.8" x2="327.66" y2="50.8" width="0.1524" layer="91"/>
<wire x1="248.92" y1="50.8" x2="251.46" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="248.92" y1="48.26" x2="271.78" y2="48.26" width="0.1524" layer="91"/>
<pinref part="J7" gate="A" pin="15"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<wire x1="248.92" y1="45.72" x2="272.542" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="J7" gate="A" pin="17"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<wire x1="248.92" y1="43.18" x2="272.542" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="J7" gate="A" pin="19"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="281.94" y1="48.26" x2="307.34" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CPU_RSTN" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="314.96" y1="48.26" x2="327.66" y2="48.26" width="0.1524" layer="91"/>
<label x="314.96" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="LED5" gate="G$1" pin="A"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="27.432" y1="182.88" x2="24.892" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED-DBG" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="15.748" y1="182.88" x2="2.54" y2="182.88" width="0.1524" layer="91"/>
<label x="5.334" y="183.388" size="1.4224" layer="95"/>
</segment>
</net>
<net name="!INP1!" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="27.178" y1="248.92" x2="38.1" y2="248.92" width="0.1524" layer="91"/>
<label x="33.02" y="248.92" size="1.4224" layer="95"/>
</segment>
</net>
<net name="!INP2!" class="0">
<segment>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="27.178" y1="241.3" x2="38.1" y2="241.3" width="0.1524" layer="91"/>
<label x="33.02" y="241.3" size="1.4224" layer="95"/>
</segment>
</net>
<net name="DAC2-OUT" class="0">
<segment>
<label x="190.5" y="246.38" size="1.4224" layer="95"/>
<pinref part="U3" gate="A" pin="VOUT"/>
<wire x1="205.74" y1="246.38" x2="190.5" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="342.392" y="17.272" size="3.81" layer="94" ratio="10">New Circuit Board</text>
<text x="415.544" y="6.35" size="3.81" layer="94" ratio="10">A</text>
<text x="7.62" y="271.78" size="2.54" layer="94" ratio="10">SEcube Connections</text>
<text x="48.895" y="250.19" size="2.54" layer="94">SEcube -  STM32 CPU</text>
<wire x1="137.16" y1="88.9" x2="424.18" y2="88.9" width="0.1524" layer="94" style="longdash"/>
<wire x1="114.3" y1="134.62" x2="114.3" y2="154.94" width="0.1524" layer="94" style="longdash"/>
<text x="361.315" y="171.45" size="2.54" layer="94">SEcube - Power Connections</text>
<wire x1="114.3" y1="154.94" x2="114.3" y2="279.4" width="0.1524" layer="94" style="longdash"/>
<wire x1="114.3" y1="154.94" x2="-7.62" y2="154.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="137.16" y1="88.9" x2="137.16" y2="0" width="0.1524" layer="94" style="longdash"/>
<text x="51.435" y="138.43" size="2.54" layer="94">SEcube -  MACHOX2_FPGA</text>
<text x="86.36" y="198.12" size="1.4224" layer="97">MISO</text>
<wire x1="137.16" y1="88.9" x2="137.16" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="137.16" y1="134.62" x2="114.3" y2="134.62" width="0.1524" layer="94" style="longdash"/>
</plain>
<instances>
<instance part="V113" gate="GND" x="345.44" y="177.8" smashed="yes">
<attribute name="VALUE" x="342.9" y="174.498" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V102" gate="GND" x="220.98" y="177.8" smashed="yes">
<attribute name="VALUE" x="218.44" y="174.498" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V109" gate="GND" x="330.2" y="246.38" smashed="yes">
<attribute name="VALUE" x="327.66" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V110" gate="GND" x="337.82" y="246.38" smashed="yes">
<attribute name="VALUE" x="335.28" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V112" gate="GND" x="345.44" y="246.38" smashed="yes">
<attribute name="VALUE" x="342.9" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V114" gate="GND" x="353.06" y="246.38" smashed="yes">
<attribute name="VALUE" x="350.52" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V116" gate="GND" x="360.68" y="246.38" smashed="yes">
<attribute name="VALUE" x="358.14" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V118" gate="GND" x="368.3" y="246.38" smashed="yes">
<attribute name="VALUE" x="365.76" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V119" gate="GND" x="375.92" y="246.38" smashed="yes">
<attribute name="VALUE" x="373.38" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V120" gate="GND" x="383.54" y="246.38" smashed="yes">
<attribute name="VALUE" x="381" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V121" gate="GND" x="391.16" y="246.38" smashed="yes">
<attribute name="VALUE" x="388.62" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V105" gate="GND" x="233.68" y="246.38" smashed="yes">
<attribute name="VALUE" x="231.14" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V104" gate="GND" x="226.06" y="246.38" smashed="yes">
<attribute name="VALUE" x="223.52" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V101" gate="GND" x="218.44" y="246.38" smashed="yes">
<attribute name="VALUE" x="215.9" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V100" gate="GND" x="210.82" y="246.38" smashed="yes">
<attribute name="VALUE" x="208.28" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V99" gate="GND" x="203.2" y="246.38" smashed="yes">
<attribute name="VALUE" x="200.66" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V98" gate="GND" x="195.58" y="246.38" smashed="yes">
<attribute name="VALUE" x="193.04" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V97" gate="GND" x="187.96" y="246.38" smashed="yes">
<attribute name="VALUE" x="185.42" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V92" gate="GND" x="180.34" y="246.38" smashed="yes">
<attribute name="VALUE" x="177.8" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V87" gate="GND" x="172.72" y="246.38" smashed="yes">
<attribute name="VALUE" x="170.18" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V122" gate="GND" x="398.78" y="246.38" smashed="yes">
<attribute name="VALUE" x="396.24" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C25" gate="G$1" x="142.24" y="256.54" smashed="yes" rot="R270">
<attribute name="NAME" x="147.32" y="254" size="1.4224" layer="95" ratio="9" rot="R270"/>
<attribute name="VALUE" x="144.78" y="254" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V76" gate="GND" x="142.24" y="246.38" smashed="yes">
<attribute name="VALUE" x="139.7" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V124" gate="G$1" x="421.64" y="269.24" smashed="yes">
<attribute name="VALUE" x="419.1" y="272.288" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="C63" gate="G$1" x="365.76" y="226.06" smashed="yes" rot="R270">
<attribute name="NAME" x="370.84" y="223.52" size="1.4224" layer="95" ratio="9" rot="R270"/>
<attribute name="VALUE" x="368.3" y="223.52" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V117" gate="GND" x="365.76" y="210.82" smashed="yes">
<attribute name="VALUE" x="363.22" y="207.518" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C61" gate="G$1" x="358.14" y="220.98" smashed="yes" rot="R270">
<attribute name="NAME" x="363.22" y="218.44" size="1.4224" layer="95" ratio="9" rot="R270"/>
<attribute name="VALUE" x="360.68" y="218.44" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V115" gate="GND" x="358.14" y="210.82" smashed="yes">
<attribute name="VALUE" x="355.6" y="207.518" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V83" gate="GND" x="165.1" y="218.44" smashed="yes">
<attribute name="VALUE" x="162.56" y="215.138" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V88" gate="GND" x="172.72" y="218.44" smashed="yes">
<attribute name="VALUE" x="170.18" y="215.138" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V93" gate="GND" x="180.34" y="218.44" smashed="yes">
<attribute name="VALUE" x="177.8" y="215.138" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V84" gate="GND" x="165.1" y="193.04" smashed="yes">
<attribute name="VALUE" x="162.56" y="189.738" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V89" gate="GND" x="172.72" y="193.04" smashed="yes">
<attribute name="VALUE" x="170.18" y="189.738" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V94" gate="GND" x="180.34" y="193.04" smashed="yes">
<attribute name="VALUE" x="177.8" y="189.738" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V85" gate="GND" x="165.1" y="167.64" smashed="yes">
<attribute name="VALUE" x="162.56" y="164.338" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V90" gate="GND" x="172.72" y="167.64" smashed="yes">
<attribute name="VALUE" x="170.18" y="164.338" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V95" gate="GND" x="180.34" y="167.64" smashed="yes">
<attribute name="VALUE" x="177.8" y="164.338" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V86" gate="GND" x="165.1" y="139.7" smashed="yes">
<attribute name="VALUE" x="162.56" y="136.398" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V91" gate="GND" x="172.72" y="139.7" smashed="yes">
<attribute name="VALUE" x="170.18" y="136.398" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V96" gate="GND" x="180.34" y="139.7" smashed="yes">
<attribute name="VALUE" x="177.8" y="136.398" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="FD2" gate="G$1" x="-7.62" y="0" smashed="yes"/>
<instance part="FD4" gate="G$2" x="322.58" y="0" smashed="yes">
<attribute name="SHEET" x="408.94" y="1.27" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="V72" gate="G$1" x="129.54" y="238.76" smashed="yes" rot="R90">
<attribute name="VALUE" x="126.492" y="236.22" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="V73" gate="G$1" x="129.54" y="213.36" smashed="yes" rot="R90">
<attribute name="VALUE" x="126.492" y="210.82" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="V74" gate="G$1" x="129.54" y="187.96" smashed="yes" rot="R90">
<attribute name="VALUE" x="126.492" y="185.42" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="V75" gate="G$1" x="129.54" y="160.02" smashed="yes" rot="R90">
<attribute name="VALUE" x="126.492" y="157.48" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="P1" gate="G$1" x="30.48" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="25.4" y="187.96" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="P4" gate="G$1" x="96.52" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="104.14" y="213.36" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="C23" gate="G$1" x="7.62" y="187.96" smashed="yes" rot="R270">
<attribute name="NAME" x="5.08" y="193.04" size="1.4224" layer="95" ratio="9" rot="R270"/>
<attribute name="VALUE" x="2.54" y="190.5" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V69" gate="GND" x="7.62" y="177.8" smashed="yes">
<attribute name="VALUE" x="5.08" y="174.498" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C24" gate="G$1" x="15.24" y="187.96" smashed="yes" rot="R270">
<attribute name="NAME" x="12.7" y="193.04" size="1.4224" layer="95" ratio="9" rot="R270"/>
<attribute name="VALUE" x="10.16" y="190.5" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V70" gate="GND" x="15.24" y="177.8" smashed="yes">
<attribute name="VALUE" x="12.7" y="174.498" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="FB3" gate="G$1" x="147.32" y="266.7" smashed="yes" rot="R180">
<attribute name="NAME" x="151.13" y="262.382" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="151.384" y="264.668" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="P2" gate="G$1" x="93.98" y="187.96" smashed="yes" rot="R270">
<attribute name="NAME" x="101.6" y="187.96" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="P3" gate="G$1" x="27.94" y="226.06" smashed="yes" rot="R90">
<attribute name="NAME" x="20.32" y="226.06" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="U6" gate="SECUBE_PWR" x="284.48" y="215.9" smashed="yes">
<attribute name="NAME" x="259.08" y="148.59" size="1.778" layer="95"/>
<attribute name="VALUE" x="259.08" y="146.05" size="1.778" layer="96"/>
</instance>
<instance part="U6" gate="SECUBE_SIGNAL" x="63.5" y="208.28" smashed="yes">
<attribute name="NAME" x="48.26" y="160.02" size="1.778" layer="95"/>
<attribute name="VALUE" x="48.26" y="157.48" size="1.778" layer="96"/>
</instance>
<instance part="U6" gate="SECUBE_FPGA" x="5.08" y="83.82" smashed="yes">
<attribute name="NAME" x="5.08" y="10.16" size="2.0828" layer="95" ratio="6"/>
<attribute name="VALUE" x="5.08" y="7.62" size="2.0828" layer="96" ratio="6"/>
</instance>
<instance part="V71" gate="GND" x="119.38" y="7.62" smashed="yes">
<attribute name="VALUE" x="116.84" y="4.318" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U14" gate="G$1" x="279.4" y="129.54" smashed="yes">
<attribute name="NAME" x="265.43" y="139.7" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="265.43" y="137.16" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="R34" gate="A" x="254" y="119.38" smashed="yes">
<attribute name="NAME" x="249.7836" y="121.2088" size="1.6764" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="257.81" y="121.158" size="1.4224" layer="96"/>
</instance>
<instance part="R35" gate="G$1" x="254" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="246.888" y="115.316" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="257.556" y="115.316" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V103" gate="G$1" x="223.52" y="139.7" smashed="yes" rot="R90">
<attribute name="VALUE" x="220.472" y="137.16" size="1.4224" layer="95" ratio="9" rot="R90"/>
</instance>
<instance part="V106" gate="GND" x="279.4" y="99.06" smashed="yes">
<attribute name="VALUE" x="276.86" y="95.758" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V107" gate="GND" x="294.64" y="114.3" smashed="yes">
<attribute name="VALUE" x="292.1" y="110.998" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V108" gate="GND" x="304.8" y="114.3" smashed="yes">
<attribute name="VALUE" x="302.26" y="110.998" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="FD3" gate="G$1" x="127" y="269.24" smashed="yes"/>
<instance part="R29" gate="G$1" x="134.62" y="266.7" smashed="yes">
<attribute name="NAME" x="133.35" y="270.764" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="133.35" y="268.986" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R30" gate="G$1" x="137.16" y="238.76" smashed="yes">
<attribute name="NAME" x="130.81" y="242.824" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="130.81" y="241.046" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R31" gate="G$1" x="137.16" y="213.36" smashed="yes">
<attribute name="NAME" x="130.81" y="217.424" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="130.81" y="215.646" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R32" gate="G$1" x="137.16" y="187.96" smashed="yes">
<attribute name="NAME" x="130.81" y="192.024" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="130.81" y="190.246" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R33" gate="G$1" x="137.16" y="160.02" smashed="yes">
<attribute name="NAME" x="130.81" y="164.084" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="130.81" y="162.306" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R36" gate="G$1" x="416.56" y="266.7" smashed="yes">
<attribute name="NAME" x="410.21" y="270.764" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="410.21" y="268.986" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="C53" gate="A" x="279.4" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="276.6472" y="102.4436" size="1.27" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="283.13" y="102.03" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C54" gate="G$1" x="294.64" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="288.036" y="124.714" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="290.322" y="124.714" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C69" gate="G$1" x="398.78" y="236.22" smashed="yes" rot="R90">
<attribute name="NAME" x="392.176" y="231.394" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="394.462" y="231.394" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="V123" gate="GND" x="398.78" y="228.6" smashed="yes">
<attribute name="VALUE" x="396.24" y="225.298" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C36" gate="G$1" x="172.72" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="168.402" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C41" gate="G$1" x="180.34" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="176.022" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C46" gate="G$1" x="187.96" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="183.642" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C47" gate="G$1" x="195.58" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="191.262" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C48" gate="G$1" x="203.2" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="198.882" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C49" gate="G$1" x="210.82" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="206.502" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C50" gate="G$1" x="218.44" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="214.122" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C51" gate="G$1" x="226.06" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="221.742" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C52" gate="G$1" x="233.68" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="229.362" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C32" gate="G$1" x="165.1" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="160.782" y="224.282" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C37" gate="G$1" x="172.72" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="168.402" y="224.282" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C42" gate="G$1" x="180.34" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="176.022" y="224.282" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C33" gate="G$1" x="165.1" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="160.782" y="198.882" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C38" gate="G$1" x="172.72" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="168.402" y="198.882" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C43" gate="G$1" x="180.34" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="176.022" y="198.882" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C34" gate="G$1" x="165.1" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="160.782" y="173.482" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C39" gate="G$1" x="172.72" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="168.402" y="173.482" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C44" gate="G$1" x="180.34" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="176.022" y="173.482" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C35" gate="G$1" x="165.1" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="160.782" y="145.542" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C40" gate="G$1" x="172.72" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="168.402" y="145.542" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C45" gate="G$1" x="180.34" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="176.022" y="145.542" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C56" gate="G$1" x="330.2" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="325.882" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C57" gate="G$1" x="337.82" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="333.502" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C59" gate="G$1" x="345.44" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="341.122" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C60" gate="G$1" x="353.06" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="348.742" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C62" gate="G$1" x="360.68" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="356.362" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C64" gate="G$1" x="368.3" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="363.982" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C65" gate="G$1" x="375.92" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="371.602" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C66" gate="G$1" x="383.54" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="379.222" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C67" gate="G$1" x="391.16" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="386.842" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C68" gate="G$1" x="398.78" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="394.462" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="C31" gate="G$1" x="165.1" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="160.782" y="252.222" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="V82" gate="GND" x="165.1" y="246.38" smashed="yes">
<attribute name="VALUE" x="162.56" y="243.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C58" gate="G$1" x="342.9" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="338.582" y="209.042" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="V111" gate="GND" x="342.9" y="203.2" smashed="yes">
<attribute name="VALUE" x="340.36" y="199.898" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V78" gate="GND" x="38.1" y="251.46" smashed="yes">
<attribute name="VALUE" x="35.56" y="248.158" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="P20" gate="G$1" x="38.1" y="259.08" smashed="yes">
<attribute name="NAME" x="38.1" y="266.7" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="J2" gate="A" x="78.74" y="33.02" smashed="yes">
<attribute name="NAME" x="89.2556" y="42.1386" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="88.6206" y="39.5986" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="C26" gate="G$1" x="0" y="254" smashed="yes" rot="R90">
<attribute name="NAME" x="-4.318" y="249.682" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="V79" gate="GND" x="0" y="243.84" smashed="yes">
<attribute name="VALUE" x="-2.54" y="240.538" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V175" gate="G$1" x="0" y="261.62" smashed="yes">
<attribute name="VALUE" x="-2.54" y="264.668" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="C28" gate="G$1" x="10.16" y="254" smashed="yes" rot="R90">
<attribute name="NAME" x="5.842" y="249.682" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="V217" gate="GND" x="10.16" y="243.84" smashed="yes">
<attribute name="VALUE" x="7.62" y="240.538" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V218" gate="G$1" x="10.16" y="261.62" smashed="yes">
<attribute name="VALUE" x="7.62" y="264.668" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="C29" gate="G$1" x="20.32" y="254" smashed="yes" rot="R90">
<attribute name="NAME" x="16.002" y="249.682" size="1.4224" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="V219" gate="GND" x="20.32" y="243.84" smashed="yes">
<attribute name="VALUE" x="17.78" y="240.538" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V220" gate="G$1" x="20.32" y="261.62" smashed="yes">
<attribute name="VALUE" x="17.78" y="264.668" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="C55" gate="G$1" x="304.8" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="300.482" y="124.968" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="309.88" y="124.46" size="1.4224" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="109.22" y1="15.24" x2="119.38" y2="15.24" width="0.1524" layer="91"/>
<pinref part="V71" gate="GND" pin="GND"/>
<wire x1="119.38" y1="15.24" x2="119.38" y2="12.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="12.7" x2="119.38" y2="10.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="17.78" x2="119.38" y2="17.78" width="0.1524" layer="91"/>
<wire x1="119.38" y1="17.78" x2="119.38" y2="15.24" width="0.1524" layer="91"/>
<junction x="119.38" y="15.24"/>
<wire x1="109.22" y1="20.32" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
<wire x1="119.38" y1="20.32" x2="119.38" y2="17.78" width="0.1524" layer="91"/>
<junction x="119.38" y="17.78"/>
<wire x1="109.22" y1="22.86" x2="119.38" y2="22.86" width="0.1524" layer="91"/>
<wire x1="119.38" y1="22.86" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
<junction x="119.38" y="20.32"/>
<wire x1="109.22" y1="25.4" x2="119.38" y2="25.4" width="0.1524" layer="91"/>
<wire x1="119.38" y1="25.4" x2="119.38" y2="22.86" width="0.1524" layer="91"/>
<junction x="119.38" y="22.86"/>
<wire x1="109.22" y1="27.94" x2="119.38" y2="27.94" width="0.1524" layer="91"/>
<wire x1="119.38" y1="27.94" x2="119.38" y2="25.4" width="0.1524" layer="91"/>
<junction x="119.38" y="25.4"/>
<wire x1="109.22" y1="30.48" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<wire x1="119.38" y1="30.48" x2="119.38" y2="27.94" width="0.1524" layer="91"/>
<junction x="119.38" y="27.94"/>
<wire x1="109.22" y1="33.02" x2="119.38" y2="33.02" width="0.1524" layer="91"/>
<wire x1="119.38" y1="33.02" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<junction x="119.38" y="30.48"/>
<wire x1="81.28" y1="15.24" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<wire x1="73.66" y1="15.24" x2="73.66" y2="7.62" width="0.1524" layer="91"/>
<wire x1="73.66" y1="7.62" x2="111.76" y2="7.62" width="0.1524" layer="91"/>
<wire x1="111.76" y1="7.62" x2="111.76" y2="12.7" width="0.1524" layer="91"/>
<wire x1="111.76" y1="12.7" x2="119.38" y2="12.7" width="0.1524" layer="91"/>
<junction x="119.38" y="12.7"/>
<pinref part="J2" gate="A" pin="15"/>
<pinref part="J2" gate="A" pin="16"/>
<pinref part="J2" gate="A" pin="14"/>
<pinref part="J2" gate="A" pin="12"/>
<pinref part="J2" gate="A" pin="10"/>
<pinref part="J2" gate="A" pin="8"/>
<pinref part="J2" gate="A" pin="6"/>
<pinref part="J2" gate="A" pin="4"/>
<pinref part="J2" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="C63" gate="G$1" pin="2"/>
<pinref part="V117" gate="GND" pin="GND"/>
<wire x1="365.76" y1="220.98" x2="365.76" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C61" gate="G$1" pin="2"/>
<pinref part="V115" gate="GND" pin="GND"/>
<wire x1="358.14" y1="215.9" x2="358.14" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="314.96" y1="182.88" x2="330.2" y2="182.88" width="0.1524" layer="91"/>
<wire x1="330.2" y1="182.88" x2="330.2" y2="180.34" width="0.1524" layer="91"/>
<wire x1="330.2" y1="180.34" x2="330.2" y2="177.8" width="0.1524" layer="91"/>
<wire x1="330.2" y1="177.8" x2="330.2" y2="175.26" width="0.1524" layer="91"/>
<wire x1="330.2" y1="175.26" x2="330.2" y2="172.72" width="0.1524" layer="91"/>
<wire x1="330.2" y1="172.72" x2="330.2" y2="170.18" width="0.1524" layer="91"/>
<wire x1="330.2" y1="170.18" x2="330.2" y2="167.64" width="0.1524" layer="91"/>
<wire x1="330.2" y1="167.64" x2="330.2" y2="165.1" width="0.1524" layer="91"/>
<wire x1="330.2" y1="165.1" x2="330.2" y2="162.56" width="0.1524" layer="91"/>
<wire x1="330.2" y1="162.56" x2="330.2" y2="160.02" width="0.1524" layer="91"/>
<wire x1="330.2" y1="160.02" x2="330.2" y2="157.48" width="0.1524" layer="91"/>
<wire x1="330.2" y1="157.48" x2="314.96" y2="157.48" width="0.1524" layer="91"/>
<wire x1="314.96" y1="160.02" x2="330.2" y2="160.02" width="0.1524" layer="91"/>
<junction x="330.2" y="160.02"/>
<wire x1="314.96" y1="162.56" x2="330.2" y2="162.56" width="0.1524" layer="91"/>
<junction x="330.2" y="162.56"/>
<wire x1="314.96" y1="165.1" x2="330.2" y2="165.1" width="0.1524" layer="91"/>
<junction x="330.2" y="165.1"/>
<wire x1="314.96" y1="167.64" x2="330.2" y2="167.64" width="0.1524" layer="91"/>
<junction x="330.2" y="167.64"/>
<wire x1="314.96" y1="170.18" x2="330.2" y2="170.18" width="0.1524" layer="91"/>
<junction x="330.2" y="170.18"/>
<wire x1="314.96" y1="172.72" x2="330.2" y2="172.72" width="0.1524" layer="91"/>
<junction x="330.2" y="172.72"/>
<wire x1="314.96" y1="175.26" x2="330.2" y2="175.26" width="0.1524" layer="91"/>
<junction x="330.2" y="175.26"/>
<wire x1="314.96" y1="177.8" x2="330.2" y2="177.8" width="0.1524" layer="91"/>
<junction x="330.2" y="177.8"/>
<wire x1="314.96" y1="180.34" x2="330.2" y2="180.34" width="0.1524" layer="91"/>
<junction x="330.2" y="180.34"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@21"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@20"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@19"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@18"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@17"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@16"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@15"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@14"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@13"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@12"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@11"/>
<wire x1="330.2" y1="182.88" x2="345.44" y2="182.88" width="0.1524" layer="91"/>
<junction x="330.2" y="182.88"/>
<pinref part="V113" gate="GND" pin="GND"/>
<wire x1="345.44" y1="182.88" x2="345.44" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="254" y1="182.88" x2="241.3" y2="182.88" width="0.1524" layer="91"/>
<wire x1="241.3" y1="182.88" x2="241.3" y2="180.34" width="0.1524" layer="91"/>
<wire x1="241.3" y1="180.34" x2="241.3" y2="177.8" width="0.1524" layer="91"/>
<wire x1="241.3" y1="177.8" x2="241.3" y2="175.26" width="0.1524" layer="91"/>
<wire x1="241.3" y1="175.26" x2="241.3" y2="172.72" width="0.1524" layer="91"/>
<wire x1="241.3" y1="172.72" x2="241.3" y2="170.18" width="0.1524" layer="91"/>
<wire x1="241.3" y1="170.18" x2="241.3" y2="167.64" width="0.1524" layer="91"/>
<wire x1="241.3" y1="167.64" x2="241.3" y2="165.1" width="0.1524" layer="91"/>
<wire x1="241.3" y1="165.1" x2="241.3" y2="162.56" width="0.1524" layer="91"/>
<wire x1="241.3" y1="162.56" x2="241.3" y2="160.02" width="0.1524" layer="91"/>
<wire x1="241.3" y1="160.02" x2="241.3" y2="157.48" width="0.1524" layer="91"/>
<wire x1="241.3" y1="157.48" x2="254" y2="157.48" width="0.1524" layer="91"/>
<wire x1="254" y1="160.02" x2="241.3" y2="160.02" width="0.1524" layer="91"/>
<junction x="241.3" y="160.02"/>
<wire x1="254" y1="162.56" x2="241.3" y2="162.56" width="0.1524" layer="91"/>
<junction x="241.3" y="162.56"/>
<wire x1="254" y1="165.1" x2="241.3" y2="165.1" width="0.1524" layer="91"/>
<junction x="241.3" y="165.1"/>
<wire x1="254" y1="167.64" x2="241.3" y2="167.64" width="0.1524" layer="91"/>
<junction x="241.3" y="167.64"/>
<wire x1="254" y1="170.18" x2="241.3" y2="170.18" width="0.1524" layer="91"/>
<junction x="241.3" y="170.18"/>
<wire x1="254" y1="172.72" x2="241.3" y2="172.72" width="0.1524" layer="91"/>
<junction x="241.3" y="172.72"/>
<wire x1="254" y1="175.26" x2="241.3" y2="175.26" width="0.1524" layer="91"/>
<junction x="241.3" y="175.26"/>
<wire x1="254" y1="177.8" x2="241.3" y2="177.8" width="0.1524" layer="91"/>
<junction x="241.3" y="177.8"/>
<wire x1="254" y1="180.34" x2="241.3" y2="180.34" width="0.1524" layer="91"/>
<junction x="241.3" y="180.34"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@1"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@0"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@2"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@3"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@4"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@5"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@6"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@7"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@8"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@9"/>
<pinref part="U6" gate="SECUBE_PWR" pin="VSS@10"/>
<wire x1="241.3" y1="182.88" x2="220.98" y2="182.88" width="0.1524" layer="91"/>
<junction x="241.3" y="182.88"/>
<pinref part="V102" gate="GND" pin="GND"/>
<wire x1="220.98" y1="182.88" x2="220.98" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="V76" gate="GND" pin="GND"/>
<wire x1="142.24" y1="251.46" x2="142.24" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="V69" gate="GND" pin="GND"/>
<wire x1="7.62" y1="182.88" x2="7.62" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="V70" gate="GND" pin="GND"/>
<wire x1="15.24" y1="182.88" x2="15.24" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V106" gate="GND" pin="GND"/>
<pinref part="C53" gate="A" pin="11"/>
<wire x1="279.4" y1="101.6" x2="279.4" y2="103.4542" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V108" gate="GND" pin="GND"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="304.8" y1="124.46" x2="304.8" y2="116.84" width="0.1524" layer="91"/>
<wire x1="304.8" y1="126.238" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C54" gate="G$1" pin="1"/>
<pinref part="V107" gate="GND" pin="GND"/>
<wire x1="294.64" y1="126.238" x2="294.64" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C69" gate="G$1" pin="1"/>
<pinref part="V123" gate="GND" pin="GND"/>
<wire x1="398.78" y1="232.918" x2="398.78" y2="231.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="1"/>
<pinref part="V87" gate="GND" pin="GND"/>
<wire x1="172.72" y1="253.238" x2="172.72" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="1"/>
<pinref part="V92" gate="GND" pin="GND"/>
<wire x1="180.34" y1="253.238" x2="180.34" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="1"/>
<pinref part="V97" gate="GND" pin="GND"/>
<wire x1="187.96" y1="253.238" x2="187.96" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="1"/>
<pinref part="V98" gate="GND" pin="GND"/>
<wire x1="195.58" y1="253.238" x2="195.58" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="1"/>
<pinref part="V99" gate="GND" pin="GND"/>
<wire x1="203.2" y1="253.238" x2="203.2" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="1"/>
<pinref part="V100" gate="GND" pin="GND"/>
<wire x1="210.82" y1="253.238" x2="210.82" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="1"/>
<pinref part="V101" gate="GND" pin="GND"/>
<wire x1="218.44" y1="253.238" x2="218.44" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="1"/>
<pinref part="V104" gate="GND" pin="GND"/>
<wire x1="226.06" y1="253.238" x2="226.06" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="1"/>
<pinref part="V105" gate="GND" pin="GND"/>
<wire x1="233.68" y1="253.238" x2="233.68" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="V83" gate="GND" pin="GND"/>
<wire x1="165.1" y1="225.298" x2="165.1" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C37" gate="G$1" pin="1"/>
<pinref part="V88" gate="GND" pin="GND"/>
<wire x1="172.72" y1="225.298" x2="172.72" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C42" gate="G$1" pin="1"/>
<pinref part="V93" gate="GND" pin="GND"/>
<wire x1="180.34" y1="225.298" x2="180.34" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="V84" gate="GND" pin="GND"/>
<wire x1="165.1" y1="199.898" x2="165.1" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C38" gate="G$1" pin="1"/>
<pinref part="V89" gate="GND" pin="GND"/>
<wire x1="172.72" y1="199.898" x2="172.72" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="1"/>
<pinref part="V94" gate="GND" pin="GND"/>
<wire x1="180.34" y1="199.898" x2="180.34" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<pinref part="V85" gate="GND" pin="GND"/>
<wire x1="165.1" y1="174.498" x2="165.1" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="1"/>
<pinref part="V90" gate="GND" pin="GND"/>
<wire x1="172.72" y1="174.498" x2="172.72" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C44" gate="G$1" pin="1"/>
<pinref part="V95" gate="GND" pin="GND"/>
<wire x1="180.34" y1="174.498" x2="180.34" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="1"/>
<pinref part="V86" gate="GND" pin="GND"/>
<wire x1="165.1" y1="146.558" x2="165.1" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C40" gate="G$1" pin="1"/>
<pinref part="V91" gate="GND" pin="GND"/>
<wire x1="172.72" y1="146.558" x2="172.72" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C45" gate="G$1" pin="1"/>
<pinref part="V96" gate="GND" pin="GND"/>
<wire x1="180.34" y1="146.558" x2="180.34" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C56" gate="G$1" pin="1"/>
<pinref part="V109" gate="GND" pin="GND"/>
<wire x1="330.2" y1="253.238" x2="330.2" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C57" gate="G$1" pin="1"/>
<pinref part="V110" gate="GND" pin="GND"/>
<wire x1="337.82" y1="253.238" x2="337.82" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C59" gate="G$1" pin="1"/>
<pinref part="V112" gate="GND" pin="GND"/>
<wire x1="345.44" y1="253.238" x2="345.44" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C60" gate="G$1" pin="1"/>
<pinref part="V114" gate="GND" pin="GND"/>
<wire x1="353.06" y1="253.238" x2="353.06" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C62" gate="G$1" pin="1"/>
<pinref part="V116" gate="GND" pin="GND"/>
<wire x1="360.68" y1="253.238" x2="360.68" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C64" gate="G$1" pin="1"/>
<pinref part="V118" gate="GND" pin="GND"/>
<wire x1="368.3" y1="253.238" x2="368.3" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C65" gate="G$1" pin="1"/>
<pinref part="V119" gate="GND" pin="GND"/>
<wire x1="375.92" y1="253.238" x2="375.92" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C66" gate="G$1" pin="1"/>
<pinref part="V120" gate="GND" pin="GND"/>
<wire x1="383.54" y1="253.238" x2="383.54" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C67" gate="G$1" pin="1"/>
<pinref part="V121" gate="GND" pin="GND"/>
<wire x1="391.16" y1="253.238" x2="391.16" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="1"/>
<pinref part="V122" gate="GND" pin="GND"/>
<wire x1="398.78" y1="253.238" x2="398.78" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="1"/>
<pinref part="V82" gate="GND" pin="GND"/>
<wire x1="165.1" y1="253.238" x2="165.1" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C58" gate="G$1" pin="1"/>
<pinref part="V111" gate="GND" pin="GND"/>
<wire x1="342.9" y1="210.058" x2="342.9" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P20" gate="G$1" pin="1"/>
<pinref part="V78" gate="GND" pin="GND"/>
<wire x1="38.1" y1="259.08" x2="38.1" y2="254" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="V79" gate="GND" pin="GND"/>
<wire x1="0" y1="250.698" x2="0" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="1"/>
<pinref part="V217" gate="GND" pin="GND"/>
<wire x1="10.16" y1="250.698" x2="10.16" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="V219" gate="GND" pin="GND"/>
<wire x1="20.32" y1="250.698" x2="20.32" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PL_0" class="0">
<segment>
<wire x1="254" y1="266.7" x2="241.3" y2="266.7" width="0.1524" layer="91"/>
<wire x1="241.3" y1="266.7" x2="241.3" y2="264.16" width="0.1524" layer="91"/>
<wire x1="241.3" y1="264.16" x2="241.3" y2="261.62" width="0.1524" layer="91"/>
<wire x1="241.3" y1="261.62" x2="241.3" y2="259.08" width="0.1524" layer="91"/>
<wire x1="241.3" y1="259.08" x2="241.3" y2="256.54" width="0.1524" layer="91"/>
<wire x1="241.3" y1="256.54" x2="241.3" y2="254" width="0.1524" layer="91"/>
<wire x1="241.3" y1="254" x2="241.3" y2="251.46" width="0.1524" layer="91"/>
<wire x1="241.3" y1="251.46" x2="241.3" y2="248.92" width="0.1524" layer="91"/>
<wire x1="241.3" y1="248.92" x2="241.3" y2="246.38" width="0.1524" layer="91"/>
<wire x1="241.3" y1="246.38" x2="241.3" y2="243.84" width="0.1524" layer="91"/>
<wire x1="241.3" y1="243.84" x2="254" y2="243.84" width="0.1524" layer="91"/>
<wire x1="254" y1="246.38" x2="241.3" y2="246.38" width="0.1524" layer="91"/>
<junction x="241.3" y="246.38"/>
<wire x1="254" y1="248.92" x2="241.3" y2="248.92" width="0.1524" layer="91"/>
<junction x="241.3" y="248.92"/>
<wire x1="254" y1="251.46" x2="241.3" y2="251.46" width="0.1524" layer="91"/>
<junction x="241.3" y="251.46"/>
<wire x1="254" y1="254" x2="241.3" y2="254" width="0.1524" layer="91"/>
<junction x="241.3" y="254"/>
<wire x1="254" y1="256.54" x2="241.3" y2="256.54" width="0.1524" layer="91"/>
<junction x="241.3" y="256.54"/>
<wire x1="254" y1="259.08" x2="241.3" y2="259.08" width="0.1524" layer="91"/>
<junction x="241.3" y="259.08"/>
<wire x1="254" y1="261.62" x2="241.3" y2="261.62" width="0.1524" layer="91"/>
<junction x="241.3" y="261.62"/>
<wire x1="254" y1="264.16" x2="241.3" y2="264.16" width="0.1524" layer="91"/>
<junction x="241.3" y="264.16"/>
<junction x="241.3" y="266.7"/>
<wire x1="241.3" y1="266.7" x2="233.68" y2="266.7" width="0.1524" layer="91"/>
<wire x1="233.68" y1="266.7" x2="226.06" y2="266.7" width="0.1524" layer="91"/>
<wire x1="226.06" y1="266.7" x2="218.44" y2="266.7" width="0.1524" layer="91"/>
<wire x1="218.44" y1="266.7" x2="210.82" y2="266.7" width="0.1524" layer="91"/>
<wire x1="210.82" y1="266.7" x2="203.2" y2="266.7" width="0.1524" layer="91"/>
<wire x1="203.2" y1="266.7" x2="195.58" y2="266.7" width="0.1524" layer="91"/>
<wire x1="195.58" y1="266.7" x2="187.96" y2="266.7" width="0.1524" layer="91"/>
<wire x1="187.96" y1="266.7" x2="180.34" y2="266.7" width="0.1524" layer="91"/>
<wire x1="180.34" y1="266.7" x2="172.72" y2="266.7" width="0.1524" layer="91"/>
<wire x1="172.72" y1="266.7" x2="165.1" y2="266.7" width="0.1524" layer="91"/>
<wire x1="165.1" y1="266.7" x2="151.13" y2="266.7" width="0.1524" layer="91"/>
<pinref part="FB3" gate="G$1" pin="1"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@0"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@1"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@2"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@3"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@4"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@5"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@6"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@7"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@8"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCORE@9"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="172.72" y1="259.842" x2="172.72" y2="266.7" width="0.1524" layer="91"/>
<junction x="172.72" y="266.7"/>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="180.34" y1="259.842" x2="180.34" y2="266.7" width="0.1524" layer="91"/>
<junction x="180.34" y="266.7"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="187.96" y1="259.842" x2="187.96" y2="266.7" width="0.1524" layer="91"/>
<junction x="187.96" y="266.7"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="195.58" y1="259.842" x2="195.58" y2="266.7" width="0.1524" layer="91"/>
<junction x="195.58" y="266.7"/>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="203.2" y1="259.842" x2="203.2" y2="266.7" width="0.1524" layer="91"/>
<junction x="203.2" y="266.7"/>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="218.44" y1="259.842" x2="218.44" y2="266.7" width="0.1524" layer="91"/>
<junction x="218.44" y="266.7"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="210.82" y1="259.842" x2="210.82" y2="266.7" width="0.1524" layer="91"/>
<junction x="210.82" y="266.7"/>
<pinref part="C51" gate="G$1" pin="2"/>
<wire x1="226.06" y1="259.842" x2="226.06" y2="266.7" width="0.1524" layer="91"/>
<junction x="226.06" y="266.7"/>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="233.68" y1="259.842" x2="233.68" y2="266.7" width="0.1524" layer="91"/>
<junction x="233.68" y="266.7"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="165.1" y1="259.842" x2="165.1" y2="266.7" width="0.1524" layer="91"/>
<junction x="165.1" y="266.7"/>
</segment>
</net>
<net name="PL1" class="0">
<segment>
<wire x1="254" y1="238.76" x2="241.3" y2="238.76" width="0.1524" layer="91"/>
<wire x1="241.3" y1="238.76" x2="241.3" y2="236.22" width="0.1524" layer="91"/>
<wire x1="241.3" y1="236.22" x2="241.3" y2="233.68" width="0.1524" layer="91"/>
<wire x1="241.3" y1="233.68" x2="254" y2="233.68" width="0.1524" layer="91"/>
<wire x1="254" y1="236.22" x2="241.3" y2="236.22" width="0.1524" layer="91"/>
<junction x="241.3" y="236.22"/>
<wire x1="241.3" y1="238.76" x2="180.34" y2="238.76" width="0.1524" layer="91"/>
<junction x="241.3" y="238.76"/>
<wire x1="180.34" y1="238.76" x2="172.72" y2="238.76" width="0.1524" layer="91"/>
<wire x1="172.72" y1="238.76" x2="165.1" y2="238.76" width="0.1524" layer="91"/>
<wire x1="165.1" y1="238.76" x2="141.986" y2="238.76" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO0@0"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO0@1"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO0@2"/>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="165.1" y1="231.902" x2="165.1" y2="238.76" width="0.1524" layer="91"/>
<junction x="165.1" y="238.76"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="172.72" y1="231.902" x2="172.72" y2="238.76" width="0.1524" layer="91"/>
<junction x="172.72" y="238.76"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="180.34" y1="231.902" x2="180.34" y2="238.76" width="0.1524" layer="91"/>
<junction x="180.34" y="238.76"/>
</segment>
</net>
<net name="PL3" class="0">
<segment>
<wire x1="254" y1="218.44" x2="241.3" y2="218.44" width="0.1524" layer="91"/>
<wire x1="241.3" y1="218.44" x2="241.3" y2="215.9" width="0.1524" layer="91"/>
<wire x1="241.3" y1="215.9" x2="241.3" y2="213.36" width="0.1524" layer="91"/>
<wire x1="241.3" y1="213.36" x2="254" y2="213.36" width="0.1524" layer="91"/>
<wire x1="254" y1="215.9" x2="241.3" y2="215.9" width="0.1524" layer="91"/>
<junction x="241.3" y="215.9"/>
<wire x1="241.3" y1="218.44" x2="200.66" y2="218.44" width="0.1524" layer="91"/>
<junction x="241.3" y="218.44"/>
<wire x1="200.66" y1="218.44" x2="200.66" y2="187.96" width="0.1524" layer="91"/>
<wire x1="200.66" y1="187.96" x2="180.34" y2="187.96" width="0.1524" layer="91"/>
<wire x1="180.34" y1="187.96" x2="172.72" y2="187.96" width="0.1524" layer="91"/>
<wire x1="172.72" y1="187.96" x2="165.1" y2="187.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="187.96" x2="141.986" y2="187.96" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO2@0"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO2@1"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO2@2"/>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="165.1" y1="181.102" x2="165.1" y2="187.96" width="0.1524" layer="91"/>
<junction x="165.1" y="187.96"/>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="172.72" y1="181.102" x2="172.72" y2="187.96" width="0.1524" layer="91"/>
<junction x="172.72" y="187.96"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="180.34" y1="181.102" x2="180.34" y2="187.96" width="0.1524" layer="91"/>
<junction x="180.34" y="187.96"/>
</segment>
</net>
<net name="CPU_VDD" class="0">
<segment>
<wire x1="314.96" y1="266.7" x2="322.58" y2="266.7" width="0.1524" layer="91"/>
<wire x1="322.58" y1="266.7" x2="322.58" y2="264.16" width="0.1524" layer="91"/>
<wire x1="322.58" y1="264.16" x2="322.58" y2="261.62" width="0.1524" layer="91"/>
<wire x1="322.58" y1="261.62" x2="322.58" y2="259.08" width="0.1524" layer="91"/>
<wire x1="322.58" y1="259.08" x2="322.58" y2="256.54" width="0.1524" layer="91"/>
<wire x1="322.58" y1="256.54" x2="322.58" y2="254" width="0.1524" layer="91"/>
<wire x1="322.58" y1="254" x2="322.58" y2="251.46" width="0.1524" layer="91"/>
<wire x1="322.58" y1="251.46" x2="322.58" y2="248.92" width="0.1524" layer="91"/>
<wire x1="322.58" y1="248.92" x2="322.58" y2="246.38" width="0.1524" layer="91"/>
<wire x1="322.58" y1="246.38" x2="322.58" y2="243.84" width="0.1524" layer="91"/>
<wire x1="322.58" y1="243.84" x2="314.96" y2="243.84" width="0.1524" layer="91"/>
<wire x1="314.96" y1="264.16" x2="322.58" y2="264.16" width="0.1524" layer="91"/>
<junction x="322.58" y="264.16"/>
<wire x1="314.96" y1="261.62" x2="322.58" y2="261.62" width="0.1524" layer="91"/>
<junction x="322.58" y="261.62"/>
<wire x1="314.96" y1="259.08" x2="322.58" y2="259.08" width="0.1524" layer="91"/>
<junction x="322.58" y="259.08"/>
<wire x1="314.96" y1="256.54" x2="322.58" y2="256.54" width="0.1524" layer="91"/>
<junction x="322.58" y="256.54"/>
<wire x1="314.96" y1="254" x2="322.58" y2="254" width="0.1524" layer="91"/>
<junction x="322.58" y="254"/>
<wire x1="314.96" y1="251.46" x2="322.58" y2="251.46" width="0.1524" layer="91"/>
<junction x="322.58" y="251.46"/>
<wire x1="314.96" y1="248.92" x2="322.58" y2="248.92" width="0.1524" layer="91"/>
<junction x="322.58" y="248.92"/>
<wire x1="314.96" y1="246.38" x2="322.58" y2="246.38" width="0.1524" layer="91"/>
<junction x="322.58" y="246.38"/>
<junction x="322.58" y="266.7"/>
<wire x1="322.58" y1="266.7" x2="330.2" y2="266.7" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@0"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@2"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@3"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@4"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@5"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@6"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@7"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@8"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@9"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VDD@1"/>
<pinref part="C69" gate="G$1" pin="2"/>
<wire x1="330.2" y1="266.7" x2="337.82" y2="266.7" width="0.1524" layer="91"/>
<wire x1="337.82" y1="266.7" x2="345.44" y2="266.7" width="0.1524" layer="91"/>
<wire x1="345.44" y1="266.7" x2="353.06" y2="266.7" width="0.1524" layer="91"/>
<wire x1="353.06" y1="266.7" x2="360.68" y2="266.7" width="0.1524" layer="91"/>
<wire x1="360.68" y1="266.7" x2="368.3" y2="266.7" width="0.1524" layer="91"/>
<wire x1="368.3" y1="266.7" x2="375.92" y2="266.7" width="0.1524" layer="91"/>
<wire x1="375.92" y1="266.7" x2="383.54" y2="266.7" width="0.1524" layer="91"/>
<wire x1="383.54" y1="266.7" x2="391.16" y2="266.7" width="0.1524" layer="91"/>
<wire x1="391.16" y1="266.7" x2="398.78" y2="266.7" width="0.1524" layer="91"/>
<wire x1="398.78" y1="266.7" x2="406.4" y2="266.7" width="0.1524" layer="91"/>
<wire x1="398.78" y1="239.522" x2="398.78" y2="241.3" width="0.1524" layer="91"/>
<wire x1="398.78" y1="241.3" x2="406.4" y2="241.3" width="0.1524" layer="91"/>
<wire x1="406.4" y1="241.3" x2="406.4" y2="266.7" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="330.2" y1="259.842" x2="330.2" y2="266.7" width="0.1524" layer="91"/>
<junction x="330.2" y="266.7"/>
<pinref part="C57" gate="G$1" pin="2"/>
<wire x1="337.82" y1="259.842" x2="337.82" y2="266.7" width="0.1524" layer="91"/>
<junction x="337.82" y="266.7"/>
<pinref part="C59" gate="G$1" pin="2"/>
<wire x1="345.44" y1="259.842" x2="345.44" y2="266.7" width="0.1524" layer="91"/>
<junction x="345.44" y="266.7"/>
<pinref part="C60" gate="G$1" pin="2"/>
<wire x1="353.06" y1="259.842" x2="353.06" y2="266.7" width="0.1524" layer="91"/>
<junction x="353.06" y="266.7"/>
<pinref part="C62" gate="G$1" pin="2"/>
<wire x1="360.68" y1="259.842" x2="360.68" y2="266.7" width="0.1524" layer="91"/>
<junction x="360.68" y="266.7"/>
<pinref part="C64" gate="G$1" pin="2"/>
<wire x1="368.3" y1="259.842" x2="368.3" y2="266.7" width="0.1524" layer="91"/>
<junction x="368.3" y="266.7"/>
<pinref part="C65" gate="G$1" pin="2"/>
<wire x1="375.92" y1="259.842" x2="375.92" y2="266.7" width="0.1524" layer="91"/>
<junction x="375.92" y="266.7"/>
<pinref part="C66" gate="G$1" pin="2"/>
<wire x1="383.54" y1="259.842" x2="383.54" y2="266.7" width="0.1524" layer="91"/>
<junction x="383.54" y="266.7"/>
<pinref part="C67" gate="G$1" pin="2"/>
<wire x1="391.16" y1="259.842" x2="391.16" y2="266.7" width="0.1524" layer="91"/>
<junction x="391.16" y="266.7"/>
<pinref part="C68" gate="G$1" pin="2"/>
<wire x1="398.78" y1="259.842" x2="398.78" y2="266.7" width="0.1524" layer="91"/>
<junction x="398.78" y="266.7"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="411.48" y1="266.7" x2="406.4" y2="266.7" width="0.1524" layer="91"/>
<junction x="406.4" y="266.7"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<wire x1="142.24" y1="261.62" x2="142.24" y2="266.7" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<pinref part="FB3" gate="G$1" pin="2"/>
<wire x1="142.24" y1="266.7" x2="143.764" y2="266.7" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="139.446" y1="266.7" x2="142.24" y2="266.7" width="0.1524" layer="91"/>
<junction x="142.24" y="266.7"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<pinref part="V124" gate="G$1" pin="VDD"/>
<wire x1="421.386" y1="266.7" x2="421.64" y2="266.7" width="0.1524" layer="91"/>
<wire x1="421.64" y1="266.7" x2="421.64" y2="269.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="S"/>
<wire x1="276.86" y1="139.7" x2="241.3" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R34" gate="A" pin="22"/>
<wire x1="248.92" y1="119.38" x2="241.3" y2="119.38" width="0.1524" layer="91"/>
<wire x1="241.3" y1="119.38" x2="241.3" y2="139.7" width="0.1524" layer="91"/>
<junction x="241.3" y="139.7"/>
<wire x1="241.3" y1="139.7" x2="223.52" y2="139.7" width="0.1524" layer="91"/>
<pinref part="V103" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="V72" gate="G$1" pin="VDD"/>
<wire x1="132.08" y1="238.76" x2="129.54" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V73" gate="G$1" pin="VDD"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="129.54" y1="213.36" x2="132.08" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V74" gate="G$1" pin="VDD"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="129.54" y1="187.96" x2="132.08" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V75" gate="G$1" pin="VDD"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="129.54" y1="160.02" x2="132.08" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="V175" gate="G$1" pin="VDD"/>
<wire x1="0" y1="257.302" x2="0" y2="261.62" width="0.1524" layer="91" style="longdash"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="V218" gate="G$1" pin="VDD"/>
<wire x1="10.16" y1="257.302" x2="10.16" y2="261.62" width="0.1524" layer="91" style="longdash"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="V220" gate="G$1" pin="VDD"/>
<wire x1="20.32" y1="257.302" x2="20.32" y2="261.62" width="0.1524" layer="91" style="longdash"/>
</segment>
</net>
<net name="CPU_VCAP1" class="0">
<segment>
<wire x1="314.96" y1="236.22" x2="365.76" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="365.76" y1="236.22" x2="365.76" y2="231.14" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VCAP1"/>
</segment>
</net>
<net name="CPU_VCAP2" class="0">
<segment>
<wire x1="314.96" y1="231.14" x2="358.14" y2="231.14" width="0.1524" layer="91"/>
<pinref part="C61" gate="G$1" pin="1"/>
<wire x1="358.14" y1="231.14" x2="358.14" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_VCAP2"/>
</segment>
</net>
<net name="PL2" class="0">
<segment>
<wire x1="254" y1="228.6" x2="241.3" y2="228.6" width="0.1524" layer="91"/>
<wire x1="241.3" y1="226.06" x2="241.3" y2="223.52" width="0.1524" layer="91"/>
<wire x1="241.3" y1="223.52" x2="254" y2="223.52" width="0.1524" layer="91"/>
<wire x1="254" y1="226.06" x2="241.3" y2="226.06" width="0.1524" layer="91"/>
<junction x="241.3" y="226.06"/>
<wire x1="241.3" y1="228.6" x2="241.3" y2="226.06" width="0.1524" layer="91"/>
<wire x1="241.3" y1="228.6" x2="193.04" y2="228.6" width="0.1524" layer="91"/>
<junction x="241.3" y="228.6"/>
<wire x1="193.04" y1="228.6" x2="193.04" y2="213.36" width="0.1524" layer="91"/>
<wire x1="193.04" y1="213.36" x2="180.34" y2="213.36" width="0.1524" layer="91"/>
<wire x1="180.34" y1="213.36" x2="172.72" y2="213.36" width="0.1524" layer="91"/>
<wire x1="172.72" y1="213.36" x2="165.1" y2="213.36" width="0.1524" layer="91"/>
<wire x1="165.1" y1="213.36" x2="141.986" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO1@0"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO1@1"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO1@2"/>
<pinref part="R31" gate="G$1" pin="2"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="165.1" y1="206.502" x2="165.1" y2="213.36" width="0.1524" layer="91"/>
<junction x="165.1" y="213.36"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="172.72" y1="206.502" x2="172.72" y2="213.36" width="0.1524" layer="91"/>
<junction x="172.72" y="213.36"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="180.34" y1="206.502" x2="180.34" y2="213.36" width="0.1524" layer="91"/>
<junction x="180.34" y="213.36"/>
</segment>
</net>
<net name="PL4" class="0">
<segment>
<wire x1="254" y1="208.28" x2="241.3" y2="208.28" width="0.1524" layer="91"/>
<wire x1="241.3" y1="205.74" x2="241.3" y2="203.2" width="0.1524" layer="91"/>
<wire x1="254" y1="203.2" x2="241.3" y2="203.2" width="0.1524" layer="91"/>
<wire x1="241.3" y1="203.2" x2="241.3" y2="198.12" width="0.1524" layer="91"/>
<wire x1="241.3" y1="198.12" x2="254" y2="198.12" width="0.1524" layer="91"/>
<junction x="241.3" y="203.2"/>
<junction x="241.3" y="208.28"/>
<wire x1="241.3" y1="208.28" x2="241.3" y2="205.74" width="0.1524" layer="91"/>
<wire x1="241.3" y1="205.74" x2="208.28" y2="205.74" width="0.1524" layer="91"/>
<wire x1="208.28" y1="205.74" x2="208.28" y2="160.02" width="0.1524" layer="91"/>
<wire x1="208.28" y1="160.02" x2="180.34" y2="160.02" width="0.1524" layer="91"/>
<wire x1="180.34" y1="160.02" x2="172.72" y2="160.02" width="0.1524" layer="91"/>
<wire x1="172.72" y1="160.02" x2="165.1" y2="160.02" width="0.1524" layer="91"/>
<wire x1="165.1" y1="160.02" x2="141.986" y2="160.02" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO3"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO4"/>
<pinref part="U6" gate="SECUBE_PWR" pin="FPGA_VCCIO5"/>
<pinref part="R33" gate="G$1" pin="2"/>
<junction x="241.3" y="205.74"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="165.1" y1="153.162" x2="165.1" y2="160.02" width="0.1524" layer="91"/>
<junction x="165.1" y="160.02"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="172.72" y1="153.162" x2="172.72" y2="160.02" width="0.1524" layer="91"/>
<junction x="172.72" y="160.02"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="180.34" y1="153.162" x2="180.34" y2="160.02" width="0.1524" layer="91"/>
<junction x="180.34" y="160.02"/>
</segment>
</net>
<net name="SC_PWR" class="0">
<segment>
<wire x1="254" y1="190.5" x2="241.3" y2="190.5" width="0.1524" layer="91"/>
<label x="241.3" y="190.5" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_SC_PWR"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="248.92" y1="114.3" x2="218.44" y2="114.3" width="0.1524" layer="91"/>
<label x="220.98" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="SC_VCC" class="0">
<segment>
<wire x1="314.96" y1="190.5" x2="330.2" y2="190.5" width="0.1524" layer="91"/>
<label x="322.58" y="190.5" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_PWR" pin="SC_VCC"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="D"/>
<wire x1="281.94" y1="139.7" x2="294.64" y2="139.7" width="0.1524" layer="91"/>
<wire x1="294.64" y1="139.7" x2="304.8" y2="139.7" width="0.1524" layer="91"/>
<wire x1="304.8" y1="139.7" x2="322.58" y2="139.7" width="0.1524" layer="91"/>
<junction x="304.8" y="139.7"/>
<label x="312.42" y="139.7" size="1.778" layer="95"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="304.8" y1="134.62" x2="304.8" y2="139.7" width="0.1524" layer="91"/>
<wire x1="294.64" y1="132.842" x2="294.64" y2="139.7" width="0.1524" layer="91"/>
<junction x="294.64" y="139.7"/>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="304.8" y1="132.842" x2="304.8" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OSC_IN" class="0">
<segment>
<wire x1="83.82" y1="182.88" x2="101.6" y2="182.88" width="0.1524" layer="91"/>
<label x="86.36" y="182.88" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PH0"/>
</segment>
</net>
<net name="OSC_OUT" class="0">
<segment>
<wire x1="83.82" y1="180.34" x2="101.6" y2="180.34" width="0.1524" layer="91"/>
<label x="86.36" y="180.34" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PH1"/>
</segment>
</net>
<net name="LED-PWR" class="0">
<segment>
<wire x1="43.18" y1="238.76" x2="22.86" y2="238.76" width="0.1524" layer="91"/>
<label x="25.4" y="238.76" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PA3"/>
</segment>
</net>
<net name="LED-RT" class="0">
<segment>
<wire x1="83.82" y1="228.6" x2="101.6" y2="228.6" width="0.1524" layer="91"/>
<label x="86.36" y="228.6" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PC9"/>
</segment>
</net>
<net name="LED-ERR" class="0">
<segment>
<wire x1="83.82" y1="220.98" x2="101.6" y2="220.98" width="0.1524" layer="91"/>
<label x="86.36" y="220.98" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PC12"/>
</segment>
</net>
<net name="VBUS" class="0">
<segment>
<wire x1="43.18" y1="172.72" x2="25.4" y2="172.72" width="0.1524" layer="91"/>
<label x="25.4" y="172.72" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB13"/>
</segment>
</net>
<net name="OTG_DP" class="0">
<segment>
<wire x1="43.18" y1="167.64" x2="25.4" y2="167.64" width="0.1524" layer="91"/>
<label x="25.4" y="167.64" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB15"/>
</segment>
</net>
<net name="OTG_DM" class="0">
<segment>
<wire x1="43.18" y1="170.18" x2="25.4" y2="170.18" width="0.1524" layer="91"/>
<label x="25.4" y="170.18" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB14"/>
</segment>
</net>
<net name="OTG_ID" class="0">
<segment>
<wire x1="43.18" y1="175.26" x2="25.4" y2="175.26" width="0.1524" layer="91"/>
<label x="25.4" y="175.26" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB12"/>
</segment>
</net>
<net name="OTG_OVR_CUR" class="0">
<segment>
<wire x1="43.18" y1="177.8" x2="25.4" y2="177.8" width="0.1524" layer="91"/>
<label x="25.4" y="177.8" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB11"/>
</segment>
</net>
<net name="OTG_PWR_ON" class="0">
<segment>
<wire x1="43.18" y1="180.34" x2="25.4" y2="180.34" width="0.1524" layer="91"/>
<label x="25.4" y="180.34" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB10"/>
</segment>
</net>
<net name="USART3_RX" class="0">
<segment>
<wire x1="83.82" y1="223.52" x2="101.6" y2="223.52" width="0.1524" layer="91"/>
<label x="86.36" y="223.52" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PC11"/>
</segment>
</net>
<net name="USART3_TX" class="0">
<segment>
<wire x1="83.82" y1="226.06" x2="101.6" y2="226.06" width="0.1524" layer="91"/>
<label x="86.36" y="226.06" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PC10"/>
</segment>
</net>
<net name="SDA3" class="0">
<segment>
<wire x1="83.82" y1="172.72" x2="101.6" y2="172.72" width="0.1524" layer="91"/>
<label x="86.36" y="172.72" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PH5"/>
</segment>
</net>
<net name="SCL3" class="0">
<segment>
<wire x1="83.82" y1="175.26" x2="101.6" y2="175.26" width="0.1524" layer="91"/>
<label x="86.36" y="175.26" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PH4"/>
</segment>
</net>
<net name="LED-DBG" class="0">
<segment>
<wire x1="83.82" y1="215.9" x2="101.6" y2="215.9" width="0.1524" layer="91"/>
<label x="86.36" y="215.9" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PD2"/>
</segment>
</net>
<net name="DAC1-OUT" class="0">
<segment>
<wire x1="43.18" y1="231.14" x2="22.86" y2="231.14" width="0.1524" layer="91"/>
<label x="25.4" y="231.14" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PA5"/>
</segment>
</net>
<net name="AIN1" class="0">
<segment>
<wire x1="43.18" y1="198.12" x2="15.24" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="15.24" y1="198.12" x2="5.08" y2="198.12" width="0.1524" layer="91"/>
<wire x1="15.24" y1="193.04" x2="15.24" y2="198.12" width="0.1524" layer="91"/>
<junction x="15.24" y="198.12"/>
<label x="33.02" y="198.12" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB0"/>
</segment>
</net>
<net name="AIN2" class="0">
<segment>
<wire x1="43.18" y1="195.58" x2="7.62" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="7.62" y1="195.58" x2="5.08" y2="195.58" width="0.1524" layer="91"/>
<wire x1="7.62" y1="193.04" x2="7.62" y2="195.58" width="0.1524" layer="91"/>
<junction x="7.62" y="195.58"/>
<label x="33.02" y="195.58" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB1"/>
</segment>
</net>
<net name="CPU_TDO" class="0">
<segment>
<wire x1="43.18" y1="193.04" x2="25.4" y2="193.04" width="0.1524" layer="91"/>
<label x="25.4" y="193.04" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB3"/>
</segment>
</net>
<net name="CPU_TRST" class="0">
<segment>
<wire x1="43.18" y1="190.5" x2="25.4" y2="190.5" width="0.1524" layer="91"/>
<label x="25.4" y="190.5" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB4"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="43.18" y1="187.96" x2="30.48" y2="187.96" width="0.1524" layer="91"/>
<pinref part="P1" gate="G$1" pin="1"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PB5"/>
</segment>
</net>
<net name="!EXT-PTT!" class="0">
<segment>
<wire x1="83.82" y1="238.76" x2="101.6" y2="238.76" width="0.1524" layer="91"/>
<label x="86.36" y="238.76" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PC0"/>
</segment>
</net>
<net name="!CPPT!" class="0">
<segment>
<wire x1="83.82" y1="236.22" x2="101.6" y2="236.22" width="0.1524" layer="91"/>
<label x="86.36" y="236.22" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PC2"/>
</segment>
</net>
<net name="AD5252_WP" class="0">
<segment>
<wire x1="83.82" y1="233.68" x2="101.6" y2="233.68" width="0.1524" layer="91"/>
<label x="86.36" y="233.68" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PC3"/>
</segment>
</net>
<net name="NSS" class="0">
<segment>
<wire x1="83.82" y1="203.2" x2="101.6" y2="203.2" width="0.1524" layer="91"/>
<label x="86.36" y="203.2" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PF6"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<wire x1="83.82" y1="200.66" x2="101.6" y2="200.66" width="0.1524" layer="91"/>
<label x="86.36" y="200.66" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PF7"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="83.82" y1="195.58" x2="101.6" y2="195.58" width="0.1524" layer="91"/>
<label x="86.36" y="195.58" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PF9"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="P4" gate="G$1" pin="1"/>
<wire x1="83.82" y1="213.36" x2="96.52" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PD12"/>
</segment>
</net>
<net name="CPU_TCK" class="0">
<segment>
<wire x1="43.18" y1="215.9" x2="22.86" y2="215.9" width="0.1524" layer="91"/>
<label x="25.4" y="215.9" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PA14"/>
</segment>
</net>
<net name="CPU_TMS" class="0">
<segment>
<wire x1="43.18" y1="218.44" x2="22.86" y2="218.44" width="0.1524" layer="91"/>
<label x="25.4" y="218.44" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PA13"/>
</segment>
</net>
<net name="LED-PTT" class="0">
<segment>
<wire x1="83.82" y1="193.04" x2="101.6" y2="193.04" width="0.1524" layer="91"/>
<label x="86.36" y="193.04" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PF10"/>
</segment>
</net>
<net name="CPU_TDI" class="0">
<segment>
<wire x1="43.18" y1="213.36" x2="22.86" y2="213.36" width="0.1524" layer="91"/>
<label x="25.4" y="213.36" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PA15"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="1"/>
<wire x1="93.98" y1="187.96" x2="83.82" y2="187.96" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PG3"/>
</segment>
</net>
<net name="FPGA_IO_PCLK" class="0">
<segment>
<pinref part="U6" gate="SECUBE_FPGA" pin="FPGA_IO_GP15"/>
<wire x1="35.56" y1="17.78" x2="81.28" y2="17.78" width="0.1524" layer="91"/>
<label x="50.8" y="17.78" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="13"/>
</segment>
</net>
<net name="FPGA_IO_GP15" class="0">
<segment>
<pinref part="U6" gate="SECUBE_FPGA" pin="FPGA_IO_GP14"/>
<wire x1="81.28" y1="20.32" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<label x="50.8" y="20.32" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="11"/>
</segment>
</net>
<net name="FPGA_IO_GP14" class="0">
<segment>
<pinref part="U6" gate="SECUBE_FPGA" pin="FPGA_IO_GP13"/>
<wire x1="35.56" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
<label x="50.8" y="22.86" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="9"/>
</segment>
</net>
<net name="FPGA_IO_GP13" class="0">
<segment>
<pinref part="U6" gate="SECUBE_FPGA" pin="FPGA_IO_GP12"/>
<wire x1="81.28" y1="25.4" x2="35.56" y2="25.4" width="0.1524" layer="91"/>
<label x="50.8" y="25.4" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="7"/>
</segment>
</net>
<net name="FPGA_IO_GP12" class="0">
<segment>
<pinref part="U6" gate="SECUBE_FPGA" pin="FPGA_IO_GP11"/>
<wire x1="35.56" y1="27.94" x2="81.28" y2="27.94" width="0.1524" layer="91"/>
<label x="50.8" y="27.94" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="5"/>
</segment>
</net>
<net name="FPGA_IO_GP11" class="0">
<segment>
<pinref part="U6" gate="SECUBE_FPGA" pin="FPGA_IO_GP10"/>
<wire x1="81.28" y1="30.48" x2="35.56" y2="30.48" width="0.1524" layer="91"/>
<label x="50.8" y="30.48" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="3"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="G"/>
<wire x1="279.4" y1="121.92" x2="279.4" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R34" gate="A" pin="11"/>
<wire x1="279.4" y1="119.38" x2="259.08" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="259.08" y1="114.3" x2="279.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="279.4" y1="114.3" x2="279.4" y2="119.38" width="0.1524" layer="91"/>
<junction x="279.4" y="119.38"/>
<junction x="279.4" y="114.3"/>
<pinref part="C53" gate="A" pin="22"/>
<wire x1="279.4" y1="109.9312" x2="279.4" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1.2V" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="129.54" y1="266.7" x2="127" y2="266.7" width="0.1524" layer="91"/>
<pinref part="FD3" gate="G$1" pin="1.2V"/>
<wire x1="127" y1="266.7" x2="127" y2="269.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CPU_RSTN" class="0">
<segment>
<wire x1="314.96" y1="220.98" x2="342.9" y2="220.98" width="0.1524" layer="91"/>
<label x="342.9" y="220.98" size="1.4224" layer="95"/>
<pinref part="U6" gate="SECUBE_PWR" pin="CPU_RSTN"/>
<pinref part="C58" gate="G$1" pin="2"/>
<wire x1="342.9" y1="220.98" x2="353.06" y2="220.98" width="0.1524" layer="91"/>
<wire x1="342.9" y1="216.662" x2="342.9" y2="220.98" width="0.1524" layer="91"/>
<junction x="342.9" y="220.98"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PA10"/>
<pinref part="P3" gate="G$1" pin="1"/>
<wire x1="43.18" y1="226.06" x2="27.94" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!INP1!" class="0">
<segment>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PA11"/>
<wire x1="43.18" y1="223.52" x2="22.86" y2="223.52" width="0.1524" layer="91"/>
<label x="25.4" y="223.52" size="1.4224" layer="95"/>
</segment>
</net>
<net name="!INP2!" class="0">
<segment>
<pinref part="U6" gate="SECUBE_SIGNAL" pin="PA12"/>
<wire x1="43.18" y1="220.98" x2="22.86" y2="220.98" width="0.1524" layer="91"/>
<label x="25.4" y="220.98" size="1.4224" layer="95"/>
</segment>
</net>
<net name="FPGA_IO_GP10" class="0">
<segment>
<wire x1="35.56" y1="33.02" x2="81.28" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U6" gate="SECUBE_FPGA" pin="FPGA_IO_GP9"/>
<pinref part="J2" gate="A" pin="1"/>
<label x="50.8" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="396.24" y="193.04" size="1.778" layer="94" ratio="10">SPKR</text>
<text x="391.16" y="149.86" size="1.778" layer="94" ratio="10">MIC</text>
<wire x1="403.86" y1="144.78" x2="414.02" y2="144.78" width="0.1524" layer="94"/>
<wire x1="414.02" y1="144.78" x2="411.48" y2="145.542" width="0.1524" layer="94"/>
<wire x1="414.02" y1="144.78" x2="411.48" y2="144.018" width="0.1524" layer="94"/>
<wire x1="408.94" y1="193.04" x2="419.1" y2="193.04" width="0.1524" layer="94"/>
<wire x1="408.94" y1="193.04" x2="411.48" y2="193.802" width="0.1524" layer="94"/>
<wire x1="408.94" y1="193.04" x2="411.48" y2="192.278" width="0.1524" layer="94"/>
<text x="416.56" y="149.86" size="1.778" layer="94" ratio="10">RIG</text>
<text x="421.64" y="193.04" size="1.778" layer="94" ratio="10">RIG</text>
<text x="132.08" y="71.12" size="1.778" layer="94" ratio="10">SPEAKER OUT</text>
<text x="396.24" y="101.6" size="1.778" layer="94" ratio="10">PTT</text>
<wire x1="403.86" y1="101.6" x2="414.02" y2="101.6" width="0.1524" layer="94"/>
<wire x1="414.02" y1="101.6" x2="411.48" y2="102.362" width="0.1524" layer="94"/>
<wire x1="414.02" y1="101.6" x2="411.48" y2="100.838" width="0.1524" layer="94"/>
<text x="416.56" y="101.6" size="1.778" layer="94" ratio="10">RIG</text>
<text x="15.24" y="160.02" size="1.778" layer="94" ratio="10">EXT MIC INPUT</text>
<text x="17.78" y="256.54" size="2.54" layer="94" ratio="10">EXTERNAL MIC PREAMP</text>
<text x="12.7" y="116.84" size="2.54" layer="94" ratio="10">SPEAKER AMP WITH DIGIPOT - (ALTERED)</text>
<text x="193.04" y="248.92" size="2.54" layer="94" ratio="10">RIG INTERFACE (ALTERED)</text>
<text x="375.92" y="81.28" size="1.4224" layer="94" ratio="10" rot="R180">PESD5Z3.3</text>
<text x="114.3" y="68.58" size="1.4224" layer="94" ratio="10">PTVS20VP1UP</text>
<text x="17.78" y="251.46" size="2.54" layer="94">(ALTERED)</text>
<text x="344.932" y="17.272" size="3.81" layer="94" ratio="10">New Circuit Board</text>
<text x="418.084" y="6.35" size="3.81" layer="94" ratio="10">A</text>
<wire x1="180.34" y1="279.4" x2="180.34" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="134.62" x2="180.34" y2="0" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="134.62" x2="-2.54" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="320.04" y1="0" x2="322.58" y2="0" width="0.1524" layer="97" style="longdash"/>
<text x="281.94" y="101.6" size="1.524" layer="97">UPDATE:
01/07/19
Needed to use an LTV-817 variant with a
higher current gain than of L or A since more current required to key radio.
=======
Therefore resistor value reduced from 390 to 60 Ohms.</text>
</plain>
<instances>
<instance part="V130" gate="GND" x="45.72" y="157.48" smashed="yes">
<attribute name="VALUE" x="43.18" y="154.178" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C74" gate="G$1" x="83.82" y="180.34" smashed="yes" rot="R270">
<attribute name="NAME" x="86.36" y="175.26" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="86.36" y="172.72" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R44" gate="G$1" x="83.82" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="78.74" y="195.58" size="1.4224" layer="95" ratio="9" rot="R90"/>
<attribute name="VALUE" x="81.28" y="195.58" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="R47" gate="G$1" x="119.38" y="175.26" smashed="yes" rot="R270">
<attribute name="NAME" x="116.84" y="178.054" size="1.4224" layer="95" ratio="9" rot="R90"/>
<attribute name="VALUE" x="116.84" y="167.64" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="V146" gate="GND" x="134.62" y="167.64" smashed="yes">
<attribute name="VALUE" x="132.08" y="164.338" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V136" gate="GND" x="83.82" y="170.18" smashed="yes">
<attribute name="VALUE" x="81.28" y="166.878" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R48" gate="G$1" x="127" y="175.26" smashed="yes" rot="R270">
<attribute name="NAME" x="124.46" y="177.8" size="1.4224" layer="95" ratio="9" rot="R90"/>
<attribute name="VALUE" x="124.46" y="167.64" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="V134" gate="GND" x="73.66" y="53.34" smashed="yes">
<attribute name="VALUE" x="71.12" y="50.038" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C76" gate="G$1" x="88.9" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="93.98" y="58.42" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="93.98" y="55.88" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R45" gate="G$1" x="99.06" y="45.72" smashed="yes" rot="R270">
<attribute name="NAME" x="101.6" y="45.72" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="101.6" y="43.18" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V138" gate="GND" x="99.06" y="35.56" smashed="yes">
<attribute name="VALUE" x="96.52" y="32.258" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V148" gate="GND" x="142.24" y="40.64" smashed="yes">
<attribute name="VALUE" x="142.24" y="37.338" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V133" gate="GND" x="66.04" y="58.42" smashed="yes">
<attribute name="VALUE" x="63.5" y="55.118" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="LD1" gate="G$1" x="302.26" y="91.44" smashed="yes">
<attribute name="NAME" x="295.275" y="97.155" size="1.778" layer="95"/>
<attribute name="VALUE" x="295.275" y="83.82" size="1.778" layer="96"/>
</instance>
<instance part="R60" gate="G$1" x="279.4" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="274.32" y="96.52" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="279.4" y="96.52" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V169" gate="G$1" x="271.78" y="99.06" smashed="yes">
<attribute name="VALUE" x="271.78" y="102.108" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="V172" gate="GND" x="327.66" y="170.18" smashed="yes">
<attribute name="VALUE" x="325.12" y="166.878" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V171" gate="GND" x="287.02" y="119.38" smashed="yes">
<attribute name="VALUE" x="284.48" y="116.078" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C77" gate="G$1" x="91.44" y="187.96" smashed="yes">
<attribute name="NAME" x="88.9" y="182.88" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="88.9" y="180.34" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U10" gate="G$1" x="266.7" y="137.16" smashed="yes">
<attribute name="NAME" x="271.78" y="134.62" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="271.78" y="132.08" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U9" gate="G$1" x="228.6" y="187.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="220.98" y="195.58" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="220.98" y="193.04" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V162" gate="GND" x="238.76" y="167.64" smashed="yes">
<attribute name="VALUE" x="236.22" y="164.338" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V161" gate="GND" x="231.14" y="175.26" smashed="yes">
<attribute name="VALUE" x="228.6" y="171.958" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V167" gate="GND" x="264.16" y="124.46" smashed="yes">
<attribute name="VALUE" x="261.62" y="121.158" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C90" gate="G$1" x="281.94" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="287.02" y="139.7" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="289.56" y="139.7" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="V126" gate="GND" x="20.32" y="208.28" smashed="yes">
<attribute name="VALUE" x="17.78" y="204.978" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V150" gate="GND" x="154.94" y="96.52" smashed="yes">
<attribute name="VALUE" x="152.4" y="93.218" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V154" gate="GND" x="215.9" y="218.44" smashed="yes">
<attribute name="VALUE" x="213.36" y="215.138" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C93" gate="G$1" x="373.38" y="149.86" smashed="yes">
<attribute name="NAME" x="368.3" y="152.4" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="375.92" y="152.4" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C95" gate="G$1" x="375.92" y="198.12" smashed="yes">
<attribute name="NAME" x="370.84" y="200.66" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="378.46" y="200.66" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C94" gate="G$1" x="373.38" y="106.68" smashed="yes">
<attribute name="NAME" x="365.76" y="109.22" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="375.92" y="109.22" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="U7" gate="G$1" x="76.2" y="66.04" smashed="yes">
<attribute name="NAME" x="68.58" y="73.66" size="1.4224" layer="95"/>
<attribute name="VALUE" x="76.2" y="73.66" size="1.4224" layer="96"/>
</instance>
<instance part="T2" gate="G$1" x="337.82" y="180.34" smashed="yes">
<attribute name="NAME" x="337.82" y="190.5" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="187.96" size="1.4224" layer="95" ratio="10"/>
</instance>
<instance part="T1" gate="G$1" x="297.18" y="132.08" smashed="yes">
<attribute name="NAME" x="297.18" y="139.7" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="297.18" y="123.19" size="1.4224" layer="95" ratio="10"/>
</instance>
<instance part="U8" gate="G$1" x="137.16" y="185.42" smashed="yes">
<attribute name="NAME" x="147.32" y="193.04" size="1.4224" layer="95" ratio="9" rot="MR0"/>
<attribute name="VALUE" x="147.32" y="190.5" size="1.4224" layer="96" ratio="9" rot="MR0"/>
</instance>
<instance part="R46" gate="G$1" x="116.84" y="187.96" smashed="yes">
<attribute name="NAME" x="114.3" y="193.04" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="114.3" y="190.5" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V142" gate="GND" x="119.38" y="165.1" smashed="yes">
<attribute name="VALUE" x="119.38" y="161.798" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V139" gate="GND" x="104.14" y="198.12" smashed="yes">
<attribute name="VALUE" x="101.6" y="194.818" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C72" gate="G$1" x="33.02" y="218.44" smashed="yes" rot="R270">
<attribute name="NAME" x="35.56" y="213.36" size="1.4224" layer="95"/>
<attribute name="VALUE" x="35.56" y="210.82" size="1.4224" layer="96"/>
</instance>
<instance part="V129" gate="GND" x="33.02" y="208.28" smashed="yes">
<attribute name="VALUE" x="30.48" y="204.978" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C87" gate="G$1" x="226.06" y="228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="228.6" y="226.06" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="228.6" y="223.52" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V158" gate="GND" x="226.06" y="218.44" smashed="yes">
<attribute name="VALUE" x="223.52" y="215.138" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="P7" gate="G$1" x="203.2" y="190.5" smashed="yes">
<attribute name="NAME" x="201.93" y="197.104" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="P8" gate="G$1" x="256.54" y="129.54" smashed="yes" rot="R180">
<attribute name="NAME" x="257.81" y="122.936" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="P6" gate="G$1" x="167.64" y="182.88" smashed="yes" rot="R180">
<attribute name="NAME" x="168.91" y="176.276" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="R41" gate="G$1" x="76.2" y="187.96" smashed="yes">
<attribute name="NAME" x="73.66" y="189.484" size="1.4224" layer="95"/>
<attribute name="VALUE" x="73.66" y="184.658" size="1.4224" layer="96"/>
</instance>
<instance part="V135" gate="G$1" x="83.82" y="236.22" smashed="yes">
<attribute name="VALUE" x="82.296" y="237.236" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V125" gate="G$1" x="20.32" y="228.6" smashed="yes">
<attribute name="VALUE" x="18.796" y="229.616" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V128" gate="G$1" x="33.02" y="228.6" smashed="yes">
<attribute name="VALUE" x="31.496" y="229.616" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R50" gate="G$1" x="160.02" y="185.42" smashed="yes" rot="R180">
<attribute name="NAME" x="157.734" y="190.5" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="157.48" y="187.96" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V145" gate="G$1" x="134.62" y="198.12" smashed="yes">
<attribute name="VALUE" x="133.096" y="199.136" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V147" gate="G$1" x="139.7" y="162.56" smashed="yes" rot="R180">
<attribute name="VALUE" x="141.224" y="161.544" size="1.4224" layer="96" ratio="9" rot="R180"/>
</instance>
<instance part="V144" gate="G$1" x="127" y="165.1" smashed="yes" rot="R180">
<attribute name="VALUE" x="128.524" y="164.084" size="1.4224" layer="96" ratio="9" rot="R180"/>
</instance>
<instance part="V141" gate="GND" x="111.76" y="165.1" smashed="yes">
<attribute name="VALUE" x="109.22" y="161.798" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C81" gate="G$1" x="104.14" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="102.108" y="177.8" size="1.4224" layer="95" rot="R90"/>
<attribute name="VALUE" x="101.6" y="162.56" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="V140" gate="GND" x="104.14" y="162.56" smashed="yes">
<attribute name="VALUE" x="101.6" y="159.258" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R43" gate="G$1" x="83.82" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="82.296" y="210.82" size="1.4224" layer="95" rot="R90"/>
<attribute name="VALUE" x="87.122" y="210.82" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C80" gate="G$1" x="96.52" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="93.98" y="200.66" size="1.4224" layer="95"/>
<attribute name="VALUE" x="88.9" y="198.12" size="1.4224" layer="96"/>
</instance>
<instance part="R40" gate="G$1" x="66.04" y="86.36" smashed="yes">
<attribute name="NAME" x="63.5" y="87.884" size="1.4224" layer="95"/>
<attribute name="VALUE" x="58.42" y="83.058" size="1.4224" layer="96"/>
</instance>
<instance part="C73" gate="G$1" x="60.96" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="53.34" y="50.8" size="1.4224" layer="95" ratio="9" rot="R90"/>
<attribute name="VALUE" x="55.88" y="50.8" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="V132" gate="GND" x="60.96" y="50.8" smashed="yes">
<attribute name="VALUE" x="60.96" y="47.498" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R37" gate="G$1" x="20.32" y="71.12" smashed="yes" rot="R270">
<attribute name="NAME" x="17.78" y="63.5" size="1.4224" layer="95" ratio="9" rot="R90"/>
<attribute name="VALUE" x="17.78" y="68.58" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="V127" gate="GND" x="20.32" y="60.96" smashed="yes">
<attribute name="VALUE" x="17.78" y="57.658" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V153" gate="G$1" x="215.9" y="238.76" smashed="yes">
<attribute name="VALUE" x="214.376" y="239.776" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V157" gate="G$1" x="226.06" y="238.76" smashed="yes">
<attribute name="VALUE" x="224.536" y="239.776" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V160" gate="G$1" x="231.14" y="200.66" smashed="yes">
<attribute name="VALUE" x="229.616" y="201.676" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V159" gate="G$1" x="220.98" y="177.8" smashed="yes" rot="R90">
<attribute name="VALUE" x="219.964" y="176.276" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="V163" gate="G$1" x="246.38" y="167.64" smashed="yes" rot="R180">
<attribute name="VALUE" x="247.904" y="166.624" size="1.4224" layer="96" ratio="9" rot="R180"/>
</instance>
<instance part="V168" gate="G$1" x="269.24" y="144.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="270.256" y="146.304" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V170" gate="G$1" x="274.32" y="127" smashed="yes" rot="R270">
<attribute name="VALUE" x="275.336" y="128.524" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V181" gate="G$1" x="388.62" y="116.84" smashed="yes">
<attribute name="VALUE" x="386.3975" y="114.3" size="1.4224" layer="96" ratio="12"/>
</instance>
<instance part="V177" gate="G$1" x="381" y="144.78" smashed="yes">
<attribute name="VALUE" x="378.7775" y="142.24" size="1.4224" layer="96" ratio="12"/>
</instance>
<instance part="V179" gate="G$1" x="383.54" y="193.04" smashed="yes">
<attribute name="VALUE" x="381.3175" y="190.5" size="1.4224" layer="96" ratio="12"/>
</instance>
<instance part="V180" gate="G$1" x="388.62" y="165.1" smashed="yes">
<attribute name="VALUE" x="386.3975" y="162.56" size="1.4224" layer="96" ratio="12"/>
</instance>
<instance part="V178" gate="G$1" x="381" y="101.6" smashed="yes">
<attribute name="VALUE" x="378.7775" y="99.06" size="1.4224" layer="96" ratio="12"/>
</instance>
<instance part="V176" gate="G$1" x="355.6" y="78.74" smashed="yes">
<attribute name="VALUE" x="353.3775" y="76.2" size="1.4224" layer="96" ratio="12"/>
</instance>
<instance part="R53" gate="G$1" x="228.6" y="205.74" smashed="yes">
<attribute name="NAME" x="223.52" y="208.28" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="228.6" y="208.28" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V166" gate="GND" x="254" y="167.64" smashed="yes">
<attribute name="VALUE" x="251.46" y="164.338" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R52" gate="G$1" x="210.82" y="187.96" smashed="yes">
<attribute name="NAME" x="205.74" y="190.5" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="210.82" y="190.5" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="CN6" gate="G$1" x="398.78" y="129.54" smashed="yes" rot="R180">
<attribute name="NAME" x="414.02" y="119.38" size="1.4224" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="414.02" y="142.24" size="1.4224" layer="95" ratio="10" rot="R180"/>
</instance>
<instance part="CN5" gate="G$1" x="396.24" y="177.8" smashed="yes" rot="R180">
<attribute name="NAME" x="411.48" y="167.64" size="1.4224" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="411.48" y="190.5" size="1.4224" layer="95" ratio="10" rot="R180"/>
</instance>
<instance part="CN2" gate="G$1" x="27.94" y="180.34" smashed="yes" rot="MR180">
<attribute name="NAME" x="12.7" y="170.18" size="1.4224" layer="95" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="12.7" y="193.04" size="1.4224" layer="95" ratio="10" rot="MR180"/>
</instance>
<instance part="CN3" gate="G$1" x="149.86" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="165.1" y="48.26" size="1.4224" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="165.1" y="71.12" size="1.4224" layer="95" ratio="10" rot="R180"/>
</instance>
<instance part="CN7" gate="G$1" x="398.78" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="411.48" y="96.52" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="401.32" y="96.52" size="1.4224" layer="95" ratio="10"/>
</instance>
<instance part="V143" gate="GND" x="119.38" y="33.02" smashed="yes">
<attribute name="VALUE" x="116.84" y="29.718" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="D6" gate="G$1" x="381" y="83.82" smashed="yes">
<attribute name="NAME" x="375.92" y="86.36" size="1.4224" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="375.92" y="83.82" size="1.4224" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="V131" gate="G$1" x="50.8" y="91.44" smashed="yes">
<attribute name="VALUE" x="48.387" y="92.075" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C84" gate="G$1" x="165.1" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="170.18" y="101.6" size="1.4224" layer="95"/>
<attribute name="VALUE" x="170.18" y="99.06" size="1.4224" layer="96"/>
</instance>
<instance part="V152" gate="GND" x="165.1" y="96.52" smashed="yes">
<attribute name="VALUE" x="162.56" y="93.218" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V151" gate="G$1" x="165.1" y="116.84" smashed="yes">
<attribute name="VALUE" x="162.687" y="117.475" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="FD5" gate="G$1" x="-2.54" y="0" smashed="yes"/>
<instance part="FD6" gate="G$2" x="327.66" y="0" smashed="yes">
<attribute name="SHEET" x="414.02" y="1.27" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="V149" gate="G$1" x="154.94" y="116.84" smashed="yes">
<attribute name="VALUE" x="152.527" y="117.475" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R61" gate="G$1" x="320.04" y="185.42" smashed="yes" rot="R180">
<attribute name="NAME" x="317.5" y="187.96" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="322.58" y="187.96" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R51" gate="G$1" x="203.2" y="134.62" smashed="yes" rot="R180">
<attribute name="NAME" x="198.12" y="137.16" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="203.2" y="137.16" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R54" gate="G$1" x="231.14" y="134.62" smashed="yes" rot="R180">
<attribute name="NAME" x="226.06" y="137.16" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="231.14" y="137.16" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V165" gate="G$1" x="251.46" y="157.48" smashed="yes">
<attribute name="VALUE" x="249.936" y="158.496" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V164" gate="GND" x="248.92" y="111.76" smashed="yes">
<attribute name="VALUE" x="246.38" y="108.458" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R49" gate="G$1" x="152.4" y="195.58" smashed="yes" rot="R90">
<attribute name="NAME" x="144.78" y="200.66" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="142.24" y="198.12" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R62" gate="G$1" x="320.04" y="160.02" smashed="yes" rot="R180">
<attribute name="NAME" x="312.928" y="161.036" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="323.596" y="161.036" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="J5" gate="G$1" x="327.66" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="330.2" y="154.94" size="1.4224" layer="95"/>
<attribute name="VALUE" x="330.2" y="152.4" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="FB9" gate="G$1" x="378.46" y="137.16" smashed="yes" rot="R180">
<attribute name="NAME" x="382.27" y="132.842" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="382.524" y="135.128" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="FB7" gate="G$1" x="373.38" y="96.52" smashed="yes" rot="R180">
<attribute name="NAME" x="377.19" y="92.202" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="377.444" y="94.488" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="FB8" gate="G$1" x="378.46" y="185.42" smashed="yes" rot="R180">
<attribute name="NAME" x="382.27" y="181.102" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="382.524" y="183.388" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="FB5" gate="G$1" x="60.96" y="187.96" smashed="yes" rot="R180">
<attribute name="NAME" x="64.77" y="183.642" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="65.024" y="185.928" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="FB6" gate="G$1" x="109.22" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="113.03" y="61.722" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="113.284" y="64.008" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="R38" gate="G$1" x="38.1" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="35.56" y="60.96" size="1.4224" layer="95" ratio="9" rot="R90"/>
<attribute name="VALUE" x="35.56" y="66.04" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="R39" gate="G$1" x="53.34" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="48.26" y="71.12" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="53.34" y="71.12" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R42" gate="G$1" x="83.82" y="226.06" smashed="yes" rot="R90">
<attribute name="NAME" x="79.756" y="219.71" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="81.534" y="219.71" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R55" gate="G$1" x="238.76" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="239.776" y="171.704" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="241.554" y="171.704" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R56" gate="G$1" x="246.38" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="247.396" y="171.704" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="249.174" y="171.704" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R58" gate="G$1" x="251.46" y="190.5" smashed="yes">
<attribute name="NAME" x="245.364" y="189.484" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="245.364" y="187.706" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R63" gate="G$1" x="320.04" y="147.32" smashed="yes">
<attribute name="NAME" x="313.944" y="146.304" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="313.944" y="144.526" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="R57" gate="G$1" x="248.92" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="244.348" y="118.364" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="246.888" y="118.364" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R59" gate="G$1" x="251.46" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="246.888" y="141.224" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="249.428" y="141.224" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C88" gate="G$1" x="242.062" y="134.62" smashed="yes">
<attribute name="NAME" x="237.8256" y="137.3728" size="1.27" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="237.412" y="130.89" size="1.27" layer="96"/>
</instance>
<instance part="C92" gate="A" x="342.9" y="137.16" smashed="yes">
<attribute name="NAME" x="338.6636" y="139.9128" size="1.27" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="333.17" y="133.43" size="1.27" layer="96"/>
</instance>
<instance part="C91" gate="A" x="302.26" y="185.42" smashed="yes">
<attribute name="NAME" x="298.0236" y="188.1728" size="1.27" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="289.99" y="181.69" size="1.27" layer="96"/>
</instance>
<instance part="D5" gate="G$1" x="375.92" y="127" smashed="yes">
<attribute name="NAME" x="369.316" y="130.556" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="369.062" y="123.444" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="D4" gate="G$1" x="375.92" y="175.26" smashed="yes">
<attribute name="NAME" x="369.316" y="178.816" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="369.062" y="171.704" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="D2" gate="G$1" x="45.72" y="175.26" smashed="yes" rot="R270">
<attribute name="NAME" x="49.276" y="181.864" size="1.4224" layer="95" rot="R270" align="center-left"/>
<attribute name="VALUE" x="42.164" y="182.118" size="1.4224" layer="96" rot="R270" align="center-left"/>
</instance>
<instance part="D3" gate="G$1" x="119.38" y="48.26" smashed="yes" rot="R270">
<attribute name="NAME" x="122.936" y="54.864" size="1.4224" layer="95" rot="R270" align="center-left"/>
<attribute name="VALUE" x="115.824" y="55.118" size="1.4224" layer="96" rot="R270" align="center-left"/>
</instance>
<instance part="V156" gate="GND" x="195.58" y="154.94" smashed="yes">
<attribute name="VALUE" x="193.04" y="151.638" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V155" gate="G$1" x="195.58" y="175.26" smashed="yes">
<attribute name="VALUE" x="194.056" y="176.276" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C86" gate="G$1" x="195.58" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="199.136" y="153.416" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="200.914" y="153.416" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C119" gate="G$1" x="205.74" y="165.1" smashed="yes" rot="R270">
<attribute name="NAME" x="208.28" y="162.56" size="1.4224" layer="95" ratio="9"/>
<attribute name="VALUE" x="208.28" y="160.02" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V215" gate="GND" x="205.74" y="154.94" smashed="yes">
<attribute name="VALUE" x="203.2" y="151.638" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V216" gate="G$1" x="205.74" y="175.26" smashed="yes">
<attribute name="VALUE" x="204.216" y="176.276" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C120" gate="G$1" x="264.16" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="266.7" y="175.26" size="1.4224" layer="95" rot="R270"/>
<attribute name="VALUE" x="269.24" y="182.88" size="1.4224" layer="96" rot="R270"/>
</instance>
<instance part="V173" gate="GND" x="264.16" y="167.64" smashed="yes">
<attribute name="VALUE" x="261.62" y="164.338" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C121" gate="A" x="81.28" y="86.36" smashed="yes">
<attribute name="VALUE" x="77.216" y="82.6262" size="1.4224" layer="96" ratio="10" rot="SR0"/>
<attribute name="NAME" x="77.1906" y="89.2048" size="1.4224" layer="95" ratio="10" rot="SR0"/>
</instance>
<instance part="V174" gate="GND" x="93.98" y="78.74" smashed="yes"/>
<instance part="C75" gate="A" x="91.44" y="66.04" smashed="yes">
<attribute name="VALUE" x="87.376" y="62.3062" size="1.4224" layer="96" ratio="10" rot="SR0"/>
<attribute name="NAME" x="87.3506" y="68.8848" size="1.4224" layer="95" ratio="10" rot="SR0"/>
</instance>
<instance part="P24" gate="G$1" x="167.64" y="159.258" smashed="yes">
<attribute name="NAME" x="166.37" y="165.862" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="V137" gate="GND" x="167.64" y="154.178" smashed="yes">
<attribute name="VALUE" x="165.1" y="150.876" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V80" gate="GND" x="284.48" y="149.86" smashed="yes">
<attribute name="VALUE" x="281.94" y="146.558" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="P22" gate="G$1" x="284.48" y="157.48" smashed="yes">
<attribute name="NAME" x="284.48" y="165.1" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="J9" gate="A" x="381" y="243.84" smashed="yes">
<attribute name="NAME" x="385.1656" y="249.1486" size="2.0828" layer="95" ratio="6" rot="SR0"/>
</instance>
<instance part="C70" gate="G$1" x="20.32" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="16.002" y="213.868" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="25.4" y="213.36" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C71" gate="G$1" x="27.94" y="78.74" smashed="yes">
<attribute name="NAME" x="23.368" y="83.058" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="22.86" y="73.66" size="1.4224" layer="96"/>
</instance>
<instance part="C78" gate="G$1" x="96.52" y="213.36" smashed="yes">
<attribute name="NAME" x="91.948" y="217.678" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="91.44" y="208.28" size="1.4224" layer="96"/>
</instance>
<instance part="C79" gate="G$1" x="111.76" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="107.442" y="170.688" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="116.84" y="170.18" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C82" gate="G$1" x="154.94" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="150.622" y="102.108" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="160.02" y="101.6" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C83" gate="G$1" x="215.9" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="211.582" y="224.028" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="220.98" y="223.52" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C85" gate="G$1" x="254" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="249.682" y="173.228" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="259.08" y="172.72" size="1.4224" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="V130" gate="GND" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="A"/>
<wire x1="45.72" y1="162.56" x2="45.72" y2="160.02" width="0.1524" layer="91"/>
<wire x1="33.02" y1="172.72" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<wire x1="40.64" y1="172.72" x2="40.64" y2="162.56" width="0.1524" layer="91"/>
<wire x1="40.64" y1="162.56" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="45.72" y1="171.704" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
<junction x="45.72" y="162.56"/>
</segment>
<segment>
<pinref part="V136" gate="GND" pin="GND"/>
<pinref part="C74" gate="G$1" pin="2"/>
<wire x1="83.82" y1="172.72" x2="83.82" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="73.66" y1="58.42" x2="73.66" y2="55.88" width="0.1524" layer="91"/>
<pinref part="V134" gate="GND" pin="GND"/>
<pinref part="U7" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="V138" gate="GND" pin="GND"/>
<wire x1="99.06" y1="38.1" x2="99.06" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="144.78" y1="50.8" x2="142.24" y2="50.8" width="0.1524" layer="91"/>
<wire x1="142.24" y1="50.8" x2="142.24" y2="43.18" width="0.1524" layer="91"/>
<pinref part="V148" gate="GND" pin="GND"/>
<pinref part="CN3" gate="G$1" pin="A"/>
</segment>
<segment>
<wire x1="68.58" y1="63.5" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="66.04" y1="63.5" x2="66.04" y2="60.96" width="0.1524" layer="91"/>
<pinref part="V133" gate="GND" pin="GND"/>
<pinref part="U7" gate="G$1" pin="-IN"/>
</segment>
<segment>
<pinref part="V171" gate="GND" pin="GND"/>
<wire x1="289.56" y1="127" x2="287.02" y2="127" width="0.1524" layer="91"/>
<wire x1="287.02" y1="127" x2="287.02" y2="121.92" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="238.76" y1="173.482" x2="238.76" y2="170.18" width="0.1524" layer="91"/>
<pinref part="V162" gate="GND" pin="GND"/>
<pinref part="R55" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="V161" gate="GND" pin="GND"/>
<pinref part="U9" gate="G$1" pin="GND"/>
<wire x1="231.14" y1="177.8" x2="231.14" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="GND"/>
<wire x1="264.16" y1="129.54" x2="264.16" y2="127" width="0.1524" layer="91"/>
<pinref part="V167" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="V150" gate="GND" pin="GND"/>
<pinref part="C82" gate="G$1" pin="1"/>
<wire x1="154.94" y1="101.6" x2="154.94" y2="99.06" width="0.1524" layer="91"/>
<wire x1="154.94" y1="103.378" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V154" gate="GND" pin="GND"/>
<pinref part="C83" gate="G$1" pin="1"/>
<wire x1="215.9" y1="223.52" x2="215.9" y2="220.98" width="0.1524" layer="91"/>
<wire x1="215.9" y1="225.298" x2="215.9" y2="223.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND"/>
<wire x1="134.62" y1="177.8" x2="134.62" y2="170.18" width="0.1524" layer="91"/>
<pinref part="V146" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="119.38" y1="170.18" x2="119.38" y2="167.64" width="0.1524" layer="91"/>
<pinref part="V142" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="V126" gate="GND" pin="GND"/>
<wire x1="20.32" y1="210.82" x2="20.32" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C70" gate="G$1" pin="1"/>
<wire x1="20.32" y1="215.138" x2="20.32" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C72" gate="G$1" pin="2"/>
<wire x1="33.02" y1="213.36" x2="33.02" y2="210.82" width="0.1524" layer="91"/>
<pinref part="V129" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="V158" gate="GND" pin="GND"/>
<pinref part="C87" gate="G$1" pin="2"/>
<wire x1="226.06" y1="220.98" x2="226.06" y2="223.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V141" gate="GND" pin="GND"/>
<pinref part="C79" gate="G$1" pin="1"/>
<wire x1="111.76" y1="170.18" x2="111.76" y2="167.64" width="0.1524" layer="91"/>
<wire x1="111.76" y1="171.958" x2="111.76" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C81" gate="G$1" pin="-"/>
<wire x1="104.14" y1="167.64" x2="104.14" y2="165.1" width="0.1524" layer="91"/>
<pinref part="V140" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="101.6" y1="213.36" x2="104.14" y2="213.36" width="0.1524" layer="91"/>
<wire x1="104.14" y1="213.36" x2="104.14" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C80" gate="G$1" pin="-"/>
<wire x1="104.14" y1="205.74" x2="101.6" y2="205.74" width="0.1524" layer="91"/>
<pinref part="V139" gate="GND" pin="GND"/>
<wire x1="104.14" y1="200.66" x2="104.14" y2="205.74" width="0.1524" layer="91"/>
<junction x="104.14" y="205.74"/>
<pinref part="C78" gate="G$1" pin="2"/>
<wire x1="99.822" y1="213.36" x2="101.6" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V132" gate="GND" pin="GND"/>
<pinref part="C73" gate="G$1" pin="2"/>
<wire x1="60.96" y1="53.34" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="20.32" y1="66.04" x2="20.32" y2="63.5" width="0.1524" layer="91"/>
<pinref part="V127" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="V166" gate="GND" pin="GND"/>
<pinref part="C85" gate="G$1" pin="1"/>
<wire x1="254" y1="172.72" x2="254" y2="170.18" width="0.1524" layer="91"/>
<wire x1="254" y1="174.498" x2="254" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="3"/>
<wire x1="330.2" y1="175.26" x2="327.66" y2="175.26" width="0.1524" layer="91"/>
<pinref part="V172" gate="GND" pin="GND"/>
<wire x1="327.66" y1="175.26" x2="327.66" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C84" gate="G$1" pin="2"/>
<wire x1="165.1" y1="101.6" x2="165.1" y2="99.06" width="0.1524" layer="91"/>
<pinref part="V152" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="248.92" y1="117.602" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<pinref part="V164" gate="GND" pin="GND"/>
<pinref part="R57" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<pinref part="V143" gate="GND" pin="GND"/>
<wire x1="119.38" y1="44.704" x2="119.38" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="195.58" y1="161.798" x2="195.58" y2="157.48" width="0.1524" layer="91"/>
<pinref part="V156" gate="GND" pin="GND"/>
<pinref part="C86" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="V215" gate="GND" pin="GND"/>
<pinref part="C119" gate="G$1" pin="2"/>
<wire x1="205.74" y1="157.48" x2="205.74" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C121" gate="A" pin="22"/>
<wire x1="87.122" y1="86.36" x2="93.98" y2="86.36" width="0.1524" layer="91"/>
<wire x1="93.98" y1="86.36" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
<pinref part="V174" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C120" gate="G$1" pin="-"/>
<pinref part="V173" gate="GND" pin="GND"/>
<wire x1="264.16" y1="172.72" x2="264.16" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V137" gate="GND" pin="GND"/>
<pinref part="P24" gate="G$1" pin="1"/>
<wire x1="167.64" y1="156.718" x2="167.64" y2="159.258" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P22" gate="G$1" pin="1"/>
<pinref part="V80" gate="GND" pin="GND"/>
<wire x1="284.48" y1="157.48" x2="284.48" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="60.96" y1="86.36" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<wire x1="50.8" y1="86.36" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<pinref part="V131" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="C84" gate="G$1" pin="1"/>
<wire x1="165.1" y1="111.76" x2="165.1" y2="114.3" width="0.1524" layer="91"/>
<pinref part="V151" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="V149" gate="G$1" pin="+5V"/>
<wire x1="154.94" y1="114.3" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C82" gate="G$1" pin="2"/>
<wire x1="154.94" y1="109.982" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="C74" gate="G$1" pin="1"/>
<wire x1="83.82" y1="185.42" x2="83.82" y2="187.96" width="0.1524" layer="91"/>
<wire x1="83.82" y1="187.96" x2="86.36" y2="187.96" width="0.1524" layer="91"/>
<junction x="83.82" y="187.96"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="83.82" y1="187.96" x2="83.82" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C77" gate="G$1" pin="1"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="81.28" y1="187.96" x2="83.82" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="96.52" y1="187.96" x2="111.76" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C77" gate="G$1" pin="2"/>
<pinref part="R46" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="83.82" y1="205.74" x2="83.82" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="83.82" y1="205.74" x2="83.82" y2="208.28" width="0.1524" layer="91"/>
<wire x1="83.82" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="91.44" y1="213.36" x2="88.9" y2="213.36" width="0.1524" layer="91"/>
<wire x1="88.9" y1="213.36" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C80" gate="G$1" pin="+"/>
<wire x1="88.9" y1="205.74" x2="91.44" y2="205.74" width="0.1524" layer="91"/>
<junction x="88.9" y="205.74"/>
<junction x="83.82" y="205.74"/>
<pinref part="C78" gate="G$1" pin="1"/>
<wire x1="93.218" y1="213.36" x2="91.44" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DAC2-OUT" class="0">
<segment>
<wire x1="22.86" y1="78.74" x2="20.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="20.32" y1="78.74" x2="5.08" y2="78.74" width="0.1524" layer="91"/>
<label x="5.08" y="79.248" size="1.4224" layer="95" ratio="10"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="78.74" width="0.1524" layer="91"/>
<junction x="20.32" y="78.74"/>
<pinref part="C71" gate="G$1" pin="1"/>
<wire x1="24.638" y1="78.74" x2="22.86" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="381" y1="220.98" x2="360.68" y2="220.98" width="0.1524" layer="91"/>
<label x="360.68" y="220.98" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="20"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="A"/>
<wire x1="292.1" y1="93.98" x2="284.48" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R60" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="274.32" y1="93.98" x2="271.78" y2="93.98" width="0.1524" layer="91"/>
<wire x1="271.78" y1="93.98" x2="271.78" y2="99.06" width="0.1524" layer="91"/>
<pinref part="V169" gate="G$1" pin="VDD"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="+IN"/>
<wire x1="238.76" y1="185.42" x2="236.22" y2="185.42" width="0.1524" layer="91"/>
<wire x1="238.76" y1="182.118" x2="238.76" y2="185.42" width="0.1524" layer="91"/>
<wire x1="246.38" y1="182.118" x2="246.38" y2="185.42" width="0.1524" layer="91"/>
<wire x1="246.38" y1="185.42" x2="238.76" y2="185.42" width="0.1524" layer="91"/>
<junction x="238.76" y="185.42"/>
<wire x1="246.38" y1="185.42" x2="254" y2="185.42" width="0.1524" layer="91"/>
<wire x1="254" y1="185.42" x2="254" y2="182.88" width="0.1524" layer="91"/>
<junction x="246.38" y="185.42"/>
<pinref part="R55" gate="G$1" pin="2"/>
<pinref part="R56" gate="G$1" pin="2"/>
<pinref part="C120" gate="G$1" pin="+"/>
<wire x1="264.16" y1="182.88" x2="264.16" y2="185.42" width="0.1524" layer="91"/>
<wire x1="264.16" y1="185.42" x2="254" y2="185.42" width="0.1524" layer="91"/>
<junction x="254" y="185.42"/>
<pinref part="C85" gate="G$1" pin="2"/>
<wire x1="254" y1="181.102" x2="254" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!CPPT!" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="C"/>
<wire x1="292.1" y1="88.9" x2="264.16" y2="88.9" width="0.1524" layer="91"/>
<label x="264.16" y="89.408" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="OUT"/>
<wire x1="274.32" y1="137.16" x2="276.86" y2="137.16" width="0.1524" layer="91"/>
<wire x1="276.86" y1="137.16" x2="276.86" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C90" gate="G$1" pin="1"/>
<wire x1="276.86" y1="137.16" x2="279.4" y2="137.16" width="0.1524" layer="91"/>
<junction x="276.86" y="137.16"/>
<wire x1="276.86" y1="149.86" x2="256.54" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="-IN"/>
<wire x1="259.08" y1="139.7" x2="256.54" y2="139.7" width="0.1524" layer="91"/>
<wire x1="256.54" y1="139.7" x2="256.54" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AD5252_A1" class="0">
<segment>
<label x="281.94" y="195.58" size="1.778" layer="95" rot="R90"/>
<wire x1="284.48" y1="185.42" x2="284.48" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C91" gate="A" pin="11"/>
<wire x1="299.0342" y1="185.42" x2="284.48" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="403.86" y1="243.84" x2="419.1" y2="243.84" width="0.1524" layer="91"/>
<label x="408.94" y="243.84" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="1"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="347.98" y1="185.42" x2="368.3" y2="185.42" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="6"/>
<pinref part="C95" gate="G$1" pin="1"/>
<wire x1="368.3" y1="185.42" x2="374.904" y2="185.42" width="0.1524" layer="91"/>
<wire x1="370.84" y1="198.12" x2="368.3" y2="198.12" width="0.1524" layer="91"/>
<wire x1="368.3" y1="198.12" x2="368.3" y2="185.42" width="0.1524" layer="91"/>
<junction x="368.3" y="185.42"/>
<pinref part="FB8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="COL"/>
<wire x1="363.22" y1="96.52" x2="369.824" y2="96.52" width="0.1524" layer="91"/>
<wire x1="309.88" y1="93.98" x2="350.52" y2="93.98" width="0.1524" layer="91"/>
<wire x1="350.52" y1="93.98" x2="350.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="350.52" y1="96.52" x2="363.22" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C94" gate="G$1" pin="1"/>
<wire x1="368.3" y1="106.68" x2="363.22" y2="106.68" width="0.1524" layer="91"/>
<wire x1="363.22" y1="106.68" x2="363.22" y2="96.52" width="0.1524" layer="91"/>
<junction x="363.22" y="96.52"/>
<pinref part="FB7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<wire x1="33.02" y1="187.96" x2="45.72" y2="187.96" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="C"/>
<pinref part="FB5" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="45.72" y1="187.96" x2="57.404" y2="187.96" width="0.1524" layer="91"/>
<wire x1="45.72" y1="184.658" x2="45.72" y2="187.96" width="0.1524" layer="91"/>
<junction x="45.72" y="187.96"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="C90" gate="G$1" pin="2"/>
<pinref part="T1" gate="G$1" pin="1"/>
<wire x1="287.02" y1="137.16" x2="289.56" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="C93" gate="G$1" pin="1"/>
<wire x1="363.22" y1="137.16" x2="374.904" y2="137.16" width="0.1524" layer="91"/>
<wire x1="368.3" y1="149.86" x2="363.22" y2="149.86" width="0.1524" layer="91"/>
<wire x1="363.22" y1="149.86" x2="363.22" y2="137.16" width="0.1524" layer="91"/>
<pinref part="FB9" gate="G$1" pin="2"/>
<pinref part="C92" gate="A" pin="22"/>
<wire x1="346.1512" y1="137.16" x2="363.22" y2="137.16" width="0.1524" layer="91"/>
<junction x="363.22" y="137.16"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="127" y1="180.34" x2="127" y2="182.88" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="+IN"/>
<wire x1="127" y1="182.88" x2="129.54" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="119.38" y1="180.34" x2="119.38" y2="182.88" width="0.1524" layer="91"/>
<wire x1="119.38" y1="182.88" x2="127" y2="182.88" width="0.1524" layer="91"/>
<junction x="127" y="182.88"/>
<wire x1="111.76" y1="180.34" x2="111.76" y2="182.88" width="0.1524" layer="91"/>
<wire x1="111.76" y1="182.88" x2="119.38" y2="182.88" width="0.1524" layer="91"/>
<junction x="119.38" y="182.88"/>
<pinref part="C81" gate="G$1" pin="+"/>
<wire x1="104.14" y1="177.8" x2="104.14" y2="182.88" width="0.1524" layer="91"/>
<wire x1="104.14" y1="182.88" x2="111.76" y2="182.88" width="0.1524" layer="91"/>
<junction x="111.76" y="182.88"/>
<pinref part="C79" gate="G$1" pin="2"/>
<wire x1="111.76" y1="178.562" x2="111.76" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="APTT" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="386.08" y1="83.82" x2="386.08" y2="96.52" width="0.1524" layer="91"/>
<wire x1="377.19" y1="96.52" x2="386.08" y2="96.52" width="0.1524" layer="91"/>
<wire x1="386.08" y1="96.52" x2="391.16" y2="96.52" width="0.1524" layer="91"/>
<wire x1="391.16" y1="96.52" x2="391.16" y2="93.98" width="0.1524" layer="91"/>
<wire x1="391.16" y1="93.98" x2="393.7" y2="93.98" width="0.1524" layer="91"/>
<pinref part="CN7" gate="G$1" pin="C"/>
<pinref part="FB7" gate="G$1" pin="1"/>
<junction x="386.08" y="96.52"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="OUT"/>
<pinref part="C76" gate="G$1" pin="1"/>
<wire x1="86.36" y1="66.04" x2="88.9" y2="66.04" width="0.1524" layer="91"/>
<wire x1="88.9" y1="66.04" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C75" gate="A" pin="11"/>
<wire x1="91.44" y1="66.04" x2="88.9" y2="66.04" width="0.1524" layer="91"/>
<junction x="88.9" y="66.04"/>
</segment>
</net>
<net name="AD5252_W1" class="0">
<segment>
<label x="274.32" y="195.58" size="1.778" layer="95" rot="R90"/>
<wire x1="276.86" y1="190.5" x2="276.86" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="2"/>
<wire x1="255.778" y1="190.5" x2="276.86" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="406.4" y1="241.3" x2="408.94" y2="241.3" width="0.1524" layer="91"/>
<label x="408.94" y="241.3" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="3"/>
<wire x1="408.94" y1="241.3" x2="419.1" y2="241.3" width="0.1524" layer="91"/>
<wire x1="403.86" y1="241.3" x2="406.4" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="64.77" y1="187.96" x2="71.12" y2="187.96" width="0.1524" layer="91"/>
<pinref part="FB5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="AVDD" class="0">
<segment>
<pinref part="V125" gate="G$1" pin="AVDD"/>
<pinref part="C70" gate="G$1" pin="2"/>
<wire x1="20.32" y1="223.52" x2="20.32" y2="226.06" width="0.1524" layer="91"/>
<wire x1="20.32" y1="221.742" x2="20.32" y2="223.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C72" gate="G$1" pin="1"/>
<pinref part="V128" gate="G$1" pin="AVDD"/>
<wire x1="33.02" y1="223.52" x2="33.02" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="V+"/>
<wire x1="134.62" y1="193.04" x2="134.62" y2="195.58" width="0.1524" layer="91"/>
<pinref part="V145" gate="G$1" pin="AVDD"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="!SHDN!"/>
<wire x1="139.7" y1="177.8" x2="139.7" y2="165.1" width="0.1524" layer="91"/>
<pinref part="V147" gate="G$1" pin="AVDD"/>
</segment>
<segment>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="127" y1="170.18" x2="127" y2="167.64" width="0.1524" layer="91"/>
<pinref part="V144" gate="G$1" pin="AVDD"/>
</segment>
<segment>
<pinref part="V153" gate="G$1" pin="AVDD"/>
<pinref part="C83" gate="G$1" pin="2"/>
<wire x1="215.9" y1="233.68" x2="215.9" y2="236.22" width="0.1524" layer="91"/>
<wire x1="215.9" y1="231.902" x2="215.9" y2="233.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C87" gate="G$1" pin="1"/>
<wire x1="226.06" y1="236.22" x2="226.06" y2="233.68" width="0.1524" layer="91"/>
<pinref part="V157" gate="G$1" pin="AVDD"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="V+"/>
<wire x1="231.14" y1="195.58" x2="231.14" y2="198.12" width="0.1524" layer="91"/>
<pinref part="V160" gate="G$1" pin="AVDD"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="!SHDN!"/>
<wire x1="226.06" y1="180.34" x2="226.06" y2="177.8" width="0.1524" layer="91"/>
<wire x1="226.06" y1="177.8" x2="223.52" y2="177.8" width="0.1524" layer="91"/>
<pinref part="V159" gate="G$1" pin="AVDD"/>
</segment>
<segment>
<wire x1="246.38" y1="173.482" x2="246.38" y2="170.18" width="0.1524" layer="91"/>
<pinref part="V163" gate="G$1" pin="AVDD"/>
<pinref part="R56" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="V+"/>
<wire x1="264.16" y1="144.78" x2="266.7" y2="144.78" width="0.1524" layer="91"/>
<pinref part="V168" gate="G$1" pin="AVDD"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="!SHDN!"/>
<wire x1="269.24" y1="129.54" x2="269.24" y2="127" width="0.1524" layer="91"/>
<wire x1="269.24" y1="127" x2="271.78" y2="127" width="0.1524" layer="91"/>
<pinref part="V170" gate="G$1" pin="AVDD"/>
</segment>
<segment>
<pinref part="V135" gate="G$1" pin="AVDD"/>
<wire x1="83.82" y1="230.886" x2="83.82" y2="233.68" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="251.46" y1="149.098" x2="251.46" y2="154.94" width="0.1524" layer="91"/>
<pinref part="V165" gate="G$1" pin="AVDD"/>
<pinref part="R59" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="195.58" y1="168.402" x2="195.58" y2="172.72" width="0.1524" layer="91"/>
<pinref part="V155" gate="G$1" pin="AVDD"/>
<pinref part="C86" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C119" gate="G$1" pin="1"/>
<wire x1="205.74" y1="172.72" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<pinref part="V216" gate="G$1" pin="AVDD"/>
</segment>
</net>
<net name="AIN2" class="0">
<segment>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="165.1" y1="185.42" x2="167.64" y2="185.42" width="0.1524" layer="91"/>
<pinref part="P6" gate="G$1" pin="1"/>
<wire x1="167.64" y1="185.42" x2="175.26" y2="185.42" width="0.1524" layer="91"/>
<wire x1="167.64" y1="182.88" x2="167.64" y2="185.42" width="0.1524" layer="91"/>
<junction x="167.64" y="185.42"/>
<label x="169.926" y="185.928" size="1.4224" layer="95" ratio="10"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="83.82" y1="220.98" x2="83.82" y2="218.44" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<wire x1="73.66" y1="73.66" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VS"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="71.12" y1="86.36" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<junction x="73.66" y="86.36"/>
<pinref part="C121" gate="A" pin="11"/>
<wire x1="73.66" y1="86.36" x2="81.28" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RF_MIC" class="0">
<segment>
<pinref part="C93" gate="G$1" pin="2"/>
<wire x1="378.46" y1="149.86" x2="381" y2="149.86" width="0.1524" layer="91"/>
<wire x1="381" y1="149.86" x2="381" y2="147.32" width="0.1524" layer="91"/>
<pinref part="V177" gate="G$1" pin="RF_MIC"/>
</segment>
<segment>
<pinref part="V181" gate="G$1" pin="RF_MIC"/>
<pinref part="CN6" gate="G$1" pin="A"/>
<wire x1="393.7" y1="121.92" x2="388.62" y2="121.92" width="0.1524" layer="91"/>
<wire x1="388.62" y1="121.92" x2="388.62" y2="119.38" width="0.1524" layer="91"/>
<junction x="388.62" y="121.92"/>
<wire x1="388.62" y1="121.92" x2="363.22" y2="121.92" width="0.1524" layer="91"/>
<wire x1="363.22" y1="121.92" x2="314.96" y2="121.92" width="0.1524" layer="91"/>
<wire x1="314.96" y1="121.92" x2="314.96" y2="127" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="4"/>
<wire x1="314.96" y1="127" x2="307.34" y2="127" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="K"/>
<wire x1="366.522" y1="127" x2="363.22" y2="127" width="0.1524" layer="91"/>
<wire x1="363.22" y1="127" x2="363.22" y2="121.92" width="0.1524" layer="91"/>
<junction x="363.22" y="121.92"/>
</segment>
</net>
<net name="RF_SPKR" class="0">
<segment>
<pinref part="C95" gate="G$1" pin="2"/>
<wire x1="381" y1="198.12" x2="383.54" y2="198.12" width="0.1524" layer="91"/>
<wire x1="383.54" y1="198.12" x2="383.54" y2="195.58" width="0.1524" layer="91"/>
<pinref part="V179" gate="G$1" pin="RF_SPKR"/>
</segment>
<segment>
<wire x1="360.68" y1="170.18" x2="365.76" y2="170.18" width="0.1524" layer="91"/>
<pinref part="CN5" gate="G$1" pin="A"/>
<wire x1="365.76" y1="170.18" x2="388.62" y2="170.18" width="0.1524" layer="91"/>
<wire x1="388.62" y1="170.18" x2="391.16" y2="170.18" width="0.1524" layer="91"/>
<pinref part="V180" gate="G$1" pin="RF_SPKR"/>
<wire x1="388.62" y1="167.64" x2="388.62" y2="170.18" width="0.1524" layer="91"/>
<junction x="388.62" y="170.18"/>
<pinref part="T2" gate="G$1" pin="4"/>
<wire x1="347.98" y1="175.26" x2="360.68" y2="175.26" width="0.1524" layer="91"/>
<wire x1="360.68" y1="175.26" x2="360.68" y2="170.18" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="K"/>
<wire x1="366.522" y1="175.26" x2="365.76" y2="175.26" width="0.1524" layer="91"/>
<wire x1="365.76" y1="175.26" x2="365.76" y2="170.18" width="0.1524" layer="91"/>
<junction x="365.76" y="170.18"/>
</segment>
</net>
<net name="RF_PTT" class="0">
<segment>
<pinref part="C94" gate="G$1" pin="2"/>
<wire x1="378.46" y1="106.68" x2="381" y2="106.68" width="0.1524" layer="91"/>
<wire x1="381" y1="106.68" x2="381" y2="104.14" width="0.1524" layer="91"/>
<pinref part="V178" gate="G$1" pin="RF_PTT"/>
</segment>
<segment>
<wire x1="355.6" y1="81.28" x2="355.6" y2="83.82" width="0.1524" layer="91"/>
<pinref part="V176" gate="G$1" pin="RF_PTT"/>
<pinref part="LD1" gate="G$1" pin="EMIT"/>
<wire x1="309.88" y1="88.9" x2="350.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="350.52" y1="88.9" x2="350.52" y2="86.36" width="0.1524" layer="91"/>
<wire x1="350.52" y1="86.36" x2="350.52" y2="83.82" width="0.1524" layer="91" style="longdash"/>
<wire x1="350.52" y1="83.82" x2="355.6" y2="83.82" width="0.1524" layer="91" style="longdash"/>
<wire x1="355.6" y1="83.82" x2="360.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="360.68" y1="83.82" x2="360.68" y2="78.74" width="0.1524" layer="91"/>
<wire x1="360.68" y1="78.74" x2="378.46" y2="78.74" width="0.1524" layer="91"/>
<junction x="355.6" y="83.82"/>
<pinref part="CN7" gate="G$1" pin="A"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="378.46" y1="78.74" x2="393.7" y2="78.74" width="0.1524" layer="91"/>
<wire x1="378.46" y1="78.74" x2="378.46" y2="83.82" width="0.1524" layer="91"/>
<junction x="378.46" y="78.74"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="238.76" y1="190.5" x2="247.142" y2="190.5" width="0.1524" layer="91"/>
<wire x1="238.76" y1="190.5" x2="238.76" y2="205.74" width="0.1524" layer="91"/>
<junction x="238.76" y="190.5"/>
<pinref part="U9" gate="G$1" pin="-IN"/>
<wire x1="236.22" y1="190.5" x2="238.76" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
<wire x1="233.68" y1="205.74" x2="238.76" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="223.52" y1="205.74" x2="218.44" y2="205.74" width="0.1524" layer="91"/>
<wire x1="218.44" y1="205.74" x2="218.44" y2="187.96" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="OUT"/>
<wire x1="220.98" y1="187.96" x2="218.44" y2="187.96" width="0.1524" layer="91"/>
<junction x="218.44" y="187.96"/>
<wire x1="218.44" y1="187.96" x2="215.9" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
</segment>
</net>
<net name="AIN1" class="0">
<segment>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="205.74" y1="187.96" x2="203.2" y2="187.96" width="0.1524" layer="91"/>
<pinref part="P7" gate="G$1" pin="1"/>
<wire x1="203.2" y1="190.5" x2="203.2" y2="187.96" width="0.1524" layer="91"/>
<label x="192.532" y="188.468" size="1.4224" layer="95" ratio="10"/>
<wire x1="203.2" y1="187.96" x2="190.5" y2="187.96" width="0.1524" layer="91"/>
<junction x="203.2" y="187.96"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="CN6" gate="G$1" pin="C"/>
<wire x1="382.27" y1="137.16" x2="388.62" y2="137.16" width="0.1524" layer="91"/>
<wire x1="388.62" y1="137.16" x2="393.7" y2="137.16" width="0.1524" layer="91"/>
<wire x1="388.62" y1="127" x2="388.62" y2="137.16" width="0.1524" layer="91"/>
<junction x="388.62" y="137.16"/>
<pinref part="FB9" gate="G$1" pin="1"/>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="388.62" y1="127" x2="379.476" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<wire x1="382.27" y1="185.42" x2="388.62" y2="185.42" width="0.1524" layer="91"/>
<wire x1="388.62" y1="185.42" x2="391.16" y2="185.42" width="0.1524" layer="91"/>
<pinref part="CN5" gate="G$1" pin="C"/>
<wire x1="388.62" y1="175.26" x2="388.62" y2="185.42" width="0.1524" layer="91"/>
<junction x="388.62" y="185.42"/>
<pinref part="FB8" gate="G$1" pin="1"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="379.476" y1="175.26" x2="388.62" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="CN3" gate="G$1" pin="B"/>
<wire x1="129.54" y1="53.34" x2="144.78" y2="53.34" width="0.1524" layer="91"/>
<pinref part="CN3" gate="G$1" pin="C"/>
<wire x1="113.03" y1="66.04" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<wire x1="119.38" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="129.54" y1="66.04" x2="144.78" y2="66.04" width="0.1524" layer="91"/>
<wire x1="129.54" y1="53.34" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<junction x="129.54" y="66.04"/>
<pinref part="FB6" gate="G$1" pin="1"/>
<pinref part="D3" gate="G$1" pin="K"/>
<wire x1="119.38" y1="57.658" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<junction x="119.38" y="66.04"/>
</segment>
</net>
<net name="AD5241_100_W1" class="0">
<segment>
<wire x1="45.72" y1="68.58" x2="45.72" y2="48.26" width="0.1524" layer="91"/>
<label x="45.72" y="48.26" size="1.4224" layer="95" rot="R90"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="48.26" y1="68.58" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="381" y1="223.52" x2="360.68" y2="223.52" width="0.1524" layer="91"/>
<label x="360.68" y="223.52" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="18"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="99.06" y1="50.8" x2="99.06" y2="53.34" width="0.1524" layer="91" style="longdash"/>
<pinref part="C76" gate="G$1" pin="2"/>
<wire x1="99.06" y1="53.34" x2="88.9" y2="53.34" width="0.1524" layer="91" style="longdash"/>
</segment>
</net>
<net name="AD5241_1M_W1" class="0">
<segment>
<label x="127" y="203.2" size="1.4224" layer="95" rot="R90"/>
<pinref part="U8" gate="G$1" pin="-IN"/>
<wire x1="127" y1="187.96" x2="129.54" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="121.92" y1="187.96" x2="127" y2="187.96" width="0.1524" layer="91"/>
<junction x="127" y="187.96"/>
<wire x1="127" y1="218.44" x2="127" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="403.86" y1="233.68" x2="419.1" y2="233.68" width="0.1524" layer="91"/>
<label x="408.94" y="233.68" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="9"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="T2" gate="G$1" pin="1"/>
<wire x1="325.12" y1="185.42" x2="330.2" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DAC1-OUT" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="198.12" y1="134.62" x2="185.42" y2="134.62" width="0.1524" layer="91"/>
<label x="185.42" y="134.62" size="1.4224" layer="95"/>
</segment>
</net>
<net name="AD5252_A3" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="208.28" y1="134.62" x2="213.36" y2="134.62" width="0.1524" layer="91"/>
<wire x1="213.36" y1="134.62" x2="213.36" y2="154.94" width="0.1524" layer="91"/>
<label x="213.36" y="142.24" size="1.4224" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="381" y1="243.84" x2="353.06" y2="243.84" width="0.1524" layer="91"/>
<label x="353.06" y="243.84" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="2"/>
</segment>
</net>
<net name="AD5252_W3" class="0">
<segment>
<wire x1="218.44" y1="134.62" x2="218.44" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="226.06" y1="134.62" x2="218.44" y2="134.62" width="0.1524" layer="91"/>
<label x="218.44" y="142.24" size="1.4224" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="381" y1="241.3" x2="353.06" y2="241.3" width="0.1524" layer="91"/>
<label x="353.06" y="241.3" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="4"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="+IN"/>
<wire x1="256.54" y1="134.62" x2="259.08" y2="134.62" width="0.1524" layer="91"/>
<pinref part="P8" gate="G$1" pin="1"/>
<wire x1="256.54" y1="129.54" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
<junction x="256.54" y="134.62"/>
<wire x1="248.92" y1="134.62" x2="251.46" y2="134.62" width="0.1524" layer="91"/>
<wire x1="251.46" y1="134.62" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
<wire x1="251.46" y1="140.462" x2="251.46" y2="134.62" width="0.1524" layer="91"/>
<junction x="251.46" y="134.62"/>
<wire x1="248.92" y1="134.62" x2="248.92" y2="126.238" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="1"/>
<pinref part="R57" gate="G$1" pin="2"/>
<pinref part="C88" gate="G$1" pin="22"/>
<wire x1="245.3132" y1="134.62" x2="248.92" y2="134.62" width="0.1524" layer="91"/>
<junction x="248.92" y="134.62"/>
</segment>
</net>
<net name="AD5241_1M_B1" class="0">
<segment>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="152.4" y1="200.66" x2="152.4" y2="218.44" width="0.1524" layer="91"/>
<label x="152.4" y="203.2" size="1.4224" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="406.4" y1="236.22" x2="408.94" y2="236.22" width="0.1524" layer="91"/>
<label x="408.94" y="236.22" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="7"/>
<wire x1="408.94" y1="236.22" x2="419.1" y2="236.22" width="0.1524" layer="91"/>
<wire x1="403.86" y1="236.22" x2="406.4" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="OUT"/>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="144.78" y1="185.42" x2="152.4" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="152.4" y1="185.42" x2="154.94" y2="185.42" width="0.1524" layer="91"/>
<wire x1="152.4" y1="185.42" x2="152.4" y2="190.5" width="0.1524" layer="91"/>
<junction x="152.4" y="185.42"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="315.722" y1="147.32" x2="312.42" y2="147.32" width="0.1524" layer="91"/>
<wire x1="312.42" y1="147.32" x2="312.42" y2="137.16" width="0.1524" layer="91"/>
<wire x1="312.42" y1="147.32" x2="312.42" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="312.42" y1="160.02" x2="314.96" y2="160.02" width="0.1524" layer="91"/>
<junction x="312.42" y="147.32"/>
<pinref part="T1" gate="G$1" pin="6"/>
<wire x1="307.34" y1="137.16" x2="312.42" y2="137.16" width="0.1524" layer="91" style="longdash"/>
<pinref part="R63" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<wire x1="327.66" y1="147.32" x2="324.358" y2="147.32" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="327.66" y1="147.32" x2="327.66" y2="149.86" width="0.1524" layer="91"/>
<junction x="327.66" y="147.32"/>
<wire x1="327.66" y1="147.32" x2="327.66" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R63" gate="G$1" pin="2"/>
<pinref part="C92" gate="A" pin="11"/>
<wire x1="327.66" y1="137.16" x2="339.6742" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="1"/>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="325.12" y1="160.02" x2="327.66" y2="160.02" width="0.1524" layer="91"/>
<wire x1="327.66" y1="160.02" x2="327.66" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="FB6" gate="G$1" pin="2"/>
<wire x1="97.282" y1="66.04" x2="105.664" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C75" gate="A" pin="22"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="+IN"/>
<wire x1="60.96" y1="68.58" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C73" gate="G$1" pin="1"/>
<wire x1="60.96" y1="66.04" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="58.42" y1="68.58" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
<junction x="60.96" y="68.58"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R54" gate="G$1" pin="1"/>
<pinref part="C88" gate="G$1" pin="11"/>
<wire x1="236.22" y1="134.62" x2="238.8362" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="2"/>
<pinref part="C91" gate="A" pin="22"/>
<wire x1="314.96" y1="185.42" x2="305.5112" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MIC" class="0">
<segment>
<pinref part="CN6" gate="G$1" pin="D"/>
<wire x1="355.6" y1="132.08" x2="393.7" y2="132.08" width="0.1524" layer="91"/>
<wire x1="355.6" y1="132.08" x2="355.6" y2="236.22" width="0.1524" layer="91"/>
<wire x1="355.6" y1="236.22" x2="381" y2="236.22" width="0.1524" layer="91"/>
<pinref part="J9" gate="A" pin="8"/>
</segment>
</net>
<net name="SPKR" class="0">
<segment>
<pinref part="CN5" gate="G$1" pin="D"/>
<wire x1="358.14" y1="180.34" x2="391.16" y2="180.34" width="0.1524" layer="91"/>
<wire x1="358.14" y1="180.34" x2="358.14" y2="233.68" width="0.1524" layer="91"/>
<wire x1="358.14" y1="233.68" x2="381" y2="233.68" width="0.1524" layer="91"/>
<pinref part="J9" gate="A" pin="10"/>
</segment>
</net>
<net name="APTT1" class="0">
<segment>
<pinref part="CN7" gate="G$1" pin="D"/>
<wire x1="353.06" y1="88.9" x2="393.7" y2="88.9" width="0.1524" layer="91"/>
<wire x1="353.06" y1="88.9" x2="353.06" y2="238.76" width="0.1524" layer="91"/>
<wire x1="353.06" y1="238.76" x2="381" y2="238.76" width="0.1524" layer="91"/>
<pinref part="J9" gate="A" pin="6"/>
</segment>
</net>
<net name="AD5241_100_A1" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="38.1" y1="60.96" x2="38.1" y2="48.26" width="0.1524" layer="91"/>
<label x="38.1" y="48.26" size="1.4224" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="381" y1="226.06" x2="360.68" y2="226.06" width="0.1524" layer="91"/>
<label x="360.68" y="226.06" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="16"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="38.1" y1="71.12" x2="38.1" y2="78.74" width="0.1524" layer="91"/>
<wire x1="38.1" y1="78.74" x2="31.242" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C71" gate="G$1" pin="2"/>
</segment>
</net>
<net name="NSS" class="0">
<segment>
<wire x1="403.86" y1="226.06" x2="419.1" y2="226.06" width="0.1524" layer="91"/>
<label x="414.02" y="226.06" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="15"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<wire x1="403.86" y1="223.52" x2="419.1" y2="223.52" width="0.1524" layer="91"/>
<label x="414.02" y="223.52" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="17"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="403.86" y1="220.98" x2="419.1" y2="220.98" width="0.1524" layer="91"/>
<label x="414.02" y="220.98" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="19"/>
</segment>
</net>
<net name="SDA3" class="0">
<segment>
<wire x1="403.86" y1="228.6" x2="419.1" y2="228.6" width="0.1524" layer="91"/>
<label x="414.02" y="228.6" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="13"/>
</segment>
</net>
<net name="SCL3" class="0">
<segment>
<wire x1="406.4" y1="231.14" x2="408.94" y2="231.14" width="0.1524" layer="91"/>
<label x="414.02" y="231.14" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="11"/>
<wire x1="408.94" y1="231.14" x2="419.1" y2="231.14" width="0.1524" layer="91"/>
<wire x1="403.86" y1="231.14" x2="406.4" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AD5252_WP" class="0">
<segment>
<wire x1="403.86" y1="238.76" x2="419.1" y2="238.76" width="0.1524" layer="91"/>
<label x="408.94" y="238.76" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="5"/>
</segment>
</net>
<net name="!INP1!" class="0">
<segment>
<wire x1="381" y1="228.6" x2="360.68" y2="228.6" width="0.1524" layer="91"/>
<label x="360.68" y="228.6" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="14"/>
</segment>
</net>
<net name="!INP2!" class="0">
<segment>
<wire x1="381" y1="231.14" x2="360.68" y2="231.14" width="0.1524" layer="91"/>
<label x="360.68" y="231.14" size="1.4224" layer="95"/>
<pinref part="J9" gate="A" pin="12"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="20.32" y="165.1" size="1.778" layer="94">SCREW TERMINAL CONNECTOR</text>
<text x="10.16" y="236.22" size="2.54" layer="94" ratio="10">INPUT POWER SUPPLY REGULATOR</text>
<text x="347.472" y="17.272" size="3.81" layer="94" ratio="10">New Circuit Board</text>
<text x="420.624" y="6.35" size="3.81" layer="94" ratio="10">A</text>
<text x="101.6" y="129.54" size="1.778" layer="94">MANUAL RESET</text>
</plain>
<instances>
<instance part="U11" gate="B" x="226.06" y="88.9" smashed="yes">
<attribute name="NAME" x="223.8756" y="92.9386" size="2.0828" layer="95" ratio="6" rot="SR0"/>
</instance>
<instance part="R65" gate="A" x="170.18" y="106.68" smashed="yes">
<attribute name="NAME" x="165.9636" y="108.5088" size="1.6764" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="166.37" y="103.378" size="1.4224" layer="96"/>
</instance>
<instance part="V197" gate="GND" x="157.48" y="81.28" smashed="yes">
<attribute name="VALUE" x="154.94" y="77.978" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V205" gate="GND" x="297.18" y="81.28" smashed="yes">
<attribute name="VALUE" x="294.64" y="77.978" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="R66" gate="A" x="182.88" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="181.0512" y="178.6636" size="1.6764" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="186.182" y="179.07" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="R67" gate="A" x="182.88" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="181.0512" y="150.7236" size="1.6764" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="186.182" y="151.13" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="V202" gate="GND" x="182.88" y="144.78" smashed="yes"/>
<instance part="C103" gate="A" x="172.72" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="169.9672" y="181.1836" size="1.27" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="176.45" y="180.77" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C104" gate="A" x="172.72" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="169.9672" y="153.2436" size="1.27" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="176.45" y="152.83" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="V201" gate="GND" x="172.72" y="147.32" smashed="yes">
<attribute name="VALUE" x="170.18" y="144.018" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V200" gate="GND" x="172.72" y="175.26" smashed="yes">
<attribute name="VALUE" x="172.72" y="171.958" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="L2" gate="G$1" x="287.02" y="182.88" smashed="yes">
<attribute name="NAME" x="281.94" y="185.42" size="1.4224" layer="95"/>
<attribute name="VALUE" x="281.94" y="180.34" size="1.4224" layer="96"/>
</instance>
<instance part="R69" gate="A" x="294.64" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="292.8112" y="171.0436" size="1.6764" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="297.942" y="171.45" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="R70" gate="A" x="294.64" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="292.8112" y="153.2636" size="1.6764" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="297.942" y="153.67" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="V204" gate="GND" x="294.64" y="149.86" smashed="yes">
<attribute name="VALUE" x="292.1" y="146.558" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C109" gate="A" x="307.34" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="304.5872" y="171.0236" size="1.27" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="311.07" y="170.61" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="L1" gate="A" x="271.78" y="127" smashed="yes">
<attribute name="VALUE" x="273.3488" y="124.5328" size="1.4224" layer="96" ratio="10" rot="SR0"/>
<attribute name="NAME" x="273.3156" y="129.3528" size="1.4224" layer="95" ratio="10" rot="SR0"/>
</instance>
<instance part="R71" gate="A" x="297.18" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="295.3512" y="115.1636" size="1.6764" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="300.482" y="115.57" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C110" gate="A" x="309.88" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="307.1272" y="115.1436" size="1.27" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="313.61" y="114.73" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R72" gate="A" x="297.18" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="295.3512" y="97.3836" size="1.6764" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="300.482" y="97.79" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="V214" gate="G$1" x="414.02" y="127" smashed="yes" rot="R270">
<attribute name="VALUE" x="414.655" y="129.413" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="C97" gate="G$1" x="114.3" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="107.95" y="181.61" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="110.49" y="181.61" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="V192" gate="GND" x="114.3" y="162.56" smashed="yes">
<attribute name="VALUE" x="111.76" y="159.258" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="D8" gate="G$1" x="104.14" y="193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="101.6" y="195.58" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="101.6" y="189.2046" size="1.4224" layer="96" ratio="10"/>
</instance>
<instance part="V191" gate="GND" x="81.28" y="175.26" smashed="yes">
<attribute name="VALUE" x="78.74" y="171.958" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="P17" gate="G$1" x="81.28" y="195.58" smashed="yes">
<attribute name="NAME" x="83.312" y="199.898" size="1.4224" layer="95"/>
</instance>
<instance part="D7" gate="G$1" x="63.5" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="66.04" y="187.96" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="66.04" y="184.404" size="1.4224" layer="96" ratio="10"/>
</instance>
<instance part="V190" gate="GND" x="63.5" y="177.8" smashed="yes">
<attribute name="VALUE" x="60.96" y="174.498" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C113" gate="G$1" x="340.36" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="336.042" y="109.728" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="345.186" y="109.22" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C111" gate="G$1" x="327.66" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="323.342" y="165.608" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="332.486" y="165.1" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C112" gate="G$1" x="340.36" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="336.042" y="165.608" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="345.186" y="165.1" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C114" gate="G$1" x="353.06" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="348.742" y="165.608" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="357.886" y="165.1" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="V206" gate="GND" x="327.66" y="157.48" smashed="yes">
<attribute name="VALUE" x="325.12" y="154.178" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V207" gate="GND" x="340.36" y="157.48" smashed="yes">
<attribute name="VALUE" x="337.82" y="154.178" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V209" gate="GND" x="353.06" y="157.48" smashed="yes">
<attribute name="VALUE" x="350.52" y="154.178" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C115" gate="G$1" x="353.06" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="348.742" y="109.728" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="357.886" y="109.22" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="V210" gate="GND" x="353.06" y="99.06" smashed="yes">
<attribute name="VALUE" x="355.6" y="98.298" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V208" gate="GND" x="340.36" y="99.06" smashed="yes">
<attribute name="VALUE" x="342.9" y="98.298" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="F1" gate="A" x="48.26" y="193.04" smashed="yes">
<attribute name="VALUE" x="47.9298" y="189.8904" size="1.4224" layer="96" ratio="10" rot="SR0"/>
<attribute name="NAME" x="47.9806" y="195.2244" size="1.4224" layer="95" ratio="10" rot="SR0"/>
</instance>
<instance part="FB10" gate="G$1" x="91.44" y="193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="95.25" y="188.722" size="1.4224" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="95.504" y="191.008" size="1.4224" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="FD7" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FD9" gate="G$2" x="330.2" y="0" smashed="yes">
<attribute name="SHEET" x="416.56" y="1.27" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="U11" gate="A" x="226.06" y="172.72" smashed="yes">
<attribute name="NAME" x="221.3356" y="204.6986" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="220.7006" y="202.1586" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="C116" gate="G$1" x="365.76" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="370.84" y="167.64" size="1.4224" layer="95" ratio="9" rot="R270"/>
<attribute name="VALUE" x="368.3" y="167.64" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V211" gate="GND" x="365.76" y="157.48" smashed="yes">
<attribute name="VALUE" x="363.22" y="154.178" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C118" gate="G$1" x="375.92" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="381" y="167.64" size="1.4224" layer="95" ratio="9" rot="R270"/>
<attribute name="VALUE" x="378.46" y="167.64" size="1.4224" layer="96" ratio="9" rot="R270"/>
</instance>
<instance part="V213" gate="GND" x="375.92" y="157.48" smashed="yes">
<attribute name="VALUE" x="373.38" y="154.178" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="FD10" gate="G$1" x="408.94" y="182.88" smashed="yes" rot="R270"/>
<instance part="R64" gate="G$1" x="129.54" y="193.04" smashed="yes">
<attribute name="NAME" x="123.19" y="197.104" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="123.19" y="195.326" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="C96" gate="G$1" x="81.28" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="87.63" y="184.15" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="87.63" y="182.118" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="C105" gate="G$1" x="172.72" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="179.07" y="125.73" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="179.07" y="123.698" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="C106" gate="A" x="185.42" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="189.6564" y="111.5472" size="1.27" layer="95" ratio="10" rot="SR180"/>
<attribute name="VALUE" x="190.07" y="118.03" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="V203" gate="G$1" x="190.5" y="157.48" smashed="yes">
<attribute name="VALUE" x="187.96" y="160.528" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="R68" gate="G$1" x="190.5" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="187.96" y="142.24" size="1.4224" layer="95" ratio="9" rot="R90"/>
<attribute name="VALUE" x="187.96" y="147.32" size="1.4224" layer="96" ratio="9" rot="R90"/>
</instance>
<instance part="V193" gate="GND" x="127" y="104.14" smashed="yes">
<attribute name="VALUE" x="124.46" y="100.838" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V194" gate="GND" x="142.24" y="116.84" smashed="yes">
<attribute name="VALUE" x="139.7" y="113.538" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="P18" gate="G$1" x="154.94" y="134.62" smashed="yes">
<attribute name="NAME" x="156.972" y="138.938" size="1.4224" layer="95"/>
</instance>
<instance part="P14" gate="G$1" x="43.18" y="215.138" smashed="yes">
<attribute name="NAME" x="41.91" y="221.742" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="P15" gate="G$1" x="48.26" y="215.138" smashed="yes">
<attribute name="NAME" x="46.99" y="221.742" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="P10" gate="G$1" x="20.32" y="226.06" smashed="yes" rot="R180">
<attribute name="NAME" x="21.59" y="219.456" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="V186" gate="GND" x="43.18" y="210.058" smashed="yes">
<attribute name="VALUE" x="40.64" y="206.756" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V188" gate="GND" x="48.26" y="210.058" smashed="yes">
<attribute name="VALUE" x="45.72" y="206.756" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V183" gate="G$1" x="20.32" y="231.14" smashed="yes">
<attribute name="VALUE" x="17.907" y="231.775" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="P11" gate="G$1" x="25.4" y="226.06" smashed="yes" rot="R180">
<attribute name="NAME" x="26.67" y="219.456" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="FD8" gate="G$1" x="25.4" y="228.6" smashed="yes"/>
<instance part="V184" gate="G$1" x="30.48" y="231.14" smashed="yes">
<attribute name="VALUE" x="28.956" y="232.156" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="P12" gate="G$1" x="30.48" y="226.06" smashed="yes" rot="R180">
<attribute name="NAME" x="31.75" y="219.456" size="1.4224" layer="95" ratio="9" rot="R180"/>
</instance>
<instance part="P13" gate="G$1" x="38.1" y="215.138" smashed="yes">
<attribute name="NAME" x="36.83" y="221.742" size="1.4224" layer="95" ratio="9"/>
</instance>
<instance part="V185" gate="GND" x="38.1" y="210.058" smashed="yes">
<attribute name="VALUE" x="35.56" y="206.756" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="J6" gate="G$1" x="27.94" y="187.96" smashed="yes">
<attribute name="NAME" x="22.606" y="175.26" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="22.606" y="172.72" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="V187" gate="GND" x="43.18" y="175.26" smashed="yes">
<attribute name="VALUE" x="40.64" y="171.958" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="SW1" gate="G$1" x="127" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="111.76" y="124.46" size="1.4224" layer="95" ratio="10"/>
<attribute name="VALUE" x="111.76" y="121.92" size="1.4224" layer="95" ratio="10"/>
</instance>
<instance part="C99" gate="A" x="149.86" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="147.1072" y="181.1836" size="1.27" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="153.59" y="180.77" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C100" gate="A" x="149.86" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="147.1072" y="153.2436" size="1.27" layer="95" ratio="10" rot="SR90"/>
<attribute name="VALUE" x="153.59" y="152.83" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="V196" gate="GND" x="149.86" y="147.32" smashed="yes">
<attribute name="VALUE" x="147.32" y="144.018" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V195" gate="GND" x="149.86" y="175.26" smashed="yes">
<attribute name="VALUE" x="149.86" y="171.958" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C101" gate="G$1" x="162.56" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="155.956" y="180.594" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="158.242" y="180.594" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C102" gate="G$1" x="162.56" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="155.956" y="152.654" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="158.242" y="152.654" size="1.4224" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="V199" gate="GND" x="162.56" y="147.32" smashed="yes">
<attribute name="VALUE" x="160.02" y="144.018" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="V198" gate="GND" x="162.56" y="175.26" smashed="yes">
<attribute name="VALUE" x="162.56" y="171.958" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C117" gate="G$1" x="365.76" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="361.442" y="109.728" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="370.586" y="109.22" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="V212" gate="GND" x="365.76" y="99.06" smashed="yes">
<attribute name="VALUE" x="368.3" y="98.298" size="1.4224" layer="96" ratio="9"/>
</instance>
<instance part="C89" gate="G$1" x="142.24" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="137.922" y="122.428" size="1.4224" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="147.32" y="121.92" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="C98" gate="G$1" x="261.62" y="187.96" smashed="yes">
<attribute name="NAME" x="257.81" y="191.77" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="257.556" y="184.15" size="1.4224" layer="96" align="center-left"/>
</instance>
<instance part="C107" gate="G$1" x="261.62" y="134.62" smashed="yes">
<attribute name="NAME" x="257.81" y="138.43" size="1.4224" layer="95" align="center-left"/>
<attribute name="VALUE" x="257.556" y="130.81" size="1.4224" layer="96" align="center-left"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$7" class="0">
<segment>
<pinref part="R65" gate="A" pin="11"/>
<wire x1="175.26" y1="106.68" x2="200.66" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="RT"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="V197" gate="GND" pin="GND"/>
<wire x1="182.1688" y1="114.3" x2="157.48" y2="114.3" width="0.1524" layer="91"/>
<wire x1="157.48" y1="114.3" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R65" gate="A" pin="22"/>
<wire x1="165.1" y1="106.68" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<wire x1="157.48" y1="106.68" x2="157.48" y2="83.82" width="0.1524" layer="91"/>
<junction x="157.48" y="106.68"/>
<wire x1="169.418" y1="121.92" x2="157.48" y2="121.92" width="0.1524" layer="91"/>
<wire x1="157.48" y1="121.92" x2="157.48" y2="114.3" width="0.1524" layer="91"/>
<junction x="157.48" y="114.3"/>
<wire x1="200.66" y1="127" x2="157.48" y2="127" width="0.1524" layer="91"/>
<wire x1="157.48" y1="127" x2="157.48" y2="121.92" width="0.1524" layer="91"/>
<junction x="157.48" y="121.92"/>
<pinref part="U11" gate="A" pin="SYNC/MODE"/>
<pinref part="C105" gate="G$1" pin="2"/>
<pinref part="C106" gate="A" pin="22"/>
</segment>
<segment>
<pinref part="V205" gate="GND" pin="GND"/>
<wire x1="297.18" y1="96.52" x2="297.18" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U11" gate="B" pin="GND"/>
<wire x1="243.84" y1="91.44" x2="297.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="297.18" y1="91.44" x2="297.18" y2="96.52" width="0.1524" layer="91"/>
<junction x="297.18" y="96.52"/>
<pinref part="R72" gate="A" pin="22"/>
<wire x1="297.18" y1="96.52" x2="297.18" y2="91.44" width="0.1524" layer="91" style="longdash"/>
<junction x="297.18" y="91.44"/>
</segment>
<segment>
<pinref part="V200" gate="GND" pin="GND"/>
<pinref part="C103" gate="A" pin="11"/>
<wire x1="172.72" y1="177.8" x2="172.72" y2="182.1942" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V201" gate="GND" pin="GND"/>
<pinref part="C104" gate="A" pin="11"/>
<wire x1="172.72" y1="149.86" x2="172.72" y2="154.2542" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R70" gate="A" pin="22"/>
<pinref part="V204" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C97" gate="G$1" pin="1"/>
<wire x1="114.3" y1="172.72" x2="114.3" y2="165.1" width="0.1524" layer="91"/>
<pinref part="V192" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="81.28" y1="182.118" x2="81.28" y2="177.8" width="0.1524" layer="91"/>
<pinref part="V191" gate="GND" pin="GND"/>
<pinref part="C96" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="63.5" y1="182.88" x2="63.5" y2="180.34" width="0.1524" layer="91"/>
<pinref part="V190" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C111" gate="G$1" pin="1"/>
<pinref part="V206" gate="GND" pin="GND"/>
<wire x1="327.66" y1="166.878" x2="327.66" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C112" gate="G$1" pin="1"/>
<pinref part="V207" gate="GND" pin="GND"/>
<wire x1="340.36" y1="166.878" x2="340.36" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C114" gate="G$1" pin="1"/>
<pinref part="V209" gate="GND" pin="GND"/>
<wire x1="353.06" y1="166.878" x2="353.06" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C113" gate="G$1" pin="1"/>
<wire x1="340.36" y1="110.998" x2="340.36" y2="101.6" width="0.1524" layer="91"/>
<pinref part="V208" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C115" gate="G$1" pin="1"/>
<pinref part="V210" gate="GND" pin="GND"/>
<wire x1="353.06" y1="110.998" x2="353.06" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C116" gate="G$1" pin="2"/>
<pinref part="V211" gate="GND" pin="GND"/>
<wire x1="365.76" y1="165.1" x2="365.76" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C118" gate="G$1" pin="2"/>
<pinref part="V213" gate="GND" pin="GND"/>
<wire x1="375.92" y1="165.1" x2="375.92" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V194" gate="GND" pin="GND"/>
<wire x1="142.24" y1="119.38" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C89" gate="G$1" pin="1"/>
<wire x1="142.24" y1="123.698" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="127" y1="111.76" x2="127" y2="106.68" width="0.1524" layer="91"/>
<pinref part="V193" gate="GND" pin="GND"/>
<pinref part="SW1" gate="G$1" pin="2"/>
<wire x1="129.54" y1="111.76" x2="127" y2="111.76" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="1"/>
<wire x1="127" y1="111.76" x2="124.46" y2="111.76" width="0.1524" layer="91"/>
<junction x="127" y="111.76"/>
</segment>
<segment>
<pinref part="V186" gate="GND" pin="GND"/>
<pinref part="P14" gate="G$1" pin="1"/>
<wire x1="43.18" y1="212.598" x2="43.18" y2="215.138" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V188" gate="GND" pin="GND"/>
<pinref part="P15" gate="G$1" pin="1"/>
<wire x1="48.26" y1="212.598" x2="48.26" y2="215.138" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V185" gate="GND" pin="GND"/>
<pinref part="P13" gate="G$1" pin="1"/>
<wire x1="38.1" y1="212.598" x2="38.1" y2="215.138" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R67" gate="A" pin="22"/>
<pinref part="V202" gate="GND" pin="GND"/>
<wire x1="182.88" y1="147.32" x2="182.88" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="3"/>
<wire x1="36.576" y1="182.88" x2="43.18" y2="182.88" width="0.1524" layer="91"/>
<pinref part="V187" gate="GND" pin="GND"/>
<wire x1="43.18" y1="182.88" x2="43.18" y2="177.8" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="36.576" y1="187.96" x2="43.18" y2="187.96" width="0.1524" layer="91"/>
<wire x1="43.18" y1="187.96" x2="43.18" y2="182.88" width="0.1524" layer="91"/>
<junction x="43.18" y="182.88"/>
</segment>
<segment>
<pinref part="C100" gate="A" pin="11"/>
<wire x1="149.86" y1="154.2542" x2="149.86" y2="149.86" width="0.1524" layer="91"/>
<pinref part="V196" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C99" gate="A" pin="11"/>
<wire x1="149.86" y1="182.1942" x2="149.86" y2="177.8" width="0.1524" layer="91"/>
<pinref part="V195" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C102" gate="G$1" pin="1"/>
<wire x1="162.56" y1="154.178" x2="162.56" y2="149.86" width="0.1524" layer="91"/>
<pinref part="V199" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C117" gate="G$1" pin="1"/>
<pinref part="V212" gate="GND" pin="GND"/>
<wire x1="365.76" y1="110.998" x2="365.76" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C101" gate="G$1" pin="1"/>
<pinref part="V198" gate="GND" pin="GND"/>
<wire x1="162.56" y1="182.118" x2="162.56" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<wire x1="200.66" y1="119.38" x2="195.58" y2="119.38" width="0.1524" layer="91"/>
<wire x1="195.58" y1="119.38" x2="195.58" y2="121.92" width="0.1524" layer="91"/>
<wire x1="195.58" y1="121.92" x2="200.66" y2="121.92" width="0.1524" layer="91"/>
<wire x1="195.58" y1="121.92" x2="176.022" y2="121.92" width="0.1524" layer="91"/>
<junction x="195.58" y="121.92"/>
<pinref part="U11" gate="A" pin="TR/SS1"/>
<pinref part="U11" gate="A" pin="TR/SS2"/>
<pinref part="C105" gate="G$1" pin="1"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<wire x1="200.66" y1="175.26" x2="195.58" y2="175.26" width="0.1524" layer="91"/>
<wire x1="195.58" y1="175.26" x2="195.58" y2="182.88" width="0.1524" layer="91"/>
<wire x1="195.58" y1="182.88" x2="195.58" y2="187.96" width="0.1524" layer="91"/>
<wire x1="195.58" y1="187.96" x2="200.66" y2="187.96" width="0.1524" layer="91"/>
<wire x1="195.58" y1="182.88" x2="185.42" y2="182.88" width="0.1524" layer="91"/>
<junction x="195.58" y="182.88"/>
<wire x1="185.42" y1="182.88" x2="185.42" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R67" gate="A" pin="11"/>
<pinref part="R66" gate="A" pin="22"/>
<wire x1="182.88" y1="177.8" x2="182.88" y2="172.72" width="0.1524" layer="91"/>
<wire x1="182.88" y1="172.72" x2="182.88" y2="160.02" width="0.1524" layer="91"/>
<wire x1="185.42" y1="172.72" x2="182.88" y2="172.72" width="0.1524" layer="91"/>
<junction x="182.88" y="172.72"/>
<pinref part="U11" gate="A" pin="EN/UV1"/>
<pinref part="U11" gate="A" pin="EN/UV2"/>
</segment>
</net>
<net name="FB1" class="0">
<segment>
<wire x1="251.46" y1="175.26" x2="261.62" y2="175.26" width="0.1524" layer="91"/>
<wire x1="261.62" y1="175.26" x2="261.62" y2="172.72" width="0.1524" layer="91"/>
<wire x1="261.62" y1="172.72" x2="251.46" y2="172.72" width="0.1524" layer="91"/>
<wire x1="261.62" y1="172.72" x2="279.4" y2="172.72" width="0.1524" layer="91"/>
<junction x="261.62" y="172.72"/>
<wire x1="279.4" y1="172.72" x2="279.4" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R69" gate="A" pin="22"/>
<pinref part="R70" gate="A" pin="11"/>
<wire x1="294.64" y1="170.18" x2="294.64" y2="167.64" width="0.1524" layer="91"/>
<wire x1="294.64" y1="167.64" x2="294.64" y2="162.56" width="0.1524" layer="91"/>
<wire x1="279.4" y1="167.64" x2="294.64" y2="167.64" width="0.1524" layer="91"/>
<junction x="294.64" y="167.64"/>
<wire x1="307.34" y1="167.64" x2="294.64" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C109" gate="A" pin="11"/>
<wire x1="307.34" y1="172.0342" x2="307.34" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="FB1@1"/>
<pinref part="U11" gate="A" pin="FB1@0"/>
</segment>
</net>
<net name="SW1" class="0">
<segment>
<wire x1="251.46" y1="182.88" x2="271.78" y2="182.88" width="0.1524" layer="91"/>
<wire x1="271.78" y1="182.88" x2="276.86" y2="182.88" width="0.1524" layer="91"/>
<wire x1="276.86" y1="182.88" x2="276.86" y2="180.34" width="0.1524" layer="91"/>
<wire x1="276.86" y1="180.34" x2="251.46" y2="180.34" width="0.1524" layer="91"/>
<wire x1="271.78" y1="187.96" x2="271.78" y2="182.88" width="0.1524" layer="91"/>
<junction x="271.78" y="182.88"/>
<wire x1="276.86" y1="182.88" x2="281.94" y2="182.88" width="0.1524" layer="91"/>
<junction x="276.86" y="182.88"/>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="U11" gate="A" pin="SW1@1"/>
<pinref part="U11" gate="A" pin="SWI@0"/>
<pinref part="C98" gate="G$1" pin="2"/>
<wire x1="266.7" y1="187.96" x2="271.78" y2="187.96" width="0.1524" layer="91"/>
<wire x1="264.922" y1="187.96" x2="266.7" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SW2" class="0">
<segment>
<wire x1="251.46" y1="129.54" x2="256.54" y2="129.54" width="0.1524" layer="91"/>
<wire x1="256.54" y1="129.54" x2="256.54" y2="127" width="0.1524" layer="91"/>
<wire x1="256.54" y1="127" x2="251.46" y2="127" width="0.1524" layer="91"/>
<wire x1="256.54" y1="127" x2="256.54" y2="124.46" width="0.1524" layer="91"/>
<junction x="256.54" y="127"/>
<wire x1="256.54" y1="124.46" x2="251.46" y2="124.46" width="0.1524" layer="91"/>
<wire x1="256.54" y1="127" x2="269.24" y2="127" width="0.1524" layer="91"/>
<pinref part="L1" gate="A" pin="2"/>
<wire x1="269.24" y1="127" x2="271.78" y2="127" width="0.1524" layer="91"/>
<wire x1="269.24" y1="134.62" x2="269.24" y2="127" width="0.1524" layer="91"/>
<junction x="269.24" y="127"/>
<pinref part="U11" gate="A" pin="SW2@1"/>
<pinref part="U11" gate="A" pin="SW2@2"/>
<pinref part="U11" gate="A" pin="SW2@0"/>
<pinref part="C107" gate="G$1" pin="2"/>
<wire x1="266.7" y1="134.62" x2="269.24" y2="134.62" width="0.1524" layer="91"/>
<wire x1="264.922" y1="134.62" x2="266.7" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P10" gate="G$1" pin="1"/>
<pinref part="V183" gate="G$1" pin="+5V"/>
<wire x1="20.32" y1="226.06" x2="20.32" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R71" gate="A" pin="11"/>
<pinref part="C110" gate="A" pin="22"/>
<wire x1="309.88" y1="122.6312" x2="309.88" y2="127" width="0.1524" layer="91"/>
<wire x1="297.18" y1="127" x2="309.88" y2="127" width="0.1524" layer="91"/>
<wire x1="297.18" y1="127" x2="297.18" y2="124.46" width="0.1524" layer="91"/>
<junction x="297.18" y="127"/>
<wire x1="289.56" y1="127" x2="297.18" y2="127" width="0.1524" layer="91"/>
<wire x1="251.46" y1="165.1" x2="289.56" y2="165.1" width="0.1524" layer="91"/>
<wire x1="289.56" y1="165.1" x2="289.56" y2="127" width="0.1524" layer="91"/>
<junction x="289.56" y="127"/>
<pinref part="L1" gate="A" pin="1"/>
<wire x1="287.02" y1="127" x2="289.56" y2="127" width="0.1524" layer="91"/>
<wire x1="353.06" y1="127" x2="340.36" y2="127" width="0.1524" layer="91"/>
<junction x="309.88" y="127"/>
<pinref part="C115" gate="G$1" pin="2"/>
<wire x1="340.36" y1="127" x2="309.88" y2="127" width="0.1524" layer="91"/>
<wire x1="353.06" y1="127" x2="353.06" y2="117.602" width="0.1524" layer="91"/>
<junction x="353.06" y="127"/>
<pinref part="U11" gate="A" pin="BIAS"/>
<pinref part="C113" gate="G$1" pin="2"/>
<wire x1="340.36" y1="117.602" x2="340.36" y2="127" width="0.1524" layer="91"/>
<junction x="340.36" y="127"/>
<pinref part="V214" gate="G$1" pin="+5V"/>
<wire x1="353.06" y1="127" x2="365.76" y2="127" width="0.1524" layer="91"/>
<pinref part="C117" gate="G$1" pin="2"/>
<wire x1="365.76" y1="127" x2="411.48" y2="127" width="0.1524" layer="91"/>
<wire x1="365.76" y1="127" x2="365.76" y2="117.602" width="0.1524" layer="91"/>
<junction x="365.76" y="127"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="63.5" y1="190.5" x2="63.5" y2="193.04" width="0.1524" layer="91"/>
<wire x1="87.884" y1="193.04" x2="81.28" y2="193.04" width="0.1524" layer="91"/>
<junction x="81.28" y="193.04"/>
<pinref part="P17" gate="G$1" pin="1"/>
<wire x1="81.28" y1="195.58" x2="81.28" y2="193.04" width="0.1524" layer="91"/>
<wire x1="81.28" y1="188.722" x2="81.28" y2="193.04" width="0.1524" layer="91"/>
<wire x1="63.5" y1="193.04" x2="81.28" y2="193.04" width="0.1524" layer="91"/>
<pinref part="F1" gate="A" pin="2"/>
<wire x1="58.42" y1="193.04" x2="63.5" y2="193.04" width="0.1524" layer="91"/>
<junction x="63.5" y="193.04"/>
<pinref part="FB10" gate="G$1" pin="2"/>
<pinref part="C96" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<wire x1="190.5" y1="157.48" x2="190.5" y2="152.4" width="0.1524" layer="91"/>
<pinref part="V203" gate="G$1" pin="VDD"/>
<pinref part="R68" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<wire x1="124.46" y1="193.04" x2="114.3" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C97" gate="G$1" pin="2"/>
<wire x1="114.3" y1="185.42" x2="114.3" y2="193.04" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="1"/>
<pinref part="D8" gate="G$1" pin="C"/>
<wire x1="106.68" y1="193.04" x2="114.3" y2="193.04" width="0.1524" layer="91"/>
<junction x="114.3" y="193.04"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<wire x1="200.66" y1="180.34" x2="187.96" y2="180.34" width="0.1524" layer="91"/>
<wire x1="187.96" y1="180.34" x2="187.96" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C104" gate="A" pin="22"/>
<wire x1="172.72" y1="160.7312" x2="172.72" y2="167.64" width="0.1524" layer="91"/>
<wire x1="172.72" y1="167.64" x2="187.96" y2="167.64" width="0.1524" layer="91"/>
<junction x="172.72" y="167.64"/>
<wire x1="142.24" y1="167.64" x2="149.86" y2="167.64" width="0.1524" layer="91"/>
<wire x1="149.86" y1="167.64" x2="162.56" y2="167.64" width="0.1524" layer="91"/>
<wire x1="162.56" y1="167.64" x2="172.72" y2="167.64" width="0.1524" layer="91"/>
<wire x1="182.88" y1="193.04" x2="200.66" y2="193.04" width="0.1524" layer="91"/>
<pinref part="R66" gate="A" pin="11"/>
<wire x1="182.88" y1="187.96" x2="182.88" y2="193.04" width="0.1524" layer="91"/>
<wire x1="182.88" y1="193.04" x2="172.72" y2="193.04" width="0.1524" layer="91"/>
<junction x="182.88" y="193.04"/>
<wire x1="172.72" y1="193.04" x2="162.56" y2="193.04" width="0.1524" layer="91"/>
<wire x1="162.56" y1="193.04" x2="149.86" y2="193.04" width="0.1524" layer="91"/>
<wire x1="149.86" y1="193.04" x2="142.24" y2="193.04" width="0.1524" layer="91"/>
<wire x1="142.24" y1="193.04" x2="134.366" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C103" gate="A" pin="22"/>
<wire x1="172.72" y1="188.6712" x2="172.72" y2="193.04" width="0.1524" layer="91"/>
<junction x="172.72" y="193.04"/>
<wire x1="142.24" y1="167.64" x2="142.24" y2="193.04" width="0.1524" layer="91"/>
<junction x="142.24" y="193.04"/>
<pinref part="U11" gate="A" pin="VIN1"/>
<pinref part="U11" gate="A" pin="VIN2"/>
<pinref part="R64" gate="G$1" pin="2"/>
<pinref part="C99" gate="A" pin="22"/>
<wire x1="149.86" y1="188.6712" x2="149.86" y2="193.04" width="0.1524" layer="91"/>
<junction x="149.86" y="193.04"/>
<pinref part="C100" gate="A" pin="22"/>
<wire x1="149.86" y1="160.7312" x2="149.86" y2="167.64" width="0.1524" layer="91"/>
<junction x="149.86" y="167.64"/>
<pinref part="C101" gate="G$1" pin="2"/>
<wire x1="162.56" y1="188.722" x2="162.56" y2="193.04" width="0.1524" layer="91"/>
<junction x="162.56" y="193.04"/>
<pinref part="C102" gate="G$1" pin="2"/>
<wire x1="162.56" y1="160.782" x2="162.56" y2="167.64" width="0.1524" layer="91"/>
<junction x="162.56" y="167.64"/>
</segment>
</net>
<net name="AVDD" class="0">
<segment>
<pinref part="P12" gate="G$1" pin="1"/>
<pinref part="V184" gate="G$1" pin="AVDD"/>
<wire x1="30.48" y1="226.06" x2="30.48" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FB2" class="0">
<segment>
<wire x1="251.46" y1="114.3" x2="256.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="256.54" y1="114.3" x2="256.54" y2="111.76" width="0.1524" layer="91"/>
<junction x="256.54" y="111.76"/>
<wire x1="256.54" y1="111.76" x2="251.46" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R71" gate="A" pin="22"/>
<wire x1="297.18" y1="114.3" x2="297.18" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C110" gate="A" pin="11"/>
<wire x1="309.88" y1="116.1542" x2="309.88" y2="111.76" width="0.1524" layer="91"/>
<wire x1="309.88" y1="111.76" x2="297.18" y2="111.76" width="0.1524" layer="91"/>
<junction x="297.18" y="111.76"/>
<pinref part="R72" gate="A" pin="11"/>
<wire x1="297.18" y1="111.76" x2="297.18" y2="106.68" width="0.1524" layer="91"/>
<wire x1="256.54" y1="111.76" x2="297.18" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="FB2@0"/>
<pinref part="U11" gate="A" pin="FB2@1"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="F1" gate="A" pin="1"/>
<wire x1="48.26" y1="193.04" x2="36.576" y2="193.04" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="1.2V" class="0">
<segment>
<pinref part="C118" gate="G$1" pin="1"/>
<pinref part="C116" gate="G$1" pin="1"/>
<pinref part="R69" gate="A" pin="11"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="292.1" y1="182.88" x2="294.64" y2="182.88" width="0.1524" layer="91"/>
<junction x="294.64" y="182.88"/>
<wire x1="294.64" y1="182.88" x2="294.64" y2="180.34" width="0.1524" layer="91"/>
<wire x1="294.64" y1="182.88" x2="307.34" y2="182.88" width="0.1524" layer="91"/>
<wire x1="307.34" y1="182.88" x2="327.66" y2="182.88" width="0.1524" layer="91"/>
<wire x1="327.66" y1="182.88" x2="340.36" y2="182.88" width="0.1524" layer="91"/>
<wire x1="340.36" y1="182.88" x2="353.06" y2="182.88" width="0.1524" layer="91"/>
<pinref part="C112" gate="G$1" pin="2"/>
<wire x1="340.36" y1="173.482" x2="340.36" y2="182.88" width="0.1524" layer="91"/>
<junction x="340.36" y="182.88"/>
<pinref part="C114" gate="G$1" pin="2"/>
<wire x1="353.06" y1="173.482" x2="353.06" y2="182.88" width="0.1524" layer="91"/>
<junction x="353.06" y="182.88"/>
<pinref part="C111" gate="G$1" pin="2"/>
<wire x1="327.66" y1="173.482" x2="327.66" y2="182.88" width="0.1524" layer="91"/>
<junction x="327.66" y="182.88"/>
<pinref part="C109" gate="A" pin="22"/>
<wire x1="307.34" y1="178.5112" x2="307.34" y2="182.88" width="0.1524" layer="91"/>
<junction x="307.34" y="182.88"/>
<wire x1="353.06" y1="182.88" x2="365.76" y2="182.88" width="0.1524" layer="91"/>
<wire x1="365.76" y1="182.88" x2="375.92" y2="182.88" width="0.1524" layer="91"/>
<wire x1="375.92" y1="182.88" x2="408.94" y2="182.88" width="0.1524" layer="91"/>
<wire x1="365.76" y1="175.26" x2="365.76" y2="182.88" width="0.1524" layer="91"/>
<junction x="365.76" y="182.88"/>
<wire x1="375.92" y1="175.26" x2="375.92" y2="182.88" width="0.1524" layer="91"/>
<junction x="375.92" y="182.88"/>
<label x="401.32" y="182.88" size="1.778" layer="95"/>
<pinref part="FD10" gate="G$1" pin="1.2V"/>
</segment>
<segment>
<pinref part="P11" gate="G$1" pin="1"/>
<pinref part="FD8" gate="G$1" pin="1.2V"/>
<wire x1="25.4" y1="226.06" x2="25.4" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="C106" gate="A" pin="11"/>
<pinref part="U11" gate="A" pin="INTVCC"/>
<wire x1="188.6458" y1="114.3" x2="200.66" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CPU_RSTN" class="0">
<segment>
<label x="101.6" y="135.128" size="1.4224" layer="95" ratio="10"/>
<wire x1="142.24" y1="132.08" x2="142.24" y2="134.62" width="0.1524" layer="91"/>
<wire x1="127" y1="134.62" x2="142.24" y2="134.62" width="0.1524" layer="91"/>
<wire x1="127" y1="132.08" x2="127" y2="134.62" width="0.1524" layer="91"/>
<junction x="127" y="134.62"/>
<wire x1="127" y1="134.62" x2="99.06" y2="134.62" width="0.1524" layer="91"/>
<wire x1="200.66" y1="170.18" x2="198.12" y2="170.18" width="0.1524" layer="91"/>
<wire x1="198.12" y1="170.18" x2="198.12" y2="167.64" width="0.1524" layer="91"/>
<wire x1="198.12" y1="167.64" x2="200.66" y2="167.64" width="0.1524" layer="91"/>
<wire x1="198.12" y1="167.64" x2="195.58" y2="167.64" width="0.1524" layer="91"/>
<wire x1="195.58" y1="167.64" x2="195.58" y2="134.62" width="0.1524" layer="91"/>
<junction x="198.12" y="167.64"/>
<wire x1="195.58" y1="134.62" x2="190.5" y2="134.62" width="0.1524" layer="91"/>
<wire x1="190.5" y1="142.24" x2="190.5" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U11" gate="A" pin="PG1"/>
<pinref part="U11" gate="A" pin="PG2"/>
<wire x1="190.5" y1="134.62" x2="154.94" y2="134.62" width="0.1524" layer="91"/>
<junction x="190.5" y="134.62"/>
<pinref part="P18" gate="G$1" pin="1"/>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="142.24" y1="134.62" x2="154.94" y2="134.62" width="0.1524" layer="91"/>
<junction x="142.24" y="134.62"/>
<junction x="154.94" y="134.62"/>
<pinref part="SW1" gate="G$1" pin="3"/>
<wire x1="124.46" y1="132.08" x2="127" y2="132.08" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="4"/>
<wire x1="127" y1="132.08" x2="129.54" y2="132.08" width="0.1524" layer="91"/>
<junction x="127" y="132.08"/>
<pinref part="C89" gate="G$1" pin="2"/>
<wire x1="142.24" y1="130.302" x2="142.24" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="FB10" gate="G$1" pin="1"/>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="95.25" y1="193.04" x2="101.6" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="U11" gate="A" pin="BOOST1"/>
<wire x1="251.46" y1="187.96" x2="256.54" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C98" gate="G$1" pin="1"/>
<wire x1="258.318" y1="187.96" x2="256.54" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="U11" gate="A" pin="BOOST2"/>
<wire x1="251.46" y1="134.62" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C107" gate="G$1" pin="1"/>
<wire x1="258.318" y1="134.62" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,360.68,137.16,U13,VDD,+5V,,,"/>
<approved hash="104,1,391.16,137.16,U13,VSS,GND,,,"/>
<approved hash="104,1,391.16,134.62,U13,DGND,GND,,,"/>
<approved hash="104,1,119.38,195.58,U12,VDD,+5V,,,"/>
<approved hash="104,1,149.86,195.58,U12,VSS,GND,,,"/>
<approved hash="104,1,149.86,193.04,U12,DGND,GND,,,"/>
<approved hash="208,1,198.12,154.94,+5V,sup,,,,"/>
<approved hash="208,1,60.96,99.06,+5V,sup,,,,"/>
<approved hash="208,1,76.2,220.98,+5V,sup,,,,"/>
<approved hash="208,1,83.82,220.98,+5V,sup,,,,"/>
<approved hash="208,1,109.22,223.52,+5V,sup,,,,"/>
<approved hash="208,1,406.4,210.82,+5V,sup,,,,"/>
<approved hash="208,1,416.56,210.82,+5V,sup,,,,"/>
<approved hash="208,1,302.26,213.36,+5V,sup,,,,"/>
<approved hash="208,1,302.26,190.5,+5V,sup,,,,"/>
<approved hash="208,1,414.02,165.1,+5V,sup,,,,"/>
<approved hash="208,1,421.64,165.1,+5V,sup,,,,"/>
<approved hash="208,1,398.78,157.48,+5V,sup,,,,"/>
<approved hash="208,1,332.74,149.86,+5V,sup,,,,"/>
<approved hash="208,3,50.8,88.9,+5V,sup,,,,"/>
<approved hash="208,3,165.1,114.3,+5V,sup,,,,"/>
<approved hash="208,3,154.94,114.3,+5V,sup,,,,"/>
<approved hash="208,4,20.32,228.6,+5V,sup,,,,"/>
<approved hash="208,4,251.46,165.1,+5V,out,,,,"/>
<approved hash="208,4,411.48,127,+5V,sup,,,,"/>
<approved hash="206,4,200.66,170.18,CPU_RSTN,,,,,"/>
<approved hash="206,4,200.66,167.64,CPU_RSTN,,,,,"/>
<approved hash="208,1,256.54,132.08,GND,sup,,,,"/>
<approved hash="208,1,276.86,142.24,GND,sup,,,,"/>
<approved hash="208,1,236.22,132.08,GND,sup,,,,"/>
<approved hash="208,1,215.9,129.54,GND,sup,,,,"/>
<approved hash="208,1,203.2,132.08,GND,sup,,,,"/>
<approved hash="208,1,38.1,177.8,GND,sup,,,,"/>
<approved hash="208,1,78.74,78.74,GND,sup,,,,"/>
<approved hash="208,1,114.3,81.28,GND,sup,,,,"/>
<approved hash="208,1,109.22,22.86,GND,sup,,,,"/>
<approved hash="208,1,218.44,185.42,GND,sup,,,,"/>
<approved hash="208,1,259.08,185.42,GND,sup,,,,"/>
<approved hash="208,1,226.06,185.42,GND,sup,,,,"/>
<approved hash="208,1,251.46,185.42,GND,sup,,,,"/>
<approved hash="208,1,190.5,238.76,GND,sup,,,,"/>
<approved hash="208,1,30.48,147.32,GND,sup,,,,"/>
<approved hash="208,1,302.26,203.2,GND,sup,,,,"/>
<approved hash="208,1,302.26,185.42,GND,sup,,,,"/>
<approved hash="208,1,406.4,60.96,GND,sup,,,,"/>
<approved hash="208,1,391.16,60.96,GND,sup,,,,"/>
<approved hash="208,1,398.78,66.04,GND,sup,,,,"/>
<approved hash="208,1,414.02,149.86,GND,sup,,,,"/>
<approved hash="208,1,421.64,149.86,GND,sup,,,,"/>
<approved hash="208,1,162.56,193.04,GND,sup,,,,"/>
<approved hash="208,1,76.2,203.2,GND,sup,,,,"/>
<approved hash="208,1,83.82,203.2,GND,sup,,,,"/>
<approved hash="208,1,403.86,132.08,GND,sup,,,,"/>
<approved hash="208,1,170.18,200.66,GND,sup,,,,"/>
<approved hash="208,1,287.02,129.54,GND,sup,,,,"/>
<approved hash="208,1,139.7,35.56,GND,sup,,,,"/>
<approved hash="208,1,259.08,254,GND,sup,,,,"/>
<approved hash="208,1,271.78,254,GND,sup,,,,"/>
<approved hash="208,1,406.4,190.5,GND,sup,,,,"/>
<approved hash="208,1,416.56,190.5,GND,sup,,,,"/>
<approved hash="208,1,266.7,142.24,GND,sup,,,,"/>
<approved hash="208,1,314.96,241.3,GND,sup,,,,"/>
<approved hash="208,1,325.12,241.3,GND,sup,,,,"/>
<approved hash="208,1,335.28,241.3,GND,sup,,,,"/>
<approved hash="208,1,210.82,40.64,GND,sup,,,,"/>
<approved hash="208,1,299.72,40.64,GND,sup,,,,"/>
<approved hash="208,1,386.08,185.42,GND,sup,,,,"/>
<approved hash="208,1,386.08,200.66,GND,sup,,,,"/>
<approved hash="208,1,337.82,127,GND,sup,,,,"/>
<approved hash="208,1,408.94,129.54,GND,sup,,,,"/>
<approved hash="208,2,119.38,10.16,GND,sup,,,,"/>
<approved hash="208,2,365.76,213.36,GND,sup,,,,"/>
<approved hash="208,2,358.14,213.36,GND,sup,,,,"/>
<approved hash="208,2,345.44,180.34,GND,sup,,,,"/>
<approved hash="208,2,220.98,180.34,GND,sup,,,,"/>
<approved hash="208,2,142.24,248.92,GND,sup,,,,"/>
<approved hash="208,2,7.62,180.34,GND,sup,,,,"/>
<approved hash="208,2,15.24,180.34,GND,sup,,,,"/>
<approved hash="208,2,279.4,101.6,GND,sup,,,,"/>
<approved hash="208,2,304.8,116.84,GND,sup,,,,"/>
<approved hash="208,2,294.64,116.84,GND,sup,,,,"/>
<approved hash="208,2,398.78,231.14,GND,sup,,,,"/>
<approved hash="208,2,172.72,248.92,GND,sup,,,,"/>
<approved hash="208,2,180.34,248.92,GND,sup,,,,"/>
<approved hash="208,2,187.96,248.92,GND,sup,,,,"/>
<approved hash="208,2,195.58,248.92,GND,sup,,,,"/>
<approved hash="208,2,203.2,248.92,GND,sup,,,,"/>
<approved hash="208,2,210.82,248.92,GND,sup,,,,"/>
<approved hash="208,2,218.44,248.92,GND,sup,,,,"/>
<approved hash="208,2,226.06,248.92,GND,sup,,,,"/>
<approved hash="208,2,233.68,248.92,GND,sup,,,,"/>
<approved hash="208,2,165.1,220.98,GND,sup,,,,"/>
<approved hash="208,2,172.72,220.98,GND,sup,,,,"/>
<approved hash="208,2,180.34,220.98,GND,sup,,,,"/>
<approved hash="208,2,165.1,195.58,GND,sup,,,,"/>
<approved hash="208,2,172.72,195.58,GND,sup,,,,"/>
<approved hash="208,2,180.34,195.58,GND,sup,,,,"/>
<approved hash="208,2,165.1,170.18,GND,sup,,,,"/>
<approved hash="208,2,172.72,170.18,GND,sup,,,,"/>
<approved hash="208,2,180.34,170.18,GND,sup,,,,"/>
<approved hash="208,2,165.1,142.24,GND,sup,,,,"/>
<approved hash="208,2,172.72,142.24,GND,sup,,,,"/>
<approved hash="208,2,180.34,142.24,GND,sup,,,,"/>
<approved hash="208,2,330.2,248.92,GND,sup,,,,"/>
<approved hash="208,2,337.82,248.92,GND,sup,,,,"/>
<approved hash="208,2,345.44,248.92,GND,sup,,,,"/>
<approved hash="208,2,353.06,248.92,GND,sup,,,,"/>
<approved hash="208,2,360.68,248.92,GND,sup,,,,"/>
<approved hash="208,2,368.3,248.92,GND,sup,,,,"/>
<approved hash="208,2,375.92,248.92,GND,sup,,,,"/>
<approved hash="208,2,383.54,248.92,GND,sup,,,,"/>
<approved hash="208,2,391.16,248.92,GND,sup,,,,"/>
<approved hash="208,2,398.78,248.92,GND,sup,,,,"/>
<approved hash="208,2,165.1,248.92,GND,sup,,,,"/>
<approved hash="208,2,342.9,205.74,GND,sup,,,,"/>
<approved hash="208,2,7.62,248.92,GND,sup,,,,"/>
<approved hash="208,3,45.72,160.02,GND,sup,,,,"/>
<approved hash="208,3,83.82,172.72,GND,sup,,,,"/>
<approved hash="208,3,73.66,55.88,GND,sup,,,,"/>
<approved hash="208,3,99.06,38.1,GND,sup,,,,"/>
<approved hash="208,3,142.24,43.18,GND,sup,,,,"/>
<approved hash="208,3,66.04,60.96,GND,sup,,,,"/>
<approved hash="208,3,287.02,121.92,GND,sup,,,,"/>
<approved hash="208,3,238.76,170.18,GND,sup,,,,"/>
<approved hash="208,3,231.14,177.8,GND,sup,,,,"/>
<approved hash="208,3,264.16,127,GND,sup,,,,"/>
<approved hash="208,3,154.94,99.06,GND,sup,,,,"/>
<approved hash="208,3,215.9,220.98,GND,sup,,,,"/>
<approved hash="208,3,134.62,170.18,GND,sup,,,,"/>
<approved hash="208,3,119.38,167.64,GND,sup,,,,"/>
<approved hash="208,3,20.32,210.82,GND,sup,,,,"/>
<approved hash="208,3,33.02,210.82,GND,sup,,,,"/>
<approved hash="208,3,226.06,220.98,GND,sup,,,,"/>
<approved hash="208,3,111.76,167.64,GND,sup,,,,"/>
<approved hash="208,3,104.14,165.1,GND,sup,,,,"/>
<approved hash="208,3,104.14,200.66,GND,sup,,,,"/>
<approved hash="208,3,60.96,53.34,GND,sup,,,,"/>
<approved hash="208,3,20.32,63.5,GND,sup,,,,"/>
<approved hash="208,3,254,170.18,GND,sup,,,,"/>
<approved hash="208,3,327.66,172.72,GND,sup,,,,"/>
<approved hash="208,3,165.1,99.06,GND,sup,,,,"/>
<approved hash="208,3,248.92,114.3,GND,sup,,,,"/>
<approved hash="208,3,119.38,35.56,GND,sup,,,,"/>
<approved hash="208,3,195.58,157.48,GND,sup,,,,"/>
<approved hash="208,3,205.74,157.48,GND,sup,,,,"/>
<approved hash="208,3,93.98,81.28,GND,sup,,,,"/>
<approved hash="208,3,264.16,170.18,GND,sup,,,,"/>
<approved hash="208,3,167.64,156.718,GND,sup,,,,"/>
<approved hash="208,3,284.48,152.4,GND,sup,,,,"/>
<approved hash="208,4,157.48,83.82,GND,sup,,,,"/>
<approved hash="208,4,200.66,127,GND,out,,,,"/>
<approved hash="208,4,297.18,83.82,GND,sup,,,,"/>
<approved hash="208,4,172.72,177.8,GND,sup,,,,"/>
<approved hash="208,4,172.72,149.86,GND,sup,,,,"/>
<approved hash="208,4,294.64,152.4,GND,sup,,,,"/>
<approved hash="208,4,114.3,165.1,GND,sup,,,,"/>
<approved hash="208,4,81.28,177.8,GND,sup,,,,"/>
<approved hash="208,4,63.5,180.34,GND,sup,,,,"/>
<approved hash="208,4,327.66,160.02,GND,sup,,,,"/>
<approved hash="208,4,340.36,160.02,GND,sup,,,,"/>
<approved hash="208,4,353.06,160.02,GND,sup,,,,"/>
<approved hash="208,4,340.36,101.6,GND,sup,,,,"/>
<approved hash="208,4,353.06,101.6,GND,sup,,,,"/>
<approved hash="208,4,365.76,160.02,GND,sup,,,,"/>
<approved hash="208,4,375.92,160.02,GND,sup,,,,"/>
<approved hash="208,4,142.24,119.38,GND,sup,,,,"/>
<approved hash="208,4,127,106.68,GND,sup,,,,"/>
<approved hash="208,4,43.18,212.598,GND,sup,,,,"/>
<approved hash="208,4,48.26,212.598,GND,sup,,,,"/>
<approved hash="208,4,38.1,212.598,GND,sup,,,,"/>
<approved hash="208,4,182.88,147.32,GND,sup,,,,"/>
<approved hash="208,4,43.18,177.8,GND,sup,,,,"/>
<approved hash="208,4,149.86,149.86,GND,sup,,,,"/>
<approved hash="208,4,149.86,177.8,GND,sup,,,,"/>
<approved hash="208,4,162.56,149.86,GND,sup,,,,"/>
<approved hash="208,4,365.76,101.6,GND,sup,,,,"/>
<approved hash="208,4,162.56,177.8,GND,sup,,,,"/>
<approved hash="206,4,200.66,121.92,N$88,,,,,"/>
<approved hash="206,4,200.66,119.38,N$88,,,,,"/>
<approved hash="206,4,251.46,180.34,SW1,,,,,"/>
<approved hash="206,4,251.46,182.88,SW1,,,,,"/>
<approved hash="206,4,251.46,127,SW2,,,,,"/>
<approved hash="206,4,251.46,124.46,SW2,,,,,"/>
<approved hash="206,4,251.46,129.54,SW2,,,,,"/>
<approved hash="113,2,284.48,209.952,U6,,,,,"/>
<approved hash="113,1,289.457,147.32,R23,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
